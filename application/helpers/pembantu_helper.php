<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('kos'))
{
  function kos(&$variable, $replacement = '')
  {
    if(empty($variable))
    {
      return $replacement;
    }elseif (!isset($variable)) {
      return $replacement;
    }elseif( $variable === FALSE ){
      return $replacement;
    }else{
      return $variable;
    }
  }
}

if (!function_exists('vdump'))
{
  function vdump($variable, $isdie = FALSE)
  {
    echo '<pre>';
    var_dump($variable);
    echo '</pre>'; 
    if($isdie){
      die;
    }
  }
}

if ( ! function_exists('tgl'))
{
    function tgl(&$tanggal, $default_format = 'd/m/Y', $replacement = '-')
    {
        if(empty($tanggal))
        {
          return $replacement;
        }elseif (!isset($tanggal)) {
          return $replacement;
        }elseif( $tanggal === FALSE ){
          return $replacement;
        }elseif($tanggal === '0000-00-00 00:00:00'){
          return $replacement;
        }else{
          $totime = strtotime($tanggal);
          $tanggal_formatted = date($default_format, $totime); 
          return $tanggal_formatted;
        }
    }   
}

if ( ! function_exists('genMonth'))
{
    function genMonth()
    {
        $array_month = array();

        $time = strtotime("-1 year", time());
        $min_year = date("Y", $time);

        for ($num=1; $num < 13 ; $num++) { 
          $tanggal = $min_year.'-'.sprintf("%02s", $num).'-01';
          $tanggal_format = sprintf("%02s", $num).'/'.$min_year;
          $temp_array = array('id' => $tanggal , 'text' => $tanggal_format);
          array_push($array_month, $temp_array);
        }

        for ($num=1; $num < 13 ; $num++) { 
          $tanggal = date('Y').'-'.sprintf("%02s", $num).'-01';
          $tanggal_format = sprintf("%02s", $num).'/'.date('Y');
          $temp_array = array('id' => $tanggal , 'text' => $tanggal_format);
          array_push($array_month, $temp_array);
        }

        return $array_month;
    }   
}

if ( ! function_exists('tagnumber'))
{
    function tagnumber(&$number)
    {
        return 'ID'.$number;
    }   
}

if (!function_exists('angka'))
{
  function angka($angka, $decimal_point = 2, $decimal_delimiter = ",")
  {
    return number_format($angka, $decimal_point, $decimal_delimiter,".");
  }
}