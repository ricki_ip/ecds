<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_tangkapan_kapal extends CI_Model
{
  
    function __construct()
    {

    }

  public function data($year)
    {
      $query = "SELECT 
                    mvc.ccsbt_registration_number,
                    mvc.vessel_name,
                    SUM(ctfish.weight) as net_weight,
                    SUM(ctfish.weight*1.15) as whole_weight
                FROM
                    mst_vessel_ccsbt mvc
                        LEFT JOIN
                    trs_ctf ctf ON ctf.vessel_registration_number = mvc.ccsbt_registration_number
                        LEFT JOIN
                    trs_ctf_fish ctfish ON ctfish.id_ctf = ctf.id_ctf
                        AND YEAR(ctfish.month_of_harvest) = '$year'
                GROUP BY mvc.vessel_registration_number
                ORDER BY net_weight DESC";

      $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function data_above($year)
    {
      $query = "SELECT 
                    mvc.ccsbt_registration_number,
                    mvc.vessel_name,
                    SUM(ctfish.weight) as net_weight,
                    SUM(ctfish.weight*1.15) as whole_weight
                FROM
                    mst_vessel_ccsbt mvc
                        LEFT JOIN
                    trs_ctf ctf ON ctf.vessel_registration_number = mvc.ccsbt_registration_number
                        LEFT JOIN
                    trs_ctf_fish ctfish ON ctfish.id_ctf = ctf.id_ctf
                        AND YEAR(ctfish.month_of_harvest) = '$year'
                WHERE mvc.gross_registered_tonnage_tons >= 30
                GROUP BY mvc.vessel_registration_number
                ORDER BY net_weight DESC";

      $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function data_below($year)
    {
      $query = "SELECT 
                    mvc.ccsbt_registration_number,
                    mvc.vessel_name,
                    SUM(ctfish.weight) as net_weight,
                    SUM(ctfish.weight*1.15) as whole_weight
                FROM
                    mst_vessel_ccsbt mvc
                        LEFT JOIN
                    trs_ctf ctf ON ctf.vessel_registration_number = mvc.ccsbt_registration_number
                        LEFT JOIN
                    trs_ctf_fish ctfish ON ctfish.id_ctf = ctf.id_ctf
                        AND YEAR(ctfish.month_of_harvest) = '$year'
                WHERE mvc.gross_registered_tonnage_tons < 30
                GROUP BY mvc.vessel_registration_number
                ORDER BY net_weight DESC";

      $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}