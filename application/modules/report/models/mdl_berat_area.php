<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_berat_area extends CI_Model
{
  
    function __construct()
    {

    }

  public function data($year)
    {
      $query = "SELECT 
                    SUM(ctfish.weight) as net_weight,
                    SUM(ctfish.weight * 1.15) as whole_weight,
                    ctfish.ccsbt_statistical_area
                FROM
                    trs_ctf_fish ctfish 
                    LEFT JOIN
                    trs_ctf ctf ON ctfish.id_ctf = ctf.id_ctf
                        AND ctf.is_verified = 'YA'
                WHERE YEAR(ctfish.month_of_harvest) = '$year'
                GROUP BY ctfish.ccsbt_statistical_area
                                ";

      $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}