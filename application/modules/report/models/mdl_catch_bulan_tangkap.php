<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_catch_bulan_tangkap extends CI_Model
{
  
    function __construct()
    {

    }

  public function data($year)
    {
      $query = "SELECT
                    bln.id_bulan,
                    bln.nama_bulan_singkat,
                    SUM(ctfish.weight) AS net_weight,
                    SUM((ctfish.weight * ttag.conversion_factor)) AS whole_weight
                FROM
                    mst_bulan bln
                        LEFT JOIN
                    trs_ctf_fish ctfish ON (MONTH(ctfish.month_of_harvest) = bln.id_bulan)
                        AND YEAR(ctfish.month_of_harvest) = '$year'
                        LEFT JOIN
                    mst_type_tag ttag ON ttag.id_type_tag = ctfish.type_tag
                        LEFT JOIN
                    trs_ctf ctf ON ctf.id_ctf = ctfish.id_ctf
                        AND ctf.is_verified = 'YA'
                GROUP BY bln.nama_bulan_inggris
                ORDER BY bln.id_bulan";

      $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}