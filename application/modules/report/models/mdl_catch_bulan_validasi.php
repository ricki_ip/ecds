<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_catch_bulan_validasi extends CI_Model
{
  
    function __construct()
    {

    }

  public function data($year)
    {
      $query = "SELECT 
                    bln.id_bulan,
                    bln.nama_bulan_singkat,
                    SUM(ctfish.weight) AS net_weight,
                    SUM((ctfish.weight * ttag.conversion_factor)) AS whole_weight
                FROM
                    mst_bulan bln
                        LEFT JOIN
                    trs_ctf ctf ON (MONTH(ctf.tgl_verifikasi) = bln.id_bulan)
                        AND YEAR(ctf.tgl_verifikasi) = '$year'
                        AND ctf.is_verified = 'YA'
                        LEFT JOIN
                    trs_ctf_fish ctfish ON ctfish.id_ctf = ctf.id_ctf
                        LEFT JOIN
                    mst_type_tag ttag ON ttag.id_type_tag = ctfish.type_tag
                GROUP BY bln.id_bulan";

      $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}