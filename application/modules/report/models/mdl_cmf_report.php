<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_cmf_report extends CI_Model
{
  
    function __construct()
    {

    }

  public function cmf()
    {
      $query = "SELECT 
                    cmf.document_number,
                    cmf.ctf_document_numbers,
                    'W' AS Wild_or_Farmed,
                    ctf.vessel_name,
                    ctf.vessel_registration_number,
                    ctf.flag_state,
                    cmfish.product_type,
                    ttag.code_tag,
                    cmfish.month_of_harvest,
                    mgear.gear_code AS gear_code_name,
                    cmfish.ccsbt_statistical_area,
                    cmfish.net_weight,
                    cmfish.total_fish,
                    cmf.name_processing_establishment,
                    cmf.address_processing_establishment,
                    pjb.nama_pejabat AS validator_name,
                    pjb.title AS validator_title,
                    'Y' AS signature,
                    cmf.authority_date,
                    'Y' AS official_stamp,
                    kbkt.nama_kabupaten_kota,
                    prp.nama_propinsi,
                    cmfex.state_entity,
                    ngr.nama_negara,
                    cmfex.name AS exportir_name,
                    mpr.company_name,
                    cmfex.date AS exportir_date,
                    'Y' AS exportir_signature,
                    'Y' AS exportir_stamp,
                    cmf.authority_date,
                    'Y' AS validator_signature,
                    'Y' AS validator_stamp,
                    cmf.tgl_verifikasi
                FROM
                    trs_cmf cmf
                        LEFT JOIN
                    trs_ctf ctf ON ctf.id_ctf = cmf.id_ctf
                        LEFT JOIN
                    trs_cmf_fish cmfish ON cmfish.id_cmf = cmf.id_cmf
                        LEFT JOIN
                    mst_type_tag ttag ON ttag.id_type_tag = cmfish.type_tag
                        LEFT JOIN
                    mst_gear mgear ON mgear.id_gear = cmfish.gear_code
                        LEFT JOIN
                    mst_pejabat pjb ON pjb.id_pejabat = cmf.id_pejabat
                        LEFT JOIN
                    trs_cmf_final_export cmfex ON cmfex.id_cmf = cmf.id_cmf 
                        LEFT JOIN
                    mst_kabupaten_kota kbkt ON kbkt.id_kabupaten_kota = cmfex.city
                        LEFT JOIN
                    mst_propinsi prp ON prp.id_propinsi = kbkt.id_propinsi
                        LEFT JOIN
                    mst_negara ngr ON ngr.id_negara = cmfex.destination
                        LEFT JOIN
                    mst_perusahaan mpr ON mpr.id_perusahaan = cmfex.company_name
                WHERE
                    cmf.is_verified = 'YA'
                    ";

      $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function from_kapi()
    {
      $query = "";

      $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}