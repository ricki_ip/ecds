<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_ctf_report extends CI_Model
{
	
    function __construct()
    {

    }

	public function ctf()
    {
    	$query = "SELECT 
                    ctf.document_number AS ctf_document_numbers,
                    'W' AS Wild_or_Farmed,
                    cmf.document_number,
                    ctf.vessel_name,
                    ctf.vessel_registration_number,
                    ctf.flag_state,
                    ccsbt_tag_number,
                    ttag.code_tag,
                    ctfish.weight,
                    ctfish.fork_length,
                    mgear.gear_code AS gear_code_name,
                    ctfish.ccsbt_statistical_area,
                    ctfish.month_of_harvest,
                    ctf.name AS name_of_certifier,
                    'Y' AS signature,
                    ctf.tgl_verifikasi AS certified_date,
                    ctf.title title_of_certifier,
                    ttag.conversion_factor,
                    (ctfish.weight * ttag.conversion_factor) AS whole_weight
                FROM
                    trs_ctf_fish ctfish
                        LEFT JOIN
                    trs_ctf ctf ON ctf.id_ctf = ctfish.id_ctf
                        LEFT JOIN
                    trs_cmf cmf ON ctfish.id_ctf = cmf.id_ctf
                        LEFT JOIN
                    mst_type_tag ttag ON ttag.id_type_tag = ctfish.type_tag
                        LEFT JOIN
                    mst_gear mgear ON mgear.id_gear = ctfish.gear_code 
                WHERE
                    ctfish.is_free = 'TIDAK'
                    AND ctf.is_verified = 'YA'
                    AND cmf.is_verified = 'YA'
                    AND ctfish.aktif = 'YA'";

    	$run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function from_kapi()
    {
    	$query = "";

    	$run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}