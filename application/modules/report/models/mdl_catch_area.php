<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_catch_area extends CI_Model
{
  
    function __construct()
    {

    }

  public function data($year)
    {
      $query = "SELECT 
                    area.code_ccsbt_area,
                    COUNT(ctf.vessel_registration_number) AS jumlah_kapal,
                    SUM(ctfish.weight) as net_weight,
                    SUM(ctfish.weight * 1.15) as whole_weight
                FROM
                    mst_ccsbt_area area
                        LEFT JOIN
                    trs_ctf_fish ctfish ON ctfish.ccsbt_statistical_area = area.code_ccsbt_area
                    AND YEAR(ctfish.month_of_harvest) = '$year'
                        LEFT JOIN
                    trs_ctf ctf ON ctf.id_ctf = ctfish.id_ctf
                GROUP BY area.id_ccsbt_area
                ";

      $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}