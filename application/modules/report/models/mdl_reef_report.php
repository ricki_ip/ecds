<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_reef_report extends CI_Model
{
  
    function __construct()
    {

    }

  public function reef()
    {
      $query = "SELECT 
    trfish.net_weight,
    trfish.product_type,
    trfish.type_tag,
    trfish.total_fish,
    mkabkot.nama_kabupaten_kota AS name_point_export_city,
	mprop.nama_propinsi AS name_point_export_province,
	mneg.nama_negara,
    mtag.code_tag,
    mpjb.nama_pejabat,
    mpjb.title,
    trcmfish.*,
    trf . *
FROM
    trs_reef trf
        INNER JOIN
    trs_reef_fish trfish ON trfish.id_reef = trf.id_reef
        LEFT JOIN
    trs_cmf trcmf ON trcmf.id_cmf = trf.id_cmf
        LEFT JOIN
    (SELECT 
        fish.id_cmf,
            fish.product_type AS cmf_product_type,
            tag.code_tag AS cmf_code_tag,
            fish.net_weight AS cmf_net_weight,
            fish.month_of_harvest AS cmf_date_previous_landing,
            fish.total_fish AS cmf_total_fish
    FROM
        trs_cmf_fish fish
    LEFT JOIN mst_type_tag tag ON tag.id_type_tag = fish.type_tag) trcmfish ON trcmfish.id_cmf = trcmf.id_cmf
        LEFT JOIN
    mst_kabupaten_kota mkabkot ON mkabkot.id_kabupaten_kota = trf.point_export_city
		LEFT JOIN
	mst_propinsi mprop ON mprop.id_propinsi = mkabkot.id_propinsi
		LEFT JOIN
	mst_negara mneg ON mneg.id_negara = trf.export_destination
        LEFT JOIN
    mst_type_tag mtag ON mtag.id_type_tag = trfish.type_tag
        LEFT JOIN
    mst_pejabat mpjb ON mpjb.id_pejabat = trf.id_pejabat
WHERE
    trf.is_verified = 'YA'
        AND trf.aktif = 'YA'
GROUP BY trfish.id_reef_fish";

      $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function from_kapi()
    {
      $query = "";

      $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}