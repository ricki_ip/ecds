<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_berat_ikan extends CI_Model
{
  
    function __construct()
    {

    }

  public function data($year, $range_berat)
    {
        $array_query = array();
        $tmp_query = "";
        foreach ($range_berat as $range) {
            $tmp_query = "  SELECT 
                                '".$range['title']."' as nrange,
                                COUNT(*) as frekuensi
                            FROM
                                trs_ctf_fish
                            WHERE
                                ".$range['query']."
                            ";

            array_push($array_query, $tmp_query);
        }

        $union = implode(" UNION ", $array_query);

        $run_query = $this->db->query($union);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}