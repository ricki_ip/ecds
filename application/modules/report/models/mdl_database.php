<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_database extends CI_Model
{
	private $db;
    private $skemaversion = '1404A';

    function __construct()
    {
        $this->db = $this->load->database('default', TRUE);

    }

    private function kabupaten_kota(){
        $this->db->select('id_kabupaten_kota, nama_kabupaten_kota');
        $run_query = $this->db->get('mst_kabupaten_kota');
       
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function dropcreatedb()
    {
        $this->load->dbforge();
        if (!$this->db->table_exists('version'))
        {
           $this->dbforge->drop_database('db_ecds');
           $this->dbforge->create_database('db_ecds');
        }
    }

    public function version()
    {
            if ($this->db->table_exists('version'))
            {
                $this->db->select('seri');
                $query = $this->db->get('version');
                $run_query = $query->row();

                $version = $run_query->seri;
                // vdump($version, true);
                if($this->skemaversion === $version)
                {
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
    }
}