<?php 
 $tmpl = array ( 'table_open'  => '<table id="table_reef_report" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('Document Numbers',
	                          'Type of Export',
	                          'Type of Shipment',
	                          'Form Number of Preceeding Document',
	                          'Exporting State/Fishing Entity',
	                          'City',
	                          'State or Province',
	                          'State / Fishing Entity',
                            'Name of Processing Establishment',
                            'Address of Processing Establishment',
                            'CMF-Catch Tagging Form Document Number',
                            'CMF-Flag State / Fishing Entity',
                            'CMF-Date of Previous Import / Landing',
                            'CMF-Product F (Fresh) or FR (Frozen)',
	                          'CMF-Type (RD/GGO/GGT/DRO/DRT/FL/OT*)',
                            'CMF-Weight (Kg)',
                            'CMF-Total Number of Whole Fish (Incld RD/GGO/GGT/DRO/DRT)',
                            'CMF-Type of Product for Other (OT)',
	                          'REEF-Product : F (Fresh) or FR (Frozen)',
                            'REEF-Type (RD/GGO/GGT/DRO/DRT/FL/OT*)',	                          
                            'REEF-Weight (Kg)',
                            'REEF-Total Number of Whole Fish (Incld RD/GGO/GGT/DRO/DRT)',
                            'REEF-Type of Product for Other (OT)',
                            'Destination (State / Fishing Entity)',
                            'Exporter Name',
                            'Exporter Signature',
                            'Exporter Date',
                            'Exporter License No./Company Name',
                            'Exporter Stamp Seal.',
	                          'Authority Name',
	                          'Authority Title',
	                          'Authority Signature',
	                          'Authority Date',
	                          'Authority Stamp Seal'
                          );
    // vdump($list_reef, true);
    if($list_reef)
    {
      $number = 1;
      foreach ($list_reef as $key => $item) {
        $this->table->add_row( 
                              $item->document_number ,
                              $item->type_of_export ,
                              $item->type_of_shipment ,
                              $item->cmf_document_numbers ,
                              'INDONESIA',                              
                              $item->name_point_export_city ,
                              $item->name_point_export_province ,                              
                              'INDONESIA',
                              $item->name_processing_establishment,
                              $item->address_processing_establishment ,
                              $item->ctf_document_numbers ,
                              'INDONESIA',
                              tgl($item->cmf_date_previous_landing,'F, Y') ,
                              $item->cmf_product_type ,
                              $item->cmf_code_tag ,
                              $item->cmf_net_weight ,
                              angka($item->cmf_total_fish,0) ,
                              '-',
                              $item->product_type,
                              $item->code_tag ,
                              $item->net_weight ,
                              $item->total_fish ,
                              '-',
                              $item->nama_negara ,
                              $item->exporter_name ,
                              'Y',
                              tgl($item->tgl_verifikasi,'d-F-Y') ,
                              $item->exporter_company_name ,
                              'Y',
                              $item->nama_pejabat ,
                              $item->title ,
                              'Y',                              
                              tgl($item->authority_date,'d-F-Y') ,
                              'Y'
                              );
        $number++;
      }
    }

    $table_list_reef = $this->table->generate();

    $this->table->clear();
?>
<div class="row">
  <div class="col-lg-12" style="overflow-x: auto;">
  <div class="panel panel-default">
    <div class="panel-body">
      <a href="xls" class="btn btn-primary">Export to XLS</a>          
    </div>
  </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12" style="overflow-x: auto;">
    <?php echo $table_list_reef; ?>    
  </div>
</div>


<script>
  
  var init_data_tables = function(){
     $('#table_reef_report').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "bFilter": true,
        "bInfo": false,
        "bPaginate": true,
"sPaginationType":"full_numbers",
"iDisplayLength": 25,
        "bSort": true,
        "scrollX": true
      });
    }

  
  s_func.push(init_data_tables);
</script>