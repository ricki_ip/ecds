<?php 
 $tmpl = array ( 'table_open'  => '<table id="table_cds_report" class="table table-bordered table-hover">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('No. ', 'Title', 'Export Link');

    if($list_cds)
    {
      $number = 1;
      foreach ($list_cds as $key => $item) {
        $link_xls = '<a href="'.$item['link'].'" class="btn btn-small btn-primary" >EXPORT</a>';
        $this->table->add_row($number.'. ', $item['title'],$link_xls);
        $number++;
      }
    }

    $table_list_cds = $this->table->generate();

    $this->table->clear();
?>

<div class="row">
  <div class="col-lg-12" style="overflow-x: auto;">
    <?php echo $table_list_cds; ?>    
  </div>
</div>


<script>
  
  var init_data_tables = function(){
     $('#table_cds_report').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "bFilter": true,
        "bInfo": false,
        "bPaginate": true,
"sPaginationType":"full_numbers",
"iDisplayLength": 25,
        "bSort": true,
        "scrollX": true
      });
    }

  
  s_func.push(init_data_tables);
</script>