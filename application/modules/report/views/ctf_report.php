<?php 
 $tmpl = array ( 'table_open'  => '<table id="table_ctf_report" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('Document Number',
                              'Wild Or Farmed',
                              'Document Number of Associated Catch Monitoring Form',
                              'Name of Vessel (or Farm)',
                              'Vessel Registraton Number (or CCSBT Farm Serial Number)',
                              'Flag State/ Fishing Entity(code)',
                              'CCSBT Tag Number',
                              'Type: RD/GG/DR',
                              'Weight (kg)',
                              'Fork Length (cm)',
                              'Gear Code(if applicable)',
                              'CCSBT Statistical Area of Catch(if applicable)',
                              'Month/Year of Harvest (1/mmm/yy)',
                              'Name of Certifier',
                              'Signature Present? (Y/N)',
                              'Date Certified(dd-mmm-yy)',
                              'Title',
                              'Conversion Factor',
                              'Whole Weight of SBT'
                               );

    if($list_ctf)
    {
      $number = 1;
      foreach ($list_ctf as $key => $item) {
        $this->table->add_row( 
                              $item->ctf_document_numbers ,
                              $item->Wild_or_Farmed ,
                              $item->document_number ,
                              $item->vessel_name ,
                              $item->vessel_registration_number ,
                              $item->flag_state ,
                              tagnumber($item->ccsbt_tag_number),
                              $item->code_tag ,
                              $item->weight ,
                              $item->fork_length ,
                              $item->gear_code_name ,
                              $item->ccsbt_statistical_area ,
                              tgl($item->month_of_harvest,'F, Y') ,
                              $item->name_of_certifier ,
                              $item->signature ,
                              tgl($item->certified_date, 'd-F-Y') ,
                              $item->title_of_certifier ,
                              $item->conversion_factor ,
                              angka($item->whole_weight)
                              );
        $number++;
      }
    }

    $table_list_ctf = $this->table->generate();

    $this->table->clear();
?>
<div class="row">
  <div class="col-lg-12" style="overflow-x: auto;">
  <div class="panel panel-default">
    <div class="panel-body">
      <a href="xls" class="btn btn-primary">Export to XLS</a>          
    </div>
  </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12" style="overflow-x: auto;">
    <?php echo $table_list_ctf; ?>    
  </div>
</div>


<script>
  
  var init_data_tables = function(){
     $('#table_ctf_report').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "bFilter": true,
        "bInfo": false,
        "bPaginate": true,
"sPaginationType":"full_numbers",
"iDisplayLength": 25,
        "bSort": true,
        "scrollX": true
      });
    }

  
  s_func.push(init_data_tables);
</script>