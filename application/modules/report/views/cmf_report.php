<?php 
 $tmpl = array ( 'table_open'  => '<table id="table_cmf_report" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('Document Number',
                              'Catch Tagging Form Document Numbers',
                              'Wild Or Farmed',
                              'Name of Catching Vessel',
                              'Registration Number',
                              'Flag State/ Fishing Entity(code)',
                              'F (Fresh) / FR (Frozen)',
                              'Type (RD/GGO/GGT/DRO/DRT/FL/OT*)',
                              'Month of Catch/ Harvest',
                              'Gear Code',
                              'CCSBT Statistical Area',
                              'Net Weight (kg)',
                              'Total Number of Whole Fish (Incld RD/GGO/GGT/DRO/DRT)',
                              ' Name of Processing Establishment ',
                              ' Address of Processing Establishment ',
                              'Name of Validator',
                              'Title of Validator',
                              'Signature (Y/N)',
                              'Date of validation',
                              'Official Stamp Seal',
                              'City',
                              ' State or Province ',
                              ' State/ Fishing Entity ',
                              'Destination (State/ Fishing Entity)',
                              'Name of Exportir',
                              'License No. / Company Name of Exportir',
                              'Date of exportir signature',
                              'Exportir Signature (Y/N)',
                              'Exportir Stamp Seal (Y/N)',
                              'Name of Validator',
                              'Title of Validator',
                              'Validator Signature (Y/N)',
                              'Date of validation',
                              'Validator Official Stamp Seal (Y/N)'
                               );
    // vdump($list_cmf, true);
    if($list_cmf)
    {
      $number = 1;
      foreach ($list_cmf as $key => $item) {
        $this->table->add_row( 
                              $item->document_number ,
                              $item->ctf_document_numbers ,
                              $item->Wild_or_Farmed ,
                              $item->vessel_name ,
                              $item->vessel_registration_number ,
                              $item->flag_state ,
                              $item->product_type,
                              $item->code_tag ,
                              tgl($item->month_of_harvest,'F, Y') ,
                              $item->gear_code_name ,
                              $item->ccsbt_statistical_area ,
                              $item->net_weight ,
                              angka($item->total_fish,0) ,
                              $item->name_processing_establishment,
                              $item->address_processing_establishment,
                              $item->validator_name ,
                              $item->validator_title ,
                              $item->signature ,
                              tgl($item->tgl_verifikasi, 'd-F-Y') ,
                              $item->official_stamp ,
                              kos($item->nama_kabupaten_kota,'-' ),
                              kos($item->nama_propinsi,'-' ),
                              kos($item->state_entity,'-' ),
                              kos($item->nama_negara,'-' ),
                              kos($item->exportir_name,'-' ),
                              kos($item->company_name,'-' ),
                              tgl($item->exportir_date, 'd-F-Y'),
                              kos($item->exportir_signature,'-' ),
                              kos($item->exportir_stamp,'-' ),
                              kos($item->validator_name,'-' ),
                              kos($item->validator_title,'-' ),
                              kos($item->validator_signature),
                              tgl($item->tgl_verifikasi, 'd-F-Y'),
                              kos($item->validator_stamp,'-') 
                              );
        $number++;
      }
    }

    $table_list_cmf = $this->table->generate();

    $this->table->clear();
?>
<div class="row">
  <div class="col-lg-12" style="overflow-x: auto;">
  <div class="panel panel-default">
    <div class="panel-body">
      <a href="xls" class="btn btn-primary">Export to XLS</a>          
    </div>
  </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12" style="overflow-x: auto;">
    <?php echo $table_list_cmf; ?>    
  </div>
</div>


<script>
  
  var init_data_tables = function(){
     $('#table_cmf_report').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "bFilter": true,
        "bInfo": false,
        "bPaginate": true,
"sPaginationType":"full_numbers",
"iDisplayLength": 25,
        "bSort": true,
        "scrollX": true
      });
    }

  
  s_func.push(init_data_tables);
</script>