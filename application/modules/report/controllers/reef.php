<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reef extends MX_Controller {

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   *    http://example.com/index.php/welcome
   *  - or -  
   *    http://example.com/index.php/welcome/index
   *  - or -
   * Since this controller is set as the default controller in 
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see http://codeigniter.com/user_guide/general/urls.html
   */
  function __construct()
  {
      parent::__construct();
      if(!$this->user->is_superadmin())
      {
        echo "Access forbidden."; echo $this->router->method; echo $this->router->class;
        die;
      }
      $this->load->config('menus');
      $this->load->config('db_timeline');
      $this->load->config('globals');
      $this->load->model('mdl_reef_report'); 
  }
  
  public function index()
  {
    $this->load->config('labels');

    $add_js = array('select2.min.js', 'jquery.dataTables.min.js');
    $add_css = array('select2.css', 'jquery.dataTables.css');

    $data['list_reef'] = $this->mdl_reef_report->reef();

    echo Modules::run('templates/page/v_form', //tipe template
              'report', //nama module
              'reef_report', //nama file view
              '', //dari labels.php
              $add_js, //plugin javascript khusus untuk page yang akan di load
              $add_css, //file css khusus untuk page yang akan di load
              $data); //array data yang akan digunakan di file view
  }

  public function xls()
  {
    $list_reef = $this->mdl_reef_report->reef();

    $this->load->library('excel');

    $filename = "reef-CDS.xlsx";


    // JUDUL
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $this->excel->setActiveSheetIndex(0);
    $this->excel->getActiveSheet()->setTitle('reef ID Report');

    $sheet = $this->excel->getActiveSheet();
    $sheet      -> setCellValue('A1', 'Re-Export Form');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 16), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('A1')->applyFromArray($styleArray);
                      $sheet->mergeCells('A1:G1');
    $array_thead = array('Document Numbers',
                            'Type of Export',
                            'Type of Shipment',
                            'Form Number of Preceeding Document',
                            'Exporting State/Fishing Entity',
                            'City',
                            'State or Province',
                            'State / Fishing Entity',
                            'Name of Processing Establishment',
                            'Address of Processing Establishment',
                            'CMF-Catch Tagging Form Document Number',
                            'CMF-Flag State / Fishing Entity',
                            'CMF-Date of Previous Import / Landing',
                            'CMF-Product F (Fresh) or FR (Frozen)',
                            'CMF-Type (RD/GGO/GGT/DRO/DRT/FL/OT*)',
                            'CMF-Weight (Kg)',
                            'CMF-Total Number of Whole Fish (Incld RD/GGO/GGT/DRO/DRT)',
                            'CMF-Type of Product for Other (OT)',
                            'REEF-Product : F (Fresh) or FR (Frozen)',
                            'REEF-Type (RD/GGO/GGT/DRO/DRT/FL/OT*)',                            
                            'REEF-Weight (Kg)',
                            'REEF-Total Number of Whole Fish (Incld RD/GGO/GGT/DRO/DRT)',
                            'REEF-Type of Product for Other (OT)',
                            'Destination (State / Fishing Entity)',
                            'Exporter Name',
                            'Exporter Signature',
                            'Exporter Date',
                            'Exporter License No./Company Name',
                            'Exporter Stamp Seal.',
                            'Authority Name',
                            'Authority Title',
                            'Authority Signature',
                            'Authority Date',
                            'Authority Stamp Seal'
                          );
    $column_letter = 'A';

    foreach ($array_thead as $key => $value) {
       $sheet      -> setCellValue($column_letter.'12', $value);
       $column_letter++;
    }                  
    $styleArray = array('font' => array('bold' => true), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $sheet->getStyle('A12:BG12')->applyFromArray($styleArray);                          
    // MASUKKAN DATA
    $cell_number = 13;

    foreach ($list_reef as $item) {
      $sheet->setCellValue('A'.$cell_number, $item->document_number);
      $sheet->setCellValue('B'.$cell_number, $item->type_of_export);
      $sheet->setCellValue('C'.$cell_number, $item->type_of_shipment);
      $sheet->setCellValue('D'.$cell_number, $item->cmf_document_numbers);
      $sheet->setCellValue('E'.$cell_number, 'INDONESIA');
      $sheet->setCellValue('F'.$cell_number, $item->name_point_export_city);
      $sheet->setCellValue('G'.$cell_number, $item->name_point_export_province);
      $sheet->setCellValue('H'.$cell_number, 'INDONESIA');
      $sheet->setCellValue('I'.$cell_number, $item->name_processing_establishment);
      $sheet->setCellValue('J'.$cell_number, $item->address_processing_establishment);
      $sheet->setCellValue('K'.$cell_number, $item->ctf_document_numbers);
      $sheet->setCellValue('L'.$cell_number, 'INDONESIA');
      $sheet->setCellValue('M'.$cell_number, tgl($item->cmf_date_previous_landing,'F, Y'));
      $sheet->setCellValue('N'.$cell_number, $item->cmf_product_type);
      $sheet->setCellValue('O'.$cell_number, $item->cmf_code_tag);
      $sheet->setCellValue('P'.$cell_number, angka($item->cmf_net_weight,0));
      $sheet->setCellValue('Q'.$cell_number, '-');
      $sheet->setCellValue('R'.$cell_number, $item->product_type);
      $sheet->setCellValue('S'.$cell_number, $item->code_tag);
      $sheet->setCellValue('T'.$cell_number, $item->net_weight);
      $sheet->setCellValue('U'.$cell_number, $item->total_fish);
      $sheet->setCellValue('V'.$cell_number, '-');
      $sheet->setCellValue('W'.$cell_number, $item->nama_negara);
      $sheet->setCellValue('X'.$cell_number, $item->exporter_name);
      $sheet->setCellValue('Y'.$cell_number, 'Y');
      $sheet->setCellValue('AA'.$cell_number, tgl($item->tgl_verifikasi,'d-F-Y'));
      $sheet->setCellValue('AB'.$cell_number, $item->exporter_company_name);
      $sheet->setCellValue('AC'.$cell_number, 'Y');
      $sheet->setCellValue('AD'.$cell_number, $item->nama_pejabat);
      $sheet->setCellValue('AE'.$cell_number, $item->title);
      $sheet->setCellValue('AF'.$cell_number, 'Y');
      $sheet->setCellValue('AG'.$cell_number, tgl($item->authority_date,'d-F-Y'));
      $sheet->setCellValue('AH'.$cell_number, 'Y');     
      $cell_number++;           
    }



                            

    //MENGATUR UKURAN KOLOM
    $array_thwidth = array( 'A' => 20,
                            'B' => 20,
                            'C' => 20,
                            'D' => 20,
                            'E' => 20,
                            'F' => 20,
                            'G' => 0,
                            'H' => 0,
                            'I' => 0,
                            'J' => 20,
                            'K' => 20,
                            'L' => 20,
                            'M' => 20,
                            'N' => 20,
                            'O' => 20,
                            'P' => 20,
                            'Q' => 20,
                            'R' => 20,
                            'S' => 20,
                            'T' => 20,
                            'U' => 20,
                            'V' => 20,
                            'W' => 20,
                            'X' => 20,
                            'Y' => 20,
                            'Z' => 20,
                          );
    foreach ($array_thwidth as $column => $width) {
        $sheet->getColumnDimension($column)->setWidth($width);
    }

    $styleArray = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $sheet->getStyle('A7:BG'.$cell_number)->applyFromArray($styleArray);

    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="REEF-CDS.xlsx"');
    header('Cache-Control: max-age=0');



    //MENGATUR BORDER Table 1

    // $styleArray = array(
    //   'borders' => array(
    //     'allborders' => array(
    //       'style' => PHPExcel_Style_Border::BORDER_THIN
    //     )
    //   )
    // );

    // $this->excel->getActiveSheet('A5:C'.$cell_number)->getStyle('A5:C'.$cell_number)->applyFromArray($styleArray);
    // unset($styleArray);



    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
    $objWriter->save('php://output');
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */