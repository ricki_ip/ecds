<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Xls_catch_bulan_validasi extends MX_Controller {

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   *    http://example.com/index.php/welcome
   *  - or -  
   *    http://example.com/index.php/welcome/index
   *  - or -
   * Since this controller is set as the default controller in 
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see http://codeigniter.com/user_guide/general/urls.html
   */
  function __construct()
  {
      parent::__construct();
      if(!$this->user->is_superadmin())
      {
        echo "Access forbidden."; echo $this->router->method; echo $this->router->class;
        die;
      }
      $this->load->model('mdl_catch_bulan_validasi');
  }

  public function index()
  {

    $this->load->library('excel');
    $workbook = $this->excel;
    
    $filename = "Catch per Bulan Validasi-CDS.xlsx";
    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0');

    $year = date('y');
    $data_xls = $this->mdl_catch_bulan_validasi->data(date('Y'));

    // vdump($data_xls, true);

    // JUDUL
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $workbook->setActiveSheetIndex(0);
    $workbook->getProperties()->setTitle("Catch per Bulan Validasi");
    $sheet = $workbook->getActiveSheet();

     $sheet      -> setCellValue('A1', 'HASIL TANGKAPAN BERDASARKAN BULAN VALIDASI');
                    // setCellValue('A3', 'TAHUN');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 16), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('A1')->applyFromArray($styleArray);
                      $sheet->mergeCells('A1:K1');
    $sheet      ->  setCellValue('A3', 'TAHUN');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 14), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('A3')->applyFromArray($styleArray);
                      $sheet->mergeCells('A3:B3');                  
    // Data 1
    $sheet      ->  setCellValue('A6', 'MONTH');
    
    // Data 2
    $sheet      ->  setCellValue('B5', 'TOTAL');
                    $sheet->mergeCells('B5:C5'); 

    $sheet      ->  setCellValue('B6', 'NET WEIGHT (KG)');
    $sheet      ->  setCellValue('C6', 'ESTIMATED WHOLE WEIGHT (KG)');

    $styleArray = array('font' => array('bold' => true,'size' => 10), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $sheet->getStyle('A6')->applyFromArray($styleArray);          
    $sheet->getStyle('B5')->applyFromArray($styleArray);
    $sheet->getStyle('B6')->applyFromArray($styleArray);  
    $sheet->getStyle('C6')->applyFromArray($styleArray);  


    // MASUKKAN DATA


    $cell_number = 7; // Nomor start baris data

    // Kolom Bulan
    
    foreach ($data_xls as $item) 
    {
      $sheet->setCellValue('A'.$cell_number, $item->nama_bulan_singkat.'-'.$year);
      $cell_number++;                                     
    }
    $cell_number = 7; // RESET Nomor start baris data
    $temp_total_net = 0;
    $temp_total_whole = 0;
    foreach ($data_xls as $item) {
      $sheet->setCellValue('B'.$cell_number, kos($item->net_weight,0) );
      $sheet->setCellValue('C'.$cell_number, kos($item->whole_weight,0) ); 
      $temp_total_net += $item->net_weight;
      $temp_total_whole += $item->whole_weight;
      $cell_number++;
    }                  
    $sheet->setCellValue('B'.$cell_number, kos($temp_total_net,0) );
    $sheet->setCellValue('C'.$cell_number, kos($temp_total_whole,0) );
                                  
     // CHART TIME!

    $categories = new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$A$7:$A$18');
    $values = new PHPExcel_Chart_DataSeriesValues('Number', 'Worksheet!$C$7:$C$18');

    $series = new PHPExcel_Chart_DataSeries(
      PHPExcel_Chart_DataSeries::TYPE_BARCHART,       // plotType
      PHPExcel_Chart_DataSeries::GROUPING_CLUSTERED,  // plotGrouping
      array(0),                                       // plotOrder
      array(),                                        // plotLabel
      array($categories),                             // plotCategory
      array($values)                                  // plotValues
    );
    $series->setPlotDirection(PHPExcel_Chart_DataSeries::DIRECTION_COL);

    $layout = new PHPExcel_Chart_Layout();
    $plotarea = new PHPExcel_Chart_PlotArea($layout, array($series));

    $chart = new PHPExcel_Chart('ESTIMATED WHOLE WEIGHT', null, null, $plotarea);

    $chart->setTopLeftPosition('B23');
    $chart->setBottomRightPosition('G36');

    $sheet->addChart($chart);               
    
    //MENGATUR UKURAN KOLOM
    $array_thwidth = array( 'A' => 15,
                            'B' => 20,
                            'C' => 30,
                          );
    foreach ($array_thwidth as $column => $width) {
        $sheet->getColumnDimension($column)->setWidth($width);
    }

    $styleArray = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $sheet->getStyle('B7:C'.$cell_number)->applyFromArray($styleArray);
    //MENGATUR BORDER Table 1

    // $styleArray = array(
    //   'borders' => array(
    //     'allborders' => array(
    //       'style' => PHPExcel_Style_Border::BORDER_THIN
    //     )
    //   )
    // );

    // $this->excel->getActiveSheet('A5:C'.$cell_number)->getStyle('A5:C'.$cell_number)->applyFromArray($styleArray);
    // unset($styleArray);



    $objWriter = PHPExcel_IOFactory::createWriter($workbook, 'Excel2007');
    $objWriter->setIncludeCharts(TRUE);    
    $objWriter->save('php://output');
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */