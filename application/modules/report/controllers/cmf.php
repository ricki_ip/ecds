<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cmf extends MX_Controller {

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   *    http://example.com/index.php/welcome
   *  - or -  
   *    http://example.com/index.php/welcome/index
   *  - or -
   * Since this controller is set as the default controller in 
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see http://codeigniter.com/user_guide/general/urls.html
   */
  function __construct()
  {
      parent::__construct();
      if(!$this->user->is_superadmin())
      {
        echo "Access forbidden."; echo $this->router->method; echo $this->router->class;
        die;
      }
      $this->load->config('menus');
      $this->load->config('db_timeline');
      $this->load->config('globals');
      $this->load->model('mdl_cmf_report'); 
  }
  
  public function index()
  {
    $this->load->config('labels');

    $add_js = array('select2.min.js', 'jquery.dataTables.min.js');
    $add_css = array('select2.css', 'jquery.dataTables.css');

    $data['list_cmf'] = $this->mdl_cmf_report->cmf();

    echo Modules::run('templates/page/v_form', //tipe template
              'report', //nama module
              'cmf_report', //nama file view
              '', //dari labels.php
              $add_js, //plugin javascript khusus untuk page yang akan di load
              $add_css, //file css khusus untuk page yang akan di load
              $data); //array data yang akan digunakan di file view
  }

  public function xls()
  {
    $list_cmf = $this->mdl_cmf_report->cmf();

    $this->load->library('excel');

    $filename = "CMF-CDS.xlsx";


    // JUDUL
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $this->excel->setActiveSheetIndex(0);
    $this->excel->getActiveSheet()->setTitle('CMF ID Report');

    $sheet = $this->excel->getActiveSheet();
    $sheet      -> setCellValue('A1', 'Catch Monitoring Form');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 16), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('A1')->applyFromArray($styleArray);
                      $sheet->mergeCells('A1:G1');
    $array_thead = array('Document Numbers',
                          'Catch Tagging Form Document Numbers',
                          'Wild or Farmed (W or F)',
                          'Name of Catching Vessel',
                          'Registration Number',
                          'Flag State/ Fishing Entity',
                          'CCSBT Farm Serial Number',
                          'Name of Farm',
                          'Document Number(s) of associated Farm Stocking(FS) Format(s)',
                          'F (Fresh) / FR (Frozen)',
                          'Type (RD/GGO/GGT/DRO/DRT/FL/OT*)',
                          'Month of Catch/ Harvest',
                          'Gear Code',
                          'CCSBT Statistical Area',
                          'Net Weight (Kg)',
                          'Total Number of Whole Fish (Incld RD/GGO/GGT/DRO/DRT)',
                          'Type of Product for Other (OT)',
                          'Conversion Factor for  Other (OT)',
                          'Name of Processing Establishment',
                          'Address of Processing Establishment',
                          'Name of Validator',
                          'Title of Validator',
                          'Signature (Y/N)',
                          'Date of validation',
                          'Official Stamp Seal',
                          'Name of Fishing Master',
                          );
    $column_letter = 'A';

    foreach ($array_thead as $key => $value) {
       $sheet      -> setCellValue($column_letter.'12', $value);
       $column_letter++;
    }                  
    $styleArray = array('font' => array('bold' => true), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $sheet->getStyle('A12:BG12')->applyFromArray($styleArray);                          
    // MASUKKAN DATA
    $cell_number = 13;

    foreach ($list_cmf as $item) {
      $sheet->setCellValue('A'.$cell_number, $item->document_number);
      $sheet->setCellValue('B'.$cell_number, $item->ctf_document_numbers);
      $sheet->setCellValue('C'.$cell_number, $item->Wild_or_Farmed);
      $sheet->setCellValue('D'.$cell_number, $item->vessel_name);
      $sheet->setCellValue('E'.$cell_number, $item->vessel_registration_number);
      $sheet->setCellValue('F'.$cell_number, $item->flag_state);
      $sheet->setCellValue('G'.$cell_number, '');
      $sheet->setCellValue('H'.$cell_number, '');
      $sheet->setCellValue('I'.$cell_number, '');
      $sheet->setCellValue('J'.$cell_number, $item->product_type);
      $sheet->setCellValue('K'.$cell_number, $item->code_tag);
      $sheet->setCellValue('L'.$cell_number, tgl($item->month_of_harvest,'F, Y'));
      $sheet->setCellValue('M'.$cell_number, $item->gear_code_name);
      $sheet->setCellValue('N'.$cell_number, $item->ccsbt_statistical_area);
      $sheet->setCellValue('O'.$cell_number, $item->net_weight);
      $sheet->setCellValue('P'.$cell_number, angka($item->total_fish,0));
      $sheet->setCellValue('Q'.$cell_number, '');
      $sheet->setCellValue('R'.$cell_number, '');
      $sheet->setCellValue('S'.$cell_number, $item->name_processing_establishment);
      $sheet->setCellValue('T'.$cell_number, $item->address_processing_establishment);
      $sheet->setCellValue('U'.$cell_number, $item->validator_name);
      $sheet->setCellValue('V'.$cell_number, $item->validator_title);
      $sheet->setCellValue('W'.$cell_number, $item->signature);
      $sheet->setCellValue('X'.$cell_number, tgl($item->tgl_verifikasi, 'd-F-Y'));
      $sheet->setCellValue('Y'.$cell_number, $item->official_stamp);
      $sheet->setCellValue('AI'.$cell_number, $item->nama_kabupaten_kota);
      $sheet->setCellValue('AJ'.$cell_number, $item->nama_propinsi);
      $sheet->setCellValue('AK'.$cell_number, $item->state_entity);
      $sheet->setCellValue('AL'.$cell_number, $item->nama_negara);
      $sheet->setCellValue('AM'.$cell_number, $item->exportir_name);
      $sheet->setCellValue('AN'.$cell_number, $item->company_name);
      $sheet->setCellValue('AO'.$cell_number, tgl($item->exportir_date, 'd-F-Y'));
      $sheet->setCellValue('AP'.$cell_number, $item->exportir_signature);
      $sheet->setCellValue('AQ'.$cell_number, $item->exportir_stamp);
      $sheet->setCellValue('AR'.$cell_number, kos($item->validator_name,'-' ));
      $sheet->setCellValue('AS'.$cell_number, kos($item->validator_title,'-' ));
      $sheet->setCellValue('AT'.$cell_number, $item->validator_signature);
      $sheet->setCellValue('AU'.$cell_number, tgl($item->tgl_verifikasi, 'd-F-Y'));
      $sheet->setCellValue('AV'.$cell_number, $item->validator_stamp);
      $cell_number++;           
    }



                            

    //MENGATUR UKURAN KOLOM
    $array_thwidth = array( 'A' => 20,
                            'B' => 20,
                            'C' => 20,
                            'D' => 20,
                            'E' => 20,
                            'F' => 20,
                            'G' => 0,
                            'H' => 0,
                            'I' => 0,
                            'J' => 20,
                            'K' => 20,
                            'L' => 20,
                            'M' => 20,
                            'N' => 20,
                            'O' => 20,
                            'P' => 20,
                            'Q' => 20,
                            'R' => 20,
                            'S' => 20,
                            'T' => 20,
                            'U' => 20,
                            'V' => 20,
                            'W' => 20,
                            'X' => 20,
                            'Y' => 20,
                            'Z' => 20,
                          );
    foreach ($array_thwidth as $column => $width) {
        $sheet->getColumnDimension($column)->setWidth($width);
    }

    $styleArray = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $sheet->getStyle('A7:BG'.$cell_number)->applyFromArray($styleArray);

    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="CMF-CDS.xlsx"');
    header('Cache-Control: max-age=0');



    //MENGATUR BORDER Table 1

    // $styleArray = array(
    //   'borders' => array(
    //     'allborders' => array(
    //       'style' => PHPExcel_Style_Border::BORDER_THIN
    //     )
    //   )
    // );

    // $this->excel->getActiveSheet('A5:C'.$cell_number)->getStyle('A5:C'.$cell_number)->applyFromArray($styleArray);
    // unset($styleArray);



    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
    $objWriter->save('php://output');
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */