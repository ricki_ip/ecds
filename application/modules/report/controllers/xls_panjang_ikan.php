<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Xls_panjang_ikan extends MX_Controller {

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   *    http://example.com/index.php/welcome
   *  - or -  
   *    http://example.com/index.php/welcome/index
   *  - or -
   * Since this controller is set as the default controller in 
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see http://codeigniter.com/user_guide/general/urls.html
   */
  private $range_panjang = array(
                              array('title' => '0', 'query' => 'fork_length > 0 AND fork_length <= 20'),
                              array('title' => '21', 'query' => 'fork_length > 20 AND fork_length <= 40'),
                              array('title' => '41', 'query' => 'fork_length > 40 AND fork_length <= 60'),
                              array('title' => '61', 'query' => 'fork_length > 60 AND fork_length <= 80'),
                              array('title' => '81', 'query' => 'fork_length > 80 AND fork_length <= 100'),
                              array('title' => '101', 'query' => 'fork_length > 100 AND fork_length <= 120'),
                              array('title' => '121', 'query' => 'fork_length > 120 AND fork_length <= 140'),
                              array('title' => '141', 'query' => 'fork_length > 140 AND fork_length <= 160'),
                              array('title' => '161', 'query' => 'fork_length > 160 AND fork_length <= 180'),
                              array('title' => '181', 'query' => 'fork_length > 180 AND fork_length <= 200'),
                              array('title' => '201', 'query' => 'fork_length > 200 AND fork_length <= 220'),
                              array('title' => '221', 'query' => 'fork_length > 220 AND fork_length <= 240'),
                             );

  function __construct()
  {
      parent::__construct();
      if(!$this->user->is_superadmin())
      {
        echo "Access forbidden."; echo $this->router->method; echo $this->router->class;
        die;
      }
      $this->load->model('mdl_panjang_ikan');
  }

  public function index()
  {
    $this->load->library('excel');
    $workbook = $this->excel;

    $filename = "Panjang Ikan-CDS.xlsx";

    // // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0');

    $year = date('Y');
    $data_xls = $this->mdl_panjang_ikan->data(date('Y'), $this->range_panjang);
    // vdump($data_xls, true);

   // JUDUL
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $workbook->setActiveSheetIndex(0);
    $workbook->getProperties()->setTitle("Panjang Ikan");
    $sheet = $workbook->getActiveSheet();

    $sheet      -> setCellValue('A1', 'FREKUENSI PANJANG IKAN');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 16), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('A1')->applyFromArray($styleArray);
                      $sheet->mergeCells('A1:G1');

    $styleArray = array('font' => array('bold' => true,'size' => 10), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $sheet->getStyle('A4:B4')->applyFromArray($styleArray);                        

    // MASUKKAN DATA
    $cell_number = 5; // Nomor start baris data

    $sheet->setCellValue('B4', $year );
    $sheet->setCellValue('A4', 'Batas Atas (Kg)' );

    foreach ($data_xls as $item) {
      $sheet->setCellValue('A'.$cell_number, $item->nrange );
      $sheet->setCellValue('B'.$cell_number, kos($item->frekuensi,0) );
      // $sheet->setCellValue($col_char.$cell_number, kos($item->whole_weight,0) ); 
      $cell_number++;
    }                  
                                  
    // CHART TIME!

    $categories = new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$A$5:$A$'.($cell_number-1));
    $values = new PHPExcel_Chart_DataSeriesValues('Number', 'Worksheet!$B$5:$B$'.($cell_number-1));

    $series = new PHPExcel_Chart_DataSeries(
      PHPExcel_Chart_DataSeries::TYPE_BARCHART,       // plotType
      PHPExcel_Chart_DataSeries::GROUPING_CLUSTERED,  // plotGrouping
      array(0),                                       // plotOrder
      array(),                                        // plotLabel
      array($categories),                             // plotCategory
      array($values)                                  // plotValues
    );
    $series->setPlotDirection(PHPExcel_Chart_DataSeries::DIRECTION_COL);

    $layout = new PHPExcel_Chart_Layout();
    $plotarea = new PHPExcel_Chart_PlotArea($layout, array($series));

    $chart = new PHPExcel_Chart('ESTIMATED WHOLE WEIGHT', null, null, $plotarea);

    $chart->setTopLeftPosition('B23');
    $chart->setBottomRightPosition('G36');

    $sheet->addChart($chart);    
    //MENGATUR UKURAN KOLOM
    $array_thwidth = array( 'A' => 15,
                            'B' => 20,
                            'C' => 30,
                          );
    foreach ($array_thwidth as $column => $width) {
        $sheet->getColumnDimension($column)->setWidth($width);
    }

    $styleArray = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $sheet->getStyle('A5:C'.$cell_number)->applyFromArray($styleArray);



    //MENGATUR BORDER Table 1

    // $styleArray = array(
    //   'borders' => array(
    //     'allborders' => array(
    //       'style' => PHPExcel_Style_Border::BORDER_THIN
    //     )
    //   )
    // );

    // $this->excel->getActiveSheet('A5:C'.$cell_number)->getStyle('A5:C'.$cell_number)->applyFromArray($styleArray);
    // unset($styleArray);


    $objWriter = PHPExcel_IOFactory::createWriter($workbook, 'Excel2007');
    $objWriter->setIncludeCharts(TRUE);    
    $objWriter->save('php://output');
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */