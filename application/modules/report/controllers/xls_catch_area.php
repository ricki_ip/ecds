<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Xls_catch_area extends MX_Controller {

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   *    http://example.com/index.php/welcome
   *  - or -  
   *    http://example.com/index.php/welcome/index
   *  - or -
   * Since this controller is set as the default controller in 
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see http://codeigniter.com/user_guide/general/urls.html
   */
  function __construct()
  {
      parent::__construct();
      if(!$this->user->is_superadmin())
      {
        echo "Access forbidden."; echo $this->router->method; echo $this->router->class;
        die;
      }
      $this->load->model('mdl_catch_area');
  }

  public function index()
  {

    $this->load->library('excel');

    $filename = "Catch Per Area-CDS.xlsx";

    $year = date('y');
    $data_xls = $this->mdl_catch_area->data(date('Y'));

    // vdump($data_xls, true);

    // JUDUL
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $this->excel->setActiveSheetIndex(0);
    $this->excel->getActiveSheet()->setTitle('Catch Per Area');

    $sheet = $this->excel->getActiveSheet();

    $sheet      -> setCellValue('A1', 'HASIL TANGKAPAN SBT PER CCSBT STATISTICAL AREA');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 16), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('A1')->applyFromArray($styleArray);
                      $sheet->mergeCells('A1:G1');

    $sheet->setCellValue('A4', 'CCSBT Statistical Area of Catch' );
    $sheet->setCellValue('B4', 'Jumlah Kapal' );
    $sheet->setCellValue('C4', 'Net Weight (Kg)' );
    $sheet->setCellValue('D4', 'Estimated Whole Weight (Kg)' );
    
    $styleArray = array('font' => array('bold' => true,'size' => 10), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $sheet->getStyle('A4:D4')->applyFromArray($styleArray);                       
    // MASUKKAN DATA


    $cell_number = 5; // Nomor start baris data
    $temp_total_net = 0;
    $temp_total_whole = 0;
    $temp_total_kapal = 0;
    foreach ($data_xls as $item) {
      $sheet->setCellValue('A'.$cell_number, $item->code_ccsbt_area );
      $sheet->setCellValue('B'.$cell_number, kos($item->jumlah_kapal,0) );
      $sheet->setCellValue('C'.$cell_number, kos($item->net_weight,0) );
      $sheet->setCellValue('D'.$cell_number, kos($item->whole_weight,0) ); 
      $temp_total_net += $item->net_weight;
      $temp_total_whole += $item->whole_weight;
      $temp_total_kapal += $item->jumlah_kapal;
      $cell_number++;
    }                  
    $sheet->setCellValue('A'.$cell_number, 'JUMLAH' );
    $sheet->setCellValue('B'.$cell_number, kos($temp_total_kapal,0) );
    $sheet->setCellValue('C'.$cell_number, kos($temp_total_net,0) );
    $sheet->setCellValue('D'.$cell_number, kos($temp_total_whole,0) );
                                  

       //MENGATUR UKURAN KOLOM
    $array_thwidth = array( 'A' => 15,
                            'B' => 20,
                            'C' => 30,
                          );
    foreach ($array_thwidth as $column => $width) {
        $sheet->getColumnDimension($column)->setWidth($width);
    }

    $styleArray = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $sheet->getStyle('A5:C'.$cell_number)->applyFromArray($styleArray);

    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0');



    //MENGATUR BORDER Table 1

    // $styleArray = array(
    //   'borders' => array(
    //     'allborders' => array(
    //       'style' => PHPExcel_Style_Border::BORDER_THIN
    //     )
    //   )
    // );

    // $this->excel->getActiveSheet('A5:C'.$cell_number)->getStyle('A5:C'.$cell_number)->applyFromArray($styleArray);
    // unset($styleArray);



    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
    $objWriter->save('php://output');
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */