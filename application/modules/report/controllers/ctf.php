<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ctf extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
			parent::__construct();
      if(!$this->user->is_superadmin())
      {
        echo "Access forbidden."; echo $this->router->method; echo $this->router->class;
        die;
      }
			$this->load->config('menus');
			$this->load->config('db_timeline');
			$this->load->config('globals');
			$this->load->model('mdl_ctf_report');	
	}
	
	public function index()
	{
		$this->load->config('labels');

		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$data['list_ctf'] = $this->mdl_ctf_report->ctf();

		echo Modules::run('templates/page/v_form', //tipe template
							'report', //nama module
							'ctf_report', //nama file view
							'', //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view
	}

	public function xls()
	{
		$list_ctf = $this->mdl_ctf_report->ctf();
    // vdump($list_ctf, true);

		$this->load->library('excel');
    $workbook = $this->excel;


    $filename = "CTF-CDS.xlsx";


    // JUDUL
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $workbook->setActiveSheetIndex(0);
    $workbook->getProperties()->setTitle("CTF ID REPORT");

    $sheet = $workbook->getActiveSheet();

    $sheet      -> setCellValue('A1', 'Catch Tagging Form');
    
    $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 16), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $sheet->getStyle('A1')->applyFromArray($styleArray);
    $sheet->mergeCells('A1:G1');

    $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS'), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $sheet->getStyle('A12:T12')->applyFromArray($styleArray);

    $array_thead = array('Document Number',
                          'Wild Or Farmed',
                          'Document Number of Associated Catch Monitoring Form',
                          'Name of Vessel (or Farm)',
                          'Vessel Registraton Number (or CCSBT Farm Serial Number)',
                          'Flag State/ Fishing Entity(code)',
                          'Information on other forms of Capture (eg Trap)',
                          'CCSBT Tag Number',
                          'Type: RD/GG/DR',
                          'Weight (kg)',
                          'Fork Length (cm)',
                          'Gear Code(if applicable)',
                          'CCSBT Statistical Area of Catch(if applicable)',
                          'Month/Year of Harvest (1/mmm/yy)',
                          'Name of Certifier',
                          'Signature Present? (Y/N)',
                          'Date Certified(dd-mmm-yy)',
                          'Title',
                          'Conversion Factor',
                          'Whole Weight of SBT');
  $column_letter = 'A';

  foreach ($array_thead as $key => $value) {
     $sheet      -> setCellValue($column_letter.'12', $value);
     $column_letter++;
  }

                            
    // MASUKKAN DATA
    $cell_number = 13;

    foreach ($list_ctf as $item) {
      $sheet->setCellValue('A'.$cell_number, $item->ctf_document_numbers);
      $sheet->setCellValue('B'.$cell_number, $item->Wild_or_Farmed);
      $sheet->setCellValue('C'.$cell_number, $item->document_number);
      $sheet->setCellValue('D'.$cell_number, $item->vessel_name);
      $sheet->setCellValue('E'.$cell_number, $item->vessel_registration_number);
      $sheet->setCellValue('F'.$cell_number, $item->flag_state);
      $sheet->setCellValue('G'.$cell_number, '');
      $sheet->setCellValue('H'.$cell_number, tagnumber($item->ccsbt_tag_number));
      $sheet->setCellValue('I'.$cell_number, $item->code_tag);
      $sheet->setCellValue('J'.$cell_number, angka($item->weight) );
      $sheet->setCellValue('K'.$cell_number, angka($item->fork_length) );
      $sheet->setCellValue('L'.$cell_number, $item->gear_code_name);
      $sheet->setCellValue('M'.$cell_number, $item->ccsbt_statistical_area);
      $sheet->setCellValue('N'.$cell_number, tgl($item->month_of_harvest,'F, Y'));
      $sheet->setCellValue('O'.$cell_number, $item->name_of_certifier);
      $sheet->setCellValue('P'.$cell_number, $item->signature);
      $sheet->setCellValue('Q'.$cell_number, tgl($item->certified_date, 'd-F-Y'));
      $sheet->setCellValue('R'.$cell_number, $item->title_of_certifier);
      $sheet->setCellValue('S'.$cell_number, $item->conversion_factor);
      $sheet->setCellValue('T'.$cell_number, angka($item->whole_weight)); 
      $cell_number++;           
    }



                            

    //MENGATUR UKURAN KOLOM
    $array_thwidth = array( 'A' => 20,
                            'B' => 20,
                            'C' => 20,
                            'D' => 20,
                            'E' => 20,
                            'F' => 20,
                            'G' => 0,
                            'H' => 20,
                            'I' => 20,
                            'J' => 20,
                            'K' => 20,
                            'L' => 20,
                            'M' => 20,
                            'N' => 20,
                            'O' => 20,
                            'P' => 20,
                            'Q' => 20,
                            'R' => 20,
                            'S' => 20,
                            'T' => 20
                          );
    foreach ($array_thwidth as $column => $width) {
        $sheet->getColumnDimension($column)->setWidth($width);
    }

    $styleArray = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $sheet->getStyle('A12:T'.$cell_number)->applyFromArray($styleArray);

    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="CTF-CDS.xlsx"');
    header('Cache-Control: max-age=0');



    //MENGATUR BORDER Table 1

    // $styleArray = array(
    //   'borders' => array(
    //     'allborders' => array(
    //       'style' => PHPExcel_Style_Border::BORDER_THIN
    //     )
    //   )
    // );

    // $this->excel->getActiveSheet('A5:C'.$cell_number)->getStyle('A5:C'.$cell_number)->applyFromArray($styleArray);
    // unset($styleArray);



    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
    $objWriter->save('php://output');
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */