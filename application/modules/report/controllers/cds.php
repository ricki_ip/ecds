<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cds extends MX_Controller {

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   *    http://example.com/index.php/welcome
   *  - or -  
   *    http://example.com/index.php/welcome/index
   *  - or -
   * Since this controller is set as the default controller in 
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see http://codeigniter.com/user_guide/general/urls.html
   */
  function __construct()
  {
      parent::__construct();
      if(!$this->user->is_superadmin())
      {
        echo "Access forbidden."; echo $this->router->method; echo $this->router->class;
        die;
      }
      $this->load->config('menus');
      $this->load->config('db_timeline');
      $this->load->config('globals');
      $this->load->model('mdl_cmf_report'); 
  }
  
  public function index()
  {
    $this->load->config('labels');

    $add_js = array('select2.min.js', 'jquery.dataTables.min.js');
    $add_css = array('select2.css', 'jquery.dataTables.css');

    $data['list_cds'] = array(
                              array('title'=> 'Catch per Bulan Tangkap', 
                                    'link'=> site_url('report/xls_catch_bulan_tangkap')  ),
                              array('title'=> 'Catch per Bulan Validasi', 
                                    'link'=> site_url('report/xls_catch_bulan_validasi')  ),
                              array('title'=> 'Berat Ikan', 
                                    'link'=> site_url('report/xls_berat_ikan')  ),
                              array('title'=> 'Panjang Ikan', 
                                    'link'=> site_url('report/xls_panjang_ikan')  ),
                              array('title'=> 'Catch per Area', 
                                    'link'=> site_url('report/xls_catch_area')  ),
                              array('title'=> 'Berat per Area', 
                                    'link'=> site_url('report/xls_berat_area')  ),
                              array('title'=> 'Tangkapan Kapal', 
                                    'link'=> site_url('report/xls_tangkapan_kapal/index/all')  ),
                              array('title'=> 'Tangkapan Kapal >= 30 GT', 
                                    'link'=> site_url('report/xls_tangkapan_kapal/index/above')  ),
                              array('title'=> 'Tangkapan Kapal < 30 GT', 
                                    'link'=> site_url('report/xls_tangkapan_kapal/index/below')  ),
                             );
    echo Modules::run('templates/page/v_form', //tipe template
              'report', //nama module
              'cds_report', //nama file view
              '', //dari labels.php
              $add_js, //plugin javascript khusus untuk page yang akan di load
              $add_css, //file css khusus untuk page yang akan di load
              $data); //array data yang akan digunakan di file view
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */