<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Xls_berat_area extends MX_Controller {

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   *    http://example.com/index.php/welcome
   *  - or -  
   *    http://example.com/index.php/welcome/index
   *  - or -
   * Since this controller is set as the default controller in 
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   *
   * @see http://codeigniter.com/user_guide/general/urls.html
   */
  function __construct()
  {
      parent::__construct();
      if(!$this->user->is_superadmin())
      {
        echo "Access forbidden."; echo $this->router->method; echo $this->router->class;
        die;
      }
      $this->load->model('mdl_berat_area');
  }

  public function index()
  {

    $this->load->library('excel');
    $workbook = $this->excel;

    $filename = "Berat per Area-CDS.xlsx";

    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0');

    $year = date('Y');
    $data_xls = $this->mdl_berat_area->data(date('Y'));

    // vdump($data_xls, true);

   // JUDUL
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $workbook->setActiveSheetIndex(0);
    $workbook->getProperties()->setTitle("Berat per Area");
    $sheet = $workbook->getActiveSheet();

    $sheet      -> setCellValue('A1', 'KOMPOSISI BERAT IKAN PER WILAYAH PENANGKAPAN IKAN');
                    // setCellValue('A3', 'TAHUN');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 16), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('A1')->applyFromArray($styleArray);
                      $sheet->mergeCells('A1:F1');

    $styleArray = array('font' => array('bold' => true,'size' => 10), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $sheet->getStyle('A4:B4')->applyFromArray($styleArray);                     

    // MASUKKAN DATA
    $cell_number = 5; // Nomor start baris data

    $sheet->setCellValue('B4', $year );
    $sheet->setCellValue('A4', 'CCSBT Statistical Area of Catch' );

    foreach ($data_xls as $item) {
      $sheet->setCellValue('A'.$cell_number, $item->ccsbt_statistical_area );
      $sheet->setCellValue('B'.$cell_number, kos($item->whole_weight,0) );
      // $sheet->setCellValue($col_char.$cell_number, kos($item->whole_weight,0) ); 
      $cell_number++;
    }                  
                                  
    // CHART TIME!

    $categories = new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$A$5:$A$'.$cell_number);
    $values = new PHPExcel_Chart_DataSeriesValues('Number', 'Worksheet!$B$5:$B$'.$cell_number);

    $series = new PHPExcel_Chart_DataSeries(
      PHPExcel_Chart_DataSeries::TYPE_BARCHART,       // plotType
      PHPExcel_Chart_DataSeries::GROUPING_CLUSTERED,  // plotGrouping
      array(0),                                       // plotOrder
      array(),                                        // plotLabel
      array($categories),                             // plotCategory
      array($values)                                  // plotValues
    );
    $series->setPlotDirection(PHPExcel_Chart_DataSeries::DIRECTION_COL);

    $layout = new PHPExcel_Chart_Layout();
    $plotarea = new PHPExcel_Chart_PlotArea($layout, array($series));

    $chart = new PHPExcel_Chart('ESTIMATED WHOLE WEIGHT', null, null, $plotarea);

    $chart->setTopLeftPosition('B23');
    $chart->setBottomRightPosition('K36');

    $sheet->addChart($chart);    
    
      //MENGATUR UKURAN KOLOM
    $array_thwidth = array( 'A' => 30,
                            'B' => 20,
                          );
    foreach ($array_thwidth as $column => $width) {
        $sheet->getColumnDimension($column)->setWidth($width);
    }

    $styleArray = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $sheet->getStyle('A5:B'.$cell_number)->applyFromArray($styleArray);



    //MENGATUR BORDER Table 1

    // $styleArray = array(
    //   'borders' => array(
    //     'allborders' => array(
    //       'style' => PHPExcel_Style_Border::BORDER_THIN
    //     )
    //   )
    // );

    // $this->excel->getActiveSheet('A5:C'.$cell_number)->getStyle('A5:C'.$cell_number)->applyFromArray($styleArray);
    // unset($styleArray);


    $objWriter = PHPExcel_IOFactory::createWriter($workbook, 'Excel2007');
    $objWriter->setIncludeCharts(TRUE);    
    $objWriter->save('php://output');
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */