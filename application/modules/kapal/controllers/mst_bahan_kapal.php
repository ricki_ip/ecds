<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_bahan_kapal extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			

		}

	public function list_bahan_kapal_json()
	{
		$this->load->model('mdl_bahan_kapal');

		$list_opsi = $this->mdl_bahan_kapal->list_opsi();

		echo json_encode($list_opsi);
	}

	public function list_bahan_kapal_array()
	{
		$this->load->model('mdl_bahan_kapal');

		$list_opsi = $this->mdl_bahan_kapal->list_opsi();

		return $list_opsi;
	}
	
}
?>