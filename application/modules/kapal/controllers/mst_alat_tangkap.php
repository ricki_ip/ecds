<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_alat_tangkap extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			

		}

	public function list_alat_tangkap_json()
	{
		$this->load->model('mdl_alat_tangkap');

		$list_opsi = $this->mdl_alat_tangkap->list_opsi();

		echo json_encode($list_opsi);
	}

	public function list_alat_tangkap_array()
	{
		$this->load->model('mdl_alat_tangkap');

		$list_opsi = $this->mdl_alat_tangkap->list_opsi();

		return $list_opsi;
	}
	
}
?>