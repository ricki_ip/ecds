<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_wilayah extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			

		}

	public function list_propinsi_json()
	{
		$this->load->model('mdl_propinsi');

		$list_opsi = $this->mdl_propinsi->list_opsi();

		echo json_encode($list_opsi);
	}

	public function list_propinsi_array()
	{
		$this->load->model('mdl_propinsi');

		$list_opsi = $this->mdl_propinsi->list_opsi();

		return $list_opsi;
	}

	public function list_kab_kota_json()
	{
		$this->load->model('mdl_kabupaten_kota');

		$list_opsi = $this->mdl_kabupaten_kota->list_opsi();

		echo json_encode($list_opsi);
	}


	public function list_kab_kota_array()
	{
		$this->load->model('mdl_kabupaten_kota');

		$list_opsi = $this->mdl_kabupaten_kota->list_opsi();

		return $list_opsi;
	}

	public function list_kaprop_json()
	{
		$this->load->model('mdl_kabupaten_kota');

		$list_opsi = $this->mdl_kabupaten_kota->list_opsi_kaprop();

		echo json_encode($list_opsi);
	}


	public function list_kaprop_array()
	{
		$this->load->model('mdl_kabupaten_kota');

		$list_opsi = $this->mdl_kabupaten_kota->list_opsi_kaprop();
		// print_r($list_opsi);die;
		return $list_opsi;
	}
}
?>