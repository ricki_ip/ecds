<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kapal extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
			parent::__construct();
			if(!$this->user->is_superadmin())
			{
				//echo "Access forbidden."; echo $this->router->method; echo $this->router->class;
				
			}
			$this->load->config('menus');
			$this->load->config('db_timeline');
			$this->load->config('globals');
			$this->load->model('mdl_kapal');
	}
	
	public function index()
	{
		$this->load->model('scrapper/mdl_scrapper');

		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$data['list_kapal'] = $this->mdl_kapal->list_kapal();
		$data['scrapper_info'] = $this->mdl_scrapper->last_update();
		$data['link_vessel_detail_ccsbt'] = 'http://www.ccsbt.org/site/authorised_vessels_detail.php?id=';

		// var_dump($data['list_kapal']);die;
		$template = 'templates/page/v_form';
		$modules = 'kapal';
		$views = 'tabel_kapal';
		$labels = 'view_kapal_table';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}



	public function edit_perusahaan($id_kapal)
	{		
		$get_detail = $this->mdl_kapal->detail_kapal($id_kapal);

		if( !$get_detail )
		{
			$data['detail_kapal'] = FALSE;
		}else{
			$data['detail_kapal'] = (array)$get_detail;
		}
		$data['id_kapal'] = $id_kapal;
		$data['submit_form'] = 'kapal/update_perusahaan';
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$template = 'templates/page/v_form';
		$modules = 'kapal';
		$views = 'form_entry_kapal';
		$labels = 'form_edit_kapal';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

	}

	public function update_perusahaan()
	{
		$this->load->model('mdl_kapal');

		$array_input = $this->input->post(NULL, TRUE);
		$id_vessel_ccsbt = $array_input['id_vessel_ccsbt'];
		unset($array_input['vessel_name']);
		unset($array_input['ccsbt_registration_number']);
		unset($array_input['owner_name']);
		// var_dump($array_input);
		// die;

		if( $this->mdl_kapal->update($id_vessel_ccsbt, $array_input) ){
			//$this->mdl_kapal->set_status_entry($id_vessel_ccsbt);
			$url = base_url('kapal/edit_perusahaan/'.$id_vessel_ccsbt);
			// var_dump($url);
			// die;
			redirect($url);
		}else{
			$url = base_url('kapal/index');
			// var_dump($url);
			// die;
			redirect($url);
		}
	}

	public function exceltest2()
	{
		//load our new PHPExcel library
		$this->load->library('excel');
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('test worksheet');
		//set cell A1 content with some text
		//$array_test = array
		for ($index=0; $index < 10 ; $index++) { 
			$this->excel->getActiveSheet()->setCellValue('A'.$index ,'Baris Ke: '.$index);
		}
		
		 
		$filename='just_some_random_name.xlsx'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
		             
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */