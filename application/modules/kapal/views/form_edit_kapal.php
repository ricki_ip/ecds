<div class="row">
  <div class="col-lg-12">
          <?php

          $input_hidden  = array('id_kapal' => $list_kapal['id_kapal'] );

          echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"', $input_hidden);

          // $attr_tanggal_surat_permohonan = array( 
          //                  'mindate' => array('time' => '1 year'), // opsi: '', array('time' => '1 year'), '2012-10-11', tidak wajib ada
          //                 // 'maxdate' => 'various', // opsi: '', array('time' => '1 year'), '2013-10-11', tidak wajib ada
          //                 // 'defaultdate' => '2013-20-12', // opsi: '', tidak wajib ada
          //                 'placeholder' => '', // wajib ada atau '' (kosong)
          //                 'name' => $form['tanggal_surat_permohonan']['name'], // wajib ada
          //                 'label' => $form['tanggal_surat_permohonan']['label'] // wajib ada
          //               );
          // echo $this->mkform->input_date($attr_tanggal_surat_permohonan);
          ?>


         <?php 


          $attr_provinsi = array( 'name' => $form['provinsi']['name'],
                                  'label' => $form['provinsi']['label'],
                                  'opsi' => Modules::run('kapal/mst_wilayah/list_propinsi_array')
                    );
          echo $this->mkform->input_select2($attr_provinsi);

          $attr_kab_kota = array('name' => $form['kab_kota']['name'],
                                 'label' => $form['kab_kota']['label'],
                                 'opsi' => Modules::run('kapal/mst_wilayah/list_kab_kota_array')
                    );
          echo $this->mkform->input_select2($attr_kab_kota);           

          $attr_tahun_pembuatan = array( 'name' => $form['tahun_pembuatan']['name'],
                                        'label' => $form['tahun_pembuatan']['label'],
                                          'value' => $list_kapal['tahun_pembuatan']

                    );          
          echo $this->mkform->input_text($attr_tahun_pembuatan);  

          $attr_nama_kapal = array( 'name' => $form['nama_kapal']['name'],
                                        'label' => $form['nama_kapal']['label'],
                                          'value' => $list_kapal['nama_kapal']
                    );
          echo $this->mkform->input_text($attr_nama_kapal);  

          $attr_tanda_selar = array( 'name' => $form['tanda_selar']['name'],
                                        'label' => $form['tanda_selar']['label'],
                                          'value' => $list_kapal['tanda_selar']
                    );          
          echo $this->mkform->input_text($attr_tanda_selar);  

          $attr_kub_penerima = array( 'name' => $form['kub_penerima']['name'],
                                        'label' => $form['kub_penerima']['label'],
                                          'value' => $list_kapal['kub_penerima']
                    );
          echo $this->mkform->input_text($attr_kub_penerima); 

          $attr_ketua = array( 'name' => $form['ketua']['name'],
                                        'label' => $form['ketua']['label'],
                                          'value' => $list_kapal['ketua']
                    );
          echo $this->mkform->input_text($attr_ketua); 

          $attr_anggota = array( 'name' => $form['anggota']['name'],
                                        'label' => $form['anggota']['label'],
                                          'value' => $list_kapal['anggota']
                    );
          echo $this->mkform->input_text($attr_anggota); 

          $attr_kontak = array( 'name' => $form['kontak']['name'],
                                        'label' => $form['kontak']['label'],
                                          'value' => $list_kapal['kontak']
                    );
          echo $this->mkform->input_text($attr_kontak); 

          $attr_alamat = array( 'name' => $form['alamat']['name'],
                                        'label' => $form['alamat']['label'],
                                          'value' => $list_kapal['alamat']
                    );
          echo $this->mkform->input_text($attr_alamat); 

          $attr_sumber_anggaran = array( 'name' => $form['sumber_anggaran']['name'],
                                         'label' => $form['sumber_anggaran']['label'],
                                         'opsi' => array(
                                                          'TP Provinsi' => 'TP Provinsi', 
                                                          'TP Kab/Kota' => 'TP Kab/Kota',
                                                          'DAK' => 'DAK'
                                                        )
                    );
          echo $this->mkform->input_select($attr_sumber_anggaran); 

          $attr_bahan_kapal = array('name' => $form['bahan_kapal']['name'],
                                    'label' => $form['bahan_kapal']['label'],
                                    'opsi' => Modules::run('kapal/mst_bahan_kapal/list_bahan_kapal_array')
                    );
          echo $this->mkform->input_select2($attr_bahan_kapal);

          $attr_gt = array( 'name' => $form['gt']['name'],
                              'label' => $form['gt']['label'],
                                          'value' => $list_kapal['gt']
                    );
          echo $this->mkform->input_text($attr_gt); 

          // TODO : Dibuat select2 ambil list dari select2
          // $test = Modules::run('refdss/mst_wilayah/list_propinsi_array');
          // var_dump($test);

          $attr_panjang_kapal = array( 'name' => $form['panjang_kapal']['name'],
                                        'label' => $form['panjang_kapal']['label'],
                                          'value' => $list_kapal['panjang_kapal']
                    );
          echo $this->mkform->input_text($attr_panjang_kapal);

          $attr_lebar_kapal = array( 'name' => $form['lebar_kapal']['name'],
                                        'label' => $form['lebar_kapal']['label'],
                                          'value' => $list_kapal['lebar_kapal']
                    );
          echo $this->mkform->input_text($attr_lebar_kapal);

          $attr_dalam_kapal = array( 'name' => $form['dalam_kapal']['name'],
                                        'label' => $form['dalam_kapal']['label'],
                                          'value' => $list_kapal['dalam_kapal']
                    );
          echo $this->mkform->input_text($attr_dalam_kapal);

          $attr_mesin = array( 'name' => $form['mesin']['name'],
                                        'label' => $form['mesin']['label'],
                                          'value' => $list_kapal['mesin']
                    );
          echo $this->mkform->input_text($attr_mesin);
          
          $attr_daya = array( 'name' => $form['daya']['name'],
                                        'label' => $form['daya']['label'],
                                          'value' => $list_kapal['daya']
                    );
          echo $this->mkform->input_text($attr_daya);
          
          $attr_pelabuhan_pangkalan = array( 'name' => $form['pelabuhan_pangkalan']['name'],
                                        'label' => $form['pelabuhan_pangkalan']['label'],
                                          'value' => $list_kapal['pelabuhan_pangkalan']
                    );
          echo $this->mkform->input_text($attr_pelabuhan_pangkalan);
          
          $attr_jenis_alat_tangkap = array( 'name' => $form['jenis_alat_tangkap']['name'],
                                            'label' => $form['jenis_alat_tangkap']['label'],
                                            'opsi' => Modules::run('kapal/mst_alat_tangkap/list_alat_tangkap_array')
                    );
          echo $this->mkform->input_select2($attr_jenis_alat_tangkap);
          
          $attr_gross_akte = array( 'name' => $form['gross_akte']['name'],
                                        'label' => $form['gross_akte']['label'],
                                          'value' => $list_kapal['gross_akte']
                    );
          echo $this->mkform->input_text($attr_gross_akte);
          
          $attr_no_siup = array( 'name' => $form['no_siup']['name'],
                                        'label' => $form['no_siup']['label'],
                                          'value' => $list_kapal['siup']
                    );
          echo $this->mkform->input_text($attr_no_siup);
          
          $attr_no_sipi = array( 'name' => $form['no_sipi']['name'],
                                        'label' => $form['no_sipi']['label'],
                                          'value' => $list_kapal['sipi']
                    );
          echo $this->mkform->input_text($attr_no_sipi);
          
          $attr_kontraktor_pembangunan = array( 'name' => $form['kontraktor_pembangunan']['name'],
                                        'label' => $form['kontraktor_pembangunan']['label'],
                                          'value' => $list_kapal['kontraktor_pembangunan']
                    );
          echo $this->mkform->input_text($attr_kontraktor_pembangunan);

          $attr_lokasi_pembangunan = array( 'name' => $form['lokasi_pembangunan']['name'],
                                        'label' => $form['lokasi_pembangunan']['label'],
                                          'value' => $list_kapal['lokasi_pembangunan']
                    );
          echo $this->mkform->input_text($attr_lokasi_pembangunan);


         ?>
  </div>
</div>   
         
<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-8">
              <button type="submit" class="btn btn-primary">Reset</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </div>
  </div>
</div>
</form>

<div id="modal-search-kapal" class="modal fade" data-width="760">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        <table class="table table-bordered">
        <thead> <th>No.</th> <th>Nama Kapal</th><th>No. SIPI</th><th>Tanggal SIPI</th></thead>
        <tbody></tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>  
</div><!-- /.modal -->


<script>
    var set_validation = function() 
    {
      $("#id_no_surat_permohonan").addClass('validate[required]');

      $('#form_entry').validationEngine();
    }//end set_validation

    var typingTimer;                //timer identifier
    var doneTypingInterval = 1500;
    var search_kapal_listener = function () {
   
      function doneTyping(query){
        $("#id_nama_kapal").popover('destroy');
        if(query !== '')
        {
          $("#id_nama_kapal").popover({ content: 'Cari Kapal : <button type="button" class="btn btn-info btn-cari-kapal">'+query+'</button>', html: true, placement: 'top'});
          $("#id_nama_kapal").popover('show');
        }
      }

      $("#id_nama_kapal").keydown(function(){
        clearTimeout(typingTimer);
      });

      $("#id_nama_kapal").keyup(function(){
        var query = $(this).val();
          typingTimer = setTimeout(function(){ doneTyping(query); }, doneTypingInterval);
      });

      function update_result(data)
      {
        // console.log(data);
        $("#modal-search-kapal .modal-title").html('');
        $("#modal-search-kapal .modal-title").text('Pencarian Kapal "'+data.search_like+'" ('+data.filter+'). Ditemukan '+data.jumlah_result+' kapal.');
        $("#modal-search-kapal tbody").html(''); // Kosongin
          if(data.jumlah_result > 0)
          {
            data.result.forEach(function(d, i){
              $("#modal-search-kapal tbody").append('<tr><td>'+(i+1)+'.</td> <td>'+d.nama_kapal+'</td><td>'+d.no_sipi+'</td><td>'+d.tanggal_sipi+' s/d '+d.tanggal_akhir_sipi+'</td>  </tr>');
            });
          }

        $("#modal-search-kapal").modal('show');
      }

      $(".form-group").on("click", ".btn-cari-kapal",function(){
          var query = $(this).text();
          // $("#modal-search-kapal .modal-title").text("Pencarin Kapal : "+query);
              $.ajax({
                dataType: "json",
                url: link_search_kapal, //
                data: { i: 'pusat', q: query}, // 
                success: update_result // 
              });
          
      });
    }//end search_kapal_listener

  s_func.push(set_validation);
  s_func.push(search_kapal_listener);
</script>