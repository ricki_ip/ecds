<?php echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');?>
<?php
// var_dump($detail_pengguna);
?>

<div class="row">
  <div class="col-lg-12">      
  <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Mapping Kapal -> Perusahaan:</h3>
        </div>
        <div class="panel-body">

          <div class="hide">
          <?php
          $attr_id_vessel_ccsbt = array( 'name' => 'id_vessel_ccsbt',
                                      'label' => 'ID',
                                      'value' => kos($detail_kapal['id_vessel_ccsbt'])
                    );
          echo $this->mkform->input_text($attr_id_vessel_ccsbt);
          ?>
          </div>
          <?php 
          

          

          $attr_id_perusahaan = array( 'name' => 'id_perusahaan',
                                      'label' => 'Perusahaan',
                                      'opsi' => Modules::run('pengaturan/mst_perusahaan/list_perusahaan_array'),
                                      'value' => kos($detail_kapal['id_perusahaan'])
                    );
          echo $this->mkform->input_select2($attr_id_perusahaan);


          $attr_vessel_name = array( 'name' => 'vessel_name',
                                      'label' => 'Vessel Name',
                                      'value' => kos($detail_kapal['vessel_name'])
                    );
          echo $this->mkform->input_text($attr_vessel_name);
          
          $attr_ccsbt_registration_number = array( 'name' => 'ccsbt_registration_number',
                                      'label' => 'CCSBT Registration Number',
                                      'value' => kos($detail_kapal['ccsbt_registration_number'])
                    );
          echo $this->mkform->input_text($attr_ccsbt_registration_number);

          $attr_owner_name = array( 'name' => 'owner_name',
                                      'label' => 'Owner',
                                      'value' => kos($detail_kapal['owner_name'])
                    );
          echo $this->mkform->input_text($attr_owner_name);

          ?>
        </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
   <button type="SUBMIT" class="btn btn-large btn-block btn-success">SUBMIT DATA</button>
  </div>     
</div>      
<?php echo form_close(); ?>

<script>
  
  var init_data_tables = function(){
      

      $("input").prop("disabled", true);
      $("#id_vessel_ccsbt").prop("disabled", false).attr('type','hidden');
    }

  
  s_func.push(init_data_tables);
</script>