<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_daftar_kapal' width='2000px' class='table table-hover table-bordered'>");
	$this->table->set_template($template);
	$this->table->set_heading($constants['th_table']);
	$counter = 1;
	if($list_kapal){
    $link_ccsbt = '';
    $link_mapping = '';
		foreach ($list_kapal as $item) {
      $link_ccsbt  = '<a href="'.$link_vessel_detail_ccsbt.$item->id_vessel_ccsbt_web.'">'.$item->ccsbt_registration_number.'</a>';
      $link_mapping = $item->id_perusahaan === '0' ? '<a href="'.site_url('kapal/edit_perusahaan/'.$item->id_vessel_ccsbt).'"> <span class="badge">Not Mapped!</span> </a>' : '<a href="'.site_url('kapal/edit_perusahaan/'.$item->id_vessel_ccsbt).'" title="'.$item->id_perusahaan.'"> '.$item->company_name.' <span class="badge" style="background-color: green;">OK</span> </a>';
			$gt_text = "";
      $this->table->add_row(
								$counter.'.',
								$item->vessel_name,
                $link_ccsbt,
                $link_mapping,
                $item->vessel_registration_number,
                $item->call_sign,
                intval( $item->gross_registered_tonnage_tons ) <= 30 ? $item->gross_registered_tonnage_tons." (<30GT)" : $item->gross_registered_tonnage_tons,
                tgl($item->date_authorisation_starts),
                tgl($item->date_authorisation_ends),
                $item->vessel_new_to_lsfv_list,
                tgl($item->tanggal_buat,'d/m/y H:i'),
                $item->owner_name,
                $item->owner_address,
                $item->operator_name,
                $item->operator_address
								);
			$counter++;
		}
	}

	$table_list_kapal = $this->table->generate();
?>

<!-- TAMPIL DATA -->
<div class="panel panel-default">
  <div class="panel-body">
    <div class="row">
      <div class="col-lg-6">
        <button type="button" class="btn btn-info scrapper-info-btn" 
                data-container="body" data-toggle="popover" data-placement="right"
                data-content="Jumlah kapal yang terdaftar di CCSB Authorised vessel berdasarkan update terakhir.">
          Total: <strong> <?php echo $scrapper_info->last_count; ?> Vessels </strong>
        </button>
        <button type="button" class="btn btn-info scrapper-info-btn"
                data-container="body" data-toggle="popover" data-placement="right"
                data-content="Sistem secara otomatis mengupdate data kapal dari website CCSBT tiap hari sekitar pukul 1 Pagi. Info waktu ini adalah waktu pengambilan data kapal yang terakhir didapatkan setelah proses update berhasil dijalankan.">
          Last Updated: <strong> <?php echo $scrapper_info->last_updated; ?> </strong>
        </button>
      </div>
      <div class="col-lg-6">
        <p class="text-right">
          <a id="update_ccsbt_vessel_list" href="<?php echo site_url('scrapper'); ?>" type="button" class="btn btn-default">Update CCSBT Vessel List</a>
        </p>
      </div>  
    </div>
  </div>
</div>
</div>
   <?php
      echo $table_list_kapal;
    ?>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	var init_datatable_kapal = function() {
    $('#table_daftar_kapal').dataTable( {
      "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
      // "aoColumns":  [
   //                      {"sClass": "text-center"},
   //                      {"sClass": "text-center"},
   //                      {"sClass": "text-center"},
   //                      {"sClass": "text-center"},
   //                      {"sClass": "text-center"},
   //                      {"sClass": "text-center"},
   //                      {"sClass": "text-center"},
   //                      {"sClass": "text-center"},
   //                      {"sClass": "text-center"},
   //                      {"sClass": "text-center"},
   //                      {"sClass": "text-center"},
   //                      {"sClass": "text-center"},
   //                      {"sClass": "text-center"},
   //                      {"sClass": "text-center"},
   //                      {"sClass": "text-center"},
   //                      {"sClass": "text-center"},
   //                      {"sClass": "text-center"},
   //                      {"sClass": "text-center"},
   //                      {"sClass": "text-center"},
   //                      {"sClass": "text-center"},
   //                      {"sClass": "text-center"},
   //                      {"sClass": "text-center"},
   //                      {"sClass": "text-center"},
   //                      {"sClass": "text-center"},
   //                      {"sClass": "text-center"}
   //                    ],
          "bFilter": true,
          "bAutoWidth": true,
          "bInfo": false,
          "bPaginate": false,
          "bSort": true,
    } );
};

var confirm_update_ccsbt = function() {
  $('#update_ccsbt_vessel_list').on('click', function(){
    var pesan = 'Sistem akan menjalankan update database kapal dari website CCSBT, proses ini membutuhkan waktu 5-15 Menit tergantung koneksi internet.'+
                'PERINGATAN: Jangan menutup laman ini hingga proses update selesai dan laman terbuka ulang.';
    return confirm(pesan);
  });
}

var scrapper_info_popover = function(){
  $('.scrapper-info-btn').on('click', function(){
      $(this).popover('show');
  });
};

s_func.push(init_datatable_kapal);
s_func.push(confirm_update_ccsbt);
s_func.push(scrapper_info_popover);
</script>