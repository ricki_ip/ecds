<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_kapal extends CI_Model
{

    function __construct()
    {

    }

    public function list_kapal()
    {
        $this->db->select('mst_vessel_ccsbt.*, mst_perusahaan.company_name, mst_perusahaan.id_perusahaan AS id_perusahaan_ori ');
        $this->db->from('mst_vessel_ccsbt');
        $this->db->join('mst_perusahaan','mst_perusahaan.id_perusahaan = mst_vessel_ccsbt.id_perusahaan','left');
       
        $run_query = $this->db->get();                            
    
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_kapal($id_vessel_ccsbt)
    {   
        $this->db->where('id_vessel_ccsbt', $id_vessel_ccsbt);
        $run_query = $this->db->get('mst_vessel_ccsbt');                            
    
        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function update($id_vessel_ccsbt, $data)
    {
        $this->db->where('id_vessel_ccsbt', $data['id_vessel_ccsbt']);
        $query = $this->db->update('mst_vessel_ccsbt',$data);

        if($this->db->affected_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }

        return $result;
    }
}