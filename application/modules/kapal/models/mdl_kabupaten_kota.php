<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_kabupaten_kota extends CI_Model
{
	private $db_monev;

    function __construct()
    {
        $this->db_monev = $this->load->database('default', TRUE);
    }

	public function list_opsi()
    {
    	$query = "SELECT id_kabupaten_kota AS id, nama_kabupaten_kota as text FROM mst_kabupaten_kota";

    	$run_query = $this->db_monev->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_opsi_kaprop()
    {
        $this->db_monev->select('id_kabupaten_kota AS id,
                                nama_propinsi AS label,
                                nama_kabupaten_kota AS text');
        $this->db_monev->from('mst_kabupaten_kota');
        $this->db_monev->join('mst_propinsi', 'mst_kabupaten_kota.id_propinsi = mst_propinsi.id_propinsi');
        $this->db_monev->order_by('mst_kabupaten_kota.id_propinsi');
        $run_query = $this->db_monev->get();   
        // $str = $this->db_monev->last_query();
        // echo $str; die;
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}