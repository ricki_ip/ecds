<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_type_tag extends CI_Model
{
	

    function __construct()
    {
        
    }

	public function list_opsi()
    {
    	$query = "SELECT id_type_tag AS id, code_tag as text FROM mst_type_tag";

    	$run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}