<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_ccsbt_area extends CI_Model
{
	

    function __construct()
    {
        
    }

	public function list_opsi()
    {
    	$query = "SELECT id_ccsbt_area AS id, code_ccsbt_area as text FROM mst_ccsbt_area";

    	$run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}