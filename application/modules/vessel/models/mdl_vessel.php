<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_vessel extends CI_Model
{
    // private $db_dss;
    

    function __construct()
    {
        // $this->db_dss = $this->load->database('db_dss', TRUE);
        
    }

    public function detil_kapal($id_kapal)
    {

        $sql = "SELECT
                db_pendaftaran_kapal.mst_kapal.id_kapal, 
                db_pendaftaran_kapal.mst_kapal.nama_kapal, 
                db_pendaftaran_kapal.mst_kapal.nama_kapal_sebelumnya,
                db_pendaftaran_kapal.mst_kapal.tahun_pembangunan,
                db_pendaftaran_kapal.mst_kapal.gt_kapal,
                db_pendaftaran_kapal.mst_kapal.nt_kapal,
                db_pendaftaran_kapal.mst_kapal.panjang_kapal,
                db_pendaftaran_kapal.mst_kapal.lebar_kapal,
                db_pendaftaran_kapal.mst_kapal.dalam_kapal,
                db_pendaftaran_kapal.mst_kapal.no_mesin,
                db_pendaftaran_kapal.mst_kapal.merek_mesin,
                db_pendaftaran_kapal.mst_kapal.tipe_mesin,
                db_pendaftaran_kapal.mst_kapal.daya_kapal,
                db_master.mst_jenis_kapal.nama_jenis_kapal,
                db_master.mst_bahan_kapal.nama_bahan_kapal,
                db_master.mst_alat_tangkap.nama_alat_tangkap, 
                db_master.mst_perusahaan.nama_penanggung_jawab, 
                db_master.mst_perusahaan.nama_perusahaan
            FROM db_pendaftaran_kapal.mst_kapal
            LEFT OUTER JOIN (   db_master.mst_perusahaan, 
                                db_master.mst_alat_tangkap, 
                                db_master.mst_jenis_kapal, 
                                db_master.mst_bahan_kapal
                            )
            ON (    db_pendaftaran_kapal.mst_kapal.id_perusahaan = db_master.mst_perusahaan.id_perusahaan
                    AND db_pendaftaran_kapal.mst_kapal.id_alat_tangkap = db_master.mst_alat_tangkap.id_alat_tangkap
                    AND db_pendaftaran_kapal.mst_kapal.id_jenis_kapal = db_master.mst_jenis_kapal.id_jenis_kapal
                    AND db_pendaftaran_kapal.mst_kapal.id_bahan_kapal = db_master.mst_bahan_kapal.id_bahan_kapal
                )
            WHERE mst_kapal.id_kapal = $id_kapal ";

        $run_query = $this->db_dss->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }  

    

    public function cari_by_vessel_name($search_like)
    {
        $sql = "SELECT
                    vessel.id_vessel_ccsbt, 
                    vessel.vessel_name, 
                    vessel.vessel_registration_number,
                    vessel.flag,
                    vessel.ccsbt_registration_number
                FROM 
                    mst_vessel_ccsbt vessel
                WHERE  vessel.vessel_name LIKE '%".$search_like."%' 
                AND date_authorisation_ends > NOW()
                AND vessel.aktif = 'YA' ";

                   // var_dump($sql);
        $run_query = $this->db->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function cari_by_vessel_name_in_perusahaan($search_like)
    {
        $id_perusahaan = $this->user->id_perusahaan();
        $sql = "SELECT
                    vessel.id_vessel_ccsbt, 
                    vessel.vessel_name, 
                    vessel.vessel_registration_number,
                    vessel.flag,
                    vessel.ccsbt_registration_number
                FROM 
                    mst_vessel_ccsbt vessel
                WHERE  vessel.ccsbt_registration_number LIKE '%".$search_like."%' 
                AND vessel.id_perusahaan = ".$id_perusahaan."
                AND date_authorisation_ends > NOW()
                AND vessel.aktif = 'YA' ";

                   // var_dump($sql);
        $run_query = $this->db->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

   

    public function list_opsi()
    {
        $query = "SELECT id_vessel_ccsbt AS id, vessel_name as text FROM mst_vessel_ccsbt";

        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function input($filter, $data)
    {
        switch ($filter) {
            case 'pusat':
                $this->db_kapi->insert('mst_kapal', $data);
                break;
            
            case 'daerah':
                $this->db_kapi->insert('mst_kapal_daerah', $data);
                break;
        }
    }

}