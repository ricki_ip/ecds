<!-- TAMPIL DATA -->
    <div id="panel-pilih-kapal" style="margin-left:12px; padding-bottom:15px;" >
        
        
        <div class="row" style="padding-bottom: 15px;">
            <div class="col-lg-3 text-right" style=" padding-right: 25px;">
              <strong>Set Vessel :</strong>
            </div>
            <div class="col-lg-8" style="margin-left: 5px;padding-left: 0px;padding-right: 20px;">
               <input id="start_search" name="id_vessel_ccsbt" type="text" class="bigdrop">
                <input  id="inputVessel_name" name="vessel_name" type="hidden" class="form-control">
            </div>
        </div>

          <style type="text/css">
            .list-group-item{
              padding: 2px 30px 0px 15px;
              background-color:transparent;
              border:0px;
            }
          </style>


        </div>


    
  </div>


<!-- ADDITIONAL JAVASCRIPT -->
<script>
  var search_response_time = 2000, //2 Detik
    thread = null,
    url_search_vessel = "<?php echo site_url('vessel/search_by_vessel_name'); ?>";
    // url_set_data     = "<?php echo base_url('refvessel/mst_vessel/set_data'); ?>";

  var result_counter = 0;
  var result_total = 0;

  function formatListvesselResult(vessel, container, query)
  {
    // var vessel = results.result;
    // var jumlah_result = results.jumlah_result;
    console.log(vessel);
    html = "";
    if(result_counter <1)
    {
    html += "<table class='table table-condensed table-bordered'>"
       + "<tr><th colspan='4'>Hasil Pencarian :"+query.term+" ("+result_total+" kapal).</th></tr>"
       + "<tr>"
       + "<th width='30%'>Name of Catching Vessel</th>"
       + "<th width='30%'>Registration Number</th>"
       + "<th width='30%'>Flag State/Fishing Entity</th></tr>"
       + "</table>";
    }
    html += "<table id='"+vessel.id_vessel_ccsbt+"' class='table table-condensed table-bordered'><tr>"
       + "<td width='30%'>"+vessel.vessel_name+"</td>"
       + "<td width='30%'>"+vessel.ccsbt_registration_number+"</td>"
       + "<td width='30%'>"+vessel.flag+"</td>"
       + "</tr></table>";

    result_counter++;

    return  html;
  }

  function formatListvesselSelection(vessel)
  {
    <?php echo $callback_function; ?>(vessel);
    return vessel.vessel_name;
  }

  function formatSearchingText(term)
  {
    return "Sedang mencari..";
  }

  function formatKurangText(term, minLength)
  {
    var char_yang_kurang = minLength - term.length,
      info_filter = $("#info_filter").text();
      info_gt = $("#toggle_gt").text();
      text_info = "Pencarian vessel "+info_gt+" berdasarkan <strong>"+info_filter+"</strong>. ";

      text_info += "Input minimal "+char_yang_kurang+" karakter";  
    return text_info;
  }

  function formatNotFound(term)
  { 
    return "Pencarian <strong>"+term+"</strong> tidak menemukan hasil. Kapal tidak terdaftar di CCSBT.";
  }

  var search_vessel_name = function (){
      $("#start_search").select2({
                  id: function(e) { return e.id_vessel_ccsbt },
                  allowClear: false,    
                  placeholder: "Pilih kapal..",
                  width: "50%",
                  cache: true,
                  minimumInputLength: 3,
                  dropdownCssClass: "bigdrop",
                  ajax: {
                          url: url_search_vessel,
                          dataType: "json",
                          quietMillis: 2000,
                          data: function(term, page){
                                         return {
                                                  q: term,
                                                };
                          },
                          results: function(data, page){
                                   result_counter = 0;
                                   result_total = data.jumlah_result;
                                   return {results: data.result};
                          },

                  },
                  formatResult: formatListvesselResult,
                  formatSelection: formatListvesselSelection,
                  formatSearching: formatSearchingText,
                  formatInputTooShort: formatKurangText,
                  formatNoMatches: formatNotFound
        });

  }

  function auto_width(){
      //jQuery.fn.exists = function(){return this.length>0;}
      if($("#start_search").val().length < 30  )
      {
         $("#start_search").css({"width":"100%"});
      }
      else{
         var panjang = $("#start_search").val().length;
         $("#start_search").css({"width":(panjang/3)+32+"%"}); 
      }
    }

    s_func.push(auto_width);
    s_func.push(search_vessel_name);
</script>