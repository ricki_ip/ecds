<!-- TAMPIL DATA -->
    <div id="panel-pilih-kapal2" style="margin-left:12px; padding-bottom:15px;" >
        
        
        <div class="row">
            <div class="col-sm-3 text-right" style=" padding-right: 25px;">
              <strong>Vessel Name</strong> 
            </div>
            <div class="col-sm-8" style="margin-left: 5px;padding-left: 0px;padding-right: 20px;">
               <input id="nama_kapal_baru"  type="text" name="nama_kapal_baru" class="form-control" />
               <div id="status"></div>
            </div>
        </div>
    
    </div>


<SCRIPT type="text/javascript">
  var url_search_kapal2 = "<?php echo base_url('refkapi/mst_kapal/search_by_nama'); ?>"

pic1 = new Image(16, 16); 
pic1.src = "<?php echo base_url('assets/kapi/images/loader.gif'); ?>";

$(document).ready(function(){

  $("#nama_kapal_baru").change(function() { 

    var usr = $("#nama_kapal_baru").val();

    if(usr.length >= 3)
    {
      $("#status").html('<img src="<?php echo base_url('assets/kapi/images/loader.gif'); ?>" align="absmiddle">&nbsp;Sedang Mengecek Nama Kapal...');

          $.ajax({  
            type: "GET",
            url: url_search_kapal2,  
            data: "q="+ usr,  
            success: function(msg){

            if(msg == 'OK')
            { 
              $("#nama_kapal_baru").removeClass('object_error'); // if necessary
              $("#nama_kapal_baru").addClass("object_ok");
              $("#status").html('&nbsp;<img src="<?php echo base_url('assets/kapi/images/accepted.png'); ?>" align="absmiddle"> <font color="Green"> Tersedia </font>  ');
              $("#nama_kapal_baru").removeClass("validate[required,custom[min]]"); // if necessary
              $('#next').prop("disabled", false);
            }  
            else  
            {  
              $("#nama_kapal_baru").removeClass('object_ok'); // if necessary
              $("#nama_kapal_baru").addClass("object_error");
              $("#nama_kapal_baru").addClass("validate[required,custom[min]]");
              $("#status").html(msg);
              $('#next').prop("disabled", true);
            }

          } 
         
        }); 

    }
    else
      {
        $("#status").html('<font color="red">Nama kapal minimal <strong>3</strong> huruf.</font>');
        $("#nama_kapal_baru").removeClass('object_ok'); // if necessary
        $("#nama_kapal_baru").addClass("object_error");
      }

  });
  
  $("#nama_kapal_baru").addClass("validate[required]"); // if necessary
  $("#panel-pilih-kapal2").validationEngine();

});

  function auto_width(){
      //jQuery.fn.exists = function(){return this.length>0;}
      if($("#nama_kapal_baru").val().length < 30  )
      {
         $("#nama_kapal_baru").css({"width":"32%"});
      }
      else{
         var panjang = $("#nama_kapal_baru").val().length;
         $("#nama_kapal_baru").css({"width":(panjang/3)+32+"%"}); 
      }
    }

  s_func.push(auto_width);

//-->
</SCRIPT>