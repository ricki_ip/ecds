<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_gear extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			

		}

	public function list_gear_code_json()
	{
		$this->load->model('mdl_gear');

		$list_opsi = $this->mdl_gear->list_opsi();

		echo json_encode($list_opsi);
	}

	public function list_gear_array()
	{
		$this->load->model('mdl_gear');

		$list_opsi = $this->mdl_gear->list_opsi();

		return $list_opsi;
	}
	
}
?>