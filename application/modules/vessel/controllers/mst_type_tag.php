<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_type_tag extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			

		}

	public function list_type_tag_json()
	{
		$this->load->model('mdl_type_tag');

		$list_opsi = $this->mdl_type_tag->list_opsi();

		echo json_encode($list_opsi);
	}

	public function list_type_tag_array()
	{
		$this->load->model('mdl_type_tag');

		$list_opsi = $this->mdl_type_tag->list_opsi();

		return $list_opsi;
	}
	
}
?>