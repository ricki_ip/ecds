<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_ccsbt_area extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			

		}

	public function list_ccsbt_area_json()
	{
		$this->load->model('mdl_ccsbt_area');

		$list_opsi = $this->mdl_ccsbt_area->list_opsi();

		echo json_encode($list_opsi);
	}

	public function list_ccsbt_area_array()
	{
		$this->load->model('mdl_ccsbt_area');

		$list_opsi = $this->mdl_ccsbt_area->list_opsi();

		return $list_opsi;
	}
	
}
?>