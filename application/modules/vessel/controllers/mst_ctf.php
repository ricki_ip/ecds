<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//mst_ctf ini mirip dengan Function yang dibuat arif di Pendok KAPI 


class Mst_ctf extends MX_Controller { 

	function __construct()
		{
			parent::__construct();
		}

	public function input_ctf1()
	{
		$this->load->model('mdl_ctf');

		$array_input = $this->input->post(NULL, TRUE);

		$id_ctf = $this->mdl_ctf->input($array_input);

		if($id_ctf !== false)
		{
			$url_redir = base_url('ctf/ctf/entry/ctf2/'.$id_ctf);
		}else{
			$url_redir = base_url('ctf/entry');
		}

		redirect($url_redir);
	}

	public function update($origin)
	{
		$this->load->model('mdl_ctf');

		$array_input = $this->input->post(NULL, TRUE);

		$id_ctf = $array_input['id_ctf'];
		$submit_to = $array_input['submit_to'];
		unset($array_input['id_ctf']);
		unset($array_input['submit_to']);

		$affected = $this->mdl_ctf->edit($id_ctf, $array_input);

		switch ($origin) {
			case 'ctf1':
				$url = 'ctf/ctf/entry/ctf1/'.$id_ctf;
				break;
			case 'ctf2':
				$url = 'ctf/ctf2/entry/ctf2/'.$id_ctf;
				break;
			case 'ctf3':
				$url = 'ctf/ctf/entry/ctf3/'.$id_ctf;
				break;
			default:
				if($submit_to === 'edit')
				{
					$url = 'ctf/ctf/edit/'.$id_ctf;
				}elseif($submit_to === 'detail')
				{
					$url = 'ctf/preview/index/'.$id_ctf;
				}

				break;
		}
			$url_redir = base_url($url);
	

		redirect($url_redir);
	}

}