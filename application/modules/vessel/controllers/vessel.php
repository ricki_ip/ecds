<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vessel extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			
			$this->load->model('mdl_vessel');
		}

	public function index()
	{
		$this->kapal();

	}

	

	public function detail_kapal()
	{
		$get_id_kapal = $this->input->get('ik', FALSE);
		$detail_result = $this->mdl_vessel->detil_kapal($get_id_kapal);

		echo json_encode($detail_result);

	}

	

	public function search_by_vessel_name()
	{
		$get_search_like = $this->input->get('q', FALSE);
		$search_result = $this->mdl_vessel->cari_by_vessel_name($get_search_like);
		$jml_search_result = count($search_result);
		if(!$search_result){
			$jml_search_result = 0;
		}
		$json = array( 
					'search_like' => $get_search_like,
					'jumlah_result' => $jml_search_result,
					'result' => $search_result
					);
		
		echo json_encode($json);
	}

	public function search_vessel_of_perusahaan()
	{
		$get_search_like = $this->input->get('q', FALSE);
		$search_result = $this->mdl_vessel->cari_by_vessel_name_in_perusahaan($get_search_like);
		$jml_search_result = count($search_result);
		if(!$search_result){
			$jml_search_result = 0;
		}
		$json = array( 
					'search_like' => $get_search_like,
					'jumlah_result' => $jml_search_result,
					'result' => $search_result
					);
		
		echo json_encode($json);
	}


	public function pilih_vessel($callback_function = 'set_data_vessel')
	{
		$data['callback_function'] = $callback_function;
		$this->load->view('pilih_vessel',$data);
	}

	public function pilih_vessel_perusahaan($callback_function = 'set_data_vessel')
	{
		$data['callback_function'] = $callback_function;
		$this->load->view('pilih_vessel_perusahaan',$data);
	}

	public function pilih_nama_kapal()
	{
		$this->load->view('pilih_kapal_by_name');
	}

	public function set_data()
	{
		//$data = $this->input->post('data');
		//$data = json_decode($data);
		//$array 		= $this->input->get('array');
		$data['data_pendok']	=	json_decode($this->input->get('array'));
		
		$data['history']	= $this->mdl_vessel->history($data['data_pendok']->id_kapal, $data['data_pendok']->id_bkp);
		//echo Modules::run('templates/page/v_form', 'refkapi', 'history_perubahan_pendok', '', '', '', $data);
		$this->load->view('history_perubahan_pendok',$data);
	}

	public function list_vessel_json()
	{
		$this->load->model('mdl_vessel');

		$list_opsi = $this->mdl_vessel->list_opsi();

		echo json_encode($list_opsi);
	}

	public function list_vessel_array()
	{
		$this->load->model('mdl_vessel');

		$list_opsi = $this->mdl_vessel->list_opsi();

		return $list_opsi;
	}

}