<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_vessel_type extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			

		}

	public function list_vessel_type_json()
	{
		$this->load->model('mdl_vessel_type');

		$list_opsi = $this->mdl_vessel_type->list_opsi();

		echo json_encode($list_opsi);
	}

	public function list_vessel_type_array()
	{
		$this->load->model('mdl_vessel_type');

		$list_opsi = $this->mdl_vessel_type->list_opsi();

		return $list_opsi;
	}
	
}
?>