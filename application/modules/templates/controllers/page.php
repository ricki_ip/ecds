<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	private $total_captured_weight = 0;
	private $percent_captured = 0;
	private $total_captured_quota = 0;
	private $treshold_quota = 0;
	private $selisih_quota = 0;
	private $tahun_quota = 0;

	private $is_off_limit = FALSE;
	private $is_close_limit = FALSE;

	function __construct()
	{
			parent::__construct();
			$this->load->config('menus');
			$this->load->config('labels');
			$this->load->config('globals');

			$this->load->model('preview/mdl_ctf_preview');
			$get_limit_quota = $this->mdl_ctf_preview->limit_quota();
			$this->total_captured_weight = $this->mdl_ctf_preview->total_weight();
			$this->treshold_quota = $get_limit_quota->treshold;
			$this->tahun_quota = $get_limit_quota->tahun;
			$this->total_captured_quota = $get_limit_quota->quota;

			$this->selisih_quota = $this->total_captured_quota - $this->total_captured_weight;
			$this->percent_captured = ($this->total_captured_weight/$this->total_captured_quota) * 100;
			if( ($this->percent_captured/100) > $this->treshold_quota)
			{
				$this->is_close_limit = TRUE;
			}

			if($this->selisih_quota < 10)
			{
				$this->is_off_limit = TRUE;
			}
	}

	/** @v_form
	 * @nama_module nama folder module nya  
	 * @file_form nama file views nya
	 * @item_label index label yang akan digunakan di labels.php
	 * @add_js array berisi nama file js yang dipanggil khusus
	 * @add_cs array berisi nama file css yang dipanggil khusus
	 * @container_data array data hasil olahan di controller asal
	 */
	public function v_form($nama_module, $file_form, $item_label, $add_js = '', $add_css = '', $container_data)
	{	
		if($this->is_off_limit)
		{
			$this->limit_page();
			die;
		}
		if( empty($item_label) )
		{
			$form_constants['title']['page'] = '';
			$form_constants['title']['panel'] = '';
			$form_constants['form'] = '';
		}else{
			$form_constants = $this->config->item($item_label);
		}
		// vdump($this->user->id_grup_pengguna());
		if ( $this->user->level() < 3 ){
			$entry_menus = 'admin_lists';
			$setting_menus = 'admin_settings';
		}else{
			$entry_menus = 'perusahaan_lists';
			$setting_menus = 'perusahaan_settings';
		}

		$menus_constants = $this->config->item($entry_menus);
		$settings_menus_constants = $this->config->item($setting_menus);

		$global_constants = $this->config->item('app');
		$assets_paths = $this->config->item('assets_paths');

		
		$head_data['page_title'] = $form_constants['title']['page'];
		$head_data['paths'] = $assets_paths;
		$head_data['additional_js'] = empty($add_js) ? FALSE : $add_js;
		$head_data['additional_css'] = empty($add_css) ? FALSE : $add_css;
		
		$app_data['app_title'] = $global_constants['header_title'];
		$app_data['app_footer'] = $global_constants['footer_text'];
		$app_data['logo_url'] = $assets_paths['kapi_images'];
		$app_data['total_captured_weight'] = $this->total_captured_weight;
		$app_data['total_captured_quota'] = $this->total_captured_quota;
		$app_data['selisih_quota'] = $this->selisih_quota;
		$app_data['tahun_quota'] = $this->tahun_quota;
		$app_data['treshold_quota'] = $this->treshold_quota;
		$app_data['percent_captured'] = $this->percent_captured;
		$app_data['is_close_limit'] = $this->is_close_limit;

		$menu_lists['menus'] = $menus_constants;
		$menu_lists['admin_menus'] = $settings_menus_constants;

		$container_data['nama_module'] = $nama_module;
		$container_data['file_form'] = $file_form;
		$container_data['panel_title'] = $form_constants['title']['panel'];
		$container_data['form'] = $form_constants['form'];
		$container_data['constants'] = $form_constants;

		
		$this->load->view('html_head', $head_data);
		$this->load->view('div_header', $app_data);
		$this->load->view('div_menu', $menu_lists);
		$this->load->view('div_container', $container_data);
		$this->load->view('html_footer', $app_data);
	}

	public function v_view($nama_module, $file_view, $file_form, $add_js, $add_css, $container_data)
	{
		if($this->is_off_limit)
		{
			$this->limit_page();
			die;
		}

		if( empty($item_label) )
		{
			$form_constants['title']['page'] = '';
			$form_constants['title']['panel'] = '';
			$form_constants['form'] = '';
		}else{
			$form_constants = $this->config->item($item_label);
		}

		
		if ( $this->user->level() < 3 ){
			$menus_constants = $this->config->item('admin_lists');
			$settings_menus_constants = $this->config->item('admin_settings');
		}else{
			$menus_constants = $this->config->item('perusahaan_lists');
			$settings_menus_constants = $this->config->item('perusahaan_settings');
		}

		// $menus_constants = $this->config->item('admin_lists');
		// $settings_menus_constants = $this->config->item('admin_settings');

		$global_constants = $this->config->item('app');
		$assets_paths = $this->config->item('assets_paths');


		$head_data['page_title'] = $form_constants['title']['page'];
		$head_data['paths'] = $assets_paths;
		$head_data['additional_js'] = empty($add_js) ? FALSE : $add_js;
		$head_data['additional_css'] = empty($add_css) ? FALSE : $add_css;
		
		$app_data['app_title'] = $global_constants['header_title'];
		$app_data['app_footer'] = $global_constants['footer_text'];
		$app_data['logo_url'] = $assets_paths['kapi_images'];
		$app_data['total_captured_weight'] = $this->total_captured_weight;
		$app_data['total_captured_quota'] = $this->total_captured_quota;
		$app_data['selisih_quota'] = $this->selisih_quota;
		$app_data['tahun_quota'] = $this->tahun_quota;
		$app_data['treshold_quota'] = $this->treshold_quota;
		$app_data['percent_captured'] = $this->percent_captured;
		$app_data['is_close_limit'] = $this->is_close_limit;
		
		$container_data['nama_module'] = $nama_module;
		$container_data['file_view'] = $file_view;
		$container_data['file_form'] = $file_form;

		// $this->load->view($nama_module.'/'.$file_view, $container_data);
// =======
		$this->load->view('login_head', $head_data);
		// $this->load->view('div_header', $app_data);
		$this->load->view('div_login', $container_data);
		$this->load->view('html_footer', $app_data);
	}

	public function v_login($nama_module, $file_view, $file_form, $add_js, $add_css, $container_data)
	{

		if( empty($item_label) )
		{
			$form_constants['title']['page'] = '';
			$form_constants['title']['panel'] = '';
			$form_constants['form'] = '';
		}else{
			$form_constants = $this->config->item($item_label);
		}

		
		$menus_constants = $this->config->item('lists');
		$settings_menus_constants = $this->config->item('admin_lists');

		$global_constants = $this->config->item('app');
		$assets_paths = $this->config->item('assets_paths');


		$head_data['page_title'] = $form_constants['title']['page'];
		$head_data['paths'] = $assets_paths;
		$head_data['additional_js'] = empty($add_js) ? FALSE : $add_js;
		$head_data['additional_css'] = empty($add_css) ? FALSE : $add_css;
		
		$app_data['app_title'] = $global_constants['header_title'];
		$app_data['app_footer'] = $global_constants['footer_text'];
		$app_data['logo_url'] = $assets_paths['kapi_images'];
		$app_data['total_captured_weight'] = $this->total_captured_weight;
		$app_data['total_captured_quota'] = $this->total_captured_quota;
		$app_data['selisih_quota'] = $this->selisih_quota;
		$app_data['tahun_quota'] = $this->tahun_quota;
		$app_data['treshold_quota'] = $this->treshold_quota;
		$app_data['percent_captured'] = $this->percent_captured;
		$app_data['is_close_limit'] = $this->is_close_limit;
		// vdump($container_data);
		$container_data['nama_module'] = $nama_module;
		$container_data['file_view'] = $file_view;
		$container_data['file_form'] = $file_form;

		// $this->load->view($nama_module.'/'.$file_view, $container_data);
// =======
		$this->load->view('login_head', $head_data);
		// $this->load->view('div_header', $app_data);
		$this->load->view('div_login', $container_data);
		$this->load->view('html_footer', $app_data);
	}

	public function limit_page()
	{
		if( empty($item_label) )
		{
			$form_constants['title']['page'] = '';
			$form_constants['title']['panel'] = '';
			$form_constants['form'] = '';
		}else{
			$form_constants = $this->config->item($item_label);
		}

		
		$menus_constants = $this->config->item('lists');
		$settings_menus_constants = $this->config->item('admin_lists');

		$global_constants = $this->config->item('app');
		$assets_paths = $this->config->item('assets_paths');


		$head_data['page_title'] = $form_constants['title']['page'];
		$head_data['paths'] = $assets_paths;
		$head_data['additional_js'] = empty($add_js) ? FALSE : $add_js;
		$head_data['additional_css'] = empty($add_css) ? FALSE : $add_css;
		
		$app_data['app_title'] = $global_constants['header_title'];
		$app_data['app_footer'] = $global_constants['footer_text'];
		$app_data['logo_url'] = $assets_paths['kapi_images'];
		$app_data['total_captured_weight'] = $this->total_captured_weight;
		$app_data['total_captured_quota'] = $this->total_captured_quota;
		$app_data['selisih_quota'] = $this->selisih_quota;
		$app_data['tahun_quota'] = $this->tahun_quota;
		$app_data['treshold_quota'] = $this->treshold_quota;
		$app_data['percent_captured'] = $this->percent_captured;
		$app_data['is_close_limit'] = $this->is_close_limit;
		// vdump($container_data);

		// $this->load->view($nama_module.'/'.$file_view, $container_data);
// =======
		$this->load->view('login_head', $head_data);
		// $this->load->view('div_header', $app_data);
		$this->load->view('div_limit_page', $app_data);
		$this->load->view('html_footer', $app_data);	
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */