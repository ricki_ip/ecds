<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Globaljs extends MX_Controller {

	
	function __construct()
	{
			parent::__construct();
			$this->load->config('menus');
			$this->load->config('globals');
	}

	
	public function index()
	{	
		$this->output->set_content_type('application/javascript');
		$this->load->view('global_js');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */