<!-- mulai #ecdscontainer --> 
<div id="ecdscontainer" class="container">
              <div class="panel panel-danger">
                  <div class="panel-heading">
                    <h3 class="panel-title text-center">Kuota Sudah Penuh</h3>
                  </div>
                  <div class="panel-body text-center">
                  <p>
                    Tangkapan SBT sudah mencapai <?php echo angka($total_captured_weight,2); ?> Kg.
                  </p>
                  <p>
                    Quota tahun ini adalah <?php echo angka($total_captured_quota,2); ?> Kg.
                  </p>
                  <p>
                    Entry CDS sudah tidak bisa dilakukan lagi di sistem ini.
                  </p>
                  </div>
              </div>
</div><!-- akhir #ecdscontainer --> 