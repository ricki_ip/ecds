<!DOCTYPE html> 
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">

    <title><?php echo $page_title?></title>

    <!-- Third party core CSS -->
    <?php if ( $this->user->is_perusahaan() ): ?>        
    <link href="<?php echo $paths['main_css'];?>/bootstrap/bootstrap.sandstone.min.css" rel="stylesheet">
    <?php else: ?>
    <link href="<?php echo $paths['main_css'];?>/bootstrap/bootstrap.simplex.min.css" rel="stylesheet">
    <?php endif ?>
    <link href="<?php echo $paths['misc_css'];?>/flick/jquery-ui-1.10.3.custom.min.css" rel="stylesheet">
    <link href="<?php echo $paths['misc_css'];?>/jquery.pnotify.default.css" rel="stylesheet">
    <link href="<?php echo $paths['misc_css'];?>/sticky-footer-navbar.css" rel="stylesheet">
    <link href="<?php echo $paths['misc_css'];?>/bootstrap-modal-bs3patch.css" rel="stylesheet">
    <link href="<?php echo $paths['misc_css'];?>/bootstrap-modal.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo $paths['main_css'];?>/style.css" rel="stylesheet">
    
    <!-- Additional css -->
    <?php if($additional_css !== FALSE)
          foreach ($additional_css as $file_css): ?>
           <link href="<?php echo $paths['misc_css'];?>/<?php echo $file_css;?>" rel="stylesheet">      
    <?php endforeach ?>

		
    <!-- Third Party js for this template-->
    <script src="<?php echo $paths['misc_js'];?>/jquery-1.10.2.min.js"></script>
    <script src="<?php echo $paths['misc_js'];?>/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="<?php echo $paths['misc_js'];?>/bootstrap.min.js"></script>
    <script src="<?php echo $paths['misc_js'];?>/jquery.pnotify.min.js"></script>
    <script src="<?php echo $paths['misc_js'];?>/bootstrap-modal.js"></script>
    <script src="<?php echo $paths['misc_js'];?>/bootstrap-modalmanager.js"></script>
    <script src="<?php echo $paths['misc_js'];?>/jquery.jkey.min.js"></script>

    <!-- Additional js -->
    <?php if($additional_js !== FALSE)
          foreach ($additional_js as $file_js): ?>
            <script type="text/javascript" src="<?php echo $paths['misc_js'];?>/<?php echo $file_js;?>"></script>        
    <?php endforeach ?>

    <!-- Global js -->
            <script type="text/javascript" src="<?php echo site_url('templates/globaljs'); ?>"></script>
            <script type="text/javascript">
            var user_data = <?php echo json_encode( $this->user->all_userdata() ); ?>;
                user_data.is_artisenal = <?php echo kos($this->user->is_artisenal(), '0') === '0' ? 'false' : 'true' ?>;
            </script>
</head>

<body>
<!-- END FILE html_head.php -->