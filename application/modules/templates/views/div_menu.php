<!-- mulai #kapinav -->
<div id="kapinav">

	<div class="navbar navbar-inverse navbar-static-top">
		<div class="container">
			<div class="navbar-header">
          	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            	<span class="icon-bar"></span>
            	<span class="icon-bar"></span>
            	<span class="icon-bar"></span>
          	</button>
          	<a class="navbar-brand" href="<?php echo site_url(); ?>" title="Aplikasi Pemantauan Dan Evaluasi Sarana Penangkapan Ikan">SDI</a>
        </div>
        <div class="navbar-collapse">
          <ul class="nav navbar-nav">
          	<?php foreach ($menus as $index => $values): ?>
          		<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php 
						echo $index;
						if(is_array($values))
						{
							echo "<b class='caret'></b>";
						}
					 ?> 
					</a>
					<?php if( is_array($values) )
					{
					?>
						<ul class="dropdown-menu">
							<?php
							    $jum_dropdown = count($values);
							    $dropdown_counter = 1;
								foreach ($values as $index_2 => $link): ?>
								
									<li><a href="<?php echo site_url($link) ?>"><?php echo $index_2; ?></a></li>
									
							<?php 
							if($dropdown_counter < $jum_dropdown)
							{
								echo "<li class='divider'></li>";
								$dropdown_counter++;
							}
								endforeach ?>
					<?php
					}
					?>
						</ul>	
					
				</li>
          	<?php endforeach ?>
          </ul>
    <?php $is_admin = TRUE; if ($is_admin): ?>
      <ul class="nav navbar-nav navbar-right">
        <?php foreach ($admin_menus as $index => $values): ?>
              <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <?php 
                echo $index;
                if(is_array($values))
                {
                  echo "<b class='caret'></b>";
                }
               ?> 
              </a>
              <?php if( is_array($values) ):?>
                <ul class="dropdown-menu">
                  <?php
                      $jum_dropdown = count($values);
                      $dropdown_counter = 1;
                    foreach ($values as $index_2 => $link): ?>
                    
                      <li><a href="<?php echo site_url($link) ?>"><?php echo $index_2; ?></a></li>
                      
                  <?php 
                  if($dropdown_counter < $jum_dropdown)
                  {
                    echo "<li class='divider'></li>";
                    $dropdown_counter++;
                  }
                    endforeach ?>
                  </ul>
              <?php endif ?>
        <?php endforeach ?>
        <li>
          <a href="<?php echo site_url('login/out'); ?>">Sign out</a>
        </li>
      </ul>
    <?php endif ?>
		
		</div><!--/.nav-collapse -->
      </div>
    </div>
</div>
<!-- akhir #kapinav -->
