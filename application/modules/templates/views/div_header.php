<!-- mulai #kapiheader --> 
<div id="kapiheader">  
	<div class="container">
		<div class="row">
			<div class="col-md-6">
  				<h1 id="kapilogo"><a href="index.php"><img src="<?php echo $logo_url;?>/logo.png" class="img-responsive" alt="SDI | Aplikasi E - Catch Document Schema"></a></h1>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<p style="text-align: right; font-size: 0.9em;"
									title="Remaining: <?php echo angka($selisih_quota,0); ?> (Kg)">
							National Quota <?php echo $tahun_quota; ?> :
								<em>
								  <?php echo angka($total_captured_quota,0); ?>								  
								 </em> Kg.  
							<br>
							National Catch <?php echo $tahun_quota; ?> : <?php echo angka($total_captured_weight,0); ?> (<?php echo angka($percent_captured,2); ?> %)
							
							<?php if ($is_close_limit): ?>
								<div class="panel panel-danger">
									<div class="panel-body">
									   Warning! Quota is above our threshold ( <?php echo $treshold_quota*100 ?>% ) . (Used <?php echo angka($percent_captured,0); ?> %)
									</div>
								</div>
							<?php endif ?>
 						</p>
					</div>
					<hr>
					<div class="col-md-12">
						 <h5 class="text-right" title="<?php echo $this->user->id_pengguna(); ?>">
						 Welcome, <?php echo $this->user->nama_pengguna(); ?>. <br>
				          (<?php if ( $this->user->is_perusahaan() ): ?>
				            <?php echo kos($this->user->company_name(),'-'); ?> | <?php echo kos($this->user->nama_asosiasi(),'-'); ?>
				          <?php else: ?>
				            <?php echo $this->user->nama_lokasi(); ?>
				          <?php endif ?>)
				        </h5>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- akhir #kapiheader --> 