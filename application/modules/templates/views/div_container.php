<!-- mulai #ecdscontainer --> 
<div id="ecdscontainer" class="container">
  <div id="header_row" class="row">
      <div class="col-lg-12">
          <h3 class="text-center"><?php echo $panel_title; ?></h3>
      </div>
  </div>
  <hr>
	<div id="main_row" class="row">
		<div id="ecdscolumn" class="col-lg-12"><!-- buka #ecdscolumn -->
        			<?php echo $this->load->view($nama_module.'/'.$file_form); ?>
      </div><!-- tutup #ecdscolumn -->
    </div> <!-- akhir #main_row -->
</div><!-- akhir #ecdscontainer --> 

