<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Scrapper extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	private $link_auth_vessel = 'http://www.ccsbt.org/site/authorised_vessels.php?task=search&vesselClass=0&day=dd&month=mm&year=yyyy&searchType_0=0&searchField_0=&searchType_1=0&searchField_1=&searchType_2=0&searchField_2=&searchType_3=0&searchField_3=&authStateEntity%5B%5D=0&vesselFlag%5B%5D=2&vesselType%5B%5D=0&sby=3&order=0&submit=';
	private $link_detail_vessel = 'http://www.ccsbt.org/site/authorised_vessels_detail.php?id=';
	function __construct()
	{
			parent::__construct();
			
			// $this->load->config('menus');
			// $this->load->config('db_timeline');
			// $this->load->config('globals');
			$this->load->library('simple_html_dom');
	}
	
	public function index()
	{
		$today_link = $this->_generate_url();
		$html = file_get_html($today_link);

		// echo "Getting vessel list. <br>"; 
		$table_vessel = $html->find('table.authorised_list tr');
		// var_dump($table_vessel);
		$array_vessels = array();

		foreach ($table_vessel as $tr) {
			  $tr_link = $tr->find('td a',0);
				if(!empty($tr_link))
				{
					$tmp_link = $tr->find('td a',0)->href;
					$link_split = explode('?id=', $tmp_link);
					$id_vessel = $link_split[1];
					$tmp_array = array(
														 'id_vessel_ccsbt' => $id_vessel,
														 'vessel_type' => ''
														 );
					array_push($array_vessels, $tmp_array);
				}
		}
		// echo "Vessel list retrieved. <br>"; 

		// var_dump($array_vessels);
		// echo "Getting each details. <br>"; 

		$limit = count($array_vessels);
		// $limit = 10;
		$counter = 0;
		$searches = array(' ','(',')','/');
		$replacements = array('_','','','_');
		foreach ($array_vessels as $vessel) {
			$tmp_link_detail = $this->link_detail_vessel.$vessel['id_vessel_ccsbt'];
			// echo "Details : ".$tmp_link_detail.". <br>"; 
			$html_vessel = file_get_html($tmp_link_detail);
			$tmp_array_detail = array();
			$table_detail = $html_vessel->find('body > div.outer > div > div.main_body > table > tbody > tr:nth-child(2) > td > div.l2_content > table > tbody > tr ');
			// var_dump($table_detail);
			// die;
			foreach ($table_detail as $tr_detail) {
				$key = $tr_detail->find('td',0)->plaintext;
				if(!empty($key))
				{
					$lower_key = strtolower($key);
					$new_key = str_replace($searches, $replacements, $lower_key);
					$value = $tr_detail->find('td',-1)->plaintext;
					if($new_key === 'date_authorisation_starts' || $new_key === 'date_authorisation_ends')
					{
						$tmp_date = datetime::createfromformat('d F Y', $value);
						$new_value = $tmp_date->format('Y-m-d');
						$array_vessels[$counter][$new_key] = $new_value;
					}elseif($new_key === 'vessel_type' && $array_vessels[$counter]['vessel_type'] !== ''){
						$array_vessels[$counter]['vessel_type_2'] = $value;
					}else{
						$array_vessels[$counter][$new_key] = $value;
					}
					
				}

			}

			$array_vessels[$counter]['tanggal_buat'] = date('Y-m-d H:i:s');
			$array_vessels[$counter]['id_vessel_ccsbt_web'] = $array_vessels[$counter]['id_vessel_ccsbt'];
			$array_vessels[$counter]['id_vessel_ccsbt'] = $array_vessels[$counter]['ccsbt_registration_number'];

			// echo "INSERT : ".$array_vessels[$counter]['id_vessel_ccsbt'].". <br>"; 	
			$this->_insert_vessel($array_vessels[$counter]);

			if($counter < $limit)
			{
				$counter++;
			}else{
				break;
			}
		}

		$uri = site_url('kapal');
		redirect($uri);
	}

	public function _insert_vessel($data)
	{
		$this->load->model('mdl_scrapper');
		$this->mdl_scrapper->insert_vessel($data);

	}

	public function _generate_url()
	{
			$temp_link = $this->link_auth_vessel;

			$ddate = date('d');
			$mmonth = date('n');
			$yyear = date('Y');

			$date_template = array('dd','mm','yyyy');
			$date_today = array($ddate, $mmonth, $yyear);

			$today_link = str_replace($date_template, $date_today, $temp_link);

			return $today_link;
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */