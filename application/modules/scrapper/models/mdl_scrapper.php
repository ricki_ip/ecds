<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_scrapper extends CI_Model
{
	// private $db_dss;
	// private $db_kapi;

    function __construct()
    {
        // $this->db_dss = $this->load->database('db_dss', TRUE);
        // $this->db_kapi = $this->load->database('default', TRUE);

    }

	public function insert_vessel($data)
    {
        $fields = array();
        $values = array();
        $updates = array();

        foreach ($data as $key => $value) {
            if($key !== 'id_vessel_ccsbt')
            {
                $tmp_update = $key."='".$value."'";
                array_push($updates, $tmp_update);
            }
            array_push($fields, $key);
            array_push($values, "'".$value."'");

        }

        $string_fields = implode(',', $fields);
        $string_values = implode(',', $values);
        $string_updates = implode(',', $updates);

        $sql = "INSERT INTO mst_vessel_ccsbt (".$string_fields.") VALUES (".$string_values.")
                ON DUPLICATE KEY UPDATE ".$string_updates."";

        $this->db->query($sql);
    	// $query_insert =  $this->db->insert('trs_vessel',$data);
    }

    public function last_update()
    {
    	$query = "SELECT COUNT(*) AS last_count , MAX(tanggal_buat) last_updated 
                    FROM mst_vessel_ccsbt
                    WHERE date_authorisation_ends > NOW()";

    	$run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }
}