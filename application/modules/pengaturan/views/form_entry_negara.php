<?php echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');?>
<?php
// var_dump($detail_pengguna);
?>

<div class="row">
  <div class="col-lg-12">      
  <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Form Entry Negara:</h3>
        </div>
        <div class="panel-body">

          <?php if ($detail_negara !== FALSE): ?>
          <input type="hidden" name="id_negara" value="<?php echo kos($id_negara) ?>">          
          <?php endif ?>

          <?php 
         
          $attr_nama_negara = array( 'name' => $form['nama_negara']['name'],
                                      'label' => $form['nama_negara']['label'],
                                      
                                      'value' => kos($detail_negara['nama_negara'])
                    );
          echo $this->mkform->input_text($attr_nama_negara);
          
          ?>
        </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
   <button type="SUBMIT" class="btn btn-large btn-block btn-success">SUBMIT DATA</button>
  </div>     
</div>      
<?php echo form_close(); ?>