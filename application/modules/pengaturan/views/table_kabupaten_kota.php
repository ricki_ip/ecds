<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_daftar_kabupaten_kota' class='table table-hover table-bordered'>");
	$this->table->set_template($template);
	$this->table->set_heading($constants['th_table']);
	$counter = 1;
	if($list_kabupaten_kota){
		// var_dump($list_kabupaten_kota);
		// die;
		foreach ($list_kabupaten_kota as $item) {
			// $link_edit = '<a class="btn btn-warning" href="'.base_url('kapal/kapal/edit/'.$item->id_kapal).'">Edit</a>';
			// $link_delete = '<a class="btn btn-danger" href="'.base_url('kapal/kapal/delete/'.$item->id_kapal).'">Hapus</a>';
			$this->table->add_row(
								$counter.'.',
								'<a href="'.site_url('pengaturan/mst_kabupaten_kota/view/'.$item->id_kabupaten_kota).'">'.$item->nama_kabupaten_kota.'</a>',
								$item->aktif
								
								);
			$counter++;

		}
	}

	$table_list_kabupaten_kota = $this->table->generate();
?>

<!-- TAMPIL DATA -->
		<?php
			echo $table_list_kabupaten_kota;
		?>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		$('#table_daftar_kabupaten_kota').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": false,
	        "bInfo": false,
	        "bPaginate": true,
"sPaginationType":"full_numbers",
"iDisplayLength": 25,
	        "bSort": true
		} );
	} );
</script>