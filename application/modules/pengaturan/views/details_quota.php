<?php echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');?>
<?php
// vdump($detail_quota, true);
?>

<div class="row">
  <div class="col-lg-12">      
  <div class="panel panel-default">
        <div class="panel-heading">
          <h2 class="panel-title">Quota Tahun <?php echo $detail_quota['tahun']; ?> :</h2>
        </div>
        <div class="panel-body">

          <?php 
         
          $attr_nama_quota = array( 'name' => 'quota_value',
                                      'label' => 'Quota (Kg)',
                                      'value' => kos($detail_quota['quota'], '0')
                    );
          echo $this->mkform->input_text($attr_nama_quota);
          
          ?>
          <div class="form-group mkform-text"><!-- mulai form text treshold -->
            <label for="quota_value" class="col-sm-3 control-label">Treshold (%)</label>
              <div class="col-sm-8">
                   <input type="number" name="treshold" id="inputTreshold" 
                        class="form-control" value="<?php echo kos($detail_quota['treshold']) *100; ?>" min="0" max="100" step="1" required="required" title="">
              </div>
            </label>
          </div>
         
        </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
   <button type="SUBMIT" class="btn btn-large btn-block btn-success">SUBMIT DATA</button>
  </div>     
</div>      
<?php echo form_close(); ?>