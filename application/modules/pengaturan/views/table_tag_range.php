<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_tag_range' class='table table-hover table-bordered'>");
	$this->table->set_template($template);
	$this->table->set_heading('No.', 'Min', 'Max','Tahun','Aksi');
	$counter = 1;
	if($list_tag_range){
		// var_dump($list_tag_range);
		// die;
		foreach ($list_tag_range as $item) {
			// $link_edit = '<a class="btn btn-warning" href="'.base_url('kapal/kapal/edit/'.$item->id_kapal).'">Edit</a>';
			// $link_delete = '<a class="btn btn-danger" href="'.base_url('kapal/kapal/delete/'.$item->id_kapal).'">Hapus</a>';
			$this->table->add_row(
								$counter.'.',
								'<strong>'.$item->min.'</strong>',
								'<strong>'.$item->max.'</strong>',
								'<strong>'.$item->tahun.'</strong>',
								'<a href="'.site_url('pengaturan/tag_range/tahun/'.$item->tahun).'" class="btn btn-xs btn-info"> EDIT </a>'
								);
			$counter++;

		}
	}

	$table_list_tag_range = $this->table->generate();
?>

<!-- TAMPIL DATA -->
<!-- <div class="row">
	<div class="col-lg-12">
		<div class="text-right">
			<a href="<?php echo site_url('pengaturan/tag_range/entry'); ?>" class="btn btn-primary">Tambah Data Quota Per Tahun</a>		
		</div>
	</div>
</div> -->
<p></p>
<div class="row">
	<div class="col-lg-12">
		<?php
			echo $table_list_tag_range;
		?>
	</div>
</div>
		

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		$('#table_tag_range').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": false,
	        "bInfo": false,
	        "bPaginate": true,
"sPaginationType":"full_numbers",
"iDisplayLength": 25,
	        "bSort": true
		} );
	} );
</script>