<div class="row">
  <div class="col-lg-2 col-lg-offset-8">
    <p>
        <?php 
        $link_aksi = '<a class="btn btn-primary btn-lg btn-block" role="button" href="'.base_url('pengaturan/mst_pejabat/edit/'.$detail_pejabat['id_pejabat']).'">'.'Edit'.'</a>';
        echo $link_aksi; 
        ?>
    </p>
  </div>
  <div class="col-lg-2">
  <p>
    <?php if ($detail_pejabat['aktif'] === 'Ya'): ?>
        <?php 
        $link_aksi = '<a class="btn btn-danger btn-lg btn-block" role="button" href="'.base_url('pengaturan/mst_pejabat/delete/'.$detail_pejabat['id_pejabat']).'">'.'Delete'.'</a>';
        echo $link_aksi; 
        ?>
    <?php else: ?>
        <?php 
        $link_aksi = '<a class="btn btn-success btn-lg btn-block" role="button" href="'.base_url('pengaturan/mst_pejabat/activate/'.$detail_pejabat['id_pejabat']).'">'.'Aktifkan'.'</a>';
        echo $link_aksi; 
        ?>
    <?php endif ?>
  </p>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
          <?php
          
          $hidden_input = array('id_pejabat' => $detail_pejabat['id_pejabat'],
                                'submit_to' => 'edit');
          echo form_open('id="form_entry" class="form-horizontal" role="form"', $hidden_input);

          ?>
         <?php 
          $attr_nama_pejabat = array( 'name' => $form['nama_pejabat']['name'],
                                        'label' => $form['nama_pejabat']['label'],
                                        'value' => $detail_pejabat['nama_pejabat']
                    );
          echo $this->mkform->input_text($attr_nama_pejabat);

          $attr_title = array( 'name' => $form['title']['name'],
                                        'label' => $form['title']['label'],
                                        'value' => $detail_pejabat['title']
                    );
          echo $this->mkform->input_text($attr_title);

          $attr_id_lokasi = array( 'name' => $form['id_lokasi']['name'],
                                        'label' => $form['id_lokasi']['label'],
                                        'value' => $detail_pejabat['nama_lokasi']
                    );
          echo $this->mkform->input_text($attr_id_lokasi);

          
         ?>
  </div>
</div>
</form>

<script>
  
  var init_data_tables = function(){
     $('#table_ctf_tag').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "2.5  %" , "sClass": "text-center"},
                        { "sWidth": "22.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                      ],
        "bFilter": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": false
      });
     
      $("input").prop("disabled", true);
      $("select").prop("disabled", true);
      $("textarea").prop("disabled", true);
    }

  
  s_func.push(init_data_tables);
</script>