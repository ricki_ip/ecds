<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_daftar_pengguna' class='table table-hover table-bordered'>");
	$this->table->set_template($template);
	$this->table->set_heading($constants['th_table']);
	$counter = 1;
	if($list_pengguna){
		foreach ($list_pengguna as $item) {
			// $link_edit = '<a class="btn btn-warning" href="'.base_url('kapal/kapal/edit/'.$item->id_kapal).'">Edit</a>';
			// $link_delete = '<a class="btn btn-danger" href="'.base_url('kapal/kapal/delete/'.$item->id_kapal).'">Hapus</a>';
			$this->table->add_row(
								$counter.'.',
								'<a href="'.site_url('pengaturan/mst_pengguna/view/'.$item->id_pengguna).'">'.$item->nama_pengguna.'</a>',
								$item->nama_grup_pengguna,
								$item->nama_lokasi,
								// $item->last_login,
								tgl($item->last_login,'l, d/m/Y (H:i)','Belum Login'),
								$item->aktif
								);
			$counter++;

		}
	}

	$table_list_pengguna = $this->table->generate();
?>

<!-- TAMPIL DATA -->
<div class="row">
	<div class="col-lg-12">
		<div class="text-right">
			<a href="<?php echo site_url('pengaturan/mst_pengguna/entry'); ?>" class="btn btn-primary">Daftarkan Pengguna</a>		
		</div>
	</div>
</div>
<p></p>
<div class="row">
	<div class="col-lg-12">
		<?php
			echo $table_list_pengguna;
		?>
	</div>
</div>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		$('#table_daftar_pengguna').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": false,
	        "bInfo": false,
	        "bPaginate": false,
	        "bSort": true
		} );
	} );
</script>