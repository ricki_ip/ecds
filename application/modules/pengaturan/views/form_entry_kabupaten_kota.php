<?php echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');?>
<?php
// var_dump($detail_pengguna);
?>

<div class="row">
  <div class="col-lg-12">      
  <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Form Entry Kabupaten / Kota:</h3>
        </div>
        <div class="panel-body">

          <?php if ($detail_kabupaten_kota !== FALSE): ?>
          <input type="hidden" name="id_kabupaten_kota" value="<?php echo kos($id_kabupaten_kota) ?>">          
          <?php endif ?>

          <?php 
         
          $attr_nama_kabupaten_kota = array( 'name' => $form['nama_kabupaten_kota']['name'],
                                      'label' => $form['nama_kabupaten_kota']['label'],
                                      
                                      'value' => kos($detail_kabupaten_kota['nama_kabupaten_kota'])
                    );
          echo $this->mkform->input_text($attr_nama_kabupaten_kota);
           $attr_aktif = array( 'name' => $form['aktif']['name'],
                                      'label' => $form['aktif']['label'],
                                      'opsi' => array('Ya' => 'Ya',
                                                          'Tidak' => 'Tidak'
                                                    ),
                                      'value' => kos($detail_kabupaten_kota['aktif'])
                    );
          echo $this->mkform->input_select($attr_aktif);
          
          ?>
        </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
   <button type="SUBMIT" class="btn btn-large btn-block btn-success">SUBMIT DATA</button>
  </div>     
</div>      
<?php echo form_close(); ?>