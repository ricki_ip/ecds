<?php echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');?>
<?php
// var_dump($detail_pengguna);
?>

<div class="row">
  <div class="col-lg-12">      
  <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Form Entry Lokasi:</h3>
        </div>
        <div class="panel-body">

          <?php if ($detail_lokasi !== FALSE): ?>
          <input type="hidden" name="id_lokasi" value="<?php echo kos($id_lokasi) ?>">          
          <?php endif ?>

          <?php 
         
          $attr_nama_lokasi = array( 'name' => $form['nama_lokasi']['name'],
                                      'label' => $form['nama_lokasi']['label'],
                                      
                                      'value' => kos($detail_lokasi['nama_lokasi'])
                    );
          echo $this->mkform->input_text($attr_nama_lokasi);

          $attr_code_area = array( 'name' => 'code_area',
                                      'label' => 'Kode Area',
                                      
                                      'value' => kos($detail_lokasi['code_area'])
                    );
          echo $this->mkform->input_text($attr_code_area);
          
          ?>
        </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
   <button type="SUBMIT" class="btn btn-large btn-block btn-success">SUBMIT DATA</button>
  </div>     
</div>      
<?php echo form_close(); ?>