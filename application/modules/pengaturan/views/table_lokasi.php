<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_daftar_lokasi' class='table table-hover table-bordered'>");
	$this->table->set_template($template);
	$this->table->set_heading($constants['th_table']);
	$counter = 1;
	if($list_lokasi){
		// var_dump($list_lokasi);
		// die;
		foreach ($list_lokasi as $item) {
			// $link_edit = '<a class="btn btn-warning" href="'.base_url('kapal/kapal/edit/'.$item->id_kapal).'">Edit</a>';
			// $link_delete = '<a class="btn btn-danger" href="'.base_url('kapal/kapal/delete/'.$item->id_kapal).'">Hapus</a>';
			$this->table->add_row(
								$counter.'.',
								'<a href="'.site_url('pengaturan/mst_lokasi/view/'.$item->id_lokasi).'">'.$item->nama_lokasi.'</a>',
								$item->code_area,
								$item->aktif
								);
			$counter++;

		}
	}

	$table_list_lokasi = $this->table->generate();
?>

<!-- TAMPIL DATA -->
<div class="row">
	<div class="col-lg-12">
		<div class="text-right">
			<a href="<?php echo site_url('pengaturan/mst_lokasi/entry'); ?>" class="btn btn-primary">Tambah Data Lokasi</a>		
		</div>
	</div>
</div>
<p></p>
<div class="row">
	<div class="col-lg-12">
		<?php
			echo $table_list_lokasi;
		?>
	</div>
</div>
		

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		$('#table_daftar_lokasi').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": false,
	        "bInfo": false,
	        "bPaginate": true,
			"sPaginationType":"full_numbers",
			"iDisplayLength": 25,
	        "bSort": true
		} );
	} );
</script>