<div class="row">
  <div class="col-lg-2 col-lg-offset-8">
    <p>
        <?php 
        $link_aksi = '<a class="btn btn-primary btn-lg btn-block" role="button" href="'.base_url('pengaturan/mst_asosiasi/edit/'.$detail_asosiasi['id_asosiasi']).'">'.'Edit'.'</a>';
        echo $link_aksi; 
        ?>
    </p>
  </div>
  <div class="col-lg-2">
  <p>
    <?php if ($detail_asosiasi['aktif'] === 'Ya'): ?>
        <?php 
        $link_aksi = '<a class="btn btn-danger btn-lg btn-block" role="button" href="'.base_url('pengaturan/mst_asosiasi/delete/'.$detail_asosiasi['id_asosiasi']).'">'.'Delete'.'</a>';
        echo $link_aksi; 
        ?>
    <?php else: ?>
        <?php 
        $link_aksi = '<a class="btn btn-success btn-lg btn-block" role="button" href="'.base_url('pengaturan/mst_asosiasi/activate/'.$detail_asosiasi['id_asosiasi']).'">'.'Aktifkan'.'</a>';
        echo $link_aksi; 
        ?>
    <?php endif ?>
  </p>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">

          <?php
          
          $hidden_input = array('id_asosiasi' => $detail_asosiasi['id_asosiasi'],
                                'submit_to' => 'edit');
          echo form_open('id="form_entry" class="form-horizontal" role="form"', $hidden_input);

          ?>
         <?php 
          $attr_asosiasi_name = array( 'name' => $form['asosiasi_name']['name'],
                                      'label' => $form['asosiasi_name']['label'],
                                      'value' => kos($detail_asosiasi['asosiasi_name'])
                    );
          echo $this->mkform->input_text($attr_asosiasi_name);
          
          $attr_kuota_asos_awal = array( 'name' => $form['kuota_asos_awal']['name'],
                                      'label' => $form['kuota_asos_awal']['label'],
                                      'value' => kos($detail_asosiasi['kuota_asos_awal'])
                    );
          echo $this->mkform->input_text($attr_kuota_asos_awal);

          $attr_asosiasi_address = array( 'name' => $form['asosiasi_address']['name'],
                                      'label' => $form['asosiasi_address']['label'],
                                      'value' => kos($detail_asosiasi['asosiasi_address'])
                    );
          echo $this->mkform->input_text($attr_asosiasi_address);



          $attr_no_telp = array( 'name' => $form['no_telp']['name'],
                                      'label' => $form['no_telp']['label'],
                                      'value' => kos($detail_asosiasi['no_telp'])
                    );
          echo $this->mkform->input_text($attr_no_telp);

          
          $attr_aktif = array( 'name' => $form['aktif']['name'],
                                        'label' => $form['aktif']['label'],
                                        'value' => $detail_asosiasi['aktif']
                    );
          echo $this->mkform->input_text($attr_aktif);
         ?>
  </div>
</div>
</form>

<script>
  
  var init_data_tables = function(){
     $('#table_ctf_tag').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "2.5  %" , "sClass": "text-center"},
                        { "sWidth": "22.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                      ],
        "bFilter": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": false
      });
     
      $("input").prop("disabled", true);
      $("select").prop("disabled", true);
      $("textarea").prop("disabled", true);
    }

  
  s_func.push(init_data_tables);
</script>