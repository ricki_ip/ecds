<div class="row">
  <div class="col-lg-2 col-lg-offset-8">
    <p>
        <?php 
        $link_aksi = '<a class="btn btn-primary btn-lg btn-block" role="button" href="'.base_url('pengaturan/mst_negara/edit/'.$detail_negara['id_negara']).'">'.'Edit'.'</a>';
        echo $link_aksi; 
        ?>
    </p>
  </div>
  <div class="col-lg-2">
  <p>
    <?php if ($detail_negara['aktif'] === 'Ya'): ?>
        <?php 
        $link_aksi = '<a class="btn btn-danger btn-lg btn-block" role="button" href="'.base_url('pengaturan/mst_negara/delete/'.$detail_negara['id_negara']).'">'.'Delete'.'</a>';
        echo $link_aksi; 
        ?>
    <?php else: ?>
        <?php 
        $link_aksi = '<a class="btn btn-success btn-lg btn-block" role="button" href="'.base_url('pengaturan/mst_negara/activate/'.$detail_negara['id_negara']).'">'.'Aktifkan'.'</a>';
        echo $link_aksi; 
        ?>
    <?php endif ?>
  </p>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
          <?php
          
          $hidden_input = array('id_negara' => $detail_negara['id_negara'],
                                'submit_to' => 'edit');
          echo form_open('id="form_entry" class="form-horizontal" role="form"', $hidden_input);

          ?>
         <?php 
          $attr_nama_negara = array( 'name' => $form['nama_negara']['name'],
                                        'label' => $form['nama_negara']['label'],
                                        'value' => $detail_negara['nama_negara']
                    );
          echo $this->mkform->input_text($attr_nama_negara);
          
         ?>
  </div>
</div>
</form>

<script>
  
  var init_data_tables = function(){
     $('#table_ctf_tag').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "2.5  %" , "sClass": "text-center"},
                        { "sWidth": "22.5%" , "sClass": "text-center"}
                        // { "sWidth": "12.5%" , "sClass": "text-center"},
                        // { "sWidth": "12.5%" , "sClass": "text-center"},
                        // { "sWidth": "12.5%" , "sClass": "text-center"},
                        // { "sWidth": "12.5%" , "sClass": "text-center"},
                        // { "sWidth": "12.5%" , "sClass": "text-center"},
                        // { "sWidth": "12.5%" , "sClass": "text-center"},
                      ],
        "bFilter": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": false
      });
     
      $("input").prop("disabled", true);
      $("select").prop("disabled", true);
      $("textarea").prop("disabled", true);
    }

  
  s_func.push(init_data_tables);
</script>