<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_daftar_negara' class='table table-hover table-bordered'>");
	$this->table->set_template($template);
	$this->table->set_heading($constants['th_table']);
	$counter = 1;
	if($list_negara){
		// var_dump($list_negara);
		// die;
		foreach ($list_negara as $item) {
			// $link_edit = '<a class="btn btn-warning" href="'.base_url('kapal/kapal/edit/'.$item->id_kapal).'">Edit</a>';
			// $link_delete = '<a class="btn btn-danger" href="'.base_url('kapal/kapal/delete/'.$item->id_kapal).'">Hapus</a>';
			$this->table->add_row(
								$counter.'.',
								'<a href="'.site_url('pengaturan/mst_negara/view/'.$item->id_negara).'">'.$item->nama_negara.'</a>',
								$item->aktif
								);
			$counter++;

		}
	}

	$table_list_negara = $this->table->generate();
?>

<!-- TAMPIL DATA -->
<div class="row">
	<div class="col-lg-12">
		<div class="text-right">
			<a href="<?php echo site_url('pengaturan/mst_negara/entry'); ?>" class="btn btn-primary">Tambah Data Negara</a>		
		</div>
	</div>
</div>
<p></p>
<div class="row">
	<div class="col-lg-12">
		<?php
			echo $table_list_negara;
		?>
	</div>
</div>
		

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		$('#table_daftar_negara').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": false,
	        "bInfo": false,
	        "bPaginate": false,
	        "bSort": true
		} );
	} );
</script>