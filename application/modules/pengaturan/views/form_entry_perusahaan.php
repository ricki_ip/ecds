<?php echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');?>
<?php
// var_dump($detail_pengguna);
?>

<div class="row">
  <div class="col-lg-12">      
  <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Form Entry perusahaan:</h3>
        </div>
        <div class="panel-body">

          <?php if ($detail_perusahaan !== FALSE): ?>
          <input type="hidden" name="id_perusahaan" value="<?php echo kos($id_perusahaan) ?>">          
          <?php endif ?>

          <?php 

          $attr_id_asosiasi = array( 'name' => $form['id_asosiasi']['name'],
                                      'label' => $form['id_asosiasi']['label'],
                                      'opsi' => Modules::run('pengaturan/mst_asosiasi/list_asosiasi_array'),
                                      'value' => kos($detail_perusahaan['id_asosiasi'])
                    );
          echo $this->mkform->input_select2($attr_id_asosiasi);
          
          $attr_company_name = array( 'name' => $form['company_name']['name'],
                                      'label' => $form['company_name']['label'],
                                      'value' => kos($detail_perusahaan['company_name'])
                    );
          echo $this->mkform->input_text($attr_company_name);

          $attr_company_address = array( 'name' => $form['company_address']['name'],
                                      'label' => $form['company_address']['label'],
                                      'value' => kos($detail_perusahaan['company_address'])
                    );
          echo $this->mkform->input_text($attr_company_address);

          $attr_owner_name = array( 'name' => $form['owner_name']['name'],
                                      'label' => $form['owner_name']['label'],
                                      'value' => kos($detail_perusahaan['owner_name'])
                    );
          echo $this->mkform->input_text($attr_owner_name);

          $attr_kuota_perus_awal = array( 'name' => $form['kuota_perus_awal']['name'],
                                      'label' => $form['kuota_perus_awal']['label'],
                                      'value' => kos($detail_perusahaan['kuota_perus_awal'])
                    );
          echo $this->mkform->input_text($attr_kuota_perus_awal);

          $attr_owner_address = array( 'name' => $form['owner_address']['name'],
                                      'label' => $form['owner_address']['label'],
                                      'value' => kos($detail_perusahaan['owner_address'])
                    );
          echo $this->mkform->input_text($attr_owner_address);

          $attr_no_telp = array( 'name' => $form['no_telp']['name'],
                                      'label' => $form['no_telp']['label'],
                                      'value' => kos($detail_perusahaan['no_telp'])
                    );
          echo $this->mkform->input_text($attr_no_telp);

          $attr_aktif = array( 'name' => $form['aktif']['name'],
                                      'label' => $form['aktif']['label'],
                                      'opsi' => array('Ya' => 'Ya',
                                                          'Tidak' => 'Tidak'
                                                    ),
                                      'value' => kos($detail_perusahaan['aktif'])
                    );
          echo $this->mkform->input_select($attr_aktif);
          ?>
        </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
   <button type="SUBMIT" class="btn btn-large btn-block btn-success">SUBMIT DATA</button>
  </div>     
</div>      
<?php echo form_close(); ?>