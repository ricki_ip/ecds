<?php echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');?>
<?php
// var_dump($detail_pengguna);
?>

<div class="row">
  <div class="col-lg-12">      
  <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Form Entry asosiasi:</h3>
        </div>
        <div class="panel-body">

          <?php if ($detail_asosiasi !== FALSE): ?>
          <input type="hidden" name="id_asosiasi" value="<?php echo kos($id_asosiasi) ?>">          
          <?php endif ?>

          <?php 

          $attr_asosiasi_name = array( 'name' => $form['asosiasi_name']['name'],
                                      'label' => $form['asosiasi_name']['label'],
                                      'value' => kos($detail_asosiasi['asosiasi_name'])
                    );
          echo $this->mkform->input_text($attr_asosiasi_name);

          $attr_asosiasi_address = array( 'name' => $form['asosiasi_address']['name'],
                                      'label' => $form['asosiasi_address']['label'],
                                      'value' => kos($detail_asosiasi['asosiasi_address'])
                    );
          echo $this->mkform->input_text($attr_asosiasi_address);

          $attr_kuota_asos_awal = array( 'name' => $form['kuota_asos_awal']['name'],
                                      'label' => $form['kuota_asos_awal']['label'],
                                      'value' => kos($detail_asosiasi['kuota_asos_awal'])
                    );
          echo $this->mkform->input_text($attr_kuota_asos_awal);

          $attr_no_telp = array( 'name' => $form['no_telp']['name'],
                                      'label' => $form['no_telp']['label'],
                                      'value' => kos($detail_asosiasi['no_telp'])
                    );
          echo $this->mkform->input_text($attr_no_telp);

          $attr_aktif = array( 'name' => $form['aktif']['name'],
                                      'label' => $form['aktif']['label'],
                                      'opsi' => array('Ya' => 'Ya',
                                                          'Tidak' => 'Tidak'
                                                    ),
                                      'value' => kos($detail_asosiasi['aktif'])
                    );
          echo $this->mkform->input_select($attr_aktif);
          ?>
        </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
   <button type="SUBMIT" class="btn btn-large btn-block btn-success">SUBMIT DATA</button>
  </div>     
</div>      
<?php echo form_close(); ?>