<div class="row">
  <div class="col-lg-2 col-lg-offset-8">
    <p>
        <?php 
        $link_aksi = '<a class="btn btn-primary btn-lg btn-block" role="button" href="'.base_url('pengaturan/mst_perusahaan/edit/'.$detail_perusahaan['id_perusahaan']).'">'.'Edit'.'</a>';
        echo $link_aksi; 
        ?>
    </p>
  </div>
  <div class="col-lg-2">
  <p>
    <?php if ($detail_perusahaan['aktif'] === 'Ya'): ?>
        <?php 
        $link_aksi = '<a class="btn btn-danger btn-lg btn-block" role="button" href="'.base_url('pengaturan/mst_perusahaan/delete/'.$detail_perusahaan['id_perusahaan']).'">'.'Delete'.'</a>';
        echo $link_aksi; 
        ?>
    <?php else: ?>
        <?php 
        $link_aksi = '<a class="btn btn-success btn-lg btn-block" role="button" href="'.base_url('pengaturan/mst_perusahaan/activate/'.$detail_perusahaan['id_perusahaan']).'">'.'Aktifkan'.'</a>';
        echo $link_aksi; 
        ?>
    <?php endif ?>
  </p>
  </div>
</div>
<hr>
<div class="row">
  <div class="col-lg-12">
    <?php
          $attr_id_asosiasi = array( 'name' => $form['id_asosiasi']['name'],
                                      'label' => $form['id_asosiasi']['label'],
                                      'opsi' => Modules::run('pengaturan/mst_asosiasi/list_asosiasi_array'),
                                      'value' => kos($detail_perusahaan['id_asosiasi'])
                    );
          echo $this->mkform->input_select2($attr_id_asosiasi);
    ?>
  </div>
</div>
<hr>
<div class="row">
  <div class="col-lg-12">

          <?php

          
          
          $hidden_input = array('id_perusahaan' => $detail_perusahaan['id_perusahaan'],
                                'submit_to' => 'edit');
          echo form_open('id="form_entry" class="form-horizontal" role="form"', $hidden_input);

          $attr_company_name = array( 'name' => $form['company_name']['name'],
                                      'label' => $form['company_name']['label'],
                                      'value' => kos($detail_perusahaan['company_name'])
                    );
          echo $this->mkform->input_text($attr_company_name);

          $attr_kuota_perus_awal = array( 'name' => $form['kuota_perus_awal']['name'],
                                      'label' => $form['kuota_perus_awal']['label'],
                                      'value' => kos($detail_perusahaan['kuota_perus_awal'])
                    );
          echo $this->mkform->input_text($attr_kuota_perus_awal);

          $attr_company_address = array( 'name' => $form['company_address']['name'],
                                      'label' => $form['company_address']['label'],
                                      'value' => kos($detail_perusahaan['company_address'])
                    );
          echo $this->mkform->input_text($attr_company_address);

          $attr_owner_name = array( 'name' => $form['owner_name']['name'],
                                        'label' => $form['owner_name']['label'],
                                        'value' => $detail_perusahaan['owner_name']
                    );
          echo $this->mkform->input_text($attr_owner_name);

          $attr_owner_address = array( 'name' => $form['owner_address']['name'],
                                        'label' => $form['owner_address']['label'],
                                        'value' => $detail_perusahaan['owner_address']
                    );
          echo $this->mkform->input_text($attr_owner_address);

          $attr_no_telp = array( 'name' => $form['no_telp']['name'],
                                      'label' => $form['no_telp']['label'],
                                      'value' => kos($detail_perusahaan['no_telp'])
                    );
          echo $this->mkform->input_text($attr_no_telp);

          $attr_aktif = array( 'name' => $form['aktif']['name'],
                                        'label' => $form['aktif']['label'],
                                        'value' => $detail_perusahaan['aktif']
                    );
          echo $this->mkform->input_text($attr_aktif);
         ?>
  </div>
</div>
</form>

<script>
  
  var init_data_tables = function(){
     $('#table_ctf_tag').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "2.5  %" , "sClass": "text-center"},
                        { "sWidth": "22.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                      ],
        "bFilter": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": false
      });
     
      $("input").prop("disabled", true);
      $("select").prop("disabled", true);
      $("textarea").prop("disabled", true);
    }

  
  s_func.push(init_data_tables);
</script>