<!-- HANDLER PRIVILLEGE IKUTI INI -->
<?php if ($this->user->level() < 3 || $this->user->id_pengguna() === $detail_pengguna['id_pengguna']): ?>
<div class="row">
  <div class="col-lg-2 col-lg-offset-8">
    <p>
        <?php 
        $link_aksi = '<a class="btn btn-primary btn-lg btn-block" role="button" href="'.base_url('pengaturan/mst_pengguna/edit/'.$detail_pengguna['id_pengguna']).'">'.'Edit'.'</a>';
        echo $link_aksi; 
        ?>
    </p>
  </div>
  
  <?php if ($this->user->level() < 3): ?>
    <div class="col-lg-2">
    <p>
      <?php if ($detail_pengguna['aktif'] === 'Ya'): ?>
          <?php 
          $link_aksi = '<a class="btn btn-danger btn-lg btn-block" role="button" href="'.base_url('pengaturan/mst_pengguna/delete/'.$detail_pengguna['id_pengguna']).'">'.'Delete'.'</a>';
          echo $link_aksi; 
          ?>
      <?php else: ?>
          <?php 
          $link_aksi = '<a class="btn btn-success btn-lg btn-block" role="button" href="'.base_url('pengaturan/mst_pengguna/activate/'.$detail_pengguna['id_pengguna']).'">'.'Aktifkan'.'</a>';
          echo $link_aksi; 
          ?>
      <?php endif ?>
    </p>
    </div>
  <?php endif ?>

</div>
<?php endif ?>

<div class="row">
  <div class="col-lg-12">

          <?php
          
          $hidden_input = array('id_pengguna' => $detail_pengguna['id_pengguna'],
                                'submit_to' => 'edit');
          echo form_open('id="form_entry" class="form-horizontal" role="form"', $hidden_input);

          ?>
         <?php 
         if($this->user->level() < 3): 
          $attr_id_grup_pengguna = array( 'name' => $form['id_grup_pengguna']['name'],
                                      'label' => $form['id_grup_pengguna']['label'],
                                      'opsi' => Modules::run('pengaturan/mst_grup_pengguna/list_grup_pengguna_array'),
                                      'value' => kos($detail_pengguna['id_grup_pengguna'])
                    );
          echo $this->mkform->input_select2($attr_id_grup_pengguna);
          
          if(!empty($detail_pengguna['id_grup_pengguna']) && $detail_pengguna['id_grup_pengguna'] === '4')
            {
                    $attr_id_asosiasi = array( 'name' => $form['id_asosiasi']['name'],
                                      'label' => $form['id_asosiasi']['label'],
                                      'opsi' => Modules::run('pengaturan/mst_asosiasi/list_asosiasi_array'),
                                      'value' => kos($detail_pengguna['id_asosiasi'])
                    );
                    echo $this->mkform->input_select2($attr_id_asosiasi);
            }
            
          $attr_kode_pengguna = array( 'name' => $form['kode_pengguna']['name'],
                                      'label' => $form['kode_pengguna']['label'],
                                      'value' => kos($detail_pengguna['kode_pengguna'])
                    );
          echo $this->mkform->input_text($attr_kode_pengguna);

          $attr_kata_kunci = array( 'name' => $form['kata_kunci']['name'],
                                      'label' => $form['kata_kunci']['label'],
                                      'value' => kos($detail_pengguna['kata_kunci'])
                    );
          echo $this->mkform->input_text($attr_kata_kunci);
          
          endif;



          $attr_nip = array( 'name' => $form['nip']['name'],
                                      'label' => $form['nip']['label'],
                                      'value' => kos($detail_pengguna['nip'])
                    );
          echo $this->mkform->input_text($attr_nip);

          $attr_nama_pengguna = array( 'name' => $form['nama_pengguna']['name'],
                                      'label' => $form['nama_pengguna']['label'],
                                      'value' => kos($detail_pengguna['nama_pengguna'])
                    );
          echo $this->mkform->input_text($attr_nama_pengguna);

          $attr_full_name = array( 'name' => $form['full_name']['name'],
                                      'label' => $form['full_name']['label'],
                                      'value' => kos($detail_pengguna['full_name'])
                    );
          echo $this->mkform->input_text($attr_full_name);

          $attr_jenis_kelamin = array( 'name' => $form['jenis_kelamin']['name'],
                                      'label' => $form['jenis_kelamin']['label'],
                                      'opsi' => array('Pria' => 'Pria',
                                                          'Wanita' => 'Wanita'
                                                    ),
                                       'value' => kos($detail_pengguna['jenis_kelamin'])              
                    );
          echo $this->mkform->input_select($attr_jenis_kelamin);

          $attr_nama_jabatan = array( 'name' => $form['nama_jabatan']['name'],
                                      'label' => $form['nama_jabatan']['label'],
                                      'value' => kos($detail_pengguna['nama_jabatan'])
                    );
          echo $this->mkform->input_text($attr_nama_jabatan);


          $attr_no_telp1 = array( 'name' => $form['no_telp1']['name'],
                                      'label' => $form['no_telp1']['label'],
                                      'value' => kos($detail_pengguna['no_telp1'])
                    );
          echo $this->mkform->input_text($attr_no_telp1);

          $attr_no_telp2 = array( 'name' => $form['no_telp2']['name'],
                                      'label' => $form['no_telp2']['label'],
                                      'value' => kos($detail_pengguna['no_telp2'])
                    );
          echo $this->mkform->input_text($attr_no_telp2);

          $attr_no_hp = array( 'name' => $form['no_hp']['name'],
                                      'label' => $form['no_hp']['label'],
                                      'value' => kos($detail_pengguna['no_hp'])
                    );
          echo $this->mkform->input_text($attr_no_hp);

          $attr_email = array( 'name' => $form['email']['name'],
                                      'label' => $form['email']['label'],
                                      'value' => kos($detail_pengguna['email'])
                    );
          echo $this->mkform->input_text($attr_email);

          if($this->user->level() < 3): 
          $attr_id_lokasi = array( 'name' => $form['id_lokasi']['name'],
                                      'label' => $form['id_lokasi']['label'],
                                      'opsi' => Modules::run('pengaturan/mst_lokasi/list_lokasi_array'),
                                      'value' => kos($detail_pengguna['id_lokasi'])
                                    //'value' => (isset($list_opsi->mst_lokasi)? $list_opsi->mst_lokasi : '')
                    );
          echo $this->mkform->input_select2($attr_id_lokasi);

          $attr_id_perusahaan = array( 'name' => $form['id_perusahaan']['name'],
                                      'label' => $form['id_perusahaan']['label'],
                                      'opsi' => Modules::run('pengaturan/mst_perusahaan/list_perusahaan_array'),
                                      'value' => kos($detail_pengguna['id_perusahaan'])
                    );
          echo $this->mkform->input_select2($attr_id_perusahaan);
          
          $attr_aktif = array( 'name' => $form['aktif']['name'],
                                      'label' => $form['aktif']['label'],
                                      'opsi' => array('Ya' => 'Ya',
                                                          'Tidak' => 'Tidak'
                                                    ),
                                      'value' => kos($detail_pengguna['aktif'])
                    );
          echo $this->mkform->input_select($attr_aktif);
          endif;
         ?>
  </div>
</div>
</form>
<script>
  
  var init_data_tables = function(){
     $('#table_ctf_tag').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "2.5  %" , "sClass": "text-center"},
                        { "sWidth": "22.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                      ],
        "bFilter": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": false
      });
     
      $("input").prop("disabled", true);
      $("select").prop("disabled", true);
      $("textarea").prop("disabled", true);
    }

  
  s_func.push(init_data_tables);
</script>