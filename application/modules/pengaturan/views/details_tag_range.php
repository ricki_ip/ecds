<?php echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');?>
<?php
// vdump($detail_tag_range, true);
?>

<div class="row">
  <div class="col-lg-12">      
  <div class="panel panel-default">
        <div class="panel-heading">
          <h2 class="panel-title">Limit Tagging <?php echo $detail_tag_range['tahun']; ?> :</h2>
        </div>
        <div class="panel-body">

          <?php 
         
          $attr_min = array( 'name' => 'min',
                                      'label' => 'Min',
                                      'value' => kos($detail_tag_range['min'], '0')
                    );
          echo $this->mkform->input_text($attr_min);

          $attr_max = array( 'name' => 'max',
                                      'label' => 'Max',
                                      'value' => kos($detail_tag_range['max'], '0')
                    );
          echo $this->mkform->input_text($attr_max);
          
          ?>
          <input type="hidden" name="tahun" value="<?php echo $detail_tag_range['tahun']; ?>" />
        </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
   <button type="SUBMIT" class="btn btn-large btn-block btn-success">SUBMIT DATA</button>
  </div>     
</div>      
<?php echo form_close(); ?>