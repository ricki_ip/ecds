
<div class="row">
  <div class="col-lg-12">
    <div class="form-group col-lg-10">
    </div>
    <div class="form-group col-lg-2">
      <div class="col-sm-offset-2 col-sm-4">
        <?php 
        $link_aksi = '<a class="btn btn-primary" href="'.base_url('pengaturan/mst_kabupaten_kota/edit/'.$detail_kabupaten_kota['id_kabupaten_kota']).'">'.'Edit'.'</a>';
        echo $link_aksi; 
        ?>
      </div>
    </div>

          <?php
          
          $hidden_input = array('id_kabupaten_kota' => $detail_kabupaten_kota['id_kabupaten_kota'],
                                'submit_to' => 'edit');
          echo form_open('id="form_entry" class="form-horizontal" role="form"', $hidden_input);

          ?>
         <?php 
          $attr_nama_kabupaten_kota = array( 'name' => $form['nama_kabupaten_kota']['name'],
                                        'label' => $form['nama_kabupaten_kota']['label'],
                                        'value' => $detail_kabupaten_kota['nama_kabupaten_kota']
                    );
          echo $this->mkform->input_text($attr_nama_kabupaten_kota);
          
         ?>
  </div>
</div>
</form>

<script>
  
  var init_data_tables = function(){
     $('#table_ctf_tag').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "2.5  %" , "sClass": "text-center"},
                        { "sWidth": "22.5%" , "sClass": "text-center"}
                        // { "sWidth": "12.5%" , "sClass": "text-center"},
                        // { "sWidth": "12.5%" , "sClass": "text-center"},
                        // { "sWidth": "12.5%" , "sClass": "text-center"},
                        // { "sWidth": "12.5%" , "sClass": "text-center"},
                        // { "sWidth": "12.5%" , "sClass": "text-center"},
                        // { "sWidth": "12.5%" , "sClass": "text-center"},
                      ],
        "bFilter": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": false
      });
     
      $("input").prop("disabled", true).removeAttr('class').css({"background-color":"#f7f8f2","border":"0px","margin-top":"8px"});
      $("select").prop("disabled", true).removeAttr('class').css({"background-color":"#f7f8f2","border":"0px","margin-top":"8px"});
      $("textarea").prop("disabled", true).removeAttr('class').css({"background-color":"#f7f8f2","border":"0px","margin-top":"8px"});
    }

  
  s_func.push(init_data_tables);
</script>