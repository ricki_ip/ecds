<?php echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');?>
<?php
// var_dump($detail_pengguna);
?>

<div class="row">
  <div class="col-lg-12">      
  <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Form Entry Pengguna:</h3>
        </div>
        <div class="panel-body">

          <?php if ($detail_pengguna !== FALSE): ?>
          <input type="hidden" name="id_pengguna" value="<?php echo kos($id_pengguna) ?>">          
          <?php endif ?>

          <?php

          if($this->user->level() < 3): 
          $attr_id_grup_pengguna = array( 'name' => $form['id_grup_pengguna']['name'],
                                      'label' => $form['id_grup_pengguna']['label'],
                                      'opsi' => Modules::run('pengaturan/mst_grup_pengguna/list_grup_pengguna_array'),
                                      'value' => kos($detail_pengguna['id_grup_pengguna'])
                    );
          echo $this->mkform->input_select2($attr_id_grup_pengguna);
          
            if(!empty($detail_pengguna['id_grup_pengguna']) && $detail_pengguna['id_grup_pengguna'] === '4')
            {
                    $attr_id_asosiasi = array( 'name' => $form['id_asosiasi']['name'],
                                      'label' => $form['id_asosiasi']['label'],
                                      'opsi' => Modules::run('pengaturan/mst_asosiasi/list_asosiasi_array'),
                                      'value' => kos($detail_pengguna['id_asosiasi'])
                    );
                    echo $this->mkform->input_select2($attr_id_asosiasi);
            }

          $attr_kode_pengguna = array( 'name' => $form['kode_pengguna']['name'],
                                      'label' => $form['kode_pengguna']['label'],
                                      'value' => kos($detail_pengguna['kode_pengguna'])
                    );
          echo $this->mkform->input_text($attr_kode_pengguna);

          $attr_kata_kunci = array( 'name' => $form['kata_kunci']['name'],
                                      'label' => $form['kata_kunci']['label'],
                                      'value' => kos($detail_pengguna['kata_kunci'])
                    );
          echo $this->mkform->input_text($attr_kata_kunci);
          
          endif;

          $attr_nip = array( 'name' => $form['nip']['name'],
                                      'label' => $form['nip']['label'],
                                      'value' => kos($detail_pengguna['nip'])
                    );
          echo $this->mkform->input_text($attr_nip);

          $attr_nama_pengguna = array( 'name' => $form['nama_pengguna']['name'],
                                      'label' => $form['nama_pengguna']['label'],
                                      'value' => kos($detail_pengguna['nama_pengguna'])
                    );
          echo $this->mkform->input_text($attr_nama_pengguna);

          $attr_full_name = array( 'name' => $form['full_name']['name'],
                                      'label' => $form['full_name']['label'],
                                      'value' => kos($detail_pengguna['full_name'])
                    );
          echo $this->mkform->input_text($attr_full_name);

          $attr_jenis_kelamin = array( 'name' => $form['jenis_kelamin']['name'],
                                      'label' => $form['jenis_kelamin']['label'],
                                      'opsi' => array('Pria' => 'Pria',
                                                          'Wanita' => 'Wanita'
                                                    ),
                                       'value' => kos($detail_pengguna['jenis_kelamin'])              
                    );
          echo $this->mkform->input_select($attr_jenis_kelamin);

          $attr_nama_jabatan = array( 'name' => $form['nama_jabatan']['name'],
                                      'label' => $form['nama_jabatan']['label'],
                                      'value' => kos($detail_pengguna['nama_jabatan'])
                    );
          echo $this->mkform->input_text($attr_nama_jabatan);


          $attr_no_telp1 = array( 'name' => $form['no_telp1']['name'],
                                      'label' => $form['no_telp1']['label'],
                                      'value' => kos($detail_pengguna['no_telp1'])
                    );
          echo $this->mkform->input_text($attr_no_telp1);

          $attr_no_telp2 = array( 'name' => $form['no_telp2']['name'],
                                      'label' => $form['no_telp2']['label'],
                                      'value' => kos($detail_pengguna['no_telp2'])
                    );
          echo $this->mkform->input_text($attr_no_telp2);

          $attr_no_hp = array( 'name' => $form['no_hp']['name'],
                                      'label' => $form['no_hp']['label'],
                                      'value' => kos($detail_pengguna['no_hp'])
                    );
          echo $this->mkform->input_text($attr_no_hp);

          $attr_email = array( 'name' => $form['email']['name'],
                                      'label' => $form['email']['label'],
                                      'value' => kos($detail_pengguna['email'])
                    );
          echo $this->mkform->input_text($attr_email);

          if($this->user->level() < 3): 
          $attr_id_lokasi = array( 'name' => $form['id_lokasi']['name'],
                                      'label' => $form['id_lokasi']['label'],
                                      'opsi' => Modules::run('pengaturan/mst_lokasi/list_lokasi_array'),
                                      'value' => kos($detail_pengguna['id_lokasi'])
                                    //'value' => (isset($list_opsi->mst_lokasi)? $list_opsi->mst_lokasi : '')
                    );
          echo $this->mkform->input_select2($attr_id_lokasi);

          $attr_id_perusahaan = array( 'name' => $form['id_perusahaan']['name'],
                                      'label' => $form['id_perusahaan']['label'],
                                      'opsi' => Modules::run('pengaturan/mst_perusahaan/list_perusahaan_array'),
                                      'value' => kos($detail_pengguna['id_perusahaan'])
                    );
          echo $this->mkform->input_select2($attr_id_perusahaan);
          
          $attr_aktif = array( 'name' => $form['aktif']['name'],
                                      'label' => $form['aktif']['label'],
                                      'opsi' => array('Ya' => 'Ya',
                                                          'Tidak' => 'Tidak'
                                                    ),
                                      'value' => kos($detail_pengguna['aktif'])
                    );
          echo $this->mkform->input_select($attr_aktif);
          endif;
          ?>
        </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
   <button type="SUBMIT" class="btn btn-large btn-block btn-success">SUBMIT DATA</button>
  </div>     
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
  // $(document).ready(
  //   function()
  //   {
  //     perusahaan();
  //     $('#id_id_grup_pengguna').change(
  //       function()
  //       {
  //         perusahaan();
  //       }
  //     );
  //   }
  // );
    
  // function perusahaan()
  // {
  //   if ( $('#id_id_grup_pengguna').val() != 4)
  //   {
  //     // $('#id_id_perusahaan').prop('selectedIndex', 1);
  //     $('#id_id_perusahaan').prop('value', "0");
  //     $('#id_id_perusahaan').prop('disabled', true);
  //   }else
  //   {
  //     $('#id_id_perusahaan').prop('disabled', false);
  //   }
  // }
</script>
