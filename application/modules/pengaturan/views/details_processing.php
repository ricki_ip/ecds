<div class="row">
  <div class="col-lg-2 col-lg-offset-8">
    <p>
        <?php 
        $link_aksi = '<a class="btn btn-primary btn-lg btn-block" role="button" href="'.base_url('pengaturan/mst_processing/edit/'.$detail_processing['id_processing']).'">'.'Edit'.'</a>';
        echo $link_aksi; 
        ?>
    </p>
  </div>
  <div class="col-lg-2">
  <p>
    <?php if ($detail_processing['aktif'] === 'Ya'): ?>
        <?php 
        $link_aksi = '<a class="btn btn-danger btn-lg btn-block" role="button" href="'.base_url('pengaturan/mst_processing/delete/'.$detail_processing['id_processing']).'">'.'Delete'.'</a>';
        echo $link_aksi; 
        ?>
    <?php else: ?>
        <?php 
        $link_aksi = '<a class="btn btn-success btn-lg btn-block" role="button" href="'.base_url('pengaturan/mst_processing/activate/'.$detail_processing['id_processing']).'">'.'Aktifkan'.'</a>';
        echo $link_aksi; 
        ?>
    <?php endif ?>
  </p>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">

          <?php
          
          $hidden_input = array('id_processing' => $detail_processing['id_processing'],
                                'submit_to' => 'edit');
          echo form_open('id="form_entry" class="form-horizontal" role="form"', $hidden_input);

          ?>
         <?php 
          $attr_company_name = array( 'name' => $form['company_name']['name'],
                                      'label' => $form['company_name']['label'],
                                      'value' => kos($detail_processing['company_name'])
                    );
          echo $this->mkform->input_text($attr_company_name);

          $attr_company_address = array( 'name' => $form['company_address']['name'],
                                      'label' => $form['company_address']['label'],
                                      'value' => kos($detail_processing['company_address'])
                    );
          echo $this->mkform->input_text($attr_company_address);

          $attr_owner_name = array( 'name' => $form['owner_name']['name'],
                                        'label' => $form['owner_name']['label'],
                                        'value' => $detail_processing['owner_name']
                    );
          echo $this->mkform->input_text($attr_owner_name);

          $attr_owner_address = array( 'name' => $form['owner_address']['name'],
                                        'label' => $form['owner_address']['label'],
                                        'value' => $detail_processing['owner_address']
                    );
          echo $this->mkform->input_text($attr_owner_address);

          $attr_no_telp = array( 'name' => $form['no_telp']['name'],
                                      'label' => $form['no_telp']['label'],
                                      'value' => kos($detail_processing['no_telp'])
                    );
          echo $this->mkform->input_text($attr_no_telp);


          $attr_is_processing_establishment = array( 'name' => $form['is_processing_establishment']['name'],
                                        'label' => $form['is_processing_establishment']['label'],
                                        'value' => kos($detail_processing['is_processing_establishment'])
          );
          echo $this->mkform->input_text($attr_is_processing_establishment);
          
          $attr_aktif = array( 'name' => $form['aktif']['name'],
                                        'label' => $form['aktif']['label'],
                                        'value' => $detail_processing['aktif']
                    );
          echo $this->mkform->input_text($attr_aktif);
         ?>
  </div>
</div>
</form>

<script>
  
  var init_data_tables = function(){
     $('#table_ctf_tag').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "2.5  %" , "sClass": "text-center"},
                        { "sWidth": "22.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                      ],
        "bFilter": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": false
      });
     
      $("input").prop("disabled", true);
      $("select").prop("disabled", true);
      $("textarea").prop("disabled", true);
    }

  
  s_func.push(init_data_tables);
</script>