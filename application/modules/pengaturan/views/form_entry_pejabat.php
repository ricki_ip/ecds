<?php echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');?>
<?php
// var_dump($detail_pengguna);
?>

<div class="row">
  <div class="col-lg-12">      
  <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Form Entry Pejabat:</h3>
        </div>
        <div class="panel-body">

          <?php if ($detail_pejabat !== FALSE): ?>
          <input type="hidden" name="id_pejabat" value="<?php echo kos($id_pejabat) ?>">          
          <?php endif ?>

          <?php 
         
          $attr_nama_pejabat = array( 'name' => $form['nama_pejabat']['name'],
                                      'label' => $form['nama_pejabat']['label'],
                                      
                                      'value' => kos($detail_pejabat['nama_pejabat'])
                    );
          echo $this->mkform->input_text($attr_nama_pejabat);

          $attr_title = array( 'name' => $form['title']['name'],
                                      'label' => $form['title']['label'],
                                      'value' => kos($detail_pejabat['title'])
                    );
          echo $this->mkform->input_text($attr_title);

          $attr_id_lokasi = array( 'name' => $form['id_lokasi']['name'],
                                      'label' => $form['id_lokasi']['label'],
                                      'opsi' => Modules::run('pengaturan/mst_lokasi/list_lokasi_array'),
                                      'value' => kos($detail_pejabat['id_lokasi'])
                    );
          echo $this->mkform->input_select2($attr_id_lokasi);
          
          ?>
        </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
   <button type="SUBMIT" class="btn btn-large btn-block btn-success">SUBMIT DATA</button>
  </div>     
</div>      
<?php echo form_close(); ?>