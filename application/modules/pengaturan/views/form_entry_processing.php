<?php echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');?>
<?php
// var_dump($detail_pengguna);
?>

<div class="row">
  <div class="col-lg-12">      
  <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Form Entry Processing Establishment:</h3>
        </div>
        <div class="panel-body">

          <?php if ($detail_processing !== FALSE): ?>
          <input type="hidden" name="id_processing" value="<?php echo kos($id_processing) ?>">          
          <?php endif ?>

          <?php 
          $attr_company_name = array( 'name' => $form['company_name']['name'],
                                      'label' => $form['company_name']['label'],
                                      'value' => kos($detail_processing['company_name'])
                    );
          echo $this->mkform->input_text($attr_company_name);

          $attr_company_address = array( 'name' => $form['company_address']['name'],
                                      'label' => $form['company_address']['label'],
                                      'value' => kos($detail_processing['company_address'])
                    );
          echo $this->mkform->input_text($attr_company_address);

          $attr_owner_name = array( 'name' => $form['owner_name']['name'],
                                      'label' => $form['owner_name']['label'],
                                      'value' => kos($detail_processing['owner_name'])
                    );
          echo $this->mkform->input_text($attr_owner_name);

          $attr_owner_address = array( 'name' => $form['owner_address']['name'],
                                      'label' => $form['owner_address']['label'],
                                      'value' => kos($detail_processing['owner_address'])
                    );
          echo $this->mkform->input_text($attr_owner_address);

          $attr_no_telp = array( 'name' => $form['no_telp']['name'],
                                      'label' => $form['no_telp']['label'],
                                      'value' => kos($detail_processing['no_telp'])
                    );
          echo $this->mkform->input_text($attr_no_telp);


          $attr_is_processing_establishment = array( 'name' => $form['is_processing_establishment']['name'],
                                      'label' => $form['is_processing_establishment']['label'],
                                      'opsi' => array('Ya' => 'Ya',
                                                          'Tidak' => 'Tidak'
                                                    ),
                                      'value' => kos($detail_processing['is_processing_establishment'])
                    );
          echo $this->mkform->input_select($attr_is_processing_establishment);

          $attr_aktif = array( 'name' => $form['aktif']['name'],
                                      'label' => $form['aktif']['label'],
                                      'opsi' => array('Ya' => 'Ya',
                                                          'Tidak' => 'Tidak'
                                                    ),
                                      'value' => kos($detail_processing['aktif'])
                    );
          echo $this->mkform->input_select($attr_aktif);
          ?>
        </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
   <button type="SUBMIT" class="btn btn-large btn-block btn-success">SUBMIT DATA</button>
  </div>     
</div>      
<?php echo form_close(); ?>