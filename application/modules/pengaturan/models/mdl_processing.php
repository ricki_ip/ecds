<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_processing extends CI_Model
{
	

    function __construct()
    {
        

    }


    public function list_processing()
    {
        // $query = 'SELECT mst_pengguna.id_pengguna, mst_pengguna.nama_pengguna, mst_grup_pengguna.nama_grup_pengguna, mst_pengguna.nama_jabatan, mst_pengguna.aktif 
        //             FROM mst_pengguna, mst_grup_pengguna
        //             where mst_pengguna.id_grup_pengguna = mst_grup_pengguna.id_grup_pengguna';

        $query = 'SELECT * FROM mst_processing ';

        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }


    public function list_opsi_processing($filter)
    {
        // $this->db->select("id_processing AS id");
        // $this->db->select('CONCAT( company_name ,"", company_address ) AS text');
        // $this->db->from("mst_processing");
        
        $sql = "SELECT id_processing AS id, CONCAT (company_name,' | ',company_address) AS text FROM mst_processing ";    
        $sql .= " WHERE aktif = 'Ya' ";    
        
        switch ($filter) { 
            case 'processing':
                $sql .= "AND is_processing_establishment = 'Ya' ";
                break;
            case 'vessel_owner':
                $sql .= "AND is_processing_establishment = 'Tidak' ";
                break;
            default:
                //Nothing to do here
                break;
        }

        

        $run_query = $this->db->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function input($data)
    {
        $this->db->insert('mst_processing', $data);
    }

    public function update($id_processing, $data)
    {
        $this->db->where('id_processing', $data['id_processing']);
        $query = $this->db->update('mst_processing',$data);

        if($this->db->affected_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }

        return $result;
    }

    public function detail_processing($id)
    {
        $sql = "SELECT *
                    FROM mst_processing 
                    WHERE mst_processing.id_processing = $id";

        $run_query = $this->db->query($sql);
        //$query = $this->db->get_where('mst_processing', array('id_processing' => $id_processing));                            

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function delete($id)
    {   
        $data = array(
               'aktif' => 'Tidak',
               'id_pengguna_ubah' => $this->user->id_pengguna(),
               'tanggal_ubah' => date('Y-m-d H:i:s')
            );

        $this->db->where('id_processing', $id);
        $this->db->update('mst_processing', $data); 

    }

    public function activate($id)
    {   
        $data = array(
               'aktif' => 'Ya',
               'id_pengguna_ubah' => $this->user->id_pengguna(),
               'tanggal_ubah' => date('Y-m-d H:i:s')
            );

        $this->db->where('id_processing', $id);
        $this->db->update('mst_processing', $data); 

    }
}