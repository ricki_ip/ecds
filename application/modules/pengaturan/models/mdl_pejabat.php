<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_pejabat extends CI_Model
{
	

    function __construct()
    {
        

    }

    public function list_pejabat()
    {
        $query = 'SELECT mst_pejabat.id_pejabat, mst_pejabat.nama_pejabat, mst_pejabat.title, mst_lokasi.nama_lokasi 
                    FROM mst_pejabat, mst_lokasi
                    where mst_pejabat.id_lokasi = mst_lokasi.id_lokasi';

        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_opsi_pejabat()
    {
        $query = "SELECT mst_pejabat.id_pejabat AS id, CONCAT(mst_pejabat.nama_pejabat,' / ',mst_pejabat.title) AS text FROM mst_pejabat WHERE aktif = 'Ya'";

        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    

     public function input($data)
    {
        $this->db->insert('mst_pejabat', $data);
    }

    public function update($id_pejabat, $data)
    {
        $this->db->where('id_pejabat', $data['id_pejabat']);
        $query = $this->db->update('mst_pejabat',$data);

        if($this->db->affected_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }

        return $result;
    }

    public function detail_pejabat($id)
    {
        $sql = "SELECT mst_pejabat.* , mst_lokasi.nama_lokasi 
                    FROM mst_pejabat, mst_lokasi 
                    WHERE mst_pejabat.id_pejabat = $id";

        $run_query = $this->db->query($sql);
        //$query = $this->db->get_where('mst_pejabat', array('id_pejabat' => $id_pejabat));                            

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function delete($id)
    {   
        $data = array(
               'aktif' => 'Tidak',
               'id_pengguna_ubah' => $this->user->id_pengguna(),
               'tanggal_ubah' => date('Y-m-d H:i:s')
            );

        $this->db->where('id_pejabat', $id);
        $this->db->update('mst_pejabat', $data); 

    }

    public function activate($id)
    {   
        $data = array(
               'aktif' => 'Ya',
               'id_pengguna_ubah' => $this->user->id_pengguna(),
               'tanggal_ubah' => date('Y-m-d H:i:s')
            );

        $this->db->where('id_pejabat', $id);
        $this->db->update('mst_pejabat', $data); 

    }
}