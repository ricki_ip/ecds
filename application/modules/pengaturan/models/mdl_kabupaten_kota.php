<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_kabupaten_kota extends CI_Model
{
  

    function __construct()
    {
        
    }

    public function list_opsi()
    {
      $query = "SELECT id_kabupaten_kota AS id, nama_kabupaten_kota as text FROM mst_kabupaten_kota";

      $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_opsi_with_propinsi()
    {
      $query = "SELECT 
                    mkk.id_kabupaten_kota AS id, concat(mkk.nama_kabupaten_kota,' - ', mpr.nama_propinsi) as text
                FROM
                    mst_kabupaten_kota mkk
                JOIN
                  mst_propinsi mpr ON mpr.id_propinsi = mkk.id_propinsi
                WHERE mkk.aktif = 'Ya' ";

      $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

     public function list_kabupaten_kota()
    {
        $query = 'SELECT mst_kabupaten_kota.id_kabupaten_kota, mst_kabupaten_kota.nama_kabupaten_kota, mst_kabupaten_kota.aktif,mst_kabupaten_kota.id_propinsi
                    FROM mst_kabupaten_kota, mst_propinsi
                    where mst_kabupaten_kota.id_propinsi = mst_propinsi.id_propinsi
                    and mst_kabupaten_kota.aktif = "Ya" ';

        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_opsi_kabupaten_kota()
    {
        $query = "SELECT mst_kabupaten_kota.id_kabupaten_kota AS id, CONCAT(mst_kabupaten_kota.nama_kabupaten_kota) AS text 
                    FROM mst_kabupaten_kota
                    WHERE mst_kabupaten_kota.aktif = 'Ya' ";

        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    

     public function input($data)
    {
        $this->db->insert('mst_kabupaten_kota', $data);
    }

    public function update($id_kabupaten_kota, $data)
    {
        $this->db->where('id_kabupaten_kota', $data['id_kabupaten_kota']);
        $query = $this->db->update('mst_kabupaten_kota',$data);

        if($this->db->affected_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }

        return $result;
    }

    public function detail_kabupaten_kota($id)
    {
        $sql = "SELECT mst_kabupaten_kota.id_kabupaten_kota, mst_kabupaten_kota.nama_kabupaten_kota
                    FROM mst_kabupaten_kota
                    WHERE mst_kabupaten_kota.id_kabupaten_kota = $id";

        $run_query = $this->db->query($sql);
        //$query = $this->db->get_where('mst_kabupaten_kota', array('id_kabupaten_kota' => $id_kabupaten_kota));                            

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function delete($id)
    {

        $sql = " UPDATE kapi_monev.mst_inka_mina SET aktif='Tidak' WHERE id_kabupaten_kota=$id ";
        
        $query = $this->db->query($sql);
        //var_dump($sql);

    }
}