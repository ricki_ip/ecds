<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_negara extends CI_Model
{
  

    function __construct()
    {
        
    }

  public function list_opsi()
    {
      $query = "SELECT id_negara AS id, nama_negara as text FROM mst_negara WHERE aktif = 'Ya'";

      $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_negara()
    {
        $this->db->where('aktif', 'Ya');        
        $run_query = $this->db->get('mst_negara');                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function delete($id)
    {   
        $data = array(
               'aktif' => 'Tidak',
               'id_pengguna_ubah' => $this->user->id_pengguna(),
               'tanggal_ubah' => date('Y-m-d H:i:s')
            );

        $this->db->where('id_negara', $id);
        $this->db->update('mst_negara', $data); 

    }

     public function activate($id)
    {   
        $data = array(
               'aktif' => 'Ya',
               'id_pengguna_ubah' => $this->user->id_pengguna(),
               'tanggal_ubah' => date('Y-m-d H:i:s')
            );

        $this->db->where('id_negara', $id);
        $this->db->update('mst_negara', $data); 

    }

    public function input($data)
    {
        $this->db->insert('mst_negara', $data);
    }

    public function update($id_negara, $data)
    {
        $this->db->where('id_negara', $data['id_negara']);
        $query = $this->db->update('mst_negara',$data);

        if($this->db->affected_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }

        return $result;
    }

    public function detail_negara($id)
    {
        $sql = "SELECT *
                    FROM mst_negara
                    WHERE mst_negara.id_negara = $id";

        $run_query = $this->db->query($sql);
        //$query = $this->db->get_where('mst_lokasi', array('id_lokasi' => $id_lokasi));                            

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }
}