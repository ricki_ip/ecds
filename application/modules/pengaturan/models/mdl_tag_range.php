<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_tag_range extends CI_Model
{
    

    function __construct()
    {
        

    }

    public function list_tag_range()
    {
        $this->db->order_by('tahun', 'desc');
        $run_query = $this->db->get('mst_tag_range');                            
        // vdump($this->db->last_query());
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_tag_range($tahun)
    {
        $this->db->where('tahun', $tahun);
        $run_query = $this->db->get('mst_tag_range');                            
        // vdump($this->db->last_query());
        if($run_query->num_rows() > 0){
            $result = $run_query->row_array();
        }else{
            $result = false;
        }
        return $result;
    }

    public function update($tahun, $data)
    {
        $this->db->where('tahun', $tahun);
        $query = $this->db->update('mst_tag_range',$data);

        if($this->db->affected_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }

        return $result;
    }


    
}