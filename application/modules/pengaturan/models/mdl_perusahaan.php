<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_perusahaan extends CI_Model
{
	

    function __construct()
    {
        

    }

    public function list_perusahaan()
    {
        // $query = 'SELECT mst_pengguna.id_pengguna, mst_pengguna.nama_pengguna, mst_grup_pengguna.nama_grup_pengguna, mst_pengguna.nama_jabatan, mst_pengguna.aktif 
        //             FROM mst_pengguna, mst_grup_pengguna
        //             where mst_pengguna.id_grup_pengguna = mst_grup_pengguna.id_grup_pengguna';

        $query = 'SELECT * FROM mst_perusahaan  
                    LEFT JOIN mst_asosiasi
                    ON mst_perusahaan.id_asosiasi = mst_asosiasi.id_asosiasi';

        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }


    public function list_opsi_perusahaan($filter ="")
    {
        // $this->db->select("id_perusahaan AS id");
        // $this->db->select('CONCAT( company_name ,"", company_address ) AS text');
        // $this->db->from("mst_perusahaan");
        
        $sql = "SELECT id_perusahaan AS id, CONCAT (company_name,' | ',company_address) AS text FROM mst_perusahaan WHERE aktif = 'Ya' ";    
        // switch ($filter) {
        //     case 'processing':
        //         $sql .= "WHERE is_processing_establishment = 'Ya' ";
        //         break;
        //     case 'vessel_owner':
        //         $sql .= "WHERE is_processing_establishment = 'Tidak' ";
        //         break;
        //     default:
        //         //Nothing to do here
        //         break;
        // }

        $run_query = $this->db->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_perusahaan_atli_json($filter ="")
    {
        // $this->db->select("id_perusahaan AS id");
        // $this->db->select('CONCAT( company_name ,"", company_address ) AS text');
        // $this->db->from("mst_perusahaan");
        
        $sql = "SELECT id_perusahaan AS id, CONCAT (company_name,' | ',company_address) AS text FROM mst_perusahaan 
                    WHERE mst_perusahaan.id_asosiasi = '1'
                    AND mst_perusahaan.aktif = 'Ya' ";    
        // switch ($filter) {
        //     case 'processing':
        //         $sql .= "WHERE is_processing_establishment = 'Ya' ";
        //         break;
        //     case 'vessel_owner':
        //         $sql .= "WHERE is_processing_establishment = 'Tidak' ";
        //         break;
        //     default:
        //         //Nothing to do here
        //         break;
        // }

        $run_query = $this->db->query($sql);                            

        // var_dump($run_query->result());
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function input($data)
    {
        $this->db->insert('mst_perusahaan', $data);
    }

    public function update($id_perusahaan, $data)
    {
        $this->db->where('id_perusahaan', $data['id_perusahaan']);
        $query = $this->db->update('mst_perusahaan',$data);

        if($this->db->affected_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }

        return $result;
    }

    public function detail_perusahaan($id)
    {
        $sql = "SELECT *
                    FROM mst_perusahaan 
                    LEFT JOIN mst_asosiasi
                    ON mst_perusahaan.id_asosiasi = mst_asosiasi.id_asosiasi
                    WHERE mst_perusahaan.id_perusahaan = $id";

        $run_query = $this->db->query($sql);
        //$query = $this->db->get_where('mst_perusahaan', array('id_perusahaan' => $id_perusahaan));                            

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function delete($id)
    {   
        $data = array(
               'aktif' => 'Tidak',
               'id_pengguna_ubah' => $this->user->id_pengguna(),
               'tanggal_ubah' => date('Y-m-d H:i:s')
            );

        $this->db->where('id_perusahaan', $id);
        $this->db->update('mst_perusahaan', $data); 

    }

    public function activate($id)
    {   
        $data = array(
               'aktif' => 'Ya',
               'id_pengguna_ubah' => $this->user->id_pengguna(),
               'tanggal_ubah' => date('Y-m-d H:i:s')
            );

        $this->db->where('id_perusahaan', $id);
        $this->db->update('mst_perusahaan', $data); 

    }
}