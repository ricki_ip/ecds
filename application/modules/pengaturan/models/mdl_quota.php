<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_quota extends CI_Model
{
	

    function __construct()
    {
        

    }

    public function list_quota()
    {
        $this->db->order_by('tahun', 'desc');
        $run_query = $this->db->get('mst_fish_quota');                            
        // vdump($this->db->last_query());
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_quota($tahun)
    {
        $this->db->where('tahun', $tahun);
        $run_query = $this->db->get('mst_fish_quota');                            
        // vdump($this->db->last_query());
        if($run_query->num_rows() > 0){
            $result = $run_query->row_array();
        }else{
            $result = false;
        }
        return $result;
    }

    public function update($id_quota, $data)
    {
        $this->db->where('id_fish_quota', $id_quota);
        $query = $this->db->update('mst_fish_quota',$data);

        if($this->db->affected_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }

        return $result;
    }

    public function total_weight()
    {
        $sql = "SELECT 
                    SUM(tti.weight * mtt.conversion_factor) AS total_weight
                FROM
                    trs_ctf_fish tti
                        LEFT JOIN
                    mst_type_tag mtt ON mtt.id_type_tag = tti.type_tag
                ";   

        // var_dump($sql);

        $run_query = $this->db->query($sql);                            

        if($run_query->num_rows() > 0){
            $get_result = $run_query->row();
            $result = $get_result->total_weight;
        }else{
            $result = false;
        }
        return $result;
    }

    
}