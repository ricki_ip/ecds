<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_asosiasi extends CI_Model
{
	

    function __construct()
    {
        

    }

    public function list_asosiasi()
    {
        // $query = 'SELECT mst_pengguna.id_pengguna, mst_pengguna.nama_pengguna, mst_grup_pengguna.nama_grup_pengguna, mst_pengguna.nama_jabatan, mst_pengguna.aktif 
        //             FROM mst_pengguna, mst_grup_pengguna
        //             where mst_pengguna.id_grup_pengguna = mst_grup_pengguna.id_grup_pengguna';

        $query = 'SELECT * FROM mst_asosiasi';

        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }


    public function list_opsi_asosiasi($filter)
    {
        // $this->db->select("id_asosiasi AS id");
        // $this->db->select('CONCAT( asosiasi_name ,"", asosiasi_address ) AS text');
        // $this->db->from("mst_asosiasi");
        
        $sql = "SELECT id_asosiasi AS id, CONCAT (asosiasi_name,' | ',asosiasi_address) AS text FROM mst_asosiasi ";          
        $sql .= "WHERE aktif = 'Ya' ";
        $run_query = $this->db->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function input($data)
    {
        $this->db->insert('mst_asosiasi', $data);
    }

    public function update($id_asosiasi, $data)
    {
        $this->db->where('id_asosiasi', $data['id_asosiasi']);
        $query = $this->db->update('mst_asosiasi',$data);

        if($this->db->affected_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }

        return $result;
    }

    public function detail_asosiasi($id)
    {
        $sql = "SELECT *
                    FROM mst_asosiasi 
                    WHERE mst_asosiasi.id_asosiasi = $id";

        $run_query = $this->db->query($sql);
        //$query = $this->db->get_where('mst_asosiasi', array('id_asosiasi' => $id_asosiasi));                            

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function delete($id)
    {   
        $data = array(
               'aktif' => 'Tidak',
               'id_pengguna_ubah' => $this->user->id_pengguna(),
               'tanggal_ubah' => date('Y-m-d H:i:s')
            );

        $this->db->where('id_asosiasi', $id);
        $this->db->update('mst_asosiasi', $data); 

    }

    public function activate($id)
    {   
        $data = array(
               'aktif' => 'Ya',
               'id_pengguna_ubah' => $this->user->id_pengguna(),
               'tanggal_ubah' => date('Y-m-d H:i:s')
            );

        $this->db->where('id_asosiasi', $id);
        $this->db->update('mst_asosiasi', $data); 

    }
}