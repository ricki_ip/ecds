<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_lokasi extends CI_Model
{
	

    function __construct()
    {
        
    }

	public function list_opsi()
    {
    	$query = "SELECT id_lokasi AS id, nama_lokasi as text FROM mst_lokasi WHERE aktif = 'Ya' ";

    	$run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_lokasi()
    {
        // $this->db->where('aktif', 'Ya');
        $run_query = $this->db->get('mst_lokasi'); 

        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_opsi_lokasi()
    {
        $query = "SELECT mst_lokasi.id_lokasi AS id, CONCAT(mst_lokasi.nama_lokasi) AS text 
                    FROM mst_lokasi
                    WHERE aktif = 'Ya'";

        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    

    public function input($data)
    {
        $this->db->insert('mst_lokasi', $data);
    }

    public function update($id_lokasi, $data)
    {
        $this->db->where('id_lokasi', $data['id_lokasi']);
        $query = $this->db->update('mst_lokasi',$data);

        if($this->db->affected_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }

        return $result;
    }

    public function detail_lokasi($id)
    {
        $sql = "SELECT *
                    FROM mst_lokasi
                    WHERE mst_lokasi.id_lokasi = $id";

        $run_query = $this->db->query($sql);
        //$query = $this->db->get_where('mst_lokasi', array('id_lokasi' => $id_lokasi));                            

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function delete($id)
    {   
        $data = array(
               'aktif' => 'Tidak',
               'id_pengguna_ubah' => $this->user->id_pengguna(),
               'tanggal_ubah' => date('Y-m-d H:i:s')
            );

        $this->db->where('id_lokasi', $id);
        $this->db->update('mst_lokasi', $data); 

    }

    public function activate($id)
    {   
        $data = array(
               'aktif' => 'Ya',
               'id_pengguna_ubah' => $this->user->id_pengguna(),
               'tanggal_ubah' => date('Y-m-d H:i:s')
            );

        $this->db->where('id_lokasi', $id);
        $this->db->update('mst_lokasi', $data); 

    }
}