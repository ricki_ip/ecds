<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_pengguna extends CI_Model
{
	

    function __construct()
    {
        

    }

    public function list_pengguna()
    {
        $query = 'SELECT mst_pengguna.id_pengguna, 
                         mst_pengguna.nama_pengguna,
                         mst_pengguna.last_login,
                         mst_grup_pengguna.nama_grup_pengguna,
                         mst_lokasi.nama_lokasi,
                         mst_pengguna.aktif 
                    FROM mst_pengguna, mst_grup_pengguna, mst_lokasi
                    WHERE mst_lokasi.id_lokasi = mst_pengguna.id_lokasi
                    AND mst_pengguna.id_grup_pengguna = mst_grup_pengguna.id_grup_pengguna';

        // $query = 'SELECT * FROM mst_pengguna, mst_grup_pengguna';

        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    

     public function input($data)
    {
        $this->db->insert('mst_pengguna', $data);
    }

    public function update($id_pengguna, $data)
    {
        $this->db->where('id_pengguna', $data['id_pengguna']);
        $query = $this->db->update('mst_pengguna',$data);

        if($this->db->affected_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }

        return $result;
    }

    public function detail_pengguna($id)
    {
        $sql = "SELECT mst_pengguna.* , mst_lokasi.nama_lokasi, mst_grup_pengguna.nama_grup_pengguna
                    FROM mst_pengguna, mst_lokasi, mst_grup_pengguna 
                    WHERE mst_pengguna.id_pengguna = $id";

        $run_query = $this->db->query($sql);
        //$query = $this->db->get_where('mst_pengguna', array('id_pengguna' => $id_pengguna));                            

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    // public function set_status_entry($id_pengguna)
    // {
    //     $date_terima = date('Y-m-d');
    //     $data = array('status_entry_bkp' => '1');
    //     $this->db->where('id_pengguna', $id_pengguna);
    //     $this->db->update('mst_pengguna', $data);

    //     $aff_rows = $this->db->affected_rows();
    //     if($aff_rows > 0)
    //     {
    //         $affected = true;   
    //     }else{
    //         $affected = false;
    //     }

    //     return $affected;

    // }

    public function delete($id)
    {   
        $data = array(
               'aktif' => 'Tidak',
               'id_pengguna_ubah' => $this->user->id_pengguna(),
               'tanggal_ubah' => date('Y-m-d H:i:s')
            );

        $this->db->where('id_pengguna', $id);
        $this->db->update('mst_pengguna', $data); 

    }

    public function activate($id)
    {   
        $data = array(
               'aktif' => 'Ya',
               'id_pengguna_ubah' => $this->user->id_pengguna(),
               'tanggal_ubah' => date('Y-m-d H:i:s')
            );

        $this->db->where('id_pengguna', $id);
        $this->db->update('mst_pengguna', $data); 

    }

	public function from_dss()
    {
    	$query = "";

    	$run_query = $this->db_dss->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function from_kapi()
    {
    	$query = "";

    	$run_query = $this->db_dss->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}