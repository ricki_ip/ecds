<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_grup_pengguna extends CI_Model
{
    

    function __construct()
    {
        
    }

    public function list_opsi()
    {
        $query = "SELECT id_grup_pengguna AS id, nama_grup_pengguna as text FROM mst_grup_pengguna";

        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function delete($id)
    {   
        $data = array(
               'aktif' => 'Tidak',
               'id_pengguna_ubah' => $this->user->id_pengguna(),
               'tanggal_ubah' => date('Y-m-d H:i:s')
            );

        $this->db->where('id_grup_pengguna', $id);
        $this->db->update('mst_grup_pengguna', $data); 

    }
}