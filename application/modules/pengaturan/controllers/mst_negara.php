<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_negara extends MX_Controller {

  function __construct()
    {
      parent::__construct();
      $method = $this->router->method;
      $allowed_methods = ['index','view','list_negara_json', 'list_negara_array'];
      if(!in_array($method, $allowed_methods))
      {
        if(!$this->user->is_superadmin())
        {
          //echo "Access forbidden."; echo $this->router->method; echo $this->router->class;
          
        }
      }
      $this->load->config('menus');
      $this->load->config('db_timeline');
      $this->load->config('globals');
      $this->load->model('mdl_negara');

    }

  public function index()
  {
    $add_js = array('select2.min.js', 'jquery.dataTables.min.js');
    $add_css = array('select2.css', 'jquery.dataTables.css');

    $data['list_negara'] = $this->mdl_negara->list_negara();

    $template = 'templates/page/v_form';
    $modules = 'pengaturan';
    $views = 'table_negara';
    $labels = 'view_negara_table';
     
    echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
  }

  public function view($id_negara)
  { 
    $get_detail_negara = $this->mdl_negara->detail_negara($id_negara);
    // print_r($get_detail_negara);
    $data['detail_negara'] = (array) $get_detail_negara;
    
    $add_js = array('select2.min.js', 'jquery.validationEngine.js','jquery.validationEngine-en.js','jquery.dataTables.min.js');
    $add_css = array('select2.css', 'validationEngine.jquery.css','jquery.dataTables.css');
    echo Modules::run('templates/page/v_form', //tipe template
              'pengaturan', //nama module
              'details_negara', //nama file view
              'form_edit_negara', //dari labels.php
              $add_js, //plugin javascript khusus untuk page yang akan di load
              $add_css, //file css khusus untuk page yang akan di load
              $data); //array data yang akan digunakan di file view
  }

  public function entry()
  {
    $add_js = array('select2.min.js');
    $add_css = array('select2.css');


    $template = 'templates/page/v_form';
    $modules = 'pengaturan';
    $views = 'form_entry_negara';
    $labels = 'form_entry_negara';

    $data['detail_negara'] = FALSE;
    
    $data['aksi'] = 'entry';

      $data['submit_form'] = 'pengaturan/mst_negara/input';
  
    //$data['submit_form'] = 'pengaturan/mst_negara/input';

     
    echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
  }

  public function input()
  {
    $array_input = $this->input->post(NULL, TRUE);

    //var_dump($array_input);
    //die;

    if( $this->mdl_negara->input($array_input) ){
      $url = base_url('pengaturan/mst_negara');
      redirect($url);
    }else{
      $url = base_url('pengaturan/mst_negara');
      redirect($url);
    }
  }

  public function edit($id_negara)
  {

    $get_detail = $this->mdl_negara->detail_negara($id_negara);

    if( !$get_detail )
    {
      $data['detail_negara'] = FALSE;
    }else{
      $data['detail_negara'] = (array)$get_detail;
    }
    $data['id_negara'] = $id_negara;
    $data['submit_form'] = 'pengaturan/mst_negara/update';
    $add_js = array('select2.min.js', 'jquery.dataTables.min.js');
    $add_css = array('select2.css', 'jquery.dataTables.css');
    $template = 'templates/page/v_form';
    $modules = 'pengaturan';
    $views = 'form_entry_negara';
    $labels = 'form_edit_negara';
     
    echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

  }

  public function update()
  {
    $this->load->model('mdl_negara');

    $array_input = $this->input->post(NULL, TRUE);
    $id_negara = $array_input['id_negara'];

    // var_dump($array_input);

    if( $this->mdl_negara->update($id_negara, $array_input) ){
      //$this->mdl_negara->set_status_entry($id_negara);
      $url = base_url('pengaturan/mst_negara/view/'.$id_negara);
      // var_dump($url);
      // die;
      redirect($url);
    }else{
      $url = base_url('pengaturan/mst_negara/view/'.$id_pengguna);
      // var_dump($url);
      // die;
      redirect($url);
    }
  } 

  public function delete($id_negara)
  {
    $this->mdl_negara->delete($id_negara);
    $url = base_url('pengaturan/mst_negara/index');
    redirect($url);
  }

  public function activate($id_negara)
  {
    $this->mdl_negara->activate($id_negara);
    $url = base_url('pengaturan/mst_negara/index');
    redirect($url);
  }

  public function list_negara_json()
  {
    $this->load->model('mdl_negara');

    $list_opsi = $this->mdl_negara->list_opsi();

    echo json_encode($list_opsi);
  }

  public function list_negara_array()
  {
    $this->load->model('mdl_negara');

    $list_opsi = $this->mdl_negara->list_opsi();

    return $list_opsi;
  }
  
}
?>