<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_processing extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
			parent::__construct();
			$method = $this->router->method;
			$allowed_methods = ['index','view','list_processing_json', 'list_processing_array'];
			if(!in_array($method, $allowed_methods))
			{
				if(!$this->user->is_superadmin())
				{
					//echo "Access forbidden."; echo $this->router->method; echo $this->router->class;
					
				}
			}
			$this->load->config('menus');
			$this->load->config('db_timeline');
			$this->load->config('globals');
			$this->load->model('mdl_processing');
			
	}
	
	public function index()
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$data['list_processing'] = $this->mdl_processing->list_processing();

		$template = 'templates/page/v_form';
		$modules = 'pengaturan';
		$views = 'table_processing';
		$labels = 'view_processing_table';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}

	public function view($id_processing)
	{	
		$get_detail_processing = $this->mdl_processing->detail_processing($id_processing);
		// print_r($get_detail_processing);
		$data['detail_processing'] = (array) $get_detail_processing;
		
		$add_js = array('select2.min.js', 'jquery.validationEngine.js','jquery.validationEngine-en.js','jquery.dataTables.min.js');
		$add_css = array('select2.css', 'validationEngine.jquery.css','jquery.dataTables.css');
		echo Modules::run('templates/page/v_form', //tipe template
							'pengaturan', //nama module
							'details_processing', //nama file view
							'form_edit_processing', //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view
	}

	public function entry()
	{
		$add_js = array('select2.min.js');
		$add_css = array('select2.css');


		$template = 'templates/page/v_form';
		$modules = 'pengaturan';
		$views = 'form_entry_processing';
		$labels = 'form_entry_processing';

		$data['detail_processing'] = FALSE;
		
		$data['aksi'] = 'entry';

			$data['submit_form'] = 'pengaturan/mst_processing/input';
	
		//$data['submit_form'] = 'pengaturan/mst_processing/input';

		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}

	public function input()
	{
		$array_input = $this->input->post(NULL, TRUE);

		//var_dump($array_input);
		//die;

		if( $this->mdl_processing->input($array_input) ){
			$url = base_url('pengaturan/mst_processing');
			redirect($url);
		}else{
			$url = base_url('pengaturan/mst_processing');
			redirect($url);
		}
	}

	public function edit($id_processing)
	{

		$get_detail = $this->mdl_processing->detail_processing($id_processing);

		if( !$get_detail )
		{
			$data['detail_processing'] = FALSE;
		}else{
			$data['detail_processing'] = (array)$get_detail;
		}
		$data['id_processing'] = $id_processing;
		$data['submit_form'] = 'pengaturan/mst_processing/update';
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$template = 'templates/page/v_form';
		$modules = 'pengaturan';
		$views = 'form_entry_processing';
		$labels = 'form_edit_processing';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

	}

	public function update()
	{
		$this->load->model('mdl_processing');

		$array_input = $this->input->post(NULL, TRUE);
		$id_processing = $array_input['id_processing'];

		// var_dump($array_input);

		if( $this->mdl_processing->update($id_processing, $array_input) ){
			//$this->mdl_processing->set_status_entry($id_processing);
			$url = base_url('pengaturan/mst_processing/view/'.$id_processing);
			// var_dump($url);
			// die;
			redirect($url);
		}else{
			$url = base_url('pengaturan/mst_processing/edit/'.$id_processing);
			// var_dump($url);
			// die;
			redirect($url);
		}
	}

	public function delete($id_processing)
	{
		$this->mdl_processing->delete($id_processing);
		$url = base_url('pengaturan/mst_processing/index');
		redirect($url);
	}

	public function activate($id_processing)
	{
		$this->mdl_processing->activate($id_processing);
		$url = base_url('pengaturan/mst_processing/index');
		redirect($url);
	}

	public function list_processing_json($filter = 'all')
	{
		$this->load->model('mdl_processing');

		$list_opsi = $this->mdl_processing->list_opsi_processing($filter);

		echo json_encode($list_opsi);
	}

	public function list_processing_array($filter = 'all')
	{
		$this->load->model('mdl_processing');

		$list_opsi = $this->mdl_processing->list_opsi_processing($filter);

		return $list_opsi;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */