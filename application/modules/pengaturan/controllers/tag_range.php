<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tag_range extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			$method = $this->router->method;
			$allowed_methods = ['index','tahun', 'list_tag_range_code_json', 'list_tag_range_array'];
			if(!in_array($method, $allowed_methods))
			{
				if(!$this->user->is_superadmin())
				{
					//echo "Access forbidden."; echo $this->router->method; echo $this->router->class;
					
				}
			}
			$this->load->config('menus');
			$this->load->config('db_timeline');
			$this->load->config('globals');
			$this->load->model('mdl_tag_range');
			

		}

	// public function index()
	// {
	// 	$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
	// 	$add_css = array('select2.css', 'jquery.dataTables.css');

	// 	$data['list_tag_range'] = $this->mdl_tag_range->list_tag_range();

	// 	$template = 'templates/page/v_form';
	// 	$modules = 'pengaturan';
	// 	$views = 'table_tag_range';
	// 	$labels = 'view_tag_range_table';
		 
	// 	echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	// }

	public function json($tahun = "")
	{
		if(empty($tahun)){
			$get_detail_tag_range = $this->mdl_tag_range->detail_tag_range( date("Y") );
		}else{
			$get_detail_tag_range = $this->mdl_tag_range->detail_tag_range($tahun);
		}

		echo json_encode($get_detail_tag_range);
	}	
	public function tahun($tahun)
	{	
		$get_detail_tag_range = $this->mdl_tag_range->detail_tag_range($tahun);
		// print_r($get_detail_tag_range);
		$data['detail_tag_range'] = $get_detail_tag_range;
		
		
		
		$data['detail_tag_range'] = $get_detail_tag_range;

		$template = 'templates/page/v_form';
		$modules = 'pengaturan';
		$views = 'details_tag_range';
		
		$data['aksi'] = 'entry';

		$data['submit_form'] = 'pengaturan/tag_range/update';
	
		//$data['submit_form'] = 'pengaturan/tag_range/input';

		$add_js = array('select2.min.js', 'jquery.validationEngine.js','jquery.validationEngine-en.js','jquery.dataTables.min.js');
		$add_css = array('select2.css', 'validationEngine.jquery.css','jquery.dataTables.css');
		echo Modules::run($template, $modules, $views, '', $add_js, $add_css, $data);
	}

	public function index()
	{

		$get_list_tag_range = $this->mdl_tag_range->list_tag_range();
		// vdump($get_list_tag_range);
		// print_r($get_list_tag_range);
		$data['list_tag_range'] = $get_list_tag_range;

		$template = 'templates/page/v_form';
		$modules = 'pengaturan';
		$views = 'table_tag_range';
		
		$data['aksi'] = 'entry';

		$data['submit_form'] = 'pengaturan/tag_range/update';
	
		//$data['submit_form'] = 'pengaturan/tag_range/input';

		$add_js = array('select2.min.js', 'jquery.validationEngine.js','jquery.validationEngine-en.js','jquery.dataTables.min.js');
		$add_css = array('select2.css', 'validationEngine.jquery.css','jquery.dataTables.css');
		echo Modules::run($template, $modules, $views, '', $add_js, $add_css, $data);
	}

	public function input()
	{
		$array_input = $this->input->post(NULL, TRUE);

		//var_dump($array_input);
		//die;

		if( $this->mdl_tag_range->input($array_input) ){
			$url = base_url('pengaturan/quota');
			redirect($url);
		}else{
			$url = base_url('pengaturan/quota');
			redirect($url);
		}
	}

	public function edit($id_tag_range)
	{

		$get_detail = $this->mdl_tag_range->detail_tag_range($id_tag_range);

		if( !$get_detail )
		{
			$data['detail_tag_range'] = FALSE;
		}else{
			$data['detail_tag_range'] = (array)$get_detail;
		}
		$data['id_tag_range'] = $id_tag_range;
		$data['submit_form'] = 'pengaturan/tag_range/update';
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$template = 'templates/page/v_form';
		$modules = 'pengaturan';
		$views = 'form_entry_tag_range';
		$labels = 'form_edit_tag_range';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

	}

	public function update()
	{
		$this->load->model('mdl_tag_range');

		$array_input = $this->input->post(NULL, TRUE);
		$data['min'] = $array_input['min'];
		$data['max'] = $array_input['max'];
		// $data['tahun'] = $array_input['tahun'];
		// $id_tag_range = '0';
		unset($array_input['id_tag_range']);
		// var_dump($array_input);die;

		if( $this->mdl_tag_range->update($array_input['tahun'], $data) ){
			//$this->mdl_tag_range->set_status_entry($id_tag_range);
			$url = base_url('pengaturan/tag_range/tahun/'.$array_input['tahun']);
			// var_dump($url);
			// die;
			redirect($url);
		}else{
			$url = base_url('pengaturan/tag_range/');
			// var_dump($url);
			// die;
			redirect($url);
		}
	}	

	public function delete($id_tag_range)
	{
		$this->mdl_tag_range->delete($id_tag_range);
		$url = base_url('pengaturan/tag_range/index');
		redirect($url);
	}

	public function activate($id_tag_range)
	{
		$this->mdl_tag_range->activate($id_tag_range);
		$url = base_url('pengaturan/tag_range/index');
		redirect($url);
	}

	public function list_tag_range_code_json()
	{
		$this->load->model('mdl_tag_range');

		$list_opsi = $this->mdl_tag_range->list_opsi();

		echo json_encode($list_opsi);
	}

	public function list_tag_range_array()
	{
		$this->load->model('mdl_tag_range');

		$list_opsi = $this->mdl_tag_range->list_opsi();

		return $list_opsi;
	}
	
}
?>