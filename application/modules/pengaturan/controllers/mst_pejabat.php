<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_pejabat extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
			parent::__construct();
			$method = $this->router->method;
			$allowed_methods = ['index','view','list_pejabat_json', 'list_pejabat_array'];
			if(!in_array($method, $allowed_methods))
			{
				if(!$this->user->is_superadmin())
				{
					//echo "Access forbidden."; echo $this->router->method; echo $this->router->class;
					
				}
			}
			$this->load->config('menus');
			$this->load->config('db_timeline');
			$this->load->config('globals');
			$this->load->model('mdl_pejabat');
			
	}
	
	public function index()
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$data['list_pejabat'] = $this->mdl_pejabat->list_pejabat();

		$template = 'templates/page/v_form';
		$modules = 'pengaturan';
		$views = 'table_pejabat';
		$labels = 'view_pejabat_table';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}

	public function view($id_pejabat)
	{	
		$get_detail_pejabat = $this->mdl_pejabat->detail_pejabat($id_pejabat);
		// print_r($get_detail_pejabat);
		$data['detail_pejabat'] = (array) $get_detail_pejabat;
		
		$add_js = array('select2.min.js', 'jquery.validationEngine.js','jquery.validationEngine-en.js','jquery.dataTables.min.js');
		$add_css = array('select2.css', 'validationEngine.jquery.css','jquery.dataTables.css');
		echo Modules::run('templates/page/v_form', //tipe template
							'pengaturan', //nama module
							'details_pejabat', //nama file view
							'form_edit_pejabat', //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view
	}

	public function entry()
	{
		$add_js = array('select2.min.js');
		$add_css = array('select2.css');


		$template = 'templates/page/v_form';
		$modules = 'pengaturan';
		$views = 'form_entry_pejabat';
		$labels = 'form_entry_pejabat';

		$data['detail_pejabat'] = FALSE;
		
		$data['aksi'] = 'entry';

			$data['submit_form'] = 'pengaturan/mst_pejabat/input';
	
		//$data['submit_form'] = 'pengaturan/mst_pejabat/input';

		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}

	public function input()
	{
		$array_input = $this->input->post(NULL, TRUE);

		//var_dump($array_input);
		//die;

		if( $this->mdl_pejabat->input($array_input) ){
			$url = base_url('pengaturan/mst_pejabat');
			redirect($url);
		}else{
			$url = base_url('pengaturan/mst_pejabat');
			redirect($url);
		}
	}

	public function edit($id_pejabat)
	{

		$get_detail = $this->mdl_pejabat->detail_pejabat($id_pejabat);

		if( !$get_detail )
		{
			$data['detail_pejabat'] = FALSE;
		}else{
			$data['detail_pejabat'] = (array)$get_detail;
		}
		$data['id_pejabat'] = $id_pejabat;
		$data['submit_form'] = 'pengaturan/mst_pejabat/update';
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$template = 'templates/page/v_form';
		$modules = 'pengaturan';
		$views = 'form_entry_pejabat';
		$labels = 'form_edit_pejabat';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

	}

	public function update()
	{
		$this->load->model('mdl_pejabat');

		$array_input = $this->input->post(NULL, TRUE);
		$id_pejabat = $array_input['id_pejabat'];

		// var_dump($array_input);

		if( $this->mdl_pejabat->update($id_pejabat, $array_input) ){
			//$this->mdl_pejabat->set_status_entry($id_pejabat);
			$url = base_url('pengaturan/mst_pejabat/view/'.$id_pejabat);
			// var_dump($url);
			// die;
			redirect($url);
		}else{
			$url = base_url('pengaturan/mst_pejabat/view/'.$id_pengguna);
			// var_dump($url);
			// die;
			redirect($url);
		}
	}

	public function delete($id_pejabat)
	{
		$this->mdl_pejabat->delete($id_pejabat);
		$url = base_url('pengaturan/mst_pejabat/index');
		redirect($url);
	}

	public function activate($id_pejabat)
	{
		$this->mdl_pejabat->activate($id_pejabat);
		$url = base_url('pengaturan/mst_pejabat/index');
		redirect($url);
	}

	public function list_pejabat_json()
	{
		$this->load->model('mdl_pejabat');

		$list_opsi = $this->mdl_pejabat->list_opsi_pejabat();

		echo json_encode($list_opsi);
	}

	public function list_pejabat_array()
	{
		$this->load->model('mdl_pejabat');

		$list_opsi = $this->mdl_pejabat->list_opsi();

		return $list_opsi;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */