<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_asosiasi extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{			
			parent::__construct();
			$this->load->config('menus');
			$this->load->config('db_timeline');
			$this->load->config('globals');
			$this->load->model('mdl_asosiasi');

		$method = $this->router->method;
		$allowed_methods = ['index','list_asosiasi_json', 'list_asosiasi_array'];
		if(!in_array($method, $allowed_methods))
		{
			if(!$this->user->is_superadmin())
			{
				//echo "Access forbidden."; echo $this->router->method; echo $this->router->class;
				
			}
		}
			
	}

	
	public function index()
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$data['list_asosiasi'] = $this->mdl_asosiasi->list_asosiasi();

		$template = 'templates/page/v_form';
		$modules = 'pengaturan';
		$views = 'table_asosiasi';
		$labels = 'view_asosiasi_table';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}

	public function view($id_asosiasi)
	{	
		$get_detail_asosiasi = $this->mdl_asosiasi->detail_asosiasi($id_asosiasi);
		// print_r($get_detail_asosiasi);
		$data['detail_asosiasi'] = (array) $get_detail_asosiasi;
		
		$add_js = array('select2.min.js', 'jquery.validationEngine.js','jquery.validationEngine-en.js','jquery.dataTables.min.js');
		$add_css = array('select2.css', 'validationEngine.jquery.css','jquery.dataTables.css');
		echo Modules::run('templates/page/v_form', //tipe template
							'pengaturan', //nama module
							'details_asosiasi', //nama file view
							'form_edit_asosiasi', //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view
	}

	public function entry()
	{

		$add_js = array('select2.min.js');
		$add_css = array('select2.css');


		$template = 'templates/page/v_form';
		$modules = 'pengaturan';
		$views = 'form_entry_asosiasi';
		$labels = 'form_entry_asosiasi';

		$data['detail_asosiasi'] = FALSE;
		
		$data['aksi'] = 'entry';

			$data['submit_form'] = 'pengaturan/mst_asosiasi/input';
	
		//$data['submit_form'] = 'pengaturan/mst_asosiasi/input';

		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}

	public function input()
	{	
		if($this->user->level() < 3){
				$array_input = $this->input->post(NULL, TRUE);

			//var_dump($array_input);
			//die;

			if( $this->mdl_asosiasi->input($array_input) ){
				$url = base_url('pengaturan/mst_asosiasi');
				redirect($url);
			}else{
				$url = base_url('pengaturan/mst_asosiasi');
				redirect($url);
			}
		}else{
			echo "Anda tidak diizinkan mengakses/menggunakan fungsi ini.";
		}
	}

	public function edit($id_asosiasi)
	{

		$get_detail = $this->mdl_asosiasi->detail_asosiasi($id_asosiasi);

		if( !$get_detail )
		{
			$data['detail_asosiasi'] = FALSE;
		}else{
			$data['detail_asosiasi'] = (array)$get_detail;
		}
		$data['id_asosiasi'] = $id_asosiasi;
		$data['submit_form'] = 'pengaturan/mst_asosiasi/update';
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$template = 'templates/page/v_form';
		$modules = 'pengaturan';
		$views = 'form_entry_asosiasi';
		$labels = 'form_edit_asosiasi';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

	}

	public function update()
	{
		if($this->user->level() < 3){

			$this->load->model('mdl_asosiasi');

			$array_input = $this->input->post(NULL, TRUE);
			$id_asosiasi = $array_input['id_asosiasi'];

			// var_dump($array_input);

			if( $this->mdl_asosiasi->update($id_asosiasi, $array_input) ){
				//$this->mdl_asosiasi->set_status_entry($id_asosiasi);
				$url = base_url('pengaturan/mst_asosiasi/view/'.$id_asosiasi);
				// var_dump($url);
				// die;
				redirect($url);
			}else{
				$url = base_url('pengaturan/mst_asosiasi/edit/'.$id_asosiasi);
				// var_dump($url);
				// die;
				redirect($url);
			}
		}else{
			echo "Anda tidak diizinkan mengakses/menggunakan fungsi ini.";
		}
	}

	public function delete($id_asosiasi)
	{
		if($this->user->level() < 3){

			$this->mdl_asosiasi->delete($id_asosiasi);
			$url = base_url('pengaturan/mst_asosiasi/index');
			redirect($url);
		}else{
			echo "Anda tidak diizinkan mengakses/menggunakan fungsi ini.";
		}
	}

	public function activate($id_asosiasi)
	{
		if($this->user->level() < 3){
			$this->mdl_asosiasi->activate($id_asosiasi);
			$url = base_url('pengaturan/mst_asosiasi/index');
			redirect($url);
		}else{
			echo "Anda tidak diizinkan mengakses/menggunakan fungsi ini.";
		}
	}

	public function list_asosiasi_json($filter = 'all')
	{
		$this->load->model('mdl_asosiasi');

		$list_opsi = $this->mdl_asosiasi->list_opsi_asosiasi($filter);

		echo json_encode($list_opsi);
	}

	public function list_asosiasi_array($filter = 'all')
	{
		$this->load->model('mdl_asosiasi');

		$list_opsi = $this->mdl_asosiasi->list_opsi_asosiasi($filter);

		return $list_opsi;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */