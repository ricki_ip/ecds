<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_pengguna extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
			parent::__construct();
			$method = $this->router->method;
			$allowed_methods = ['index','view'];
			if(!in_array($method, $allowed_methods))
			{
				if(!$this->user->is_superadmin())
				{
					//echo "Access forbidden."; echo $this->router->method; echo $this->router->class;
					
				}
			}
			$this->load->config('menus');
			$this->load->config('db_timeline');
			$this->load->config('globals');
			$this->load->model('mdl_pengguna');
			//$this->load->model('mdl_lokasi');
			
	}
	
	public function index()
	{
		if( $this->user->level() > 2 )
		{
			$this->view( $this->user->id_pengguna() );
		}else{
			$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
			$add_css = array('select2.css', 'jquery.dataTables.css');

			$data['list_pengguna'] = $this->mdl_pengguna->list_pengguna();

			$template = 'templates/page/v_form';
			$modules = 'pengaturan';
			$views = 'table_pengguna';
			$labels = 'view_pengguna_table';
			 
			echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
		}
	}

	public function view($id_pengguna)
	{	
		$get_detail_pengguna = $this->mdl_pengguna->detail_pengguna($id_pengguna);
		// print_r($get_detail_pengguna);
		$data['detail_pengguna'] = (array) $get_detail_pengguna;
		
		$add_js = array('select2.min.js', 'jquery.validationEngine.js','jquery.validationEngine-en.js','jquery.dataTables.min.js');
		$add_css = array('select2.css', 'validationEngine.jquery.css','jquery.dataTables.css');
		echo Modules::run('templates/page/v_form', //tipe template
							'pengaturan', //nama module
							'details_pengguna', //nama file view
							'form_edit_pengguna', //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view
	}

	public function entry()
	{
		$add_js = array('select2.min.js', 'jquery.validationEngine.js','jquery.validationEngine-en.js','jquery.dataTables.min.js');
		$add_css = array('select2.css', 'validationEngine.jquery.css','jquery.dataTables.css');

		$data['detail_pengguna'] = FALSE;
		
		$data['aksi'] = 'entry';

		$data['submit_form'] = 'pengaturan/mst_pengguna/input';
	
		//$data['submit_form'] = 'pengaturan/mst_pengguna/input';

		 
		echo Modules::run('templates/page/v_form', //tipe template
							'pengaturan', //nama module
							'form_entry_pengguna', //nama file view
							'form_entry_pengguna', //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view
	}

	public function input()
	{
		$array_input = $this->input->post(NULL, TRUE);

		//var_dump($array_input);
		//die;

		if( $this->mdl_pengguna->input($array_input) ){
			$url = base_url('pengaturan/mst_pengguna');
			redirect($url);
		}else{
			$url = base_url('pengaturan/mst_pengguna');
			redirect($url);
		}
	}

	public function edit($id_pengguna)
	{

		$get_detail = $this->mdl_pengguna->detail_pengguna($id_pengguna);

		if( !$get_detail )
		{
			$data['detail_pengguna'] = FALSE;
		}else{
			$data['detail_pengguna'] = (array)$get_detail;
		}
		$data['id_pengguna'] = $id_pengguna;
		$data['submit_form'] = 'pengaturan/mst_pengguna/update';
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$template = 'templates/page/v_form';
		$modules = 'pengaturan';
		$views = 'form_entry_pengguna';
		$labels = 'form_edit_pengguna';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

	}

	public function update()
	{
		$this->load->model('mdl_pengguna');

		$array_input = $this->input->post(NULL, TRUE);
		$id_pengguna = $array_input['id_pengguna'];

		// var_dump($array_input);

		if( $this->mdl_pengguna->update($id_pengguna, $array_input) ){
			//$this->mdl_pengguna->set_status_entry($id_pengguna);
			$url = base_url('pengaturan/mst_pengguna/view/'.$id_pengguna);
			// var_dump($url);
			// die;
			redirect($url);
		}else{
			$url = base_url('pengaturan/mst_pengguna/view/'.$id_pengguna);
			// var_dump($url);
			// die;
			redirect($url);
		}
	}

	public function delete($id_pengguna)
	{
		$this->mdl_pengguna->delete($id_pengguna);
		$url = base_url('pengaturan/mst_pengguna/index');
		redirect($url);
	}

	public function activate($id_pengguna)
	{
		$this->mdl_pengguna->activate($id_pengguna);
		$url = base_url('pengaturan/mst_pengguna/index');
		redirect($url);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */