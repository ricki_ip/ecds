<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Quota extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			$method = $this->router->method;
			$allowed_methods = ['index','tahun', 'list_quota_code_json', 'list_quota_array'];
			if(!in_array($method, $allowed_methods))
			{
				if(!$this->user->is_superadmin())
				{
					//echo "Access forbidden."; echo $this->router->method; echo $this->router->class;
					
				}
			}
			$this->load->config('menus');
			$this->load->config('db_timeline');
			$this->load->config('globals');
			$this->load->model('mdl_quota');
			

		}

	// public function index()
	// {
	// 	$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
	// 	$add_css = array('select2.css', 'jquery.dataTables.css');

	// 	$data['list_quota'] = $this->mdl_quota->list_quota();

	// 	$template = 'templates/page/v_form';
	// 	$modules = 'pengaturan';
	// 	$views = 'table_quota';
	// 	$labels = 'view_quota_table';
		 
	// 	echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	// }

	public function tahun($tahun)
	{	
		$get_detail_quota = $this->mdl_quota->detail_quota($tahun);
		// print_r($get_detail_quota);
		$data['detail_quota'] = $get_detail_quota;
		
		
		
		$data['detail_quota'] = $get_detail_quota;

		$template = 'templates/page/v_form';
		$modules = 'pengaturan';
		$views = 'details_quota';
		
		$data['aksi'] = 'entry';

		$data['submit_form'] = 'pengaturan/quota/update';
	
		//$data['submit_form'] = 'pengaturan/quota/input';

		$add_js = array('select2.min.js', 'jquery.validationEngine.js','jquery.validationEngine-en.js','jquery.dataTables.min.js');
		$add_css = array('select2.css', 'validationEngine.jquery.css','jquery.dataTables.css');
		echo Modules::run($template, $modules, $views, '', $add_js, $add_css, $data);
	}

	public function index()
	{

		$get_list_quota = $this->mdl_quota->list_quota();
		// vdump($get_list_quota);
		// print_r($get_list_quota);
		$data['list_quota'] = $get_list_quota;

		$template = 'templates/page/v_form';
		$modules = 'pengaturan';
		$views = 'table_quota';
		
		$data['aksi'] = 'entry';

		$data['submit_form'] = 'pengaturan/quota/update';
	
		//$data['submit_form'] = 'pengaturan/quota/input';

		$add_js = array('select2.min.js', 'jquery.validationEngine.js','jquery.validationEngine-en.js','jquery.dataTables.min.js');
		$add_css = array('select2.css', 'validationEngine.jquery.css','jquery.dataTables.css');
		echo Modules::run($template, $modules, $views, '', $add_js, $add_css, $data);
	}

	public function input()
	{
		$array_input = $this->input->post(NULL, TRUE);

		//var_dump($array_input);
		//die;

		if( $this->mdl_quota->input($array_input) ){
			$url = base_url('pengaturan/quota');
			redirect($url);
		}else{
			$url = base_url('pengaturan/quota');
			redirect($url);
		}
	}

	public function edit($id_quota)
	{

		$get_detail = $this->mdl_quota->detail_quota($id_quota);

		if( !$get_detail )
		{
			$data['detail_quota'] = FALSE;
		}else{
			$data['detail_quota'] = (array)$get_detail;
		}
		$data['id_quota'] = $id_quota;
		$data['submit_form'] = 'pengaturan/quota/update';
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$template = 'templates/page/v_form';
		$modules = 'pengaturan';
		$views = 'form_entry_quota';
		$labels = 'form_edit_quota';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

	}

	public function update()
	{
		$this->load->model('mdl_quota');

		$array_input = $this->input->post(NULL, TRUE);
		$data['quota'] = $array_input['quota_value'];
		$data['treshold'] = $array_input['treshold']/100;
		$id_quota = '0';
		// unset($array_input['id_quota']);
		// var_dump($array_input);

		if( $this->mdl_quota->update($id_quota, $data) ){
			//$this->mdl_quota->set_status_entry($id_quota);
			$url = base_url('pengaturan/quota/');
			// var_dump($url);
			// die;
			redirect($url);
		}else{
			$url = base_url('pengaturan/quota/');
			// var_dump($url);
			// die;
			redirect($url);
		}
	}	

	public function delete($id_quota)
	{
		$this->mdl_quota->delete($id_quota);
		$url = base_url('pengaturan/quota/index');
		redirect($url);
	}

	public function activate($id_quota)
	{
		$this->mdl_quota->activate($id_quota);
		$url = base_url('pengaturan/quota/index');
		redirect($url);
	}

	public function list_quota_code_json()
	{
		$this->load->model('mdl_quota');

		$list_opsi = $this->mdl_quota->list_opsi();

		echo json_encode($list_opsi);
	}

	public function list_quota_array()
	{
		$this->load->model('mdl_quota');

		$list_opsi = $this->mdl_quota->list_opsi();

		return $list_opsi;
	}
	
}
?>