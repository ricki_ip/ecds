<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_grup_pengguna extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			
			
		}

	public function list_grup_pengguna_code_json()
	{
		$this->load->model('mdl_grup_pengguna');

		$list_opsi = $this->mdl_grup_pengguna->list_opsi();

		echo json_encode($list_opsi);
	}

	public function list_grup_pengguna_array()
	{
		$this->load->model('mdl_grup_pengguna');

		$list_opsi = $this->mdl_grup_pengguna->list_opsi();

		return $list_opsi;
	}
	
}
?>