<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_kabupaten_kota extends MX_Controller {

  function __construct()
    {
      parent::__construct();
      $method = $this->router->method;
      $allowed_methods = ['index','view','list_kabupaten_kota_json', 'list_kabupaten_kota_array'];
      if(!in_array($method, $allowed_methods))
      {
        if(!$this->user->is_superadmin())
        {
          //echo "Access forbidden."; echo $this->router->method; echo $this->router->class;
          
        }
      }
      $this->load->config('menus');
      $this->load->config('db_timeline');
      $this->load->config('globals');
      $this->load->model('mdl_kabupaten_kota');

    }

  public function index()
  {
    $add_js = array('select2.min.js', 'jquery.dataTables.min.js');
    $add_css = array('select2.css', 'jquery.dataTables.css');

    $data['list_kabupaten_kota'] = $this->mdl_kabupaten_kota->list_kabupaten_kota();

    $template = 'templates/page/v_form';
    $modules = 'pengaturan';
    $views = 'table_kabupaten_kota';
    $labels = 'view_kabupaten_kota_table';
     
    echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
  }

  public function view($id_kabupaten_kota)
  { 
    $get_detail_kabupaten_kota = $this->mdl_kabupaten_kota->detail_kabupaten_kota($id_kabupaten_kota);
    // print_r($get_detail_kabupaten_kota);
    $data['detail_kabupaten_kota'] = (array) $get_detail_kabupaten_kota;
    
    $add_js = array('select2.min.js', 'jquery.validationEngine.js','jquery.validationEngine-en.js','jquery.dataTables.min.js');
    $add_css = array('select2.css', 'validationEngine.jquery.css','jquery.dataTables.css');
    echo Modules::run('templates/page/v_form', //tipe template
              'pengaturan', //nama module
              'details_kabupaten_kota', //nama file view
              'form_edit_kabupaten_kota', //dari labels.php
              $add_js, //plugin javascript khusus untuk page yang akan di load
              $add_css, //file css khusus untuk page yang akan di load
              $data); //array data yang akan digunakan di file view
  }

  public function entry()
  {
    $add_js = array('select2.min.js');
    $add_css = array('select2.css');


    $template = 'templates/page/v_form';
    $modules = 'pengaturan';
    $views = 'form_entry_kabupaten_kota';
    $labels = 'form_entry_kabupaten_kota';

    $data['detail_kabupaten_kota'] = FALSE;
    
    $data['aksi'] = 'entry';

      $data['submit_form'] = 'pengaturan/mst_kabupaten_kota/input';
  
    //$data['submit_form'] = 'pengaturan/mst_kabupaten_kota/input';

     
    echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
  }

  public function input()
  {
    $array_input = $this->input->post(NULL, TRUE);

    //var_dump($array_input);
    //die;

    if( $this->mdl_kabupaten_kota->input($array_input) ){
      $url = base_url('pengaturan/mst_kabupaten_kota');
      redirect($url);
    }else{
      $url = base_url('pengaturan/mst_kabupaten_kota');
      redirect($url);
    }
  }

  public function edit($id_kabupaten_kota)
  {

    $get_detail = $this->mdl_kabupaten_kota->detail_kabupaten_kota($id_kabupaten_kota);

    if( !$get_detail )
    {
      $data['detail_kabupaten_kota'] = FALSE;
    }else{
      $data['detail_kabupaten_kota'] = (array)$get_detail;
    }
    $data['id_kabupaten_kota'] = $id_kabupaten_kota;
    $data['submit_form'] = 'pengaturan/mst_kabupaten_kota/update';
    $add_js = array('select2.min.js', 'jquery.dataTables.min.js');
    $add_css = array('select2.css', 'jquery.dataTables.css');
    $template = 'templates/page/v_form';
    $modules = 'pengaturan';
    $views = 'form_entry_kabupaten_kota';
    $labels = 'form_edit_kabupaten_kota';
     
    echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

  }

  public function update()
  {
    $this->load->model('mdl_kabupaten_kota');

    $array_input = $this->input->post(NULL, TRUE);
    $id_kabupaten_kota = $array_input['id_kabupaten_kota'];

    // var_dump($array_input);

    if( $this->mdl_kabupaten_kota->update($id_kabupaten_kota, $array_input) ){
      //$this->mdl_kabupaten_kota->set_status_entry($id_kabupaten_kota);
      $url = base_url('pengaturan/mst_kabupaten_kota/view/'.$id_kabupaten_kota);
      // var_dump($url);
      // die;
      redirect($url);
    }else{
      $url = base_url('pengaturan/mst_kabupaten_kota/view/'.$id_pengguna);
      // var_dump($url);
      // die;
      redirect($url);
    }
  }  

  public function list_kabupaten_kota_json($with_propinsi = 'propinsi')
  {
    $this->load->model('mdl_kabupaten_kota');

    if($with_propinsi === 'propinsi')
    {
      $list_opsi = $this->mdl_kabupaten_kota->list_opsi_with_propinsi();   
    }else{
      $list_opsi = $this->mdl_kabupaten_kota->list_opsi();   
    }

    echo json_encode($list_opsi);
  }

  public function list_kabupaten_kota_array()
  {
    $this->load->model('mdl_lokasi');

    $list_opsi = $this->mdl_lokasi->list_opsi();

    return $list_opsi;
  }
  
}
?>