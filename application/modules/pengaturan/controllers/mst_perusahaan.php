<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_perusahaan extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
			parent::__construct();
			$method = $this->router->method;
			$allowed_methods = ['index','view','list_perusahaan_json', 'list_perusahaan_atli_json', 'list_perusahaan_array'];
			if(!in_array($method, $allowed_methods))
			{
				if(!$this->user->is_superadmin())
				{
					//echo "Access forbidden."; echo $this->router->method; echo $this->router->class;
					
				}
			}
			$this->load->config('menus');
			$this->load->config('db_timeline');
			$this->load->config('globals');
			$this->load->model('mdl_perusahaan');
			
	}
	
	public function index()
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$data['list_perusahaan'] = $this->mdl_perusahaan->list_perusahaan();

		$template = 'templates/page/v_form';
		$modules = 'pengaturan';
		$views = 'table_perusahaan';
		$labels = 'view_perusahaan_table';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}

	public function view($id_perusahaan)
	{	
		$get_detail_perusahaan = $this->mdl_perusahaan->detail_perusahaan($id_perusahaan);
		// print_r($get_detail_perusahaan);
		$data['detail_perusahaan'] = (array) $get_detail_perusahaan;
		
		$add_js = array('select2.min.js', 'jquery.validationEngine.js','jquery.validationEngine-en.js','jquery.dataTables.min.js');
		$add_css = array('select2.css', 'validationEngine.jquery.css','jquery.dataTables.css');
		echo Modules::run('templates/page/v_form', //tipe template
							'pengaturan', //nama module
							'details_perusahaan', //nama file view
							'form_edit_perusahaan', //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view
	}

	public function entry()
	{
		$add_js = array('select2.min.js');
		$add_css = array('select2.css');


		$template = 'templates/page/v_form';
		$modules = 'pengaturan';
		$views = 'form_entry_perusahaan';
		$labels = 'form_entry_perusahaan';

		$data['detail_perusahaan'] = FALSE;
		
		$data['aksi'] = 'entry';

			$data['submit_form'] = 'pengaturan/mst_perusahaan/input';
	
		//$data['submit_form'] = 'pengaturan/mst_perusahaan/input';

		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}

	public function input()
	{
		$array_input = $this->input->post(NULL, TRUE);

		//var_dump($array_input);
		//die;

		if( $this->mdl_perusahaan->input($array_input) ){
			$url = base_url('pengaturan/mst_perusahaan');
			redirect($url);
		}else{
			$url = base_url('pengaturan/mst_perusahaan');
			redirect($url);
		}
	}

	public function edit($id_perusahaan)
	{

		$get_detail = $this->mdl_perusahaan->detail_perusahaan($id_perusahaan);

		if( !$get_detail )
		{
			$data['detail_perusahaan'] = FALSE;
		}else{
			$data['detail_perusahaan'] = (array)$get_detail;
		}
		$data['id_perusahaan'] = $id_perusahaan;
		$data['submit_form'] = 'pengaturan/mst_perusahaan/update';
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$template = 'templates/page/v_form';
		$modules = 'pengaturan';
		$views = 'form_entry_perusahaan';
		$labels = 'form_edit_perusahaan';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

	}

	public function update()
	{
		$this->load->model('mdl_perusahaan');

		$array_input = $this->input->post(NULL, TRUE);
		$id_perusahaan = $array_input['id_perusahaan'];

		// var_dump($array_input);

		if( $this->mdl_perusahaan->update($id_perusahaan, $array_input) ){
			//$this->mdl_perusahaan->set_status_entry($id_perusahaan);
			$url = base_url('pengaturan/mst_perusahaan/view/'.$id_perusahaan);
			// var_dump($url);
			// die;
			redirect($url);
		}else{
			$url = base_url('pengaturan/mst_perusahaan/edit/'.$id_perusahaan);
			// var_dump($url);
			// die;
			redirect($url);
		}
	}

	public function delete($id_perusahaan)
	{
		$this->mdl_perusahaan->delete($id_perusahaan);
		$url = base_url('pengaturan/mst_perusahaan/index');
		redirect($url);
	}

	public function activate($id_perusahaan)
	{
		$this->mdl_perusahaan->activate($id_perusahaan);
		$url = base_url('pengaturan/mst_perusahaan/index');
		redirect($url);
	}

	public function list_perusahaan_json($filter = 'all')
	{
		$this->load->model('mdl_perusahaan');

		$list_opsi = $this->mdl_perusahaan->list_opsi_perusahaan($filter);

		echo json_encode($list_opsi);
	}


	public function list_perusahaan_atli_json($filter = 'all')
	{
		$this->load->model('mdl_perusahaan');

		$list_opsi = $this->mdl_perusahaan->list_perusahaan_atli_json($filter);

		echo json_encode($list_opsi);
	}

	public function list_perusahaan_array($filter = 'all')
	{
		$this->load->model('mdl_perusahaan');

		$list_opsi = $this->mdl_perusahaan->list_opsi_perusahaan($filter);

		return $list_opsi;
	}

	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */