<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_lokasi extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			$method = $this->router->method;
			$allowed_methods = ['index','list_lokasi_code_json', 'list_lokasi_array'];
			if(!in_array($method, $allowed_methods))
			{
				if(!$this->user->is_superadmin())
				{
					//echo "Access forbidden."; echo $this->router->method; echo $this->router->class;
					
				}
			}
			$this->load->config('menus');
			$this->load->config('db_timeline');
			$this->load->config('globals');
			$this->load->model('mdl_lokasi');
			

		}

	public function index()
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$data['list_lokasi'] = $this->mdl_lokasi->list_lokasi();

		$template = 'templates/page/v_form';
		$modules = 'pengaturan';
		$views = 'table_lokasi';
		$labels = 'view_lokasi_table';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}

	public function view($id_lokasi)
	{	
		$get_detail_lokasi = $this->mdl_lokasi->detail_lokasi($id_lokasi);
		// print_r($get_detail_lokasi);
		$data['detail_lokasi'] = (array) $get_detail_lokasi;
		
		$add_js = array('select2.min.js', 'jquery.validationEngine.js','jquery.validationEngine-en.js','jquery.dataTables.min.js');
		$add_css = array('select2.css', 'validationEngine.jquery.css','jquery.dataTables.css');
		echo Modules::run('templates/page/v_form', //tipe template
							'pengaturan', //nama module
							'details_lokasi', //nama file view
							'form_edit_lokasi', //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view
	}

	public function entry()
	{
		$add_js = array('select2.min.js');
		$add_css = array('select2.css');


		$template = 'templates/page/v_form';
		$modules = 'pengaturan';
		$views = 'form_entry_lokasi';
		$labels = 'form_entry_lokasi';

		$data['detail_lokasi'] = FALSE;
		
		$data['aksi'] = 'entry';

			$data['submit_form'] = 'pengaturan/mst_lokasi/input';
	
		//$data['submit_form'] = 'pengaturan/mst_lokasi/input';

		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}

	public function input()
	{
		$array_input = $this->input->post(NULL, TRUE);

		//var_dump($array_input);
		//die;

		if( $this->mdl_lokasi->input($array_input) ){
			$url = base_url('pengaturan/mst_lokasi');
			redirect($url);
		}else{
			$url = base_url('pengaturan/mst_lokasi');
			redirect($url);
		}
	}

	public function edit($id_lokasi)
	{

		$get_detail = $this->mdl_lokasi->detail_lokasi($id_lokasi);

		if( !$get_detail )
		{
			$data['detail_lokasi'] = FALSE;
		}else{
			$data['detail_lokasi'] = (array)$get_detail;
		}
		$data['id_lokasi'] = $id_lokasi;
		$data['submit_form'] = 'pengaturan/mst_lokasi/update';
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$template = 'templates/page/v_form';
		$modules = 'pengaturan';
		$views = 'form_entry_lokasi';
		$labels = 'form_edit_lokasi';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

	}

	public function update()
	{
		$this->load->model('mdl_lokasi');

		$array_input = $this->input->post(NULL, TRUE);
		$id_lokasi = $array_input['id_lokasi'];

		// var_dump($array_input);

		if( $this->mdl_lokasi->update($id_lokasi, $array_input) ){
			//$this->mdl_lokasi->set_status_entry($id_lokasi);
			$url = base_url('pengaturan/mst_lokasi/view/'.$id_lokasi);
			// var_dump($url);
			// die;
			redirect($url);
		}else{
			$url = base_url('pengaturan/mst_lokasi/view/'.$id_pengguna);
			// var_dump($url);
			// die;
			redirect($url);
		}
	}	

	public function delete($id_lokasi)
	{
		$this->mdl_lokasi->delete($id_lokasi);
		$url = base_url('pengaturan/mst_lokasi/index');
		redirect($url);
	}

	public function activate($id_lokasi)
	{
		$this->mdl_lokasi->activate($id_lokasi);
		$url = base_url('pengaturan/mst_lokasi/index');
		redirect($url);
	}

	public function list_lokasi_code_json()
	{
		$this->load->model('mdl_lokasi');

		$list_opsi = $this->mdl_lokasi->list_opsi();

		echo json_encode($list_opsi);
	}

	public function list_lokasi_array()
	{
		$this->load->model('mdl_lokasi');

		$list_opsi = $this->mdl_lokasi->list_opsi();

		return $list_opsi;
	}
	
}
?>