<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
  
class Mdl_login extends CI_Model {
 
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
 
    function login($username) {
        //create query to connect user login database
        $this->db->select('mst_pengguna.id_pengguna,
                           mst_pengguna.nama_pengguna,
                           mst_pengguna.kode_pengguna AS username,
                           mst_pengguna.kata_kunci AS password,
                           mst_pengguna.id_perusahaan,
                           mst_pengguna.id_grup_pengguna,
                           mst_pengguna.id_asosiasi AS id_asosiasi_user,                           
                           mst_lokasi.nama_lokasi,
                           mst_lokasi.code_area,
                           mst_grup_pengguna.nama_grup_pengguna,
                           mst_perusahaan.company_name,
                           mst_perusahaan.id_asosiasi,
                           mst_perusahaan.kuota_perus_awal,
                           mst_asosiasi.asosiasi_name,
                           mst_asosiasi.kuota_asos_awal');
        $this->db->from('mst_pengguna');
        $this->db->join('mst_lokasi','mst_lokasi.id_lokasi = mst_pengguna.id_lokasi','left');
        $this->db->join('mst_perusahaan','mst_perusahaan.id_perusahaan = mst_pengguna.id_perusahaan','left');
        $this->db->join('mst_asosiasi','mst_asosiasi.id_asosiasi = mst_perusahaan.id_asosiasi','left');        
        $this->db->join('mst_grup_pengguna','mst_grup_pengguna.id_grup_pengguna = mst_pengguna.id_grup_pengguna','left');
        $this->db->where('kode_pengguna', $username);
        $this->db->limit(1);
         
        //get query and processing
        $query = $this->db->get();
        if($query->num_rows() == 1) { 
            return $query->result_array(); //if data is true
        } else {
            return false; //if data is wrong
        }
    }

    function last_login($id_pengguna) {
      $data = array(
               'last_login' => date('Y-m-d H:i:s')
            );
      // var_dump(date('Y-m-d H:i:s'));
      // var_dump( date_default_timezone_get());
      // die;
      $this->db->where('id_pengguna', $id_pengguna);
      $this->db->update('mst_pengguna', $data); 

    }
}