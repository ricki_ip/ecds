<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
	{
			parent::__construct();
			$this->load->config('menus');
			$this->load->config('db_timeline');
			$this->load->config('globals');
            $this->load->model('mdl_login');
        $this->load->helper(array('form', 'url','html'));
        $this->load->library(array('form_validation','session'));
			
	}

	function index() {
		// vdump($this->auth->loggedin());

		if ($this->auth->loggedin()) {
            redirect('dashboard');
    }else{
      $data['error'] = '';
        // form submitted
        if ($this->input->post('username') && $this->input->post('password')) {
            $remember = $this->input->post('remember') ? TRUE : FALSE;
            
            // get user from database
            
            $user = $this->mdl_login->login($this->input->post('username'));
            
            if ($user) {
                $userdata = $user[0];
                // vdump($userdata);
                // compare passwords BELUM PAKE MD5
                if ($this->input->post('password') === $userdata['password']) {
                    // mark userdata as logged in
                    $this->auth->login($userdata['id_pengguna'], $remember);
                    unset($userdata['password']); // Lepas password dari result
                    $this->session->set_userdata('info',$userdata); // store di session
                    // vdump($userdata, true);
                    $this->mdl_login->last_login($userdata['id_pengguna']);
                    redirect('dashboard');
                } else {

                    $data['error'] = 'Wrong username or password';
                }
            } else {
                $data['error'] = 'Username does not exist';
            }
        }

    }
          $data['submit_form'] = 'login';
    
          // vdump($data);
          $this->v_login( //tipe template
                    'login', //nama module
                    'v_login', //nama file view
                    'view_login', //nama file form
                    '', //plugin javascript khusus untuk page yang akan di load
                    '', //file css khusus untuk page yang akan di load
                    $data); //array data yang akan digunakan di file view

   }

   public function v_login($nama_module, $file_view, $file_form, $add_js, $add_css, $container_data)
  {

    if( empty($item_label) )
    {
      $form_constants['title']['page'] = 'CDS - Login';
      $form_constants['title']['panel'] = '';
      $form_constants['form'] = '';
    }else{
      $form_constants = $this->config->item($item_label);
    }

    
    $menus_constants = $this->config->item('lists');
    $settings_menus_constants = $this->config->item('admin_lists');

    $global_constants = $this->config->item('app');
    $assets_paths = $this->config->item('assets_paths');


    $head_data['page_title'] = $form_constants['title']['page'];
    $head_data['paths'] = $assets_paths;
    $head_data['additional_js'] = empty($add_js) ? FALSE : $add_js;
    $head_data['additional_css'] = empty($add_css) ? FALSE : $add_css;
    
    $app_data['app_title'] = $global_constants['header_title'];
    $app_data['app_footer'] = $global_constants['footer_text'];
    $app_data['logo_url'] = $assets_paths['kapi_images'];
    // $app_data['total_captured_weight'] = $this->total_captured_weight;
    // $app_data['total_captured_quota'] = $this->total_captured_quota;
    // $app_data['selisih_quota'] = $this->selisih_quota;
    // $app_data['percent_captured'] = $this->percent_captured;
    // vdump($container_data);
    $container_data['nama_module'] = $nama_module;
    $container_data['file_view'] = $file_view;
    $container_data['file_form'] = $file_form;

    // $this->load->view($nama_module.'/'.$file_view, $container_data);
// =======
    $this->load->view('templates/login_head', $head_data);
    // $this->load->view('div_header', $app_data);
    $this->load->view('templates/div_login', $container_data);
    $this->load->view('templates/html_footer', $app_data);
  }

   function out(){
   	$this->auth->logout();
		$this->session->set_userdata('info',FALSE); // store di session
   	redirect('login');
   }

   function verify() {
        $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');
 
        if($this->form_validation->run() === FALSE) {
            
                redirect(base_url('#'), 'refresh');
           
        } else {
                //Go to private area
                redirect(base_url('dashboard'), 'refresh');
        }       
    }
	 /*
	buat session set_userdata "logged_in"
	untuk session 
	  - id_pengguna
	  - nama_pengguna
	  - id_gurp_pengguna
	  - id_perusahaan
	*/
	function check_database($password) {
	   //Field validation succeeded.  Validate against database
	   $username = $this->input->post('username');
	   //query the database
	   $result = $this->login->login($username, $password);
	   if($result) {
	       $sess_array = array();
	       foreach($result as $row) {
	           //create the session
	           // $sess_array = array('id_pengguna' => $row->id_pengguna,
	           //     'nama_pengguna' => $row->nama_pengguna,
	           //     'id_grup_pengguna' => $row->id_grup_pengguna,
	           //     'id_perusahaan' => $row->id_perusahaan,
            //        'nama_perusahaan' => $row->company_name,
            //        'id_asosiasi' => $row->id_asosiasi,
            //        'nama_asosiasi' => $row->asosiasi_name,
	           //     'nama_lokasi' => $row->nama_lokasi,
            //        'kuota_perus_awal' => $row->kuota_perus_awal
	           //     );
	           // //set session with value from database
	           // $this->session->set_userdata('logged_in', $sess_array);
	           }
	           // var_dump($sess_array);
	    return TRUE;
	    } else {
	        //if form validate false
	        $this->form_validation->set_message('check_database', 'Invalid username or password');
	        return FALSE;
	    }
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */