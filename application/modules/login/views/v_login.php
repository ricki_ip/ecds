<div class="container">
<?php echo form_open($submit_form, 'id="form_entry" class="form-signin" role="form"'); ?>
        <h2 class="form-signin-heading text-center">Sistem Aplikasi CDS</h2>
        <input type="text" class="form-control" name="username" placeholder="Username" required autofocus>
        <input type="password" class="form-control" name="password" placeholder="Password" required>
        <!-- <div class="g-recaptcha" data-sitekey="6Lfi4f8SAAAAAL78AyyLLORxyPBHNoYliGej5EJj"></div> -->
         <!--  <label class="checkbox">
          <input type="checkbox" name="remember" value="remember-me"> Remember me
        </label> -->
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        <a class="btn btn-lg btn-info btn-block" href="<?php echo site_url('help'); ?>">Forum</a>
        <?php if (!empty($error)): ?>
        <p>
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong><?php echo $error; ?></strong> 
            
          </div>
        </p>      
        <?php endif ?>      
<p>
</p>
<?php echo form_close(); ?>
</div>
