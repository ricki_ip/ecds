<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_database extends CI_Model
{
	private $db;
    private $skemaversion = '0205A';

    function __construct()
    {
        $this->db = $this->load->database('default', TRUE);

    }

    private function kabupaten_kota(){
        $this->db->select('id_kabupaten_kota, nama_kabupaten_kota');
        $run_query = $this->db->get('mst_kabupaten_kota');
       
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function get_ctf_docs()
    {
        $query = "SELECT * FROM ctf_tag_report 
                    GROUP BY ctf_document_number
                    LIMIT 99999999";
        $run_query = $this->db->query($query);
       
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;   
    }

    public function get_vessel($vessel_reg)
    {
        $query = "SELECT * FROM mst_vessel_ccsbt 
                    WHERE ccsbt_registration_number = '".$vessel_reg."'
                    GROUP BY ccsbt_registration_number";
        $run_query = $this->db->query($query);
       
        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;   
    }

    public function get_ctf_fish($ctf_docnum)
    {
        $query = "SELECT * FROM ctf_tag_report 
                    WHERE ctf_document_number = '".$ctf_docnum."'
                    AND tag_number NOT IN 
                       ('ID14018851' ,
                        'ID14018852',
                        'ID14018853',
                        'ID14018854',
                        'ID14018071',
                        'ID14016289',
                        'ID14017450')";
        $run_query = $this->db->query($query);
       
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;   
    }

    public function dropcreatedb()
    {
        $this->load->dbforge();
        if (!$this->db->table_exists('version'))
        {
           $this->dbforge->drop_database('db_ecds');
           $this->dbforge->create_database('db_ecds');
        }
    }

    public function version()
    {
            if ($this->db->table_exists('version'))
            {
                $this->db->select('seri');
                $query = $this->db->get('version');
                $run_query = $query->row();

                $version = $run_query->seri;
                // vdump($version, true);
                if($this->skemaversion === $version)
                {
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
    }
}