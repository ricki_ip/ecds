<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Xls_catch_area extends MX_Controller {

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   *    http://example.com/index.php/welcome
   *  - or -  
   *    http://example.com/index.php/welcome/index
   *  - or -
   * Since this controller is set as the default controller in 
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see http://codeigniter.com/user_guide/general/urls.html
   */
  function __construct()
  {
      parent::__construct();
  }

  public function index()
  {

    $this->load->library('excel');

    $filename = "Catch per Area.xlsx";


    // JUDUL
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $this->excel->setActiveSheetIndex(0);
    $this->excel->getActiveSheet()->setTitle('Catch per Area');

    $sheet = $this->excel->getActiveSheet();

    $sheet      -> setCellValue('A1', 'TANGKAPAN SBT BERDASARKAN CCSBT STATISTICAL AREA OF CATCH');
                    // setCellValue('A3', 'TAHUN');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 16), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('A1')->applyFromArray($styleArray);
                      $sheet->mergeCells('A1:M1');

    $sheet      ->  setCellValue('A3', 'CCSBT Statistical Area of Catch');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 12), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('A3')->applyFromArray($styleArray);
                      $sheet->mergeCells('A3:A4');                  

    $sheet      ->  setCellValue('B3', 'TAHUN');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 12), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('B3')->applyFromArray($styleArray);
                      $sheet->mergeCells('B3:D3');                  
    // Data 1
    $sheet      ->  setCellValue('B4', 'JUMLAH KAPAL');
    
    // Data 2  
    $sheet      ->  setCellValue('C4', 'CATCH (KG)');

    // Data 3
    $sheet      ->  setCellValue('D4', '1,15');
    
    // Data 4
    $sheet      ->  setCellValue('A20', 'JUMLAH');
    
    $sheet      ->  setCellValue('A22', 'Catatan :');
    $sheet      ->  setCellValue('A23', '- Data tahun 2010 sudah termasuk data artisanal (Cilacap+Palabuhanratu = 9.699+3.889kg) dan miss data dari LPPMHP = 8.160kg');
    $sheet      ->  setCellValue('A24', '- Jumlah kapal tahun 2010 (186 kapal) hanya berdasarkan data CDS, untuk kapal artisanal dan miss data LPPMHP tidak diketahui angka pasti');
    $sheet      ->  setCellValue('A25', '- Jumlah kapal dan catch tahun 2012 dan 2013 adalah data sementara');

    // Data 5
    $sheet      ->  setCellValue('A28', 'Tahun');
                    $sheet->mergeCells('A28:A29');

    // Data 6
    $sheet      ->  setCellValue('B28', 'Area');

    $sheet      ->  setCellValue('A28', 'Persentase');
                    $sheet->mergeCells('C28:H28');

    // Data 6
    $sheet      ->  setCellValue('B28', 'Area');

    // Data 7
    $sheet      ->  setCellValue('C29', 'Jumlah Kapal');
                    $sheet->mergeCells('C29:E29');

    // Data 8
    $sheet      ->  setCellValue('F29', 'Catch');
                    $sheet->mergeCells('F29:H29');

    $sheet      ->  setCellValue('A47', 'Catatan :');                              
    $sheet      ->  setCellValue('A48', '* Tangkapan sampai bulan Agustus 2013');                              
                                  
    // MASUKKAN DATA



                            

    //MENGATUR UKURAN KOLOM
    // $sheet->getColumnDimension('A')->setWidth(5);
    // $sheet->getColumnDimension('B')->setWidth(82);
    // $sheet->getColumnDimension('C')->setWidth(15);

    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Catch per Area.xlsx"');
    header('Cache-Control: max-age=0');



    //MENGATUR BORDER Table 1

    // $styleArray = array(
    //   'borders' => array(
    //     'allborders' => array(
    //       'style' => PHPExcel_Style_Border::BORDER_THIN
    //     )
    //   )
    // );

    // $this->excel->getActiveSheet('A5:C'.$cell_number)->getStyle('A5:C'.$cell_number)->applyFromArray($styleArray);
    // unset($styleArray);



    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
    $objWriter->save('php://output');
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */