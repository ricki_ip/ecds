<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Xls_tangkapan_kapal extends MX_Controller {

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   *    http://example.com/index.php/welcome
   *  - or -  
   *    http://example.com/index.php/welcome/index
   *  - or -
   * Since this controller is set as the default controller in 
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see http://codeigniter.com/user_guide/general/urls.html
   */
  function __construct()
  {
      parent::__construct();
  }

  public function index()
  {

    $this->load->library('excel');

    $filename = "Tangkapan-Kapal.xlsx";


    // JUDUL
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $this->excel->setActiveSheetIndex(0);
    $this->excel->getActiveSheet()->setTitle('Tangkapan Kapal');

    $sheet = $this->excel->getActiveSheet();

    $sheet      -> setCellValue('A1', 'NAMA DAN JUMLAH TANGKAPAN KAPAL PENANGKAP SBT');
                    // setCellValue('A3', 'TAHUN');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 16), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('A1')->applyFromArray($styleArray);
                      $sheet->mergeCells('A1:D1');

    $sheet      -> setCellValue('A3', 'TAHUN');
                    // setCellValue('A3', 'TAHUN');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 16), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('A3')->applyFromArray($styleArray);
                      $sheet->mergeCells('A3:B3');

    // Data 1
    $sheet      -> setCellValue('A5', 'NO');
                    // setCellValue('A3', 'TAHUN');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 12), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('A5')->applyFromArray($styleArray);

     // Data 2
    $sheet      -> setCellValue('B5', 'NAMA KAPAL');
                    // setCellValue('A3', 'TAHUN');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 12), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('B5')->applyFromArray($styleArray);

     // Data 3
    $sheet      -> setCellValue('C5', 'NET WEIGHTS (KG)');
                    // setCellValue('A3', 'TAHUN');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 12), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('C5')->applyFromArray($styleArray);

     // Data 4
    $sheet      -> setCellValue('D5', 'ESTIMATED WHOLE WEIGHT (KG)');
                    // setCellValue('A3', 'TAHUN');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 12), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('D5')->applyFromArray($styleArray);                                                
                            
    // MASUKKAN DATA



                            

    //MENGATUR UKURAN KOLOM
    // $sheet->getColumnDimension('A')->setWidth(5);
    // $sheet->getColumnDimension('B')->setWidth(82);
    // $sheet->getColumnDimension('C')->setWidth(15);

    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Tangkapan-Kapal.xlsx"');
    header('Cache-Control: max-age=0');



    //MENGATUR BORDER Table 1

    // $styleArray = array(
    //   'borders' => array(
    //     'allborders' => array(
    //       'style' => PHPExcel_Style_Border::BORDER_THIN
    //     )
    //   )
    // );

    // $this->excel->getActiveSheet('A5:C'.$cell_number)->getStyle('A5:C'.$cell_number)->applyFromArray($styleArray);
    // unset($styleArray);



    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
    $objWriter->save('php://output');
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */