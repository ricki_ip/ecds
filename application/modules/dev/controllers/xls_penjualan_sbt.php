<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Xls_penjualan_sbt extends MX_Controller {

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   *    http://example.com/index.php/welcome
   *  - or -  
   *    http://example.com/index.php/welcome/index
   *  - or -
   * Since this controller is set as the default controller in 
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see http://codeigniter.com/user_guide/general/urls.html
   */
  function __construct()
  {
      parent::__construct();
  }

  public function index()
  {

    $this->load->library('excel');

    $filename = "Penjualan-SBT.xlsx";


    // JUDUL
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $this->excel->setActiveSheetIndex(0);
    $this->excel->getActiveSheet()->setTitle('Penjualan SBT');

    $sheet = $this->excel->getActiveSheet();

    $sheet      -> setCellValue('A1', 'TUJUAN PENJUALAN SBT');
                    // setCellValue('A3', 'TAHUN');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 16), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('A1')->applyFromArray($styleArray);
                      $sheet->mergeCells('A1:G1');

    $sheet      -> setCellValue('A3', 'TOTAL TANGKAPAN');
                    // setCellValue('A3', 'TAHUN');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 14), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('A3')->applyFromArray($styleArray);
                      $sheet->mergeCells('A3:B3');
    
    $sheet      -> setCellValue('C3', 'TUJUAN PENJUALAN');
                    // setCellValue('A3', 'TAHUN');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 14), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('C3')->applyFromArray($styleArray);
                      $sheet->mergeCells('C3:L3');

    $sheet      -> setCellValue('C4', 'LDP');
                    // setCellValue('A3', 'TAHUN');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 14), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('C4')->applyFromArray($styleArray);
                      $sheet->mergeCells('C4:D4');

    $sheet      -> setCellValue('E4', 'JUMLAH EXPORT (KG)');
                    // setCellValue('A3', 'TAHUN');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 14), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('E4')->applyFromArray($styleArray);
                      $sheet->mergeCells('E4:F4');

    $sheet      -> setCellValue('G4', 'EXPORT');
                    // setCellValue('A3', 'TAHUN');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 14), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('G4')->applyFromArray($styleArray);
                      $sheet->mergeCells('G4:L4');

     // Data 1
    $sheet      -> setCellValue('A6', 'Net Weights (Kg)');
                    // setCellValue('A3', 'TAHUN');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 12), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('A6')->applyFromArray($styleArray);

     // Data 2
    $sheet      -> setCellValue('B6', 'Estimated Whole Weight (Kg)');
                    // setCellValue('A3', 'TAHUN');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 12), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('B6')->applyFromArray($styleArray);

    // Data 3
    $sheet      -> setCellValue('C6', 'Net Weights (Kg)');
                    // setCellValue('A3', 'TAHUN');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 12), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('C6')->applyFromArray($styleArray);

     // Data 4
    $sheet      -> setCellValue('D6', 'Estimated Whole Weight (Kg)');
                    // setCellValue('A3', 'TAHUN');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 12), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('D6')->applyFromArray($styleArray);

    // Data 5
    $sheet      -> setCellValue('E6', 'Net Weights (Kg)');
                    // setCellValue('A3', 'TAHUN');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 12), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('E6')->applyFromArray($styleArray);

     // Data 6
    $sheet      -> setCellValue('F6', 'Estimated Whole Weight (Kg)');
                    // setCellValue('A3', 'TAHUN');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 12), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('F6')->applyFromArray($styleArray);

    // Data 7
    $sheet      -> setCellValue('G6', 'Net Weights (Kg)');
                    // setCellValue('A3', 'TAHUN');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 12), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('G6')->applyFromArray($styleArray);

     // Data 8
    $sheet      -> setCellValue('H6', 'Estimated Whole Weight (Kg)');
                    // setCellValue('A3', 'TAHUN');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 12), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('H6')->applyFromArray($styleArray);                                                                                                                                              
    // MASUKKAN DATA



                            

    //MENGATUR UKURAN KOLOM
    // $sheet->getColumnDimension('A')->setWidth(5);
    // $sheet->getColumnDimension('B')->setWidth(82);
    // $sheet->getColumnDimension('C')->setWidth(15);

    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Penjualan-SBT.xlsx"');
    header('Cache-Control: max-age=0');



    //MENGATUR BORDER Table 1

    // $styleArray = array(
    //   'borders' => array(
    //     'allborders' => array(
    //       'style' => PHPExcel_Style_Border::BORDER_THIN
    //     )
    //   )
    // );

    // $this->excel->getActiveSheet('A5:C'.$cell_number)->getStyle('A5:C'.$cell_number)->applyFromArray($styleArray);
    // unset($styleArray);



    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
    $objWriter->save('php://output');
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */