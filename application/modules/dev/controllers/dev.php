<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dev extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
			parent::__construct();
			$this->load->config('menus');
			$this->load->config('db_timeline');
			$this->load->config('globals');
			$this->load->model('mdl_database');	
	}
	
	public function index()
	{
		$this->load->config('labels');

		$data['database_timeline'] = $this->config->item('database_timeline');

		$data['labels'] =  Array(
									'form_kapal' => $this->config->item('form_kapal')
								);
		
		$data['db_uptodate'] = $this->mdl_database->version();
		echo Modules::run('templates/page/v_form', //tipe template
							'dev', //nama module
							'index', //nama file view
							'', //dari labels.php
							'', //plugin javascript khusus untuk page yang akan di load
							'', //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view
	}

	public function layout_ctf()
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$data['hasil'] = '';

		// $views = 'layout_ctf';
		// $template='templates/page/v_form';
		// $modules='dev';
		// $labels='';
		// // echo Modules::run($views, $add_js, $add_css);
		// echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

		$views = $this->load->view('layout_ctf', $data);
		echo Modules::run($views, $add_js, $add_css);
		
	}

	public function layout_cmf()
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$data['hasil'] = '';
		
		$views = $this->load->view('layout_cmf', $data);
		echo Modules::run($views, $add_js, $add_css);
		
	}

	public function layout_reef()
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$data['hasil'] = '';
		
		$views = $this->load->view('layout_reef', $data);
		echo Modules::run($views, $add_js, $add_css);
		
	}

	public function import()
	{
		$this->load->model('mdl_database');

		$ctf_docs = $this->mdl_database->get_ctf_docs();
		$duplicate_docs = array();
		$clean_docs = array();

		$arr_num = array();
		$last_num = 303; // 302 

		for ($i=1; $i < $last_num; $i++) { 
			array_push($arr_num, $i);
		}
		foreach ($ctf_docs as $key => $ctf) {
			$arr_docnum = explode(".B", $ctf->ctf_document_number);
			$docnum = $arr_docnum[1];
			$is_dup = strpos($docnum, "B");
			
			if($is_dup === false)
			{
				array_push($clean_docs, $ctf);				
			}else{
				array_push($duplicate_docs, $ctf);
			}			
		}

			
			// Shows dupl
			// echo "<h1>Dupl</h1>";
			// echo "<pre>";
			// echo print_r(length($duplicate_docs));
			// echo "<hr>";
			// echo print_r($duplicate_docs);
			// echo "</pre><hr>";

	echo "<table>";
	foreach ($clean_docs as $key => $doc) {
			$ctf_fishes = $this->mdl_database->get_ctf_fish($doc->ctf_document_number);	
			$ctf_vessel = $this->mdl_database->get_vessel($doc->vessel_reg);		
			echo "<thead><tr><th><h1>".$doc->ctf_document_number."</h1><small>".count($ctf_fishes)."</small></th>";
			if($ctf_vessel)
			{			
				echo "<th>".$ctf_vessel->vessel_name." (".$doc->vessel_reg.")</th>";
			}else{
				echo "<th>Vessel Not Found (".$doc->vessel_reg.")</th>";
			}
			echo "<th>".$doc->title." (".$doc->name.")</th>";
			echo "</tr></thead>";
			echo "<tbody>";
				if($ctf_fishes)
				{
					$num = 1;
					foreach ($ctf_fishes as $key => $fish) {
						echo "<tr>";
						echo "<td>".$num++.".</td>";
						echo "<td>".$fish->tag_number."</td>";
						echo "<td></td>";
						echo "</tr>";					
					}
				}else{
					echo "<tr><td colspan='2'>Notfound</td></tr>";
				}
			echo "</tbody>";
			}		
	echo "<table>";

	}

	public function dropcreatedb(){
		$this->load->model('mdl_database');
		
		$this->mdl_database->dropcreatedb();
		redirect(site_url());
	}

	public function update_database(){
		$this->load->model('mdl_database');
		
		if( $this->mdl_database->update_database() ){
			$url = base_url();
			redirect($url);
		}else{
			$url = base_url();
			redirect($url);
		}
	}

	public function update_alat_tangkap(){
		$this->load->model('mdl_database');
		
		if( $this->mdl_database->update_alat_tangkap() ){
			$url = base_url();
			redirect($url);
		}else{
			$url = base_url();
			redirect($url);
		}
	}

	function logout() {
         //remove all session data
         $this->session->unset_userdata('logged_in');
         $this->session->sess_destroy();
         redirect(base_url('login/login'), 'refresh');
     }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */