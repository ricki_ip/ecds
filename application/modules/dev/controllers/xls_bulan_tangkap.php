<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Xls_bulan_tangkap extends MX_Controller {

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   *    http://example.com/index.php/welcome
   *  - or -  
   *    http://example.com/index.php/welcome/index
   *  - or -
   * Since this controller is set as the default controller in 
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see http://codeigniter.com/user_guide/general/urls.html
   */
  function __construct()
  {
      parent::__construct();
  }

  public function index()
  {

    $this->load->library('excel');

    $filename = "Catch per Bulan Tangkap.xlsx";


    // JUDUL
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $this->excel->setActiveSheetIndex(0);
    $this->excel->getActiveSheet()->setTitle('Catch per Bulan Tangkap');

    $sheet = $this->excel->getActiveSheet();

    $sheet      -> setCellValue('A1', 'HASIL TANGKAPAN BERDASARKAN BULAN PENANGKAPAN');
                    // setCellValue('A3', 'TAHUN');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 16), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('A1')->applyFromArray($styleArray);
                      $sheet->mergeCells('A1:K1');
    $sheet      ->  setCellValue('A3', 'TAHUN');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 14), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('A3')->applyFromArray($styleArray);
                      $sheet->mergeCells('A3:B3');                  

    // Data 1
    $sheet      ->  setCellValue('A6', 'MONTH');
    
    // Data 2
    $sheet      ->  setCellValue('B5', 'TOTAL');
                    $sheet->mergeCells('B5:C5'); 

    $sheet      ->  setCellValue('B6', 'NET WEIGHT (KG)');
    $sheet      ->  setCellValue('C6', 'ESTIMATED WHOLE WEIGHT (KG)');

    // Data 3
    $sheet      ->  setCellValue('D5', 'BALI');
                    $sheet->mergeCells('D5:E5'); 

    $sheet      ->  setCellValue('D6', 'NET WEIGHT (KG)');
    $sheet      ->  setCellValue('E6', 'ESTIMATED WHOLE WEIGHT (KG)');

    // Data 4
    $sheet      ->  setCellValue('F5', 'JAKARTA');
                    $sheet->mergeCells('F5:G5'); 

    $sheet      ->  setCellValue('F6', 'NET WEIGHT (KG)');
    $sheet      ->  setCellValue('G6', 'ESTIMATED WHOLE WEIGHT (KG)');
                                    
    // Data 5
    $sheet      ->  setCellValue('H5', 'CILACAP');
                    $sheet->mergeCells('H5:I5'); 

    $sheet      ->  setCellValue('H6', 'NET WEIGHT (KG)');
    $sheet      ->  setCellValue('I6', 'ESTIMATED WHOLE WEIGHT (KG)');

    // Data 6
    $sheet      ->  setCellValue('J5', 'PALABUHAN RATU');
                    $sheet->mergeCells('J5:K5'); 

    $sheet      ->  setCellValue('J6', 'NET WEIGHT (KG)');
    $sheet      ->  setCellValue('K6', 'ESTIMATED WHOLE WEIGHT (KG)');                  

                            
    // MASUKKAN DATA



                            

    //MENGATUR UKURAN KOLOM
    // $sheet->getColumnDimension('A')->setWidth(5);
    // $sheet->getColumnDimension('B')->setWidth(82);
    // $sheet->getColumnDimension('C')->setWidth(15);

    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Catch per Bulan Tangkap.xlsx"');
    header('Cache-Control: max-age=0');



    //MENGATUR BORDER Table 1

    // $styleArray = array(
    //   'borders' => array(
    //     'allborders' => array(
    //       'style' => PHPExcel_Style_Border::BORDER_THIN
    //     )
    //   )
    // );

    // $this->excel->getActiveSheet('A5:C'.$cell_number)->getStyle('A5:C'.$cell_number)->applyFromArray($styleArray);
    // unset($styleArray);



    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
    $objWriter->save('php://output');
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */