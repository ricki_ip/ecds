<html lang="en">
<head>

  <!-- Bootstrap core CSS -->
  <link href="<?php echo site_url('assets/third_party/css').'/bootstrap.min.css';?>" rel="stylesheet">

  <!-- Custom styles for this template -->
  <!--  <link href="assets/ecds/css/style.css" rel="stylesheet"> -->

  

</head>

<body>
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
          <table class="table">
            <tr>
              <th rowspan="3" width="10%" style="text-align: center;"><img src="../assets/third_party/images/ccsbt-logo.jpg" width="70" height="70"></th>
              <th rowspan="3" width="20%" style="font-family:arial;color:white;font-size:18px;">Commision for the Conservation of Southern Bluefin Tuna</th>
              <th rowspan="2" width="35%" style="text-align: center;font-family:arial;color:white;font-size:18px;">RE-EXPORT/EXPORT AFTER LANDING OF DOMESTIC PRODUCT FORM</th>
              <th width="35%" style="background-color:black;background-color:black;font-family:arial;color:white;font-size:18px;">Document Number</th>
            </tr>
            <tr>
              <td style="background-color:white;"><?php echo kos($hasil['document_number']); ?></td>
            </tr>
            <tr>
              <td style="background-color:black;font-family:arial;color:white;font-size:18px;text-align: center;">Catch Documentation Scheme</td>
              <td></td>
            </tr>
          </table>
      </div>      
    </div>

    <div class="row">
      <div class="col-lg-9">
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>  
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><input type="checkbox">Re-Export</td>
                <td><img src="../assets/third_party/images/panah.png"></td>
                <td><input type="checkbox">Export after Landing of Domestic Product</td>
                <td>(tick only one)</td>
              </tr> 
            </tbody>
          </table>
          <table class="table table-bordered">
            <thead>
              <tr>  
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><input type="checkbox">Full Shipment</td>
                <td><img src="../assets/third_party/images/panah.png"></td>
                <td><input type="checkbox">Partial Shipment</td>
                <td>(tick only one)</td>
              </tr> 
            </tbody>
          </table>
        </div>
      </div> 
      <div class="col-lg-10">
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
            </thead>
            <tbody>
              <tr width="50%">
                <td>Form Number of Preceding Document (Catch Monitoring Form, or Re-Export/Export After Landing of Domestic Product Form)</td>
                <td width="50%"></td>
              </tr> 
            </tbody>
          </table>
        </div>
      </div>      
    </div>

    <div class="row">
     <!-- <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">EXPORT SECTION</h3>
      </div>
      <div class="panel-body"> -->
      <div class="col-lg-12">
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
               <th rowspan="2" width="25%">Exporting State/Fishing Entity</th>	
               <th colspan="3" width="75%" style="text-align: center;">Point of Export</th>
             </tr>
             <tr>
              <th width="25%">City</th>
              <th width="25%">State or Province</th>
              <th width="25%">State/Fishing Entity</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </tbody>
        </table>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th width="40%">Name of Processing Establishment (if applicable)</th>
              <th width="60%">Address of Processing Establishment (if applicable)</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td></td>
              <td></td>
            </tr>
          </tbody>
        </table>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th width="40%">Catch Tagging Form Document Numbers (if applicable)</th>
              <td width="60%"></td>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>

      <div class="col-lg-6">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th colspan="4">Description of Fish from previous CDS Document</th>
            </tr>
            <tr>
              <td colspan="2" width="50%">Flag State/Fishing Entity</td>
              <td colspan="2" width="50%">Date of previous Import/Landing</td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td colspan="2"></td>
              <td colspan="2"></td>
            </tr>
            <tr>
              <td width="25%">Product: F(Fresh) / FR (Frozen)</td>
              <td width="25%">Type: RD/GGO/GGT/DRO/DRT/FL/OT*</td>
              <td width="25%">Weight (kg)</td>
              <td width="25%">Total Number of whole Fish (including RD/GGO/GGT/DRO/DRT)</td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td colspan="2">* For Other (OT): Describe the type of product</td>
              <td colspan="2"></td>
            </tr>
          </tbody>
        </table>
      </div> 
      <div class="col-lg-6">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th colspan="4">Description of Fish being Exported</th>
            </tr>
            <tr>
              <td>Product: F(Fresh) / FR (Frozen)</td>
              <td>Type: RD/GGO/GGT/DRO/DRT/FL/OT*</td>
              <td>Weight (kg)</td>
              <td>Total Number of whole Fish (including RD/GGO/GGT/DRO/DRT)</td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td colspan="2">* For Other (OT): Describe the type of product</td>
              <td colspan="2"></td>
            </tr>
          </tbody>
        </table>
      </div> 
      <div class="col-lg-7">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th colspan="4">Destination (State/Fishing Entity</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td></td>
            </tr>
          </tbody>
        </table>
      </div> 
      <div class="col-lg-12">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th colspan="4">Certification by Exporter: I certify that the above information is complete, true and correct to the best of my knowledge and belief</th>
            </tr>
            <tr>
              <td width="35%">Name</td>
              <td width="25%">Signature</td>
              <td width="15%">Date</td>
              <td width="25%">License No. / Company Name</td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </tbody>
        </table>
        <table class="table table-bordered">
          <thead>
          </thead>
          <tbody>
            <tr>
              <th colspan="3" width="75%">Validation by Authority: I validate that the above information is complete, true and correct to the best of my knowledge and belief</th>
              <td rowspan="4" width="25%"></td>
            </tr>
            <tr>
              <td>Name and Title</td>
              <td colspan="2">Signature</td>
            </tr>
            <tr>
              <td rowspan="2"></td>
              <td colspan="2"></td>
            </tr>
            <tr>
              <td width="5%">Date</td>
              <td width="25%"></td>
            </tr>
          </tbody>
        </table>
      </div>
  <!--   </div>
  </div> -->
</div>
</div>

<div class="row">
  <div class="col-lg-8">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th colspan="3" style="text-align: center;">Final Point of Import</th>
        </tr>
        <tr>
          <td width="30%">City</td>
          <td width="35%">State or Province</td>
          <td width="35%">State/Fishing Entity</td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="col-lg-12">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th colspan="4">Certification by Importer: I certify that the above information is complete, true and correct to the best of my knowledge and belief</th>
        </tr>
        <tr>
          <td width="25%">Name</td>
          <td width="35%">Address</td>
          <td width="25%">Signature</td>
          <td width="15%">Date</td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </tbody>
    </table>
    <table class="table table-bordered">
      <thead>
      </thead>
      <tbody>
        <tr>
          <td>NOTE: The organization/person which validates the Export section shall verify the copy of the original CCSBT CDS Document. Sucha verified copy of the original CCSBT CDS.
            document must be attached to the Re-export/Export after Landing of Domestic Product (RE) Form. When SBT is Exported, all verified copies of concerned forms must be attached.</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

</div>
</body>
</html> 
