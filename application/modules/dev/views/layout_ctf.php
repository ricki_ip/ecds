<?php
    $tmpl = array ( 'table_open'  => '<table id="table_ctf_tag" class="table table-hover">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('No.','CCSBT Tag Number', 'Type','Weight (Kg)','Fork length (cm)','Gear Code','Area Of Catch', 'Month of Harvest');


    $number = 1;
    foreach ($list_tags as $key => $items) {
      $this->table->add_row($number.'. ',
                            tagnumber($items->ccsbt_tag_number),
                            $items->code_tag,
                            $items->weight,
                            $items->fork_length,
                            $items->gear_code,
                            $items->ccsbt_statistical_area,
                            tgl($items->month_of_harvest,'m/Y')
                            );
      $number++;
    }
    $table_ctf_tag = $this->table->generate();
    // var_dump($detail_ctf);  
?>

<html lang="en">
<head>

    <title>ECDS</title>
    <!-- Bootstrap core CSS -->
    <link href="<?php echo site_url('assets/third_party/css').'/bootstrap.min.css';?>" rel="stylesheet">

    <!-- Custom styles for this template -->
   <!--  <link href="assets/ecds/css/style.css" rel="stylesheet"> -->
    
<style type="text/css">
  table{
    border-collapse: collapse;
    border: 1px solid black;
  }
  table tr,th,td{
    border: 1px solid black;
  }
</style>

</head>

<body>

<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <div class="table-responsive">
        <!-- <table class="table table-bordered">
          <thead>
            <tr>
              <th rowspan="3" width="5%"><img src="../assets/third_party/images/ccsbt-logo.jpg" width="70" height="70"></th>
              <th rowspan="3" width="20%">Commision for the Conservation of Southern Bluefin Tuna</th>
              <th width="50%">CATCH TAGGING FORM</th>
              <th width="25%">Document Number</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>T - </td>
              <td>T - </td>
            <tr>
              <td>Catch Documentation Scheme</td>
              <td>Catch Documentation Scheme</td>
            </tr> 
          </tbody>
        </table> -->
        <table class="table table-bordered" style="background-color:black;">
          <tr>
            <th rowspan="3" width="10%" style="text-align: center;"><img src="../assets/third_party/images/ccsbt-logo.jpg" width="70" height="70"></th>
            <th rowspan="3" width="20%" style="font-family:arial;color:white;font-size:18px;">Commision for the Conservation of Southern Bluefin Tuna</th>
            <th rowspan="2" width="35%" style="text-align: center;font-family:arial;color:white;font-size:18px;">CATCH TAGGING FORM</th>
            <th width="35%" style="font-family:arial;color:white;font-size:18px;">Document Number</th>
          </tr>
          <tr>


            <td style="background-color:white;"><?php echo $hasil['document_number']; ?></td>
          </tr>
          <tr>
            <td style="font-family:arial;color:white;font-size:18px;text-align: center;">Catch Documentation Scheme</td>
            <td></td>
          </tr>
        </table>

      </div>
    </div>      
  </div>

  <div class="row">
    <div class="col-lg-7">
      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>  
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><input type="checkbox">Wild Harvest</td>
              <td><img src="../assets/third_party/images/panah.png"></td>
              <td><input type="checkbox">Farmed</td>
              <td>(tick only one)</td>
            </tr> 
          </tbody>
        </table>
      </div>
    </div>      
  </div>

  <div class="row">
    <div class="col-lg-9">
      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th width="60%">Document Number of Associated Catch of Monitoring Form</th>
              <th width="50%"></th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>      
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">CATCH SECTION</h3>
        </div>
        <div class="panel-body">
      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>Name of Fishing Vessel</th>
              <th>Vessel Registration Number</th>
              <th>Flag State/Fishing Entity</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><?php echo $hasil['vessel_name']; ?></td>
              <td><?php echo $hasil['vessel_registration_number']; ?></td>
              <td><?php echo $hasil['flag_state']; ?></td>
            </tr>
          </tbody>
        </table>
      </div>

      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Information on Other form(s) of Capture (eg. Trap)</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><?php echo $hasil['information_other_form']; ?></td>
          </tr>
        </tbody>
      </table>
     </div>
      <?php echo $table_ctf_tag; ?>
          <table class="table table-bordered">
            <tbody>
              <tr>
                <td>Certification: I certify that the above information is complete, true and correct to the best of my knowledge and belief.
                </td>  
              </tr>
            </tbody>
          </table>
        </div>
      </div>
  </div>
  <div class="row">
     <div class="col-lg-12">
       <table class="table table-bordered">
            <thead>
              <tr>
                <th width="35%">Name</th>
                <th width="35%">Signature</th>
                <th width="10%">Date</th>
                <th width="20%">Title</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><?php echo $hasil['name']; ?></td>
                <td><?php echo $hasil['signature']; ?></td>
                <td><?php echo $hasil['date']; ?></td>
                <td><?php echo $hasil['title']; ?></td>
              </tr>
            </tbody>
          </table>
     </div>
  </div>  
</div>        
<!-- 
          

         

         

     

 -->
  </body>
</html> 
