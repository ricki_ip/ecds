<?php 
 $tmpl = array ( 'table_open'  => '<table id="table_catch_bulan_tangkap_report" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('MONTH',
                              'TOTAL NET WEIGHT (KG)',
                              'TOTAL ESTIMATED WHOLE WEIGHT (KG)',
                              'BALI NET WEIGHT (KG)',
                              'BALI ESTIMATED WHOLE WEIGHT (KG)',
                              'JAKARTA NET WEIGHT (KG)',
                              'JAKARTA TOTAL ESTIMATED WHOLE WEIGHT (KG)',
                              'CILACAP NET WEIGHT (KG)',
                              'CILACAP TOTAL ESTIMATED WHOLE WEIGHT (KG)',
                              'PALABUHANRATU NET WEIGHT (KG)',
                              'PALABUHANRATU ESTIMATED WHOLE WEIGHT (KG)'
                               );
    // vdump($list_cmf, true);
    // if($list_cmf)
    // {
    //   $number = 1;
    //   foreach ($list_cmf as $key => $item) {
    //     $this->table->add_row( 
    //                           $item->document_number ,
    //                           $item->ctf_document_numbers ,
    //                           $item->Wild_or_Farmed ,
    //                           $item->vessel_name ,
    //                           $item->vessel_registration_number ,
    //                           $item->flag_state ,
    //                           $item->product_type,
    //                           $item->code_tag ,
    //                           tgl($item->month_of_harvest,'F, Y') ,
    //                           $item->gear_code_name ,
    //                           $item->ccsbt_statistical_area ,
    //                           $item->net_weight ,
    //                           angka($item->total_fish,0)
                             
    //                           );
    //     $number++;
    //   }
    // }

    // $table_list_cmf = $this->table->generate();

    $this->table->clear();
?>
<div class="row">
  <div class="col-lg-12" style="overflow-x: auto;">
  <div class="panel panel-default">
    <div class="panel-body">
      <a href="xls" class="btn btn-primary">Export to XLS</a>          
    </div>
  </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12" style="overflow-x: auto;">
    <?php //echo $table_list_cmf; ?>    
  </div>
</div>


<script>
  
  var init_data_tables = function(){
     $('#table_cmf_report').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "bFilter": true,
        "bInfo": false,
        "bPaginate": true,
"sPaginationType":"full_numbers",
"iDisplayLength": 25,
        "bSort": true,
        "scrollX": true
      });
    }

  
  s_func.push(init_data_tables);
</script>