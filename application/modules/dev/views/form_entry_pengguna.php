<div class="row">
  <div class="col-lg-12">
          <?php

          echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');

          ?>
         <?php

          $attr_id_grup_pengguna = array( 'name' => $form['id_grup_pengguna']['name'],
                                      'label' => $form['id_grup_pengguna']['label']
                    );
          echo $this->mkform->input_text($attr_id_grup_pengguna);

          $attr_kode_pengguna = array( 'name' => $form['kode_pengguna']['name'],
                                      'label' => $form['kode_pengguna']['label']
                    );
          echo $this->mkform->input_text($attr_kode_pengguna);

          $attr_nip = array( 'name' => $form['nip']['name'],
                                      'label' => $form['nip']['label']
                    );
          echo $this->mkform->input_text($attr_nip);

          $attr_nama_pengguna = array( 'name' => $form['nama_pengguna']['name'],
                                      'label' => $form['nama_pengguna']['label']
                    );
          echo $this->mkform->input_text($attr_nama_pengguna);

          $attr_full_name = array( 'name' => $form['full_name']['name'],
                                      'label' => $form['full_name']['label']
                    );
          echo $this->mkform->input_text($attr_full_name);

          $attr_jenis_kelamin = array( 'name' => $form['jenis_kelamin']['name'],
                                      'label' => $form['jenis_kelamin']['label'],
                                      'opsi' => array('Laki - Laki' => 'Laki - Laki',
                                                          'Perempuan' => 'Perempuan'
                                                    ) 
                    );
          echo $this->mkform->input_select($attr_jenis_kelamin);

          $attr_nama_jabatan = array( 'name' => $form['nama_jabatan']['name'],
                                      'label' => $form['nama_jabatan']['label']
                    );
          echo $this->mkform->input_text($attr_nama_jabatan);

           $attr_kata_kunci = array( 'name' => $form['kata_kunci']['name'],
                                      'label' => $form['kata_kunci']['label']
                    );
          echo $this->mkform->input_text($attr_kata_kunci);

          $attr_no_telp1 = array( 'name' => $form['no_telp1']['name'],
                                      'label' => $form['no_telp1']['label']
                    );
          echo $this->mkform->input_text($attr_no_telp1);

          $attr_no_telp2 = array( 'name' => $form['no_telp2']['name'],
                                      'label' => $form['no_telp2']['label']
                    );
          echo $this->mkform->input_text($attr_no_telp2);

          $attr_no_hp = array( 'name' => $form['no_hp']['name'],
                                      'label' => $form['no_hp']['label']
                    );
          echo $this->mkform->input_text($attr_no_hp);

          $attr_email = array( 'name' => $form['email']['name'],
                                      'label' => $form['email']['label']
                    );
          echo $this->mkform->input_text($attr_email);

          $attr_aktif = array( 'name' => $form['aktif']['name'],
                                      'label' => $form['aktif']['label'],
                                      'opsi' => array('Ya' => 'Ya',
                                                          'Tidak' => 'Tidak'
                                                    )
                    );
          echo $this->mkform->input_select($attr_aktif);

         ?>
  </div>
</div>
         
<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-8">
              <button type="submit" class="btn btn-primary">Reset</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </div>
  </div>
</div>
</form>