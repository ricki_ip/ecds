<html lang="en">
<head>

  <title></title>
  <!-- Bootstrap core CSS -->
  <link href="<?php echo site_url('assets/third_party/css').'/bootstrap.min.css';?>" rel="stylesheet">

  

  <!-- Custom styles for this template -->
  <!--  <link href="assets/ecds/css/style.css" rel="stylesheet"> -->
  <div class="row">
      <div class="col-lg-12">
        <div class="table-responsive">
          <table class="table">
          <tr>
            <th rowspan="2" width="15%" style="text-align:center;background-color:black;"><img src= "<?php echo base_url("assets/third_party/images/ccsbt-logo.jpg"); ?>" width="90" height="90"> </th>
            <th rowspan="2" width="20%" style="font-family:arial;color:white;font-size:14px;background-color:black;">Commision for the Conservation of Southern Bluefin Tuna</th>
            <th width="30%" style="text-align: center;font-family:arial;color:white;font-size:20px;background-color:black;">CATCH MONITORING FORM</th>
            <th width="20%" style="font-family:arial;color:white;font-size:12px;background-color:black;text-align:center">Document Number</th>
            <th rowspan="2" width="15%" style="font-family:arial;color:white;font-size:18px;background-color:black;text-align:center;"><img src="<?php echo base_url("assets/third_party/images/kkp.png"); ?>" width="90" height="90"></th>
          </tr>
          <tr>
          </tr>
          <tr>
            <td style="font-family:arial;color:white;font-size:13px;text-align:center;background-color:black;">Catch Documentation Scheme</td>
            <td style="background-color:white;"><?php echo kos($hasil['document_number'], 'Temporary:'.$hasil['temp_doc_id']); ?></td>
          </tr>
        </table>
        </div>
      </div>      
    </div>
 

</head>

<body>
  <div class="container">

    

    <div class="row">
      <div class="col-lg-12">
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
            </thead>
            <tbody>
              <tr>
                <td width="30%">Catch Tagging Form Document Numbers</td>
                <td width=""><?php echo $hasil['ctf_document_numbers']; ?></td>
              </tr> 
            </tbody>
          </table>
        </div>
      </div>      
    </div>

    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">CATCH / HARVEST SECTION - Tick and complete only one part</h3>
        </div>
        <div class="panel-body">
        <div class="table-responsive">
          <table align="right" class="table table-bordered">
            <thead>
              <tr>
               <th width="3%"><input type="checkbox"></th>	
               <th rowspan="2" width="17%">For Wild Fishery</th>
               <th width="40%">Name of Catching Vessel</th>
               <th width="20%">Registration Number</th>
               <th width="20%">Flag State/Fishing Entity</th>
             </tr>
           </thead>
           <tbody>
            <tr>
              <td colspan="2"></td>
              <td><?php echo $hasil['vessel_name']; ?></td>
              <td><?php echo $hasil['vessel_registration_number']; ?></td>
              <td><?php echo 'INDONESIA'; ?></td>
            </tr>
          </tbody>
        </table>
        <table class="table table-bordered">
          <thead>
            <tr>
            	<th width="3%"><input type="checkbox"></th>
              <th rowspan="2" width="17%">For Farmed SBT</th>
              <th width="20%">CCSBT Farm Serial Number</th>
              <th width="60%">Name of Farm</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td colspan="2" rowspan="3"></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td colspan="2">Document Number(s) of associated Farm Stocking (FS) Form(s)</td>
            </tr>
            <tr>
              <td colspan="2"></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

   <!--  <div class="col-lg-12"> -->
     <table class="table table-bordered">
      <thead>
        <tr><th colspan="7" style="text-align: center;">Description of Fish</th></tr>

        <tr>
          <th width="25%">Product: F (Fresh)/ FR (Frozen)</th>
          <th width="7.5%">Type: RD/GGO/GGT/DRO/DRT/FL/OT*</th>
          <th width="20%">Month of Catch/Harvest (mm/yy)</th>
          <th width="10%">Gear Code</th>
          <th width="20%">CCSBT Statistical Area</th>
          <th width="7.5%">Net Weight (kg)</th>
          <th width="10%">Total Number of whole Fish (including RD/GGO/GGT/DRO/DRT)</th>

        </tr>
      </thead>
      <tbody>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td colspan="2">For Other (OT): Describe the Type of Product</td>
          <td colspan="2"></td>
          <td colspan="2">For Other (OT): Specify Conversion Factor</td>
          <td></td>
        </tr>
      </tbody>
    </table>
    <table class="table table-bordered">
     <thead>
      <th>Name of Processing Establishment (if applicable)</th>
      <th>Address of Processing Establishment (if applicable)</th>
    </thead>
    <tbody>
      <tr>
        <td><?php echo $hasil['name_processing_establishment']; ?></td>  
        <td><?php echo $hasil['address_processing_establishment']; ?></td>  
      </tr>
    </tbody>
  </table>

  <table class="table table-bordered">
   <thead>
          		<!-- <th colspan="4">Validation by Authority (not required for exports transhipped at sea): I validate that the above information is complete, trure and correct to the best of my knowledge and belief</th>
          		<th></th> -->
          	</thead>
            <tbody>
            	<tr>
            		<td colspan="4" width="75%">Validation by Authority (not required for exports transhipped at sea): I validate that the above information is complete, trure and correct to the best of my knowledge and belief</td>
            		<td rowspan="3" width="25%"></td>
            	</tr>	
              <tr>
                <td rowspan="2" width="10%">Name and Title</td>  
                <td rowspan="2" width="30%"><?php echo $hasil['authority_name_title']; ?></td>
                <td width="5%">Signature</td>
                <td width="20%"></td>
                <!--  <td rowspan="2"></td> -->

              </tr>
              <tr>
                <td>Date</td>
                <td><?php echo $hasil['authority_date']; ?></td>
                <!-- <td></td>  -->
              </tr>
            </tbody>
          </table>
        
      </div>
    </div>
      </div>

      <div class="row">
       <div class="col-lg-12">
         <table class="table table-bordered">
          <thead>
            <tr>
              <th><input type="checkbox"></th>
              <th>Transhipment</th>
              <th colspan="5">Certification by Master of Fishing Vessel: I certify that the catch/harvest information is complete, true and correct to the best of my knowledge and belief</th>
            </tr>
          </thead>
          <tbody>
            <tr>
             <td width="4%"></td>
              <td width="10%">Name</td>
            <td width="26%"></td>
            <td width="10%">Date</td>
            <td width="20%"></td>
            <td width="10%">Signature</td>
            <td width="20%"></td>
           </tr>
         </tbody>
       </table>
       <table class="table table-bordered">
        <thead>
          <tr>
            <th>Name of Receiving Vessel</th>
            <th>Registration Number</th>
            <th>Flag State/Fishing Entity</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td></td>
            <td></td>
            <td></td>
          </tr>
        </tbody>
      </table>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th colspan="6">Certification by Master of Receiving Vessel: I certify that the above information is complete, true and correct to the best of my knowledge and belief</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td width="10%">Name</td>
            <td width="30%"></td>
            <td width="10%">Date</td>
            <td width="20%"></td>
            <td width="10%">Signature</td>
            <td width="20%"></td>
          </tr>
        </tbody>
      </table>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th colspan="6">Signature of Observer (only for transhipment at sea):</th>
          </tr>
        </thead>
        <tbody>
          <tr>
             <td width="10%">Name</td>
            <td width="30%"></td>
            <td width="10%">Date</td>
            <td width="20%"></td>
            <td width="10%">Signature</td>
            <td width="20%"></td>
          </tr>
        </tbody>
      </table>
    </div>

    <div class="col-lg-10">
     <table class="table table-bordered">
      <thead>
        <tr>
          <th><input type="checkbox"></th>
          <th>Export</th>
          <th colspan="5">Point of Export*</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td width="5%"></td>
          <td width="10%">City</td>
          <td width="20%"><?php echo $hasil['city']; ?></td>
          <td width="10%">State or Province</td>
          <td width="20%"><?php echo $hasil['province']; ?></td>
          <td width="10%">State/Fishing Entity</td>
          <td width="25%"><?php echo $hasil['state_entity']; ?></td>
       </tr>
     </tbody>
   </table>
 </div>

 <div class="col-lg-2">
   <table class="table table-bordered">
    <thead>
      <tr>
        <th>Destination (State/Fishing Entity)</th>
      </tr>
    </thead>
    <tbody>
      <tr>
       <td><?php echo $hasil['destination']; ?></td>
     </tr>
   </tbody>
 </table>
</div>

<div class="col-lg-12">
 <table class="table table-bordered">
  <thead>
    <tr>
      <th colspan="4">Certfication by Exporter: I certify that the above information is complete, true and correct to the best pf my knowledge and belief</th>
    </tr>
  </thead>
  <tbody>
    <tr>
     <td>Name</td>
     <td>State or Province</td>
     <td>State/Fishing Entity</td>
     <td>Signature</td>
   </tr>
   <tr>
    <td><?php echo $hasil['name']; ?></td>
    <td><?php echo $hasil['province']; ?></td>
    <td><?php echo $hasil['state_entity']; ?></td>
    <td></td>
  </tr>
</tbody>
</table>
<table class="table table-bordered">
 <thead>
          		<!-- <th colspan="4">Validation by Authority (not required for exports transhipped at sea): I validate that the above information is complete, trure and correct to the best of my knowledge and belief</th>
          		<th></th> -->
          	</thead>
            <tbody>
              <tr>
                <td colspan="4" width="75%">Validation by Authority: I validate that the above information is complete, trure and correct to the best of my knowledge and belief</td>
                <td rowspan="3" width="25%"></td>
              </tr> 
              <tr>
                <td rowspan="2" width="10%">Name and Title</td>  
                <td rowspan="2" width="30%"><?php echo $hasil['authority_name_title']; ?></td>
                <td width="5%">Signature</td>
                <td width="20%"></td>
                <!--  <td rowspan="2"></td> -->

              </tr>
              <tr>
                <td>Date</td>
                <td><?php echo $hasil['authority_date']; ?></td>
                <!-- <td></td>  -->
              </tr>
            </tbody>
          </table>
        </div>

      </div>

      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">FINAL PRODUCT DESTINATION SECTION - tick and complete only one destination</h3>
            </div>
            <div class="panel-body">
              <div class="table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                     <th><input type="checkbox"></th>	
                     <th>Landing of Domestic Product for Domestic sale</th>
                     <th colspan="4">Certification of Domestic Sale. I certify that the above information is complete, true and correct to the best of my knowledge and belief.</th>
                   </tr>
                   <tr>
                     <th rowspan="5"></th>
                     <th>Name</th>	
                     <th>Address</th>
                     <th>Date</th>
                     <th>Weight (kg)</th>
                     <th>Signature</th>
                   </tr>
                 </thead>
                 <tbody>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                </tbody>
              </table>

              <table class="table table-bordered">
                <thead>
                  <tr>
                   <th width="5%"><input type="checkbox"></th>	
                   <th width="10%">Import</th>
                   <th colspan="6" width="85%" style="text-align: center;">Final Point of Import</th>
                 </tr>
                 <tbody>
                  <tr>
                    <td width="5%"></td>
                    <td width="10%">City</td>
                    <td width="20%"><?php echo $hasil['city']; ?></td>
                    <td width="10%">State or Province</td>
                    <td width="20%"><?php echo $hasil['province']; ?></td>
                    <td width="10%">State/Fishing Entity</td>
                    <td width="25%"><?php echo $hasil['state_entity']; ?></td>
                  </tr>
                </tbody>
              </table>

              <table class="table table-bordered">
                <thead>
                  <tr>
                   <th colspan="4">Certification by Importer: I certify that the above information is complete, true and correct to the best of my knowledge and belief</th>
                 </tr>
                 <tr>
                   <th>Name</th>
                   <th>Address</th>
                   <th>Date</th>
                   <th>Signature</th>
                 </tr>
                 <tbody>
                  <tr>
                    <td><?php echo $hasil['name']; ?></td>
                    <td><?php echo $hasil['address']; ?></td>
                    <td><?php echo $hasil['date']; ?></td>
                    <td></td>
                  </tr>
                </tbody>
              </table>

            </div>
          </div>
        </div>
      </div>
    </div>


  </div>        
<!-- 
          

         

         

     

-->
</body>
</html> 
