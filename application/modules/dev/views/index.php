<div class="row">
	<div class="col-lg-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3>DEVELOPMENT INFO</h3>
			</div>
			<div class="panel-body">
					
			<!-- 	<p>History update database SDI <br> <strong>(file: application/config/db_timeline.php)</strong> :</p>
			 -->	<?php foreach ($database_timeline as $item): ?>
					<div class="panel">
						<strong title="tanggal_update">( <?php echo $item['tanggal_update']; ?> )</strong>
						Link: <a href="<?php echo $item['link_dropbox']; ?>">
						<?php echo $item['nama_file_dropbox']; ?>
						</a>
						<br>
						Notes:
						<?php echo $item['notes']; ?>
						(<em title="pelaku"><?php echo $item['pelaku']; ?></em>)
					</div>
					<!-- <div class="panel">
						<strong title="tanggal_update">2 Febuari 2014</strong>
						<br>
						<a class="btn btn-warning" href="<?php echo base_url('dev/dev/update_database'); ?>">UPDATE</a>
						Notes: Penting..!! Klik Tombol 'UPDATE' Untuk Update Database
					</div> -->
				<?php endforeach ?>
			</div>
		</div>
	</div>

<div class="col-lg-6">
<div class="panel panel-primary">
						  <div class="panel-heading">
								<h3 class="panel-title">Update Database!</h3>
						  </div>
						  <?php if (!$db_uptodate): ?>
						  	<div class="panel-body">
								<div class="alert alert-danger">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<strong>PERHATIAN!</strong> Proses update program ECDS telah berhasil. 
									Namun agar fitur-fitur terbaru bisa berjalan, maka database harus diupdate terlebih dahulu.
									Ikuti langkah-langkah berikut untuk mengupdate database ECDS.
								</div>
						  </div>
						  	<ul class="list-group">
									<li class="list-group-item">1. Unduh File Database Berikut (~60Kb)  (Simpan di desktop atau lokasi yang mudah dicari.)
									<a href="https://dl.dropboxusercontent.com/u/4285847/db_ecds_14April14.sql.zip">db_ecds_14April.sql.zip</a>
									</li>
									<li class="list-group-item">2. Klik link berikut: 
									<a href="<?php echo site_url('dev/dropcreatedb') ?>">DROP-CREATEDATABASE</a>
									<em>(Jika sudah program akan kembali ke halaman ini)</em>
									</li>
									<li class="list-group-item">3. Buka phpmyadmin dengan mengklik link berikut:
									<a href="http://localhost/phpmyadmin" target="_blank">PHPMYADMIN</a></li>
									<li class="list-group-item">4. Di phpmyadmin, sebelah kiri atas, klik icon db. Akan terlihat  Klik </li>
									<li class="list-group-item">5. Jika benar, maka di bagian atas (sejajar dengan logo PHPMYADMIN) 
									Akan tertulis: <strong>Server: localhost:8889 »Database: db_ecds</strong>.</li>
									<li class="list-group-item">6. Setelah itu, klik tab import. Letaknya di bagian atas, sejajar dengan tab structure.</li>
									<li class="list-group-item">7. Jika benar, halaman import telah dibuka. Di bagian atas akan tertulis: 
									<strong>Importing into the database "db_ecds"</strong></li>
									<li class="list-group-item">8. Klik Choose File, lalu pilih File Database yang telah diunduh di langkah pertama.</li>
									<li class="list-group-item">9. Jika file database sudah dipilih, masih di halaman yang sama, klik Go dibagian bawah. Setelah itu phpmyadmin akan mulai mengupdate database.</li>
									<li class="list-group-item">10. Jika sudah selesai, akan muncul tulisan:
									<strong> Import has been successfully finished, 70 queries executed. (db_ecds_14April.sql.zip)</strong>.</li>
									<li class="list-group-item">11. Sampai tahap ini proses update database telah selesai, dan sistem ECDS sudah bisa digunakan.</li>
								</ul>
						  <?php else: ?>
						  	<div class="panel-body">
								<div class="alert alert-success">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<strong>SELAMAT!</strong> Proses update program ECDS telah berhasil. Proses update database ECDS telah berhasil.
									VERSI DATABASE: 0205A
								</div>
						  </div>
						  <?php endif ?>
						  
					</div>
</div>