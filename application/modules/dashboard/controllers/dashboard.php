<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
			parent::__construct();
			$this->load->config('menus');
			$this->load->config('db_timeline');
			$this->load->config('globals');
			$this->load->model('mdl_dashboard');	
			$this->load->model('preview/mdl_ctf_preview');	
			$this->load->model('preview/mdl_cmf_preview');	
			$this->load->model('preview/mdl_reef_preview');	
	}
	
	public function index()
	{
		$this->load->config('labels');
		
		$arr_datas = array( 
						'CTF ' => $this->mdl_ctf_preview->list_ctf('100'),
						'CMF ' => $this->mdl_cmf_preview->list_cmf('100'),
						'REEF ' => $this->mdl_reef_preview->list_reef('100')
						);
		
		$data['arr_cds'] = $arr_datas;

		// vdump($this->user->id_asosiasi_user());

		if($this->user->level() < 4)
		{
			$data['monitor_kuota_perusahaan'] = $this->mdl_ctf_preview->monitor_kuota_perusahaan();						
		}
		
		if($this->user->is_asosiasi()){
			$data['monitor_kuota_perusahaan'] = $this->mdl_ctf_preview->monitor_kuota_perusahaan('','',$this->user->id_asosiasi_user());									
		}

		

		if($this->user->is_perusahaan())
		{
			// vdump($this->user->id_perusahaan());
			$data['monitor_kuota_perusahaan'] = $this->mdl_ctf_preview->monitor_kuota_perusahaan($this->user->id_perusahaan());									
		}



		echo Modules::run('templates/page/v_form', //tipe template
							'dashboard', //nama module
							'index', //nama file view
							'', //dari labels.php
							'', //plugin javascript khusus untuk page yang akan di load
							'', //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */