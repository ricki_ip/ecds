<?php $forms = ['ctf','cmf','reef'];
		$layouts = ['layout_ctf','layout_cmf','layout_reef']; 
		$index_forms = 0; ?>
<div class="row">
	
	<?php foreach ($arr_cds as $title => $datas): ?>	
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
			<h3 class="text-center"><?php echo $title; ?></h3>
			<div class="panel panel-default panel-list-cds">				
				<!-- Table -->
				<table class="table table-condensed table-hover ">
					<tbody>
						<?php if ($datas): ?>
							<?php foreach ($datas as $record): ?>
							<?php $id_doc = ''; 
								switch ($index_forms) {
									case 0:
										$id_doc = $record->id_ctf;
										break;
									case 1:
										$id_doc = $record->id_cmf;
										break;
									case 2:
										$id_doc = $record->id_reef;										
										break;
								}
							?>
							<?php if(kos($record->document_number, '-') !== '-'):?>
							<tr>
							<?php else: ?>
							<tr class="bg-warning">	
							<?php endif;?>

								<td>
									<?php echo kos($record->temp_doc_id) ?>
								</td>
								<td>
									<?php echo kos($record->document_number, '-') ?>								
								</td>
							<?php if($forms[$index_forms] === 'reef'): ?>								
								<td>	
									<a href="<?php echo site_url('/preview/'.$forms[$index_forms].'/view/'.$id_doc); ?>" target="_blank">							
										<?php echo kos($record->name).' - '.tgl($record->tanggal_buat, 'H:i (d M)') ?>								
									</a>
								</td>
							<?php else: ?>
								<td>								
									<?php echo kos($record->vessel_name, '-') ?>								
								</td>
								<td>	
									<a href="<?php echo site_url('/preview/'.$forms[$index_forms].'/view/'.$id_doc); ?>" target="_blank">							
										<?php echo kos($record->name).' - '.tgl($record->tanggal_buat, 'H:i (d M)') ?>								
									</a>
								</td>
							<?php endif; ?>								
								<td>
									<a href="<?php echo site_url('/cetak/'.$layouts[$index_forms].'/'.$id_doc); ?>" target="_blank">							
										FORM								
									</a>
								</td>
							</tr>
							<?php endforeach ?>		
						<?php else: ?>
							<tr>
								<td colspan="4">-</td>
							</tr>
						<?php endif ?>										
					</tbody>
				</table>
			</div>
		</div>
	<?php $index_forms++; endforeach; ?>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="panel panel-primary">
					<div class="panel-heading text-center">Monitor Kuota</div>
					<table class="table table-condensed table-hover">
						<tbody>
						<?php if($monitor_kuota_perusahaan):?>
							<?php foreach ($monitor_kuota_perusahaan as $item) {
							?>
							<tr>
								<td class="text-center">
									<strong>
									<?php echo angka($item->total_weight,0); ?> Kg.
									</strong>
									( <?php
										if($item->total_weight > 0): 
											echo angka(intval($item->total_weight)/intval($item->kuota_perus_awal) *100).' %'; 
										else:
											echo "0 %";
										endif;								
									?> )
								</td>
								<td class="text-left">
									<?php echo $item->company_name; ?> 
								</td>
								<td class="text-right">
									<?php echo kos($item->asosiasi_name,'-'); ?>									
								</td>
							</tr>
							<?php
							}
							?>	
						<?php else: ?>
						<tr>
							<td>
								-
							</td>
						</tr>					
						<?php endif; ?>					
						</tbody>
					</table>
				</div>
	</div>
</div>