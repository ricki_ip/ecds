<?php
      $array_menu_item = array(
              array(
                  'title' => 'Delete',
                  'url' =>  site_url('entry/ctf/delete/'),
                  'class' => 'btn-danger',
                  'privillege' => array('Verified' => array(1,2,3),
                              'Waiting' => array(1,2,3,4,5)
                              )
                 ),
              array(
                  'title' => 'Edit',
                  'url' => site_url('entry/ctf/edit/'),
                  'class' => 'btn-warning',
                  'privillege' => array('Verified' => array(1,2,3),
                              'Waiting' => array(1,2,3,4,5)
                              )
                 ),
              );

    $status_verifikasi = $detail_ctf['is_verified'] === 'YA' ? 'Verified' : 'Waiting';
    $link_manage = '';

    foreach ($array_menu_item as $key => $property) {
          if( in_array($this->user->level(), $property['privillege'][$status_verifikasi]) )
          {
            $link_manage .= '<div class="col-lg-6">
                              <a id="btn-'.strtolower($property['title']).'" 
                              href="'.$property['url'].'/'.$detail_ctf['id_ctf'].'" 
                              class="btn btn-large btn-block '.$property['class'].'"
                              >'.$property['title'].'</a>
                            </div>'; 
          }
      }

    $tmpl = array ( 'table_open'  => '<table id="table_ctf_tag" class="table table-hover">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('No.','CCSBT Tag Number', 'Type','Weight (Kg)','Fork length (cm)','Gear Code','Area Of Catch', 'Month of Harvest','Available');

    // var_dump($list_tags);  

    $number = 1;
    foreach ($list_tags as $key => $items) {
      $this->table->add_row($number.'. ',
                            tagnumber($items->ccsbt_tag_number),
                            $items->code_tag,
                            $items->weight,
                            $items->fork_length,
                            $items->gear_code,
                            $items->ccsbt_statistical_area,
                            tgl($items->month_of_harvest,'m/Y'),
                            $items->is_free
                            );
      $number++;
    }
    $table_ctf_tag = $this->table->generate();
?>
<?php if ($detail_ctf['is_verified'] === 'TIDAK'): ?>
<div class="row">
  <div class="col-lg-12">
    <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Info!</strong> Dokumen ini belum diverifikasi. Petugas validator akan memverifikasi dokumen ini untuk mendapatkan nomor dokumen.
        <br>
        <strong>Nomor Dokumen Sementara: </strong> <?php echo $detail_ctf['temp_doc_id']; ?>
        <br>
        Gunakan nomor dokumen sementara ini di form CMF.
    </div>
  </div>     
</div>   
<?php endif ?>

<div class="row">
  
  <?php 
      echo $link_manage;
   ?> 

</div>
<hr>
<div class="row">
  <div class="col-lg-12">
          <?php
          
          $hidden_input = array('id_ctf' => $detail_ctf['id_ctf'],
                                'submit_to' => 'edit');
          echo form_open('id="form_entry" class="form-horizontal" role="form"', $hidden_input);

          ?>
         <?php 
          $attr_document_number = array( 'name' => $form['document_number']['name'],
                                        'label' => $form['document_number']['label'],
                                        'value' => kos($detail_ctf['document_number'],'Belum verifikasi. Nomor sementara:'.$detail_ctf['temp_doc_id'])
                    );
          echo $this->mkform->input_text($attr_document_number);

          $attr_vessel_name = array( 'name' => $form['vessel_name']['name'],
                                        'label' => $form['vessel_name']['label'],
                                        'value' => $detail_ctf['vessel_name']
                    );
          echo $this->mkform->input_text($attr_vessel_name);

          $attr_vessel_registration_number = array( 'name' => $form['vessel_registration_number']['name'],
                                        'label' => $form['vessel_registration_number']['label'],
                                        'value' => $detail_ctf['vessel_registration_number']
                    );
          echo $this->mkform->input_text($attr_vessel_registration_number);

          // $attr_flag = array( 'name' => $form['flag']['name'],
          //                               'label' => $form['flag']['label'],
          //                               'opsi' => Modules::run('vessel/mst_vessel_flag/list_vessel_flag_array'),
          //                               'value' => $detail_ctf['flag']
          //           );
          // echo $this->mkform->input_select2($attr_flag);
          $attr_flag_state = array( 'name' => $form['flag_state']['name'],
                                        'label' => $form['flag_state']['label'],
                                        'value' => $detail_ctf['flag_state']
                    );
          echo $this->mkform->input_text($attr_flag_state);

          $attr_name = array( 'name' => $form['name']['name'],
                                        'label' => $form['name']['label'],
                                        'value' => $detail_ctf['name']
                    );
          echo $this->mkform->input_text($attr_name);

          $attr_title = array( 'name' => $form['title']['name'],
                                        'label' => $form['title']['label'],
                                        'value' => $detail_ctf['title']
                    );
          echo $this->mkform->input_text($attr_title);

          $attr_date = array( 'name' => $form['date']['name'],
                                        'label' => $form['date']['label'],
                                        'value' => tgl($detail_ctf['tanggal_buat'],'F jS,Y')
                    );
          echo $this->mkform->input_text($attr_date);

          // $attr_tanggal_surat_permohonan = array( 
          //                  'mindate' => array('time' => '1 year'), // opsi: '', array('time' => '1 year'), '2012-10-11', tidak wajib ada
          //                 // 'maxdate' => 'various', // opsi: '', array('time' => '1 year'), '2013-10-11', tidak wajib ada
          //                 'default_date' => $detail_pendok['tanggal_surat_permohonan'], // opsi: '', tidak wajib ada
          //                 'placeholder' => '', // wajib ada atau '' (kosong)
          //                 'name' => $form['tanggal_surat_permohonan']['name'], // wajib ada
          //                 'label' => $form['tanggal_surat_permohonan']['label'] // wajib ada
          //               );
          // echo $this->mkform->input_date($attr_tanggal_surat_permohonan);

          

         ?>
  </div>
</div>
<hr>
<div class="row">
  <div class="col-lg-12"> 
          <?php echo $table_ctf_tag; ?>
  </div>
</div>
</form>
<hr>
<div class="row">
  <div class="col-lg-6">
   <a href="<?php echo site_url('cetak/layout_ctf/'.$detail_ctf['id_ctf']); ?>" class="btn btn-large btn-block btn-warning">CETAK FORM</a>
  </div>
  <?php if ($this->user->level() < 5): ?>
    <div class="col-lg-6">
    <?php if ($detail_ctf['is_verified'] === 'TIDAK'): ?>
      <a href='#modal-id' class="btn btn-large btn-block btn-info" data-toggle="modal" >VERIFIKASI</a>
    <?php else: ?>
    <a href="" class="btn btn-large btn-block btn-success">SUDAH DIVERIFIKASI</a>
    <?php endif ?>
    </div>    
  <?php endif ?>
</div>  


<div class="modal fade" id="modal-id">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Verifikasi dokumen CTF ini?</h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-warning">
            <p>Berikut nomor dokumen CTF yang akan didapatkan jika dokumen ini diverifikasi. </p>
        </div>

        <ul class="list-group">
          <li class="list-group-item">
            <strong>Document Number</strong>: <?php echo $calon_doc_number; ?>
          </li> 
        </ul>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <a href="<?php echo site_url('entry/ctf/verifikasi/'.$detail_ctf['id_ctf']); ?>"
          class="btn btn-primary">Verifikasi</a>
      </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal -->

<script>
    
  var init_form = function()
  {
      $("#btn-delete").on("click", function(e){
        e.preventDefault();
        if(
            confirm("Anda akan menghapus dokumen ini. Dokumen CDS lain yang terhubung akan ikut terhapus. Lanjutkan?")
          )
        {
          window.open($(this).attr("href"), "_self");
        }
      });
  }


  var init_data_tables = function(){
     $('#table_ctf_tag').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "2.5  %" , "sClass": "text-center"},
                        { "sWidth": "17.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "5%" , "sClass": "text-center"},
                      ],
        "bFilter": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": false
      });
     
      $("input").prop("disabled", true);
      $("select").prop("disabled", true);
      $("textarea").prop("disabled", true);
    }

  
  s_func.push(init_data_tables);
  s_func.push(init_form);
</script>