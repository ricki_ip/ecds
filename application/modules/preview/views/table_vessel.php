<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_daftar_kapal' class='table table-hover table-bordered'>");
	$this->table->set_template($template);
	$this->table->set_heading($constants['th_table']);
	$counter = 1;
	if($list_kapal){
		foreach ($list_kapal as $item) {
			$link_edit = '<a class="btn btn-warning" href="'.base_url('kapal/kapal/edit/'.$item->id_kapal).'">Edit</a>';
			$link_delete = '<a class="btn btn-danger" href="'.base_url('kapal/kapal/delete/'.$item->id_kapal).'">Hapus</a>';
			$this->table->add_row(
								$counter.'.',
								$item->document_number,
								$item->vessel_name,
								$item->vessel_registration_number,
								$item->flag_state,
								$item->tanda_selar
								// $item->kub_penerima,
								// $item->ketua,
								// $item->anggota,
								// $item->kontak,
								// $item->alamat,
								// $item->sumber_anggaran,
								// $item->bahan_kapal,
								// $item->gt,
								// 'P: '.$item->panjang_kapal.'<br>L: '.$item->lebar_kapal.'<br>D: '.$item->dalam_kapal,
								// $item->mesin,
								// $item->daya,
								// $item->pelabuhan_pangkalan,
								// $item->jenis_alat_tangkap,
								// $item->gross_akte,
								// $item->siup,
								// $item->sipi,
								// $item->kontraktor_pembangunan,
								// $item->lokasi_pembangunan,
								// $link_edit.' '.$link_delete
								);
			$counter++;
		}
	}

	$table_list_kapal = $this->table->generate();
?>

<!-- TAMPIL DATA -->
		<?php
			echo $table_list_kapal;
		?>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		$('#table_daftar_kapal').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": false,
	        "bInfo": false,
	        "bPaginate": true,
"sPaginationType":"full_numbers",
"iDisplayLength": 25,
	        "bSort": true
		} );
	} );
</script>