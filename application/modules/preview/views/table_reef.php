<?php
  //OLAH DATA TAMPIL
  $template = array( "table_open" => "<table id='table_daftar_reef' class='table table-hover table-bordered'>");
  $this->table->set_template($template);
  $this->table->set_heading($constants['th_table']);
  $counter = 1;
  $array_menu_item = array(
              array(
                  'title' => 'View',
                  'url' =>  site_url('preview/reef/view/'),
                  'privillege' => array('Verified' => array(1,2,3,4),
                              'Waiting' => array(1,2,3,4)
                              )
                 ),
              array(
                  'title' => 'Edit',
                  'url' => site_url('entry/reef/edit/'),
                  'privillege' => array('Verified' => array(1,2,3),
                              'Waiting' => array(1,2,3,4)
                              )
                 ),
              array(
                  'title' => 'Cetak',
                  'url' => site_url('cetak/layout_reef/'),
                  'privillege' => array('Verified' => array(1,2,3,4),
                              'Waiting' => array(1,2,3,4)
                              )
                 ),
              );
  if($list_reef){
    foreach ($list_reef as $item) {
      $status_verifikasi = $item->is_verified === 'YA' ? 'Verified' : 'Waiting';

      $link_verifikasi = '<div class="btn-group">
                  <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                    '.$status_verifikasi.' <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu" role="menu">';
      foreach ($array_menu_item as $key => $property) {
          if( in_array($this->user->level(), $property['privillege'][$status_verifikasi]) )
          {
            $link_verifikasi .= '<li><a href="'.$property['url'].'/'.$item->id_reef.'" target="_blank">'.$property['title'].'</a></li>'; 
          }
      } 
      $link_verifikasi .= '</ul>
                          </div>';

      $this->table->add_row(
                $counter.'.',
                '<a href="'.base_url('preview/reef/view/'.$item->id_reef).'">'.kos($item->document_number, $item->temp_doc_id).'</a>',
                '<a href="'.base_url('preview/cmf/view/'.$item->id_cmf).'">'.kos($item->cmf_document_numbers, '-').'</a>',
                $item->exporter_name,
                $item->exporter_company_name,
                $link_verifikasi
                // $item->authority_name_title,
                // $item->authority_signature,
                // $item->authority_date
                );
      $counter++;
    }
  }

  $table_list_reef = $this->table->generate();
?>

<!-- TAMPIL DATA -->
    <?php
      echo $table_list_reef;
    ?>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
  $(document).ready( function () {
    $('#table_daftar_reef').dataTable( {
      "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
      "aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        // {"sClass": "text-center"},
                        // {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
          "bFilter": true,
          "bAutoWidth": false,
          "bInfo": false,
          "bPaginate": true,
"sPaginationType":"full_numbers",
"iDisplayLength": 25,
          "pagingType": "full_numbers",
          "displayLength": 25,
          "bSort": true
    } );
  } );
</script>