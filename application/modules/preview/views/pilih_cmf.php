<!-- TAMPIL DATA -->
    <div id="panel-pilih-kapal" style="margin-left:12px; padding-bottom:15px;" >
        
        
        <div class="row" style="padding-bottom: 15px;">
            <div class="col-lg-3 text-right" style=" padding-right: 25px;">
              <strong>CMF | CTF Document Number</strong>
            </div>
            <div class="col-lg-8" style="margin-left: 5px;padding-left: 0px;padding-right: 20px;">
               <input id="start_search" name="id_cmf" type="text" class="bigdrop">
                <input  id="inputVessel_name" name="cmf_document" type="hidden" class="form-control">
            </div>
        </div>

          <style type="text/css">
            .list-group-item{
              padding: 2px 30px 0px 15px;
              background-color:transparent;
              border:0px;
            }
          </style>


        </div>


    
  </div>


<!-- ADDITIONAL JAVASCRIPT -->
<script>
  var search_response_time = 2000, //2 Detik
    thread = null,
    url_search_cmf = "<?php echo base_url('preview/cmf/search_doc_number'); ?>";
    // url_set_data     = "<?php echo base_url('refCMFDOC/mst_CMFDOC/set_data'); ?>";

  function formatListCMFDOCResult(CMFDOC)
  {
    console.log(CMFDOC);
    var doc_identifier = CMFDOC.is_verified === 'TIDAK' ? CMFDOC.temp_doc_id : CMFDOC.document_number+' (Verified)';
    html = "<table id='"+CMFDOC.id_cmf+"' class='table table-condensed table-bordered'><tr>"
       + "<th>CMF / CTF Document Numbers</th>"
       + "<th>Name of Processing Establishment</th>"
       + "<th>Entry Date</th></tr><tr>"
       + "<td>"+doc_identifier+" / "+CMFDOC.ctf_document_number+"</td>"
       + "<td>"+CMFDOC.name_processing_establishment+"</td>"
       + "<td>"+CMFDOC.result_tanggal_buat+"</td>"
       + "</tr></table>"
    return  html;

  }

  function formatListCMFDOCSelection(CMFDOC)
  {
    <?php echo $callback_function; ?>(CMFDOC);
    var doc_identifier = CMFDOC.is_verified === 'TIDAK' ? CMFDOC.temp_doc_id : CMFDOC.document_number;
        doc_identifier += ' | '+CMFDOC.ctf_document_number;
    return doc_identifier;
  }

  function formatSearchingText(term)
  {
    return "Sedang mencari..";
  }

  function formatKurangText(term, minLength)
  {
    var char_yang_kurang = minLength - term.length;
      text_info = "Pencarian CMF berdasarkan <strong>Nomor Dokumen CMF</strong> / <strong>Nomor Dokumen CMF Sementara</strong>. ";

      text_info += "Input minimal "+char_yang_kurang+" angka";  
    return text_info;
  }

  function formatNotFound(term)
  { 
    return "Pencarian <strong>"+term+"</strong> tidak menemukan hasil. Dokumen CMF belum diverifikasi?";
  }

  var search_cmf_document = function (){
      $("#start_search").select2({
                                    id: function(e) { return e.id_cmf },
                                    allowClear: false,    
                                    placeholder: "Search For CMF Document Number or Temporary Document Number..",
                                    width: "80%",
                                    cache: true,
                                    minimumInputLength: 3,
                                    dropdownCssClass: "bigdrop",
                                    ajax: {
                                            url: url_search_cmf,
                                            dataType: "json",
                                            quietMillis: 2000,
                                            data: function(term, page){
                                                           return {
                                                                    q: term,
                                                                  };
                                        },
                                        results: function(data, page){
                                                     return {results: data.result};
                                            }
                                    },
                                    formatResult: formatListCMFDOCResult,
                                    formatSelection: formatListCMFDOCSelection,
                                    formatSearching: formatSearchingText,
                                    formatInputTooShort: formatKurangText,
                                    formatNoMatches: formatNotFound
                                    });

  }

  function auto_width(){
      //jQuery.fn.exists = function(){return this.length>0;}
      if($("#start_search").val().length < 30  )
      {
         $("#start_search").css({"width":"32%"});
      }
      else{
         var panjang = $("#start_search").val().length;
         $("#start_search").css({"width":(panjang/3)+32+"%"}); 
      }
    }

    s_func.push(auto_width);
    s_func.push(search_cmf_document);
</script>