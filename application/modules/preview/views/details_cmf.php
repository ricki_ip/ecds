<?php
    $array_menu_item = array(
            array(
                'title' => 'Delete',
                'url' =>  site_url('entry/cmf/delete/'),
                'class' => 'btn-danger',
                'privillege' => array('Verified' => array(1,2,3),
                            'Waiting' => array(1,2,3,4,5)
                            )
               ),
            array(
                'title' => 'Edit',
                'url' => site_url('entry/cmf/edit/'),
                'class' => 'btn-warning',
                'privillege' => array('Verified' => array(1,2,3),
                            'Waiting' => array(1,2,3,4,5)
                            )
               ),
            );
    $status_verifikasi = $detail_cmf['is_verified'] === 'YA' ? 'Verified' : 'Waiting';
    $link_manage = '';

    foreach ($array_menu_item as $key => $property) {
          if( in_array($this->user->level(), $property['privillege'][$status_verifikasi]) )
          {
            $link_manage .= '<div class="col-lg-6">
                              <a id="btn-'.strtolower($property['title']).'"
                              href="'.$property['url'].'/'.$detail_cmf['id_cmf'].'"
                              class="btn btn-large btn-block '.$property['class'].'"
                              >'.$property['title'].'</a>
                            </div>';
          }
      }

    $tmpl = array ( 'table_open'  => '<table id="table_cmf_tag" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('No.','Product', 'Type', 'Month of Catch','Gear Code','CCSBT Statistical Area', 'Net Weight (Kg)','Total Number of whole Fish' );

    if($detail_cmf_fish)
    {
      $number = 1;
      foreach ($detail_cmf_fish as $key => $item) {
        $product = $item->product_type === 'F' ? 'FRESH' : 'FROZEN';
        $this->table->add_row( $number.". ",
          $product,
          $item->code_tag,
          tgl($item->month_of_harvest,'m/Y'),
          $item->nama_gear_code,
          $item->ccsbt_statistical_area,
          $item->net_weight,
          $item->total_fish);
        $number++;
      }
    }

    $table_cmf_tag = $this->table->generate();

    $this->table->clear();
 ?>
  <?php

          $hidden_input = array('id_cmf' => $detail_cmf['id_cmf'],
                                'submit_to' => 'edit');
          echo form_open('id="form_entry" class="form-horizontal" role="form"', $hidden_input);

  ?>
  <?php if ($detail_cmf['is_verified'] === 'TIDAK'): ?>
<div class="row">
  <div class="col-lg-12">
    <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Info!</strong> Dokumen ini belum diverifikasi. Lakukan verifikasi dokumen untuk mendapatkan nomor dokumen.
        <br>
        <strong>Nomor Dokumen Sementara: </strong> <?php echo $detail_cmf['temp_doc_id']; ?>
        <br>
    </div>
  </div>
</div>
<?php endif ?>
<div class="row">

  <?php
      echo $link_manage;
   ?>

</div>
<hr>
<div class="row">
  <div class="col-lg-12">

         <?php
          $attr_document_number = array( 'name' => $form['document_number']['name'],
                                        'label' => $form['document_number']['label'],
                                        'value' => kos($detail_cmf['document_number'], 'Belum verifikasi. Nomor sementara:'.$detail_cmf['temp_doc_id'])
                    );
          echo $this->mkform->input_text($attr_document_number);

          $attr_ctf_document_numbers = array( 'name' => $form['ctf_document_numbers']['name'],
                                        'label' => $form['ctf_document_numbers']['label'],
                                        'value' => kos($detail_cmf['ctf_document_number'], 'Belum verifikasi. Nomor sementara:'.$detail_cmf['ctf_temp_doc_id'])
                    );
          ?>
          <p class="text-center">
            <strong>Link to CTF : </strong>
            <a href="<?php echo site_url('preview/ctf/view/'.$detail_cmf['id_ctf']); ?>">
              <?php echo $detail_cmf['ctf_temp_doc_id']; ?>
            </a>
          </p>
          <?php
          // echo '<pre>';
          // print_r($detail_cmf);
          // echo '</pre>';
          echo $this->mkform->input_text($attr_ctf_document_numbers);

          $attr_name_processing_establishment = array( 'name' => $form['name_processing_establishment']['name'],
                                        'label' => $form['name_processing_establishment']['label'],
                                        'value' => $detail_cmf['name_processing_establishment']
                    );
          echo $this->mkform->input_text($attr_name_processing_establishment);

          // $attr_flag = array( 'name' => $form['flag']['name'],
          //                               'label' => $form['flag']['label'],
          //                               'opsi' => Modules::run('vessel/mst_vessel_flag/list_vessel_flag_array'),
          //                               'value' => $detail_cmf['flag']
          //           );
          // echo $this->mkform->input_select2($attr_flag);
          $attr_address_processing_establishment = array( 'name' => $form['address_processing_establishment']['name'],
                                        'label' => $form['address_processing_establishment']['label'],
                                        'value' => $detail_cmf['address_processing_establishment']
                    );
          echo $this->mkform->input_text($attr_address_processing_establishment);

           $attr_authority_name_title = array( 'name' => $form['authority_name_title']['name'],
                                        'label' => $form['authority_name_title']['label'],
                                        'value' => $detail_cmf['cmf_authority_name_title']
                    );
          echo $this->mkform->input_text($attr_authority_name_title);

           $attr_authority_date = array( 'name' => $form['authority_date']['name'],
                                        'label' => $form['authority_date']['label'],
                                        'value' => tgl($detail_cmf['tanggal_buat'],'F jS,Y')
                    );
          echo $this->mkform->input_text($attr_authority_date);


         ?>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
          <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">Description of Fish</h3>
              </div>
              <div class="panel-body">
                <?php echo $table_cmf_tag; ?>
              </div>
          </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
          <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">Product Destination :
                <?php echo strtoupper($destination); ?>
                </h3>
              </div>
              <div class="panel-body">
                <?php if ($destination === 'domestic'): ?>
                   <?php
              $attr_domestic_name = array( 'name' => $form['domestic_name']['name'],
                                        'label' => $form['domestic_name']['label'],
                                        'value' => kos($detail_domestic['name'])
                    );
              echo $this->mkform->input_text($attr_domestic_name);

              $attr_domestic_address = array( 'name' => $form['domestic_address']['name'],
                                        'label' => $form['domestic_address']['label'],
                                        'value' => kos($detail_domestic['address'])
                    );
              echo $this->mkform->input_text($attr_domestic_address);

              $attr_domestic_weight = array( 'name' => $form['domestic_weight']['name'],
                                        'label' => $form['domestic_weight']['label'],
                                        'value' => kos($detail_domestic['weight'])
                    );
              echo $this->mkform->input_text($attr_domestic_weight);
            ?>
                <?php else: ?>
                    <!-- EXPORT -->
                   <?php
              $attr_export_destination = array( 'name' => $form['export_destination']['name'],
                                        'label' => $form['export_destination']['label'],
                                        'value' => kos($detail_export['negara_export'])
                    );
              echo $this->mkform->input_text($attr_export_destination);
             ?>
              <hr>
              <p class="text-right">Point of Export</p>
              <?php
              $attr_export_city = array( 'name' => $form['export_city']['name'],
                                        'label' => $form['export_city']['label'],
                                        'value' => kos($detail_export['kota_asal'])
                    );
              echo $this->mkform->input_text($attr_export_city);

              $attr_export_province = array( 'name' => $form['export_province']['name'],
                                        'label' => $form['export_province']['label'],
                                        'value' => kos($detail_export['propinsi_asal'])
                    );
              echo $this->mkform->input_text($attr_export_province);

              $attr_export_state_entity = array( 'name' => $form['export_state_entity']['name'],
                                        'label' => $form['export_state_entity']['label'],
                                        'value' => kos($detail_export['state_entity'])
                    );
              echo $this->mkform->input_text($attr_export_state_entity);
             ?>
              <hr>
              <p class="text-right">Certification by Exporter</p>
             <?php
              $attr_exporter_name = array( 'name' => $form['exporter_name']['name'],
                                        'label' => $form['exporter_name']['label'],
                                        'value' => kos($detail_export['name'])
                    );
              echo $this->mkform->input_text($attr_exporter_name);

              $attr_exporter_company_name = array( 'name' => $form['exporter_company_name']['name'],
                                        'label' => $form['exporter_company_name']['label'],
                                        'value' => kos($detail_cmf['exporter_company'])
                    );
              echo $this->mkform->input_text($attr_exporter_company_name);
              ?>
              <?php endif ?>
              </div>
          </div>
  </div>
</div>

</form>

<div class="row">
  <div class="col-lg-6">
   <a href="<?php echo site_url('cetak/layout_cmf/'.$detail_cmf['id_cmf']); ?>" class="btn btn-large btn-block btn-warning">CETAK FORM</a>
  </div>
  <?php if ($this->user->level() < 5): ?>
    <div class="col-lg-6">
    <?php if ($detail_cmf['is_verified'] === 'TIDAK'): ?>
      <a href='#modal-id' class="btn btn-large btn-block btn-info" data-toggle="modal" >VERIFIKASI</a>
    <?php else: ?>
    <a href="" class="btn btn-large btn-block btn-success">SUDAH DIVERIFIKASI</a>
    <?php endif ?>
    </div>
  <?php endif ?>
</div>


<div class="modal fade" id="modal-id">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Verifikasi dokumen CMF ini?</h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-warning">
            <p>Berikut nomor dokumen CMF yang akan didapatkan jika dokumen ini diverifikasi. </p>
        </div>

        <ul class="list-group">
          <li class="list-group-item">
            <strong>Document Number</strong>: <?php echo $calon_doc_number; ?>
          </li>

        </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <a href="<?php echo site_url('entry/cmf/verifikasi/'.$detail_cmf['id_cmf'].'/'.$detail_cmf['id_ctf']); ?>"
          class="btn btn-primary">Verifikasi</a>
      </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal -->
<script>

  var init_form = function() 
  {
     $("#btn-delete").on("click", function(e){
        e.preventDefault();
        if(
            confirm("Anda akan menghapus dokumen ini. Dokumen REEF lain yang terhubung akan ikut terhapus. Lanjutkan?")
          )
        {
          window.open($(this).attr("href"), "_self");
        }
      });

      $("input").prop("disabled", true);
      $("select").prop("disabled", true);
      $("textarea").prop("disabled", true);
      // $("input").prop("disabled", true);
      // $("select").prop("disabled", true);
      // $("textarea").prop("disabled", true);
  }


  var submit_listener = function(){
    $(".btn-submit").click(function(event){
      event.preventDefault();
      var submit_to = $(this).data('submitTo');
      $("input[name=submit_to]").val(submit_to);
      $("#form_entry").submit();
    });
  };
  s_func.push(submit_listener);
  s_func.push(init_form);
  // s_func.push(cek_final);
</script>
