<?php 
   $array_menu_item = array(
            array(
                'title' => 'Delete',
                'url' =>  site_url('entry/reef/delete/'),
                'class' => 'btn-danger',
                'privillege' => array('Verified' => array(1,2,3),
                            'Waiting' => array(1,2,3,4,5)
                            )
               ),
            array(
                'title' => 'Edit',
                'url' => site_url('entry/reef/edit/'),
                'class' => 'btn-warning',
                'privillege' => array('Verified' => array(1,2,3),
                            'Waiting' => array(1,2,3,4,5)
                            )
               ),
            );
    $status_verifikasi = $detail_reef['is_verified'] === 'YA' ? 'Verified' : 'Waiting';
    $link_manage = '';

    foreach ($array_menu_item as $key => $property) {
          if( in_array($this->user->level(), $property['privillege'][$status_verifikasi]) )
          {
            $link_manage .= '<div class="col-lg-6">
                              <a id="btn-'.strtolower($property['title']).'"
                              href="'.$property['url'].'/'.$detail_reef['id_reef'].'"
                              class="btn btn-large btn-block '.$property['class'].'"
                              >'.$property['title'].'</a>
                            </div>';
          }
    }

    $button_check_all = 'Check All <input type="checkbox" name="check_all" id="btn_checkall" value="checkall">';
    $tmpl = array ( 'table_open'  => '<table id="table_reef_fish" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('No.','Product', 'Type', 'Weight (Kg)','Total Number of whole Fish');

    if($detail_reef_fish)
    {
      $number = 1;
      foreach ($detail_reef_fish as $key => $item) {
        $product = $item->product_type === 'F' ? 'FRESH' : 'FROZEN';
        $this->table->add_row( $number.". ",
          $product,
          $item->code_tag,
          $item->net_weight,
          $item->total_fish);

        $number++;
      }
    }

    $table_reef_fish = $this->table->generate();

    $this->table->clear();
    $tmpl = array ( 'table_open'  => '<table id="table_cmf_tag" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('No.','Product', 'Type', 'Weight (Kg)','Total Number of whole Fish' );

    if($detail_cmf_fish)
    {
      $number = 1;
      foreach ($detail_cmf_fish as $key => $item) {
        $product = $item->product_type === 'F' ? 'FRESH' : 'FROZEN';
        $this->table->add_row( $number.". ",
          $product,
          $item->code_tag,
          $item->net_weight,
          $item->total_fish);
        $number++;
      }
    }


    $table_cmf_tag = $this->table->generate();


    $this->table->clear();

    
?>

<?php
  echo form_open('', 'id="form_entry" class="form-horizontal" role="form"');
?>
<?php if ($detail_reef['is_verified'] === 'TIDAK'): ?>
<div class="row">
  <div class="col-lg-12">
    <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Info!</strong> Dokumen ini belum diverifikasi. Lakukan verifikasi dokumen untuk mendapatkan nomor dokumen.
        <br>
        <strong>Nomor Dokumen Sementara: </strong> <?php echo $detail_reef['temp_doc_id']; ?>
        <br>
    </div>
  </div>     
</div>   
<?php endif ?>
<div class="row">

  <?php
      echo $link_manage;
   ?>

</div>
<hr>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">EXPORT SECTION</h3>
        </div>
        <div class="panel-body">
              <?php 
            // vdump($detail_reef);

              $attr_document_number = array( 'name' => $form['document_number']['name'],
                                            'label' => $form['document_number']['label'],
                                            'value' => kos($detail_reef['document_number'], 'Belum verifikasi. Nomor sementara:'.$detail_reef['temp_doc_id'])
                        );
              echo $this->mkform->input_text($attr_document_number);

              $attr_cmf_document_numbers = array( 'name' => $form['cmf_document_numbers']['name'],
                                            'label' => $form['cmf_document_numbers']['label'],
                                            'value' => kos($detail_reef['cmf_document_numbers'], 'Belum verifikasi. ')
                        );
              echo $this->mkform->input_text($attr_cmf_document_numbers);

              $attr_ctf_document_numbers = array( 'name' => $form['ctf_document_numbers']['name'],
                                            'label' => $form['ctf_document_numbers']['label'],
                                            'value' => kos($detail_reef['ctf_document_numbers'], 'Belum verifikasi. ')
                        );
              echo $this->mkform->input_text($attr_ctf_document_numbers);
              ?>
              <hr>
              <?php
              $attr_type_of_export = array( 'name' => $form['type_of_export']['name'],
                                        'label' => $form['type_of_export']['label'],
                                        'value' => kos($detail_reef['type_of_export'])
                    );
              echo $this->mkform->input_text($attr_type_of_export);

              $attr_export_destination = array( 'name' => $form['export_destination']['name'],
                                        'label' => $form['export_destination']['label'],
                                        'value' => kos($detail_reef['export_destination'])
                    );
              echo $this->mkform->input_text($attr_export_destination);
             ?>
            <input type="hidden" name="name_export_destination" id="inputProduct_destination" class="form-control" value="">
            <hr>
            <p class="text-center">Point of Export</p>
            <?php
              $attr_point_export_city = array( 'name' => $form['point_export_city']['name'],
                                        'label' => $form['point_export_city']['label'],
                                        'value' => kos($detail_reef['point_export_city'])
                    );
              echo $this->mkform->input_text($attr_point_export_city);

              // $attr_point_export_province = array( 'name' => $form['point_export_province']['name'],
              //                           'label' => $form['point_export_province']['label'],
              //                           'value' => kos($detail_reef['point_export_province'])
              //       );
              // echo $this->mkform->input_text($attr_point_export_province);

              $attr_point_export_state_entity = array( 'name' => $form['point_export_state_entity']['name'],
                                        'label' => $form['point_export_state_entity']['label'],
                                        'value' => kos($detail_reef['point_export_state_entity'])
                    );
              echo $this->mkform->input_text($attr_point_export_state_entity);
             ?>
            <hr>
            <p class="text-center">Certification by Exporter</p>
             <?php
              $attr_exporter_name = array( 'name' => $form['exporter_name']['name'],
                                        'label' => $form['exporter_name']['label'],
                                        'value' => kos($detail_reef['exporter_name'])
                    );
              echo $this->mkform->input_text($attr_exporter_name);

              $attr_exporter_company = array( 'name' => $form['exporter_company']['name'],
                                        'label' => $form['exporter_company']['label'],
                                        'value' => kos($detail_reef['exporter_company'])
                    );
              echo $this->mkform->input_text($attr_exporter_company);
             ?>
          <input type="hidden" name="exporter_company_name" id="inputExporter_company_name" class="form-control" value="">
          <input type="hidden" name="exporter_company_address" id="inputExporter_company_address" class="form-control" value="">
          <hr>
          <p class="text-center"> Validation by Authority </p>
   
          <?php

          $attr_authority_name_title = array( 'name' => 'id_pejabat',
                                        'label' => $form['authority_name_title']['label'],
                                        'value' => kos($detail_reef['id_pejabat'])
                    );
          echo $this->mkform->input_text($attr_authority_name_title);

          $attr_authority_date = array( 'name' => $form['authority_date']['name'],
                                        'label' => $form['authority_date']['label'],
                                        'value' => kos($detail_reef['authority_date'])
                    );
          echo $this->mkform->input_date($attr_authority_date);

         ?>
         <input type="hidden" name="authority_name_title" id="inputAuthority_name_title" class="form-control" value="">
         <?php //echo $table_reef_fish; ?>
         <input type="hidden" name="id_ctf" id="inputId_ctf" class="form-control" value="">
         <input type="hidden" name="id_cmf" id="inputId_cmf" class="form-control" value="">
         <input type="hidden" name="cmf_document_numbers" id="inputCtf_document_numbers" class="form-control" value="">
         <input type="hidden" name="cmf_document_numbers" id="inputCmf_document_numbers" class="form-control" value="">
         <input type="hidden" name="type_of_shipment" id="inputShipment_type" class="form-control" value="FULL">
         <hr>
        </div>
      </div>  
  </div>
</div>
<hr>
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
        Description of fish from previous CDS Document
        </div>
        <div class="panel-body">
        <?php 
              $attr_name_processing_establishment = array( 'name' => 'id_perusahaan',
                                        'label' => $form['name_processing_establishment']['label'],
                                        'value' => kos($detail_reef['id_perusahaan'])
                    );
              echo $this->mkform->input_text($attr_name_processing_establishment);
          
              $attr_previous_state_entity = array( 'name' => $form['previous_state_entity']['name'],
                                        'label' => $form['previous_state_entity']['label'],
                                        'value' => kos($detail_reef['previous_state_entity'])
                    );
              echo $this->mkform->input_text($attr_previous_state_entity);

              $attr_previous_date_landing = array( 'name' => $form['previous_date_landing']['name'],
                                        'label' => $form['previous_date_landing']['label'],
                                        'value' => kos($detail_reef['previous_date_landing'])
                    );
              echo $this->mkform->input_text($attr_previous_date_landing);
        ?>
        <input type="hidden" name="name_processing_establishment" id="inputEstablishment_name" class="form-control" value="">
        <input type="hidden" name="address_processing_establishment" id="inputEstablishment_address" class="form-control" value="">  
          
        <?php echo $table_cmf_tag; ?>
        </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
        <p class="text-center lead">Shipment</p>
  </div>
  <div class="col-lg-6 col-lg-offset-3">
    <button type="button" id="btn_set_reef_full" class="btn btn-medium btn-block btn-success" disabled>
    <?php echo kos($detail_reef['type_of_shipment']) ?> SHIPMENT</button>           
  </div>

</div>
<hr>
<div class="row">
  <div class="col-lg-12                                                                                 ">
    <div class="panel panel-default">
        <div class="panel-heading">
        Description of fish being exported
        </div>
        <div class="panel-body">          
           <?php echo $table_reef_fish; ?>
           <hr>
           <?php           
          $attr_description_for_other = array( 'name' => 'description_for_other',
                                        'label' => 'For Other (OT): Describe the Type Of Product :',
                                        'value' => kos($detail_reef['description_for_other'])
                    );
              echo $this->mkform->input_text($attr_description_for_other);
          ?>
           <!-- <input type="hidden" name="id_tags" id="inputCtf_tags" class="form-control" value=""> -->
        </div>
    </div>
  </div>
</div>

<hr>
</form>

<div class="row">
  <div class="col-lg-6">
   <a href="<?php echo site_url('cetak/layout_reef/'.$detail_reef['id_reef']); ?>" class="btn btn-large btn-block btn-warning">CETAK FORM</a>
  </div>
  <?php if ($this->user->level() < 5): ?>
    <div class="col-lg-6">
    <?php if ($detail_reef['is_verified'] === 'TIDAK'): ?>
      <a href='#modal-id' class="btn btn-large btn-block btn-info" data-toggle="modal" >VERIFIKASI</a>
    <?php else: ?>
    <a href="" class="btn btn-large btn-block btn-success">SUDAH DIVERIFIKASI</a>
    <?php endif ?>
    </div>    
  <?php endif ?>
</div>

<div class="modal fade" id="modal-id">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Verifikasi dokumen REEF ini?</h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-warning">
            <p>Berikut nomor dokumen REEF yang akan didapatkan jika dokumen ini diverifikasi. </p>
        </div>

        <ul class="list-group">
          <li class="list-group-item">
            <strong>Document Number</strong>: <?php echo $calon_doc_number; ?>
          </li> 
        </ul>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <a href="<?php echo site_url('entry/reef/verifikasi/'.$detail_reef['id_reef']); ?>"
          class="btn btn-primary">Verifikasi</a>
      </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal -->
<script>
   
  var opsi_type_tag = <?php echo $opsi_type_tag; ?>;
  var opsi_gear_code = <?php echo $opsi_gear_code; ?>;
  var opsi_ccsbt_area = <?php echo $opsi_ccsbt_area; ?>;
  var opsi_pejabat = <?php echo $opsi_pejabat; ?>;
  var opsi_processing = <?php echo $opsi_processing; ?>;
  var opsi_negara = <?php echo $opsi_negara; ?>;
  var opsi_kabkota = <?php echo $opsi_kabkota; ?>;

  var action_export = '<?php echo 'cmf/input_export'; ?>';
  var action_domestic = '<?php echo 'cmf/input_domestic'; ?>';

  var init_form = function()
  {
    $("#btn-delete").on("click", function(e){
        e.preventDefault();
        if(
            confirm("Anda akan menghapus dokumen ini. Lanjutkan?")
          )
        {
          window.open($(this).attr("href"), "_self");
        }
      });
    $("#id_previous_state_entity").prop('readonly', true);    
    $("#id_previous_date_landing").prop('readonly', true);
    $("#id_type_of_export").prop('readonly', true);
    $("#id_document_number").prop('readonly', true);
    $("#id_cmf_document_numbers").prop('readonly', true);
    $("#id_ctf_document_numbers").prop('readonly', true);

    $("#id_flag_state").prop('readonly',true);
    $("#id_exporter_name").prop('readonly', true);
    $('#id_point_export_state_entity').prop('readonly',true).val('INDONESIA');


    $("#id_pejabat").removeClass('form-control');
    $("#id_pejabat").select2({  width: "100%",
                                              data: opsi_pejabat
                                            });
    $("#id_pejabat").select2("readonly",true);

    // $("#id_name_processing_establishment").prop('readonly',true);
    // $("#id_address_processing_establishment").prop('readonly',true);

    
    $("#id_export_destination").removeClass('form-control');
    $("#id_export_destination").select2({  width: "100%",
                                              data: opsi_negara
                                        });
    $("#id_export_destination").select2("readonly", true);

    $("#id_point_export_city").removeClass('form-control');
    $("#id_point_export_city").select2({  width: "100%",
                                              data: opsi_kabkota
                                        });    
    $("#id_point_export_city").select2("readonly", true);


    $("#id_perusahaan").removeClass('form-control');
    $("#id_perusahaan").select2({  width: "100%",
                                              data: opsi_processing
                                            });
    $("#id_perusahaan").select2("readonly", true);

    $("#id_exporter_company").removeClass('form-control');
    $("#id_exporter_company").select2({  width: "100%",
                                              data: opsi_processing
                                            });  
    $("#id_exporter_company").select2("readonly", true); 

 
  }


  var rowCount = { reef: 0, cmf: 0 };
  var opsi_product = [{ id: "F", text: "FRESH"}, { id: "FR", text: "FROZEN"}];
  var init_data_tables = function()
  {
      $('#table_reef_fish').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                       { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "25%" , "sClass": "text-center"},
                        { "sWidth": "20%" , "sClass": "text-center"},
                        { "sWidth": "20%" , "sClass": "text-center"},
                        { "sWidth": "20%" , "sClass": "text-center"},
                      ],
        "bFilter": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": false
      });

       $('#table_cmf_tag').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "15%" , "sClass": "text-center"},
                        { "sWidth": "20%" , "sClass": "text-center"},
                        { "sWidth": "15%" , "sClass": "text-center"},
                        { "sWidth": "35%" , "sClass": "text-center"},
                      ],
        "bFilter": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": false
      });
      
    }
   
  s_func.push(init_form);
  s_func.push(init_data_tables);


</script>