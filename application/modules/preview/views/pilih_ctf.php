<!-- TAMPIL DATA -->
    <div id="panel-pilih-kapal" style="margin-left:12px; padding-bottom:15px;" >
        
        
        <div class="row" style="padding-bottom: 15px;">
            <div class="col-lg-3 text-right" style=" padding-right: 25px;">
              <strong>CTF Document Number</strong>
            </div>
            <div class="col-lg-8" style="margin-left: 5px;padding-left: 0px;padding-right: 20px;">
               <input id="start_search" name="id_ctf" type="text" class="bigdrop">
                <input  id="inputVessel_name" name="ctf_document" type="hidden" class="form-control">
            </div>
        </div>

          <style type="text/css">
            .list-group-item{
              padding: 2px 30px 0px 15px;
              background-color:transparent;
              border:0px;
            }
          </style>


        </div>


    
  </div>


<!-- ADDITIONAL JAVASCRIPT -->
<script>
  var search_response_time = 2000, //2 Detik
    thread = null,
    url_search_ctf = "<?php echo base_url('preview/ctf/search_doc_number'); ?>";
    // url_set_data     = "<?php echo base_url('refCTFDOC/mst_CTFDOC/set_data'); ?>";

  function formatListCTFDOCResult(CTFDOC)
  {
    console.log(CTFDOC);
    var doc_identifier = CTFDOC.is_verified === 'TIDAK' ? CTFDOC.temp_doc_id : CTFDOC.document_number+' (Verified)';
    html = "<table id='"+CTFDOC.id_ctf+"' class='table table-condensed table-bordered'><tr>"
       + "<th>CTF Document Number/Temporary Document Number</th>"
       + "<th>Name of Catching Vessel</th>"
       + "<th>Entry Date</th></tr><tr>"
       + "<td>"+doc_identifier+"</td>"
       + "<td>"+CTFDOC.vessel_name+"</td>"
       + "<td>"+CTFDOC.result_tanggal_buat+"</td>"
       + "</tr></table>"
    return  html;

  }

  function formatListCTFDOCSelection(CTFDOC)
  {
    <?php echo $callback_function; ?>(CTFDOC);
    var doc_identifier = CTFDOC.is_verified === 'TIDAK' ? CTFDOC.temp_doc_id : CTFDOC.document_number+' (Verified)';
    return doc_identifier;
  }

  function formatSearchingText(term)
  {
    return "Sedang mencari..";
  }

  function formatKurangText(term, minLength)
  {
    var char_yang_kurang = minLength - term.length;
      text_info = "Pencarian CTF berdasarkan <strong>Nomor Dokumen CTF</strong> / <strong>Nomor Dokumen CTF Sementara</strong>. ";

      text_info += "Input minimal "+char_yang_kurang+" angka";  
    return text_info;
  }

  function formatNotFound(term)
  { 
    return "Pencarian <strong>"+term+"</strong> tidak menemukan hasil. Dokumen CTF belum diverifikasi?";
  }

  var search_ctf_document = function (){
      $("#start_search").select2({
                                    id: function(e) { return e.id_ctf },
                                    allowClear: false,    
                                    placeholder: "Search For CTF Document Number or Temporary Document Number..",
                                    width: "80%",
                                    cache: true,
                                    minimumInputLength: 3,
                                    dropdownCssClass: "bigdrop",
                                    ajax: {
                                            url: url_search_ctf,
                                            dataType: "json",
                                            quietMillis: 2000,
                                            data: function(term, page){
                                                           return {
                                                                    q: term,
                                                                  };
                                        },
                                        results: function(data, page){
                                                     return {results: data.result};
                                            }
                                    },
                                    formatResult: formatListCTFDOCResult,
                                    formatSelection: formatListCTFDOCSelection,
                                    formatSearching: formatSearchingText,
                                    formatInputTooShort: formatKurangText,
                                    formatNoMatches: formatNotFound
                                    });

  }

  function auto_width(){
      //jQuery.fn.exists = function(){return this.length>0;}
      if($("#start_search").val().length < 30  )
      {
         $("#start_search").css({"width":"32%"});
      }
      else{
         var panjang = $("#start_search").val().length;
         $("#start_search").css({"width":(panjang/3)+32+"%"}); 
      }
    }

    s_func.push(auto_width);
    s_func.push(search_ctf_document);
</script>