<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ctf extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
			parent::__construct();
			$this->load->config('menus');
			$this->load->config('db_timeline');
			$this->load->config('globals');
			$this->load->model('mdl_ctf_preview');
	}

	public function _generate_doc_number()
	{
		$data['doc_area'] = $this->user->code_area();
		$data['doc_prefix'] = "T-ID";
		$data['doc_year'] = date("y");

		//get last ctf id from table counter
		$get_last_ctf_id = $this->mdl_ctf_entry->last_ctf_id($data['doc_area'], $data['doc_year']);
		$last_ctf_id = $get_last_ctf_id->last_ctf_id;
		$last_ctf_id++;
		$ctf_doc_id = str_pad($last_ctf_id, 4, "0", STR_PAD_LEFT);

		$data['doc_id'] = $ctf_doc_id;

		$data['calon_doc_number'] = $data['doc_prefix'].$data['doc_year'].'-'.$data['doc_area'].'-'.$data['doc_id'];

		return $data;
	}
	
	
	public function index()
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$data['list_ctf'] = $this->mdl_ctf_preview->list_ctf();

		$template = 'templates/page/v_form';
		$modules = 'preview';
		$views = 'table_ctf';
		$labels = 'view_ctf_table';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}

	public function pilih_ctf($callback_function = 'set_data_ctf')
	{
		$data['callback_function'] = $callback_function;
		$this->load->view('pilih_ctf',$data);
	}

	public function search_doc_number()
	{
		$get_search_like = $this->input->get('q', FALSE);
		$search_result = $this->mdl_ctf_preview->cari_by_doc_number($get_search_like);
		$jml_search_result = count($search_result);
		if(!$search_result){
			$jml_search_result = 0;
		}else{
			foreach ($search_result as $item) {
					// $item->fmt_authority_date = tgl($item->authority_date,'F, Y');
					$item->fmt_tanggal_buat = tgl($item->tanggal_buat,'F, Y');
					$item->result_tanggal_buat = tgl($item->tanggal_buat,'d F Y');
			}	
		}
		$json = array( 
					'search_like' => $get_search_like,
					'jumlah_result' => $jml_search_result,
					'result' => $search_result
					);
		
		echo json_encode($json);
	}

	public function json_ctf_tags_all()
	{
		$id_ctf = $this->input->get('id_tags', FALSE);
		$search_result = $this->mdl_ctf_preview->ctf_tag_grouping($id_ctf);
		$jml_search_result = count($search_result);
		if(!$search_result){
			$jml_search_result = 0;
		}else{
			foreach ($search_result as $item) {
					// $item->fmt_authority_date = tgl($item->authority_date,'F, Y');
					$item->fmt_month_catch = tgl($item->month_of_harvest,'F, Y');
					$item->result_month_catch = tgl($item->month_of_harvest,'d F Y');					
					$item->fmt_tanggal_buat = tgl($item->tanggal_buat,'F, Y');
					$item->result_tanggal_buat = tgl($item->tanggal_buat,'d F Y');
			}	
		}
		
		$json = array( 
					'search_like' => $id_ctf,
					'jumlah_result' => $jml_search_result,
					'result' => $search_result
					);
		
		echo json_encode($json);
	}

	public function json_ctf_tags()
	{
		$id_ctf = $this->input->get('id_ctf', TRUE);
		$sealed = $this->input->get('sealed', TRUE) ? FALSE : TRUE;
		// vdump($sealed);
		$search_result = $this->mdl_ctf_preview->ctf_tags($id_ctf, $sealed);
		$jml_search_result = count($search_result);
		if(!$search_result){
			$jml_search_result = 0;
		}else{
			foreach ($search_result as $item) {
					// $item->fmt_authority_date = tgl($item->authority_date,'F, Y');
					$item->fmt_month_catch = tgl($item->month_of_harvest,'F, Y');
					$item->result_month_catch = tgl($item->month_of_harvest,'d F Y');					
					$item->fmt_tanggal_buat = tgl($item->tanggal_buat,'F, Y');
					$item->result_tanggal_buat = tgl($item->tanggal_buat,'d F Y');
			}	
		}

		$json = array( 
					'search_like' => $id_ctf,
					'jumlah_result' => $jml_search_result,
					'result' => $search_result
					);
		
		echo json_encode($json);
	}

	public function view($id_ctf)
	{	
		$this->load->model('entry/mdl_ctf_entry');

		$doc_prefix = "T-ID";
		$doc_year = date("y");
		$doc_area = "B";

		//get last ctf id from table counter
		$get_last_ctf_id = $this->mdl_ctf_entry->last_ctf_id($doc_area, $doc_year);
		$last_ctf_id = $get_last_ctf_id->last_ctf_id;
		$last_ctf_id++;
		$ctf_doc_id = str_pad($last_ctf_id, 4, "0", STR_PAD_LEFT);

		$doc_id = $ctf_doc_id;

		$generate_doc_number = $this->_generate_doc_number();
		$data['calon_doc_number'] = $generate_doc_number['calon_doc_number']; 

		$get_detail_ctf = $this->mdl_ctf_preview->detail_ctf($id_ctf);
		// print_r($get_detail_ctf);
		$data['detail_ctf'] = (array) $get_detail_ctf;
		
		$data['list_tags'] = $this->mdl_ctf_preview->ctf_tags($id_ctf);

		$data['opsi_type_tag'] = Modules::run('vessel/mst_type_tag/list_type_tag_json');
		$data['opsi_gear_code'] = Modules::run('vessel/mst_gear/list_gear_code_json');
		$data['opsi_ccsbt_area'] = Modules::run('vessel/mst_ccsbt_area/list_ccsbt_area_json');



		$add_js = array('select2.min.js', 'jquery.validationEngine.js','jquery.validationEngine-en.js','jquery.dataTables.min.js');
		$add_css = array('select2.css', 'validationEngine.jquery.css','jquery.dataTables.css');
		echo Modules::run('templates/page/v_form', //tipe template
							'preview', //nama module
							'details_ctf', //nama file view
							'form_ctf', //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */