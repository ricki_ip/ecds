<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cmf extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
			parent::__construct();
			$this->load->config('menus');
			$this->load->config('db_timeline');
			$this->load->config('globals');
			$this->load->model('mdl_cmf_preview');
	}

	public function _generate_doc_number()
	{
		$data['doc_area'] = $this->user->code_area();
		$data['doc_prefix'] = "CM-ID";
		$data['doc_year'] = date("y");

		//get last cmf id from table counter
		$get_last_cmf_id = $this->mdl_cmf_entry->last_cmf_id($data['doc_area'], $data['doc_year']);
		$last_cmf_id = $get_last_cmf_id->last_cmf_id;
		$last_cmf_id++;
		$cmf_doc_id = str_pad($last_cmf_id, 4, "0", STR_PAD_LEFT);

		$data['doc_id'] = $cmf_doc_id;

		$data['calon_doc_number'] = $data['doc_prefix'].$data['doc_year'].'-'.$data['doc_area'].'-'.$data['doc_id'];

		return $data;
	}
	
	public function index()
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$data['list_cmf'] = $this->mdl_cmf_preview->list_cmf();
		$template = 'templates/page/v_form';
		$modules = 'preview';
		$views = 'table_cmf';
		$labels = 'view_cmf_table';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}

	public function pilih_cmf($callback_function = 'set_data_cmf')
	{
		$data['callback_function'] = $callback_function;
		$this->load->view('pilih_cmf',$data);
	}

	public function search_doc_number()
	{
		$get_search_like = $this->input->get('q', FALSE);
		$search_result = $this->mdl_cmf_preview->cari_by_doc_number($get_search_like);
		$jml_search_result = count($search_result);
		if(!$search_result){
			$jml_search_result = 0;
		}else{
			foreach ($search_result as $item) {
					$item->fmt_authority_date = tgl($item->authority_date,'F, Y');
					$item->fmt_tanggal_buat = tgl($item->tanggal_buat,'F, Y');
					$item->result_tanggal_buat = tgl($item->tanggal_buat,'d F Y');
			}	
		}
		
		// vdump($search_result);
		$json = array( 
					'search_like' => $get_search_like,
					'jumlah_result' => $jml_search_result,
					'result' => $search_result
					);
		
		echo json_encode($json);
	}

	public function json_cmf_tags()
	{
		$id_cmf = $this->input->get('id_cmf', FALSE);
		// $sealed = empty( $this->input->get('sealed', '') ) ? FALSE : TRUE;
		$search_result = $this->mdl_cmf_preview->detail_cmf_fish($id_cmf);
		$jml_search_result = count($search_result);
		if(!$search_result){
			$jml_search_result = 0;
		}
		$json = array( 
					'search_like' => $id_cmf,
					'jumlah_result' => $jml_search_result,
					'result' => $search_result
					);
		
		echo json_encode($json);
	}

	public function view($id_cmf)
	{	
		$this->load->model('entry/mdl_cmf_entry');

		$doc_prefix = "CM-ID";
		$doc_year = date("y");
		$doc_area = "B";

		//get last ctf id from table counter
		$get_last_cmf_id = $this->mdl_cmf_entry->last_cmf_id($doc_area, $doc_year);
		$last_cmf_id = $get_last_cmf_id->last_cmf_id;
		$last_cmf_id++;
		$cmf_doc_id = str_pad($last_cmf_id, 4, "0", STR_PAD_LEFT);

		$doc_id = $cmf_doc_id;

		$generate_doc_number = $this->_generate_doc_number();
		$data['calon_doc_number'] = $generate_doc_number['calon_doc_number'];

		$get_detail_cmf = $this->mdl_cmf_preview->detail_cmf($id_cmf);
		$get_detail_cmf_fish = $this->mdl_cmf_preview->detail_cmf_fish($id_cmf);
		
		$data['destination'] = $get_detail_cmf->product_destination;
		$data['detail_domestic'] = FALSE;
		$data['detail_export'] = FALSE;
		
		if($data['destination'] === 'domestic')
		{
			$data['detail_domestic'] = (array) $this->mdl_cmf_preview->detail_domestic($id_cmf);
		}else{ //export
			$data['detail_export'] = (array) $this->mdl_cmf_preview->detail_export($id_cmf);
		}
		// print_r($get_detail_ctf);
		$data['detail_cmf'] = (array) $get_detail_cmf;
		$data['detail_cmf_fish'] =  $get_detail_cmf_fish;

		//$status_dokumen = $data['detail_pendok']['status_pendok'];

		$add_js = array('select2.min.js', 'jquery.validationEngine.js','jquery.validationEngine-en.js');
		$add_css = array('select2.css', 'validationEngine.jquery.css');
		echo Modules::run('templates/page/v_form', //tipe template
							'preview', //nama module
							'details_cmf', //nama file view
							'form_entry_cmf', //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */