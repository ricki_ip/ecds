<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reef extends MX_Controller {

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   *    http://example.com/index.php/welcome
   *  - or -  
   *    http://example.com/index.php/welcome/index
   *  - or -
   * Since this controller is set as the default controller in 
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see http://codeigniter.com/user_guide/general/urls.html
   */
  function __construct()
  {
      parent::__construct();
      $this->load->config('menus');
      $this->load->config('db_timeline');
      $this->load->config('globals');
      $this->load->model('mdl_reef_preview');
  }

  public function _generate_doc_number()
  {
    $data['doc_area'] = $this->user->code_area();
    $data['doc_prefix'] = "RE-ID";
    $data['doc_year'] = date("y");

    //get last reef id from table counter
    $get_last_reef_id = $this->mdl_reef_entry->last_reef_id($data['doc_area'], $data['doc_year']);
    $last_reef_id = $get_last_reef_id->last_reef_id;
    $last_reef_id++;
    $reef_doc_id = str_pad($last_reef_id, 4, "0", STR_PAD_LEFT);

    $data['doc_id'] = $reef_doc_id;

    $data['calon_doc_number'] = $data['doc_prefix'].$data['doc_year'].'-'.$data['doc_area'].'-'.$data['doc_id'];

    return $data;
  }
  
  public function index()
  {
    $add_js = array('select2.min.js', 'jquery.dataTables.min.js');
    $add_css = array('select2.css', 'jquery.dataTables.css');

    $data['list_reef'] = $this->mdl_reef_preview->list_reef();
    $template = 'templates/page/v_form';
    $modules = 'preview';
    $views = 'table_reef';
    $labels = 'view_reef_table';
     
    echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
  }

  public function view($id_reef)
  { 
    $this->load->model('entry/mdl_reef_entry');
    $this->load->model('mdl_cmf_preview');

    $doc_prefix = "RE-ID";
    $doc_year = date("y");
    $doc_area = "B";

    //get last ctf id from table counter
    $get_last_reef_id = $this->mdl_reef_entry->last_reef_id($doc_area, $doc_year);
    $last_reef_id = $get_last_reef_id->last_reef_id;
    $last_reef_id++;
    $reef_doc_id = str_pad($last_reef_id, 4, "0", STR_PAD_LEFT);

    $doc_id = $reef_doc_id;

    $generate_doc_number = $this->_generate_doc_number();
    $data['calon_doc_number'] = $generate_doc_number['calon_doc_number'];


    $get_detail_reef = $this->mdl_reef_preview->detail_reef($id_reef);
    $get_detail_reef_fish = $this->mdl_reef_preview->detail_reef_fish($id_reef);

    $get_detail_cmf_fish = $this->mdl_cmf_preview->detail_cmf_fish($get_detail_reef['id_cmf']);

    $data['detail_reef'] = $get_detail_reef;
    $data['detail_cmf_fish'] =  $get_detail_cmf_fish;
    $data['detail_reef_fish'] =  $get_detail_reef_fish;

    $data['opsi_type_tag'] = Modules::run('vessel/mst_type_tag/list_type_tag_json');
    $data['opsi_gear_code'] = Modules::run('vessel/mst_gear/list_gear_code_json');
    $data['opsi_ccsbt_area'] = Modules::run('vessel/mst_ccsbt_area/list_ccsbt_area_json');  
    $data['opsi_pejabat'] = Modules::run('pengaturan/mst_pejabat/list_pejabat_json');
    $data['opsi_processing'] = Modules::run('pengaturan/mst_processing/list_processing_json','processing');
    $data['opsi_negara'] = Modules::run('pengaturan/mst_negara/list_negara_json');
    $data['opsi_kabkota'] = Modules::run('pengaturan/mst_kabupaten_kota/list_kabupaten_kota_json');

  

    $add_js = array('select2.min.js', 'jquery.dataTables.min.js');
    $add_css = array('select2.css', 'jquery.dataTables.css');
    echo Modules::run('templates/page/v_form', //tipe template
              'preview', //nama module
              'details_reef', //nama file view
              'form_reef', //dari labels.php
              $add_js, //plugin javascript khusus untuk page yang akan di load
              $add_css, //file css khusus untuk page yang akan di load
              $data); //array data yang akan digunakan di file view
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */