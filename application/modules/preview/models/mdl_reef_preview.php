<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_reef_preview extends CI_Model
{
  

    function __construct()
    {
        

    }

    public function list_reef($limit = '9999')
    {
        if($this->user->level() < 5)
        { 
            $query = 'SELECT 
                            reef . *
                        FROM
                            trs_reef reef
                        ORDER BY reef.id_reef DESC
                        LIMIT 0, '.$limit;
        }else{
            $query = 'SELECT 
                            reef . *
                        FROM
                            trs_reef reef
                        WHERE 
                            reef . id_pengguna_buat = '.$this->user->id_pengguna().'
                        ORDER BY reef.id_reef DESC
                        LIMIT 0, '.$limit;
        }
        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }


    public function detail_reef($id_reef)
    {
        $sql = "SELECT 
                    reef . *,
                    mprop.nama_propinsi AS asal_export_propinsi,
                    mcity.nama_kabupaten_kota AS asal_export_city,
                    mstate.nama_negara AS nama_negara_export
                FROM
                    trs_reef reef
                    LEFT JOIN mst_kabupaten_kota mcity ON mcity.id_kabupaten_kota = reef.point_export_city
                    LEFT JOIN mst_propinsi mprop ON mprop.id_propinsi = mcity.id_propinsi
                    LEFT JOIN mst_negara mstate ON mstate.id_negara = reef.export_destination
                    WHERE reef.id_reef = $id_reef";

        $run_query = $this->db->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->row_array();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_reef_fish($id_reef)
    {
        $sql = "SELECT 
                    mtag.code_tag,                    
                    tfish . *
                FROM
                    trs_reef_fish tfish
                        JOIN
                    mst_type_tag mtag ON mtag.id_type_tag = tfish.type_tag                        
                    WHERE tfish.id_reef = $id_reef";

        $run_query = $this->db->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_domestic($id_reef)
    {
        $sql = "SELECT * FROM trs_reef_final_domestic
                    WHERE trs_reef_final_domestic.id_reef = $id_reef";

        $run_query = $this->db->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_export($id_reef)
    {
        $sql = "SELECT 
                    mkot.nama_kabupaten_kota as kota_asal,
                    mprop.nama_propinsi as propinsi_asal,
                    mneg.nama_negara as negara_export,
                    mprs.company_name as exporter_company,
                    tfe . *
                FROM
                    trs_reef_final_export tfe
                        LEFT JOIN
                    mst_kabupaten_kota mkot ON mkot.id_kabupaten_kota = tfe.city
                        LEFT JOIN
                    mst_propinsi mprop ON mprop.id_propinsi = mkot.id_propinsi
                        LEFT JOIN
                    mst_negara mneg ON mneg.id_negara = tfe.destination
                        LEFT JOIN
                    mst_perusahaan mprs ON mprs.id_perusahaan = tfe.company_name
                    WHERE tfe.id_reef = $id_reef";

        $run_query = $this->db->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }
}