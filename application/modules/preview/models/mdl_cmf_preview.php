<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_cmf_preview extends CI_Model
{
	

    function __construct()
    {
        

    }

    public function list_cmf($limit = '9999')
    {
        if($this->user->level() < 5)
        {   
            $query = 'SELECT 
                            ctf.temp_doc_id AS ctf_temp_doc_id,
                            ctf.document_number AS ctf_document_number, 
                            cmf . *
                        FROM
                            trs_cmf cmf
                                LEFT JOIN
                            trs_ctf ctf ON ctf.id_ctf = cmf.id_ctf
                        GROUP BY cmf.id_cmf
                        ORDER BY cmf.id_cmf DESC
                        LIMIT 0, '.$limit;
        }else{
            $query = 'SELECT 
                            ctf.temp_doc_id AS ctf_temp_doc_id,
                            ctf.document_number AS ctf_document_number, 
                            cmf . *
                        FROM
                            trs_cmf cmf
                                LEFT JOIN
                            trs_ctf ctf ON ctf.id_ctf = cmf.id_ctf
                        WHERE cmf.id_pengguna_buat = '.$this->user->id_pengguna().'
                        GROUP BY cmf.id_cmf
                        ORDER BY cmf.id_cmf DESC
                        LIMIT 0, '.$limit;
        }
        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function cari_by_doc_number($doc_number)
    {
        $this->db->select('trs_cmf.*');
        $this->db->select('trs_ctf.document_number AS ctf_document_number');
        // $this->db->select('mst_vessel_ccsbt.owner_address');
        $this->db->from('trs_cmf');
        $this->db->join('trs_ctf', 'trs_ctf.id_ctf = trs_cmf.id_ctf');
        $this->db->like('trs_cmf.temp_doc_id',$doc_number);
        $this->db->or_like('trs_cmf.document_number',$doc_number);
        $this->db->where('trs_cmf.product_destination','domestic');
        $this->db->where('trs_cmf.is_verified','YA');
        $this->db->where('trs_cmf.aktif','YA');

        if( $this->user->is_perusahaan() )
        {
            $this->db->where('trs_cmf.id_pengguna_buat',$this->user->id_pengguna());
        }
        // $this->db->where('is_verified','YA');
        $run_query = $this->db->get();                            

        // var_dump($this->db->last_query());
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_cmf($id_cmf)
    {
        $sql = "SELECT 
                    ctf.temp_doc_id AS ctf_temp_doc_id,
                    ctf.document_number AS ctf_document_number,
                    CONCAT( pjb.nama_pejabat, ' - ',pjb.title) AS cmf_authority_name_title,
                    mtp.company_name AS exporter_company,
                    cmf . *
                FROM
                    trs_cmf cmf
                        LEFT JOIN
                    trs_ctf ctf ON ctf.id_ctf = cmf.id_ctf
                        LEFT JOIN
                    mst_pejabat pjb ON pjb.id_pejabat = cmf.id_pejabat
                        LEFT JOIN
                    mst_perusahaan mtp ON mtp.id_perusahaan = cmf.id_exporter_company_name
                    WHERE cmf.id_cmf = $id_cmf";

        $run_query = $this->db->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_cmf_fish($id_cmf)
    {
        $sql = "SELECT 
                    mtag.code_tag, 
                    mgear.gear_code as nama_gear_code,
                    tfish . *
                FROM
                    trs_cmf_fish tfish
                        JOIN
                    mst_type_tag mtag ON mtag.id_type_tag = tfish.type_tag
                        JOIN
                    mst_gear mgear ON mgear.id_gear = tfish.gear_code
                    WHERE tfish.id_cmf = $id_cmf";

        $run_query = $this->db->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
    

    public function detail_domestic($id_cmf)
    {
        $sql = "SELECT * FROM trs_cmf_final_domestic
                    WHERE trs_cmf_final_domestic.id_cmf = $id_cmf";

        $run_query = $this->db->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_domestic_fish($id_cmf)
    {
        $sql = "SELECT mst_type_tag.code_tag, trs_cmf_fish.*
                    FROM trs_cmf_fish
                    LEFT JOIN mst_type_tag ON mst_type_tag.id_type_tag = trs_cmf_fish.type_tag
                    WHERE trs_cmf_fish.id_cmf = $id_cmf";

        $run_query = $this->db->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_export($id_cmf)
    {
        $sql = "SELECT 
                    mkot.nama_kabupaten_kota as kota_asal,
                    mprop.nama_propinsi as propinsi_asal,
                    mneg.nama_negara as negara_export,
                    mprs.company_name as exporter_company,
                    tfe . *
                FROM
                    trs_cmf_final_export tfe
                        LEFT JOIN
                    mst_kabupaten_kota mkot ON mkot.id_kabupaten_kota = tfe.city
                        LEFT JOIN
                    mst_propinsi mprop ON mprop.id_propinsi = mkot.id_propinsi
                        LEFT JOIN
                    mst_negara mneg ON mneg.id_negara = tfe.destination
                        LEFT JOIN
                    mst_perusahaan mprs ON mprs.id_perusahaan = tfe.company_name
                    WHERE tfe.id_cmf = $id_cmf";

        $run_query = $this->db->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    // public function detil_kapal($id)
    // {
    //     $sql = "SELECT *
    //                 FROM kapi_monev.mst_inka_mina
    //                 WHERE mst_inka_mina.id_kapal = $id ";

    //     $run_query = $this->db_kapi->query($sql);                            

    //     if($run_query->num_rows() > 0){
    //         $result = $run_query->row();
    //     }else{
    //         $result = false;
    //     }
    //     return $result;
    // }
}