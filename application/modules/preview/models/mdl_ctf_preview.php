<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_ctf_preview extends CI_Model
{
	

    function __construct()
    {
        

    }

    public function list_ctf($limit = '9999')
    {
        if($this->user->level() < 5)
        {            
            $query = ' SELECT * FROM trs_ctf ORDER BY tanggal_buat DESC LIMIT 0, '.$limit;
        }else{
            $query = ' SELECT * FROM trs_ctf 
                        WHERE id_pengguna_buat = '.$this->user->id_pengguna().' ORDER BY tanggal_buat DESC
                        LIMIT 0, '.$limit;            
        }
        // vdump($query);
        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    

    public function cari_by_doc_number($doc_number)
    {
        $this->db->select('trs_ctf.*');
        $this->db->select('trs_ctf.vessel_registration_number AS ccsbt_registration_number'); 
        // $this->db->select('mst_vessel_ccsbt.ccsbt_registration_number');
        // $this->db->select('mst_vessel_ccsbt.owner_name');
        // $this->db->select('mst_vessel_ccsbt.owner_address');
        $this->db->from('trs_ctf');
        // $this->db->join('mst_vessel_ccsbt','mst_vessel_ccsbt.id_vessel_ccsbt = trs_ctf.id_vessel_ccsbt');
        $this->db->like('temp_doc_id',$doc_number);
        $this->db->or_like('document_number',$doc_number);
        $this->db->where('trs_ctf.aktif','YA');
        if( $this->user->is_perusahaan() )
        {
            $this->db->where('trs_ctf.id_pengguna_buat',$this->user->id_pengguna());
        }
        // $this->db->where('is_verified','YA');
        $run_query = $this->db->get();                            

        // var_dump($this->db->last_query());
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_ctf($id_ctf)
    {
        $this->db->select('trs_ctf.*');
        $this->db->select('mst_vessel_ccsbt.owner_name');
        $this->db->where('id_ctf',$id_ctf);
        $this->db->join('mst_vessel_ccsbt', 'mst_vessel_ccsbt.id_vessel_ccsbt = trs_ctf.id_vessel_ccsbt');
        $run_query = $this->db->get('trs_ctf');                            
        // vdump($this->db->last_query());
        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function ctf_tag_grouping($id_tag)
    {
        $sql = "SELECT 
                concat(mtt.code_tag,'_',mg.gear_code,'_',tti.month_of_harvest,'_',tti.ccsbt_statistical_area) AS grouping,
                SUM(tti.weight) AS total_weight,
                COUNT(tti.id_tag_information) AS total_fish, 
                mtt.code_tag, 
                mg.gear_code,
                tti.month_of_harvest,
                tti.ccsbt_statistical_area,
                tti.type_tag,
                tti.gear_code AS tti_gear_code 
                FROM trs_ctf_fish tti 
                LEFT JOIN mst_type_tag mtt ON mtt.id_type_tag = tti.type_tag 
                LEFT JOIN mst_gear mg ON mg.id_gear = tti.gear_code 
                WHERE tti.id_tag_information IN ($id_tag)
                GROUP BY grouping";
        // var_dump($sql);
        $run_query = $this->db->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function ctf_count_tags($id_ctf, $sealed = 'all')
    {
        $sql = "SELECT 
                    COUNT(tti.id_tag_information) AS total_fish,
                    SUM(tti.weight) AS total_weight 
                    FROM trs_ctf_fish tti 
                    WHERE tti.id_ctf = $id_ctf
                    GROUP BY tti.id_ctf
                ";
        if($sealed === 'free')
        {
            $sql .= "AND tti.is_free = 'YA'";
        }elseif( $sealed === 'sealed' )
        {
            $sql .= "AND tti.is_free = 'TIDAK'";
        }        
        // $sealed === 'all' { nothing to add to query }

        // var_dump($sql);

        $run_query = $this->db->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function ctf_tags($id_ctf,$sealed = FALSE)
    {
        $sql = "SELECT 
                    concat(mtt.code_tag,'_',mg.gear_code,'_',tti.month_of_harvest,'_',tti.ccsbt_statistical_area) AS grouping,
                    tti . *, 
                    mtt.code_tag, 
                    mg.gear_code,
                    mtt.code_tag, 
                    tti.month_of_harvest,
                    tti.ccsbt_statistical_area,
                    tti.type_tag,
                    tti.gear_code AS tti_gear_code
                    FROM trs_ctf_fish tti 
                    LEFT JOIN mst_type_tag mtt ON mtt.id_type_tag = tti.type_tag 
                    LEFT JOIN mst_gear mg ON mg.id_gear = tti.gear_code 
                    WHERE tti.id_ctf = $id_ctf
                ";
        if($sealed)
        {
            $sql .= "AND tti.is_free = 'YA'";
        }        

        // var_dump($sql);

        $run_query = $this->db->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function limit_quota($paramyear = '')
    {
        $year = empty($paramyear) ? date('Y') : $paramyear;
        $query = "SELECT quota, treshold, tahun FROM mst_fish_quota
                    WHERE tahun = ".$year." ";
        // $this->db->select('quota');
        // $this->db->select('treshold');
        $run_query = $this->db->query($query);                            
        // vdump($this->db->last_query());
        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function total_weight($paramyear = '')
    {
        $year = empty($paramyear) ? date('Y') : $paramyear;
        $sql = "SELECT 
                    SUM(tti.weight * mtt.conversion_factor) AS total_weight
                FROM
                    trs_ctf_fish tti
                        LEFT JOIN
                    mst_type_tag mtt ON mtt.id_type_tag = tti.type_tag
                        LEFT JOIN
                    trs_ctf ctf ON ctf.id_ctf = tti.id_ctf
                WHERE YEAR(tti.month_of_harvest) = ".$year."
                AND ctf.is_verified = 'YA' 
                ";   

        // var_dump($sql);

        $run_query = $this->db->query($sql);                            

        if($run_query->num_rows() > 0){
            $get_result = $run_query->row();
            $result = $get_result->total_weight;
        }else{
            $result = false;
        }
        return $result;
    }

    public function total_weight_perusahaan($id_perusahaan, $paramyear = '')
    {
        $year = empty($paramyear) ? date('Y') : $paramyear;
        $sql = "SELECT 
                    SUM(tti.weight * mtt.conversion_factor) AS total_weight
                FROM
                    trs_ctf_fish tti
                        LEFT JOIN
                    mst_type_tag mtt ON mtt.id_type_tag = tti.type_tag
                        LEFT JOIN
                    trs_ctf ctf ON ctf.id_ctf = tti.id_ctf
                WHERE YEAR(tti.month_of_harvest) = ".$year."
                AND ctf.is_verified = 'YA' 
                AND tti.id_perusahaan = ".$id_perusahaan." ";   

        // var_dump($sql);

        $run_query = $this->db->query($sql);                            

        if($run_query->num_rows() > 0){
            $get_result = $run_query->row();
            $result = $get_result->total_weight;
        }else{
            $result = false;
        }
        return $result;
    }

    public function monitor_kuota_perusahaan($id_perusahaan = '', $paramyear = '',$id_asosiasi='')
    {
        // vdump($id_asosiasi);
        $year = empty($paramyear) ? date('Y') : $paramyear;
        $sql = "SELECT 
                    ctf.id_perusahaan,
                    mta.asosiasi_name,
                    mtp.company_name,
                    SUM(tti.weight * mtt.conversion_factor) AS total_weight,
                    mtp.kuota_perus_awal
                FROM
                    trs_ctf_fish tti
                        LEFT JOIN
                    mst_type_tag mtt ON mtt.id_type_tag = tti.type_tag
                        LEFT JOIN
                    trs_ctf ctf ON ctf.id_ctf = tti.id_ctf
                        LEFT JOIN
                    mst_perusahaan mtp ON mtp.id_perusahaan = ctf.id_perusahaan
                        LEFT JOIN
                    mst_asosiasi mta ON mta.id_asosiasi = ctf.id_asosiasi
                WHERE YEAR(tti.month_of_harvest) = ".$year." 
                AND ctf.is_verified = 'YA' ";
        if(!empty($id_asosiasi)){
            $sql .= " AND mta.id_asosiasi = ".$id_asosiasi." ";
        }
        
        if(!empty($id_perusahaan)){
            $sql .= " AND ctf.id_perusahaan = ".$id_perusahaan." ";
        }else{
            $sql .=  "GROUP BY ctf.id_perusahaan";               
        }         

        // var_dump($sql);

        $run_query = $this->db->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    
}