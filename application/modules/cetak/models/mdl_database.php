<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_database extends CI_Model
{
	private $db_kapi;

    function __construct()
    {
        $this->db_kapi = $this->load->database('default', TRUE);

    }

    private function kabupaten_kota(){
        $this->db_kapi->select('id_kabupaten_kota, nama_kabupaten_kota');
        $run_query = $this->db_kapi->get('mst_kabupaten_kota');
       
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function update_database()
    {

        /*
            Note:
            - update untuk nama kab_kota pada tabel mst_inka mina karena tidak terdaftar pada tabel mst_kabupaten_kota
                biasanya karena perbedaan penulisan
            - tanda '??' artinya galang kurang yakin untuk perubahan namanya, masih menebak2
        */
        //??atau diganti ke 'Kab. Minahasa' 
        //ini tambah kekurangannya kalau diklik 2kali jadi error karena duplicate id
        //insert 'Kab. Minahasa Tenggara
        // $data_insert = array ('id_kabupaten_kota' => '498',
        //                         'nama_kabupaten_kota' => 'Kab. Minahasa Tenggara',
        //                         'status_daerah' => 'Kabupaten',
        //                         'id_propinsi' => 24,
        //                         'aktif' => 'Ya',
        //                         'hapus' => 'Tidak'
        //                         );
        // $this->db_kapi->insert('mst_kabupaten_kota', $data_insert);

        //?? atau ganti ke Kab. Penajam Paser Utara 
        //atau tambah Kab.  Paser
        //update 'Kab. Minahasa Tenggara ' => 'Kab. Minahasar'
        $data_update = array ('kab_kota' => 'Kab. Minahasa');
        $this->db_kapi->where('kab_kota', 'Kab. Minahasa Tenggara');
        $this->db_kapi->update('mst_inka_mina', $data_update);
        
        //?? atau ganti ke Kab. Penajam Paser Utara 
        //atau tambah Kab.  Paser
        //update 'Kab. Paser ' => 'Kab. Pasir'
        $data_update = array ('kab_kota' => 'Kab. Pasir');
        $this->db_kapi->where('kab_kota', 'Kab. Paser ');
        $this->db_kapi->update('mst_inka_mina', $data_update);

        //?? atau tambah kab. penajam
        //update 'Kab. Penajam' => 'Kab. Penajam Paser Utara'
        $data_update = array ('kab_kota' => 'Kab. Penajam Paser Utara');
        $this->db_kapi->where('kab_kota', 'Kab. Penajam');
        $this->db_kapi->update('mst_inka_mina', $data_update);
    
        //??atau tambah 'Kab. Mamuju Utara'
        //update 'Kab. Mamuju ' => 'Kab. Mamuju Utara'
        $data_update = array ('kab_kota' => 'Kab. Mamuju Utara');
        $this->db_kapi->where('kab_kota', 'Kab. Mamuju');
        $this->db_kapi->update('mst_inka_mina', $data_update);

        //?? atau ganti ke Kab. Konawe Selatan 
        //atau tambah kab. Konawe
         //update 'Kab. Konawe ' => 'Kab. Konawe Utara'
        $data_update = array ('kab_kota' => 'Kab. Konawe Utara');
        $this->db_kapi->where('kab_kota', 'Kab. Konawe ');
        $this->db_kapi->update('mst_inka_mina', $data_update);

        //??atau tambah kab. bintuni
        //update 'Kab. Bintuni' => 'Kab. Teluk Bintuni'
        $data_update = array ('kab_kota' => 'Kab. Teluk Bintuni');
        $this->db_kapi->where('kab_kota', 'Kab. Bintuni');
        $this->db_kapi->update('mst_inka_mina', $data_update);

        //?? atau tambah kab. sangihe
         //update 'Kab. Kep. Sangihe' => 'Kab. Sangihe Talaud'
        $data_update = array ('kab_kota' => 'Kab. Sangihe Talaud');
        $this->db_kapi->where('kab_kota', 'Kab. Kep. Sangihe');
        $this->db_kapi->update('mst_inka_mina', $data_update);
        
        //?? atau tambah kab. Polewai Mandar
        //update 'Kab. Polewali Mandar' => 'Kab. Polewali Mamasa'
        $data_update = array ('kab_kota' => 'Kab. Polewali Mamasa');
        $this->db_kapi->where('kab_kota', 'Kab. Polewali Mandar');
        $this->db_kapi->update('mst_inka_mina', $data_update);
        
        //?? tatau tambah kab. biak
        //update 'Kab. Biak' => 'Kab. Biak Numfor'
        $data_update = array ('kab_kota' => 'Kab. Biak Numfor');
        $this->db_kapi->where('kab_kota', 'Kab. Biak');
        $this->db_kapi->update('mst_inka_mina', $data_update);
        
        //update 'Tj. Jabung Barat' => 'Kab. Tanjung Jabung Barat'
        $data_update = array ('kab_kota' => 'Kab. Tanjung Jabung Barat');
        $this->db_kapi->where('kab_kota', 'Tj.Jabung Barat');
        $this->db_kapi->or_where('kab_kota', 'Kab. Tj. Jabung Barat');
        $this->db_kapi->update('mst_inka_mina', $data_update);

        //update 'Kota. Tidore Kepulauan' => 'Kota Tidore Kepulauan'
        $data_update = array ('kab_kota' => 'Kota Tidore Kepulauan');
        $this->db_kapi->where('kab_kota', 'Kota. Tidore Kepulauan');
        $this->db_kapi->update('mst_inka_mina', $data_update);

        //update 'Kota Tanjung Pinang' => 'Kota Tanjungpinang'
        $data_update = array ('kab_kota' => 'Kota Tanjungpinang');
        $this->db_kapi->where('kab_kota', 'Kota Tanjung Pinang');
        $this->db_kapi->update('mst_inka_mina', $data_update);

        //update 'Kota Lhoksuemawe' => 'Kota Lhokseumawe'
        $data_update = array ('kab_kota' => 'Kota Lhokseumawe');
        $this->db_kapi->where('kab_kota', 'Kota Lhoksuemawe');
        $this->db_kapi->update('mst_inka_mina', $data_update);

        //update 'Kota Bandar Lampung' => 'Kota Bandarlampung'
        $data_update = array ('kab_kota' => 'Kota Bandarlampung');
        $this->db_kapi->where('kab_kota', 'Kota Bandar Lampung');
        $this->db_kapi->update('mst_inka_mina', $data_update);

        //update 'Kota Adm Jakarta Utara' => 'Kota Bandarlampung'
        $data_update = array ('kab_kota' => 'Jakarta Utara');
        $this->db_kapi->where('kab_kota', 'Kota Adm Jakarta Utara');
        $this->db_kapi->update('mst_inka_mina', $data_update);

        //update 'Kep. Anambas ' => 'Kab. Kep. Anambas'
        $data_update = array ('kab_kota' => 'Kab. Kep. Anambas');
        $this->db_kapi->where('kab_kota', 'Kep. Anambas');
        $this->db_kapi->update('mst_inka_mina', $data_update);

        //update 'Kab. Belitung' => 'Kab.Belitung'
        $data_update = array ('kab_kota' => 'Kab.Belitung');
        $this->db_kapi->where('kab_kota', 'Kab. Belitung');
        $this->db_kapi->update('mst_inka_mina', $data_update);

        //update 'Kab. Tolitoli' => 'Kab. Toli-Toli'
        $data_update = array ('kab_kota' => 'Kab. Toli-Toli');
        $this->db_kapi->where('kab_kota', 'Kab. Tolitoli');
        $this->db_kapi->update('mst_inka_mina', $data_update);

        //update 'Kab. ' => 'Kab. Toli-Toli'
        $data_update = array ('kab_kota' => 'Kab. Toli-Toli');
        $this->db_kapi->where('kab_kota', 'Kab. Tolitoli');
        $this->db_kapi->update('mst_inka_mina', $data_update);

        //update 'Kab. Tojo Unauna' => 'Kab. Tojo Una-Una'
        $data_update = array ('kab_kota' => 'Kab. Tojo Una-Una');
        $this->db_kapi->where('kab_kota', 'Kab. Tojo Unauna');
        $this->db_kapi->update('mst_inka_mina', $data_update);

        //update 'Kab. Tanjung Balai' => 'Kota Tanjung Balai'
        $data_update = array ('kab_kota' => 'Kota Tanjung Balai');
        $this->db_kapi->where('kab_kota', 'Kab. Tanjung Balai');
        $this->db_kapi->update('mst_inka_mina', $data_update);

        //update 'Kab. Talaud' => 'Kab. Kep. Talaud'
        $data_update = array ('kab_kota' => 'Kab. Kep. Talaud');
        $this->db_kapi->where('kab_kota', 'Kab. Talaud');
        $this->db_kapi->update('mst_inka_mina', $data_update);

        //update 'Kab. Raja Ampat' => 'Kab. Kep. Raja Ampat'
        $data_update = array ('kab_kota' => 'Kab. Kep. Raja Ampat');
        $this->db_kapi->where('kab_kota', 'Kab. Raja Ampat');
        $this->db_kapi->update('mst_inka_mina', $data_update);

        //update 'Kab. Parigi Moutong' => 'Kab. Parigi Mountong'
        $data_update = array ('kab_kota' => 'Kab. Parigi Mountong');
        $this->db_kapi->where('kab_kota', 'Kab. Parigi Moutong');
        $this->db_kapi->update('mst_inka_mina', $data_update);

        //update 'Kab. Ogan Komering Ilir ' => 'Kab. Ogan Ilir'
        $data_update = array ('kab_kota' => 'Kab. Ogan Ilir');
        $this->db_kapi->where('kab_kota', 'Kab. Ogan Komering Ilir ');
        $this->db_kapi->update('mst_inka_mina', $data_update);

         //update 'Kab. Kutai Kartanegara' => 'Kab. Kutai Kertanegara'
        $data_update = array ('kab_kota' => 'Kab. Kutai Kertanegara');
        $this->db_kapi->where('kab_kota', 'Kab. Kutai Kartanegara');
        $this->db_kapi->update('mst_inka_mina', $data_update);

         //update 'Kab. Kulonprogo' => 'Kab. Kulon Progo'
        $data_update = array ('kab_kota' => 'Kab. Kulon Progo');
        $this->db_kapi->where('kab_kota', 'Kab. Kulonprogo');
        $this->db_kapi->update('mst_inka_mina', $data_update);

         //update 'Kab. Kuburaya' => 'Kab. Kubu Raya'
        $data_update = array ('kab_kota' => 'Kab. Kubu Raya');
        $this->db_kapi->where('kab_kota', 'Kab. Kuburaya');
        $this->db_kapi->update('mst_inka_mina', $data_update);

         //update 'Kab. Kota Baru ' => 'Kab. Kotabaru'
        $data_update = array ('kab_kota' => 'Kab. Kotabaru');
        $this->db_kapi->where('kab_kota', 'Kab. Kota Baru ');
        $this->db_kapi->update('mst_inka_mina', $data_update);

         //update 'Kab. Kepulauan Sula' => 'Kab. Kep. Sula'
        $data_update = array ('kab_kota' => 'Kab. Kep. Sula');
        $this->db_kapi->where('kab_kota', 'Kab. Kepulauan Sula');
        $this->db_kapi->update('mst_inka_mina', $data_update);

        //update 'Kab. Kep. Meranti' => 'Kab. Kep. Meranti*'
        $data_update = array ('kab_kota' => 'Kab. Kep. Meranti*');
        $this->db_kapi->where('kab_kota', 'Kab. Kep. Meranti');
        $this->db_kapi->update('mst_inka_mina', $data_update);

        //update 'Kab. Kep Meranti' => 'Kab. Kep. Meranti*'
        $data_update = array ('kab_kota' => 'Kab. Kep. Meranti*');
        $this->db_kapi->where('kab_kota', 'Kab. Kep Meranti');
        $this->db_kapi->update('mst_inka_mina', $data_update);

        //update 'Kab. Indragilir Hilir' => 'Kab. Indragiri Hilir'
        $data_update = array ('kab_kota' => 'Kab. Indragiri Hilir');
        $this->db_kapi->where('kab_kota', 'Kab. Indragilir Hilir');
        $this->db_kapi->update('mst_inka_mina', $data_update);

        //update 'Kab. Halmahera Tengah' => 'Kab. Halmahera  Tengah'
        $data_update = array ('kab_kota' => 'Kab. Halmahera  Tengah');
        $this->db_kapi->where('kab_kota', 'Kab. Halmahera Tengah');
        $this->db_kapi->update('mst_inka_mina', $data_update);
       
        //update 'Kab. Fak Fak' => 'Kab. Fak-Fak'
        $data_update = array ('kab_kota' => 'Kab. Fak-Fak');
        $this->db_kapi->where('kab_kota', 'Kab. Fak Fak');
        $this->db_kapi->update('mst_inka_mina', $data_update);
       
        //update 'Kab. Bolaang Mongondow Selatan' => 'Kab. Bolaangmongondow Selatan'
        $data_update = array ('kab_kota' => 'Kab. Bolaangmongondow Selatan');
        $this->db_kapi->where('kab_kota', 'Kab. Bolaang Mongondow Selatan');
        $this->db_kapi->update('mst_inka_mina', $data_update);

        //update 'Kab. Bireuen' => 'Kab. Bireun'
        $data_update = array ('kab_kota' => 'Kab. Bireun');
        $this->db_kapi->where('kab_kota', 'Kab. Bireuen');
        $this->db_kapi->update('mst_inka_mina', $data_update);

        //update 'Kab. Bintan ' <= 'Kab. Bintan (Kep. Riau)'
        $data_update = array ('nama_kabupaten_kota' => 'Kab. Bintan');
        $this->db_kapi->where('id_kabupaten_kota', 147);
        $this->db_kapi->update('mst_kabupaten_kota', $data_update);

        //update 'Aceh Barat Daya' => 'Kab. Aceh Barat Daya'
        $data_update = array ('kab_kota' => 'Kab. Aceh Barat Daya');
        $this->db_kapi->where('kab_kota', 'Aceh Barat Daya');
        $this->db_kapi->update('mst_inka_mina', $data_update);

        //update 'Kota Pangkal Pinang' => 'Kota Pangkalpinang'
        $data_update = array ('kab_kota' => 'Kota Pangkalpinang');
        $this->db_kapi->where('kab_kota', 'Kota Pangkal Pinang');
        $this->db_kapi->update('mst_inka_mina', $data_update);

        //update 'Kota Makassar' => 'Kota Makasar'
        $data_update = array ('kab_kota' => 'Kota Makasar');
        $this->db_kapi->where('kab_kota', 'Kota Makassar');
        $this->db_kapi->update('mst_inka_mina', $data_update);

        //update 'Kab.Belitung' => 'Kab. Belitung'
        $data_update = array ('kab_kota' => 'Kab. Belitung');
        $this->db_kapi->where('kab_kota', 'Kab.Belitung');
        $this->db_kapi->update('mst_inka_mina', $data_update);



        //update mst_inka_mina.kab_kota menjadi id_kabupaten_kota
        $data['kabupaten_kota'] = $this->kabupaten_kota();                  
        if(count($data) != 0){
            foreach ($data['kabupaten_kota'] as $item) {
                $data_update = array ('kab_kota' => $item->id_kabupaten_kota);
                $this->db->where('kab_kota', $item->nama_kabupaten_kota);
                $this->db->update('mst_inka_mina', $data_update);
            }
        }
        
        //ubah variabel string ke int
        $sql = "ALTER TABLE  `mst_inka_mina` 
                CHANGE  `kab_kota` `kab_kota` INT( 11 ) UNSIGNED NOT NULL,
                CHANGE  `anggota` `anggota` INT(11) UNSIGNED NOT NULL,
                CHANGE  `panjang_kapal` `panjang_kapal` FLOAT NOT NULL,
                CHANGE  `lebar_kapal` `lebar_kapal` FLOAT NOT NULL,
                CHANGE  `dalam_kapal` `dalam_kapal` FLOAT NOT NULL,
                CHANGE  `daya` `daya` INT(11) UNSIGNED NOT NULL
                        ";
        $this->db_kapi->query($sql);

        
    }
}