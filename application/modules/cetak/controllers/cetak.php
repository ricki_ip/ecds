<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cetak extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
			parent::__construct();
			$this->load->config('menus');
			$this->load->config('db_timeline');
			$this->load->config('globals');
			$this->load->model('preview/mdl_ctf_preview');
			$this->load->model('preview/mdl_cmf_preview');
				
	}

	public function index($id_ctf)
	{
		//mengambil tipe permohonan di table pendok
		$get_permohonan = (array)$this->mdl_pendok->get_permohonan($id_pendok);

		// if($is_final === 'final')
		// {
		// 	//menjalankan fungsi set_final untuk merubah status_pendok menjadi FINAL
		// 	$this->mdl_pendok->set_final($id_pendok);
		// 	//jika tipe permohonan = baru
		// 	if($get_permohonan['tipe_permohonan'] === 'BARU'){
		// 		$this->mdl_pendok->trigger_kapal_trs_bkp($id_pendok);
		// 	}
		// }

		$timestamp =  date('d/m/Y');
		$paths = $this->config->item('assets_paths');
		$forms = $this->config->item('form_ctf');
		$nama_hari = $this->config->item('Hari_f');
		$nama_bulan = $this->config->item('Bulan_f');
		$get_detail_ctf = $this->mdl_ctf_entry->get_detail($id);
		$data['detail_ctf'] = (array) $get_detail_ctf[0];
		$data['is_preview'] = FALSE;
		$data['css'] = NULL;
		$data['forms'] = $forms['form'];
		$data['nama_hari'] = $nama_hari;
		$data['nama_bulan'] = $nama_bulan;
		$data['id_ctf'] = $id_pendok;

		$css = file_get_contents($paths['main_css'].'/cetak_profil.css');
		$dokumen = $this->load->view('print_tanda_terima_pendok', $data, TRUE);
		//$dokumen = '<p>Hallo</p>';
		// var_dump($css);
		// var_dump($dokumen);

		ob_end_clean();
		$this->load->library('mpdf/mpdf');
		$this->mpdf=new mPDF('c','A4','','',32,25,27,25,16,13);
		$this->mpdf->SetHTMLHeader('Tanda Terima Permohonan Pendaftaran Kapal | |NO. PENDOK ('.$data['detail_pendok']['no_rekam_pendok'].')	');
		$this->mpdf->SetHTMLFooter('{PAGENO}|| Direktorat Kapal Perikanan dan Alat Penangkapan Ikan ||'.$timestamp);
		$this->mpdf->WriteHTML($css,1);
		$this->mpdf->WriteHTML($dokumen,2);
		if($is_final === 'final')
		{
			$this->mpdf->SetJS('this.print();');
		}
		$this->mpdf->Output('Catch Tagging Form.pdf','I');

	}

	// public function preview_before($id_pendok)
	// {
	// 	$timestamp =  date('d/m/Y');
	// 	$paths = $this->config->item('assets_paths');
	// 	$forms = $this->config->item('form_ctf');
	// 	$nama_hari = $this->config->item('Hari_f');
	// 	$nama_bulan = $this->config->item('Bulan_f');
	// 	$css = file_get_contents($paths['main_css'].'/cetak_profil.css');

	// 	$get_detail_ctf = $this->mdl_ctf_entry->get_detail($id_pendok);
	// 	$data['id_pendok'] = $id_pendok;
	// 	$data['detail_pendok'] = (array) $get_detail_pendok[0];
	// 	$data['is_preview'] = TRUE;
	// 	$data['forms'] = $forms['form'];
	// 	$data['css'] = $css;
	// 	$data['nama_hari'] = $nama_hari;
	// 	$data['nama_bulan'] = $nama_bulan;
	// 	$this->load->view('print_ctf', $data);
	// }

	// public function preview($id_pendok)
	// {	
	// 	$get_detail_pendok = $this->mdl_pendok->get_detail($id_pendok);
	// 	$data['detail_pendok'] = (array) $get_detail_pendok[0];

	// 	// $data['edit_link'] = base_url('pendok/main/edit/index');
	// 	// $data['cetak_link'] = base_url('pendok/cetak/index');
	// 	$data['id_pendok'] = $id_pendok;

	// 	$add_js = array('select2.min.js');
	// 	$add_css = array('select2.css');
	// 	echo Modules::run('templates/page/v_form', //tipe template
	// 						'pendok', //nama module
	// 						'preview_print', //nama file view
	// 						'label_print_pendok', //dari labels.php
	// 						$add_js, //plugin javascript khusus untuk page yang akan di load
	// 						$add_css, //file css khusus untuk page yang akan di load
	// 						$data); //array data yang akan digunakan di file view
	// }	
	
	// public function index()
	// {
	// 	$this->load->config('labels');

	// 	$data['database_timeline'] = $this->config->item('database_timeline');

	// 	$data['labels'] =  Array(
	// 								'form_kapal' => $this->config->item('form_kapal')
	// 							);
	// 	echo Modules::run('templates/page/v_form', //tipe template
	// 						'dev', //nama module
	// 						'index', //nama file view
	// 						'', //dari labels.php
	// 						'', //plugin javascript khusus untuk page yang akan di load
	// 						'', //file css khusus untuk page yang akan di load
	// 						$data); //array data yang akan digunakan di file view
	// }

	public function layout_ctf($id_ctf)
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		
		$get_detail_ctf = $this->mdl_ctf_preview->detail_ctf($id_ctf);
		// print_r($get_detail_ctf);
		$data['hasil'] = (array) $get_detail_ctf;

		$data['list_tags'] = $this->mdl_ctf_preview->ctf_tags($id_ctf);

		//$data['hasil'] = '';

		$views = 'layout_ctf';
		$template='templates/page/v_form';
		$modules='cetak';
		$labels='';
		// echo Modules::run($views, $add_js, $add_css);
		// echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

		$css_bootstrap = file_get_contents(base_url('assets/ecds/css').'/bootstrap_cetak.css');
		// $css_cetak = file_get_contents(base_url('assets/ecds/css').'/cetak_ctf.css');
		$dokumen = $this->load->view('layout_ctf', $data, TRUE);

		// ob_end_clean();
		$this->load->library('mpdf/mpdf');
		// $this->mpdf=new mPDF('utf-8', 'A5-L');
		$this->mpdf=new mPDF('c',   // mode - default ''
                          'Folio',    // format - A4, for example, default ''
                          0,     // font size - default 0
                          '',    // default font family
                          0,    // margin_left
                          0,    // margin right
                          0,    // margin top
                          0,    // margin bottom
                          0,     // margin header
                          0,     // margin footer
                          'P');  // L - landscape, P - portrait
		$this->mpdf->SetHeader('{PAGENO}');

		$this->mpdf->WriteHTML($css_bootstrap,1);
		// $this->mpdf->WriteHTML($css_cetak,1);
		// $this->mpdf->WriteHTML('<pagebreak suppress="on" />');
		$this->mpdf->WriteHTML($dokumen,2);
		if($is_final === 'final')
		{
			$this->mpdf->SetJS('this.print();');
		}
		$this->mpdf->Output('CTF.pdf','I');

		// $views = $this->load->view('layout_ctf', $data);
		// echo Modules::run($views, $add_js, $add_css);
		
	}

	public function layout_cmf($id_cmf)
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		
		$get_detail_cmf = $this->mdl_cmf_preview->detail_cmf($id_cmf);
		$get_detail_cmf_fish = $this->mdl_cmf_preview->detail_cmf_fish($id_cmf);

		$data['destination'] = $get_detail_cmf->product_destination;
		$data['detail_domestic'] = FALSE;
		$data['detail_export'] = FALSE;
		
		if($data['destination'] === 'domestic')
		{
			$data['detail_domestic'] = (array) $this->mdl_cmf_preview->detail_domestic($id_cmf);
			$get_domestic_fish = (array) $this->mdl_cmf_preview->list_domestic_fish($id_cmf);
			$array_fish_type = array();
			// var_dump(count($get_domestic_fish));

			foreach ($get_domestic_fish as $item) {
				if( !isset($array_fish_type[$item->code_tag]) )
				{
					// array_push($array_fish_type, $item->code_tag);
					$array_fish_type[$item->code_tag] = intval($item->net_weight);
				}else{
					$array_fish_type[$item->code_tag] = $array_fish_type[$item->code_tag] + intval($item->net_weight);
				}
			}
			// var_dump(array_keys($array_fish_type)[0]);die;
			$data['list_domestic_fish'] = $array_fish_type;

		}else{ //export
			$data['detail_export'] = (array) $this->mdl_cmf_preview->detail_export($id_cmf);
		}
		// print_r($get_detail_ctf);
		$data['hasil'] = (array) $get_detail_cmf;
		$data['detail_cmf_fish'] =  $get_detail_cmf_fish;
		// vdump($data, true);
		// $data['hasil'] = '';
		
		// $views = $this->load->view('layout_cmf', $data);
		// echo Modules::run($views, $add_js, $add_css);
		$views = 'layout_cmf';
		$template='templates/page/v_form';
		$modules='cetak';
		$labels='';
		// echo Modules::run($views, $add_js, $add_css);
		// echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

		$css_bootstrap = file_get_contents(base_url('assets/ecds/css').'/bootstrap_cetak.css');
		// $css_bootstrap = file_get_contents(base_url('assets/third_party/css').'/bootstrap.min.css');
		// $css = file_get_contents(base_url('assets/third_party/css').'/coba.css');
		$dokumen = $this->load->view('layout_cmf', $data, TRUE);

		// ob_end_clean();
		$this->load->library('mpdf/mpdf');
		// $this->mpdf=new mPDF('utf-8', 'A5-L');
		$this->mpdf=new mPDF('c',   // mode - default ''
                          'Folio',    // format - A4, for example, default ''
                          0,     // font size - default 0
                          '',    // default font family
                          5,    // margin_left
                          0,    // margin right
                          0,    // margin top
                          0,    // margin bottom
                          0,     // margin header
                          0,     // margin footer
                          'P');  // L - landscape, P - portrait
		$this->mpdf->SetHeader('{PAGENO}');
		$this->mpdf->WriteHTML($css_bootstrap,1);
		$this->mpdf->WriteHTML($dokumen,2);
		if($is_final === 'final')
		{
			$this->mpdf->SetJS('this.print();');
		}
		$this->mpdf->Output('CMF.pdf','I');
		
	}

	public function layout_reef($id_reef)
	{
		$this->load->model('preview/mdl_reef_preview');
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$get_detail_reef = $this->mdl_reef_preview->detail_reef($id_reef);
		// print_r($get_detail_reef);
		$data['hasil'] = (array) $get_detail_reef;

		// vdump($data['hasil'],true);
		$data['list_cmf_fish'] = $this->mdl_cmf_preview->detail_cmf_fish($data['hasil']['id_cmf']);		
		$data['list_reef_fish'] = $this->mdl_reef_preview->detail_reef_fish($id_reef);
		
		// vdump($data['list_reef_fish'],true);

		// $views = $this->load->view('layout_reef', $data);
		// echo Modules::run($views, $add_js, $add_css);
		$views = 'layout_reef';
		$template='templates/page/v_form';
		$modules='cetak';
		$labels='';
		// echo Modules::run($views, $add_js, $add_css);
		// echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
		// die;
		$css_bootstrap = file_get_contents(base_url('assets/ecds/css').'/bootstrap_cetak.css');
		// $css_bootstrap = file_get_contents(base_url('assets/third_party/css').'/bootstrap.min.css');
		$dokumen = $this->load->view('layout_reef', $data, TRUE);

		// vdump($data['hasil']['cmf_document_numbers'], true);
		// ob_end_clean();
		$this->load->library('mpdf/mpdf');
		// $this->mpdf=new mPDF('utf-8', 'A5-L');
		$this->mpdf=new mPDF('c',   // mode - default ''
                          'Folio',    // format - A4, for example, default ''
                          0,     // font size - default 0
                          '',    // default font family
                          0,    // margin_left
                          0,    // margin right
                          0,    // margin top
                          0,    // margin bottom
                          0,     // margin header
                          0,     // margin footer
                          'P');  // L - landscape, P - portrait
		$this->mpdf->SetHeader('{PAGENO}');

		$this->mpdf->WriteHTML($css_bootstrap,1);
		$this->mpdf->WriteHTML($dokumen,2);
		if($is_final === 'final')
		{
			$this->mpdf->SetJS('this.print();');
		}
		$this->mpdf->Output('REEF.pdf','I');
		
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */