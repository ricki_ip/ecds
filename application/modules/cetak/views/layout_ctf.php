<?php
    $tmpl = array ( 'table_open'  => '<table id="table_ctf_tag" class="table table-bordered table-ctf" style="font-size:12px;text-align:center;">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('CCSBT Tag Number', 'Type','Weight (Kg)','Fork length (cm)','Gear Code','Area Of Catch', 'Month of Harvest');
    // vdump($list_tags);

    $total_weight = 0;
    $limit_row = 33;
    foreach ($list_tags as $key => $items) {
      $this->table->add_row(
                            tagnumber($items->ccsbt_tag_number),
                            $items->code_tag,
                            $items->weight,
                            $items->fork_length,
                            $items->gear_code,
                            $items->ccsbt_statistical_area,
                            tgl($items->month_of_harvest,'F, Y')
                            );
      $total_weight += $items->weight;
      $limit_row--;
    }

    for ($i=0; $i < $limit_row ; $i++) { 
      $this->table->add_row(
                            '-',
                            '-',
                            '-',
                            '-',
                            '-',
                            '-',
                            '-');
    }

    $this->table->add_row(
                            '',
                            '',
                            '',
                            '',
                            '',
                            '<strong>Total Weight (Kg)</strong>',
                            $total_weight
                            );
    $table_ctf_tag = $this->table->generate();
  
?>

<html lang="en">
<head></head>
<body>
<div class="container">
  <div class="row">
    <div class="text-right">
      <small>
        <em>
          Page 1 of 1
        </em>
      </small>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div>
        <!-- <table class="table table-bordered">
          <thead>
            <tr>
              <th rowspan="3" width="5%"><img src="../assets/third_party/images/ccsbt-logo.jpg" width="70" height="70"></th>
              <th rowspan="3" width="20%">Commision for the Conservation of Southern Bluefin Tuna</th>
              <th width="50%">CATCH TAGGING FORM</th>
              <th width="25%">Document Number</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>T - </td>
              <td>T - </td>
            <tr>
              <td>Catch Documentation Scheme</td>
              <td>Catch Documentation Scheme</td>
            </tr> 
          </tbody>
        </table> -->

        <table class="table">
          <tr>
            <th rowspan="2" width="15%" style="text-align:center;background-color:black;"><img src= "<?php echo base_url("assets/third_party/images/ccsbt-logo.jpg"); ?>" width="90" height="90"> </th>
            <th rowspan="2" width="20%" style="font-family:arial;color:white;font-size:14px;background-color:black;">Commision for the Conservation of Southern Bluefin Tuna</th>
            <th width="30%" style="text-align: center;font-family:arial;color:white;font-size:20px;background-color:black;">CATCH TAGGING FORM</th>
            <th width="20%" style="font-family:arial;color:white;font-size:12px;background-color:black;text-align:center">Document Number</th>
            <th rowspan="2" width="15%" style="font-family:arial;color:white;font-size:18px;background-color:black;text-align:center;"><img src="<?php echo base_url("assets/third_party/images/kkp.png"); ?>" width="90" height="90"></th>
          </tr>
          <tr>
            
          </tr>
          <tr>
            <td style="font-family:arial;color:white;font-size:13px;text-align:center;background-color:black;">Catch Documentation Scheme</td>
            <td style="background-color:white;"><?php echo kos($hasil['document_number'], 'Temporary:'.$hasil['temp_doc_id']); ?></td>
          </tr>
        </table>

      </div>
    </div>      
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>  
            </tr>
          </thead>
          <tbody>
            <tr>
              <td width="5%"><img src="<?php echo base_url("assets/third_party/images/checkbox.png"); ?>" height="30px"></td>
              <td style="font-size:12px;text-align:center;" width="20%">Wild Harvest</td>
              <td width="50%" style="text-align:center;"><img src="<?php echo base_url("assets/third_party/images/panah.png"); ?>" height="10px" width="300px"></td>
              <td width="5%"><img src="<?php echo base_url("assets/third_party/images/checkbox_kosong.png"); ?>" height="30px"></td>
              <td style="font-size:12px;text-align:center;" width="20%">Farmed (tick only one)</td>
            </tr> 
          </tbody>
        </table>
      </div>
    </div>      
  </div>

  <div class="row">
    <div class="col-lg-9">
      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th width="200px" style="font-size:12px;padding: 10px 10px;text-align: center;background-color: #ddd;" height="30px">Document Number of Associated Catch of Monitoring Form</th>
              <th width="250px"><?php echo $hasil['document_number_associated_cmf']; ?></th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>      
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="table-responsive">
        <table class="table table-bordered" style="font-size:12px;">
          <thead>
            <tr>
              <th colspan="3" style="font-family:arial;color:white;font-size:18px; background-color:black;">CATCH SECTION</th>
            </tr>
            <tr>
              <th style="padding:5px 5px;background-color: #ddd;">Name of Fishing Vessel</th>
              <th style="padding:5px 5px;background-color: #ddd;">Vessel Registration Number</th>
              <th style="padding:5px 5px;background-color: #ddd;">Flag State/Fishing Entity</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td style="padding:10px 10px;text-align: center;"><?php echo $hasil['vessel_name']; ?></td>
              <td style="padding:10px 10px;text-align: center;"><?php echo $hasil['vessel_registration_number']; ?></td>
              <td style="padding:10px 10px;text-align: center;"><?php echo $hasil['flag_state']; ?></td>
            </tr>
          </tbody>
        </table>
      </div>
      <table class="table table-bordered" style="font-size:12px;">
        <thead>
          <tr>
            <th style="padding:5px 5px;background-color: #ddd;">Information on Other form(s) of Capture (eg. Trap)</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td height="15px" style="padding:5px 5px;">-</td>
          </tr>
        </tbody>
      </table>
      Tag Information
      <?php echo $table_ctf_tag; ?>
          <table class="table table-bordered" style="font-size:12px;">
            <tbody>
              <tr>
                <td>Certification: I certify that the above information is complete, true and correct to the best of my knowledge and belief.
                </td>  
              </tr>
            </tbody>
          </table>
      </div>
  </div>

  <div class="row">
     <div class="col-lg-12">
       <table class="table table-bordered" style="font-size:12px;">
            <thead>
              <tr>
                <th width="35%" style="padding: 10px 10px;text-align: center;background-color: #ddd;">Name</th>
                <th width="35%" style="padding: 10px 10px;text-align: center;background-color: #ddd;">Signature</th>
                <th width="10%" style="padding: 10px 10px;text-align: center;background-color: #ddd;">Date</th>
                <th width="20%" style="padding: 10px 10px;text-align: center;background-color: #ddd;">Title</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td style="padding: 10px 10px;text-align: center;"><?php echo kos($hasil['name'],'-'); ?></td>
                <td style="padding: 10px 10px;text-align: center;"><?php echo kos($hasil['signature'],' '); ?></td>
                <td style="padding: 10px 10px;text-align: center;"><?php echo tgl($hasil['tgl_verifikasi'],'F jS,Y'); ?></td>
                <td style="padding: 10px 10px;text-align: center;"><?php echo kos($hasil['title'],$hasil['owner_name']); ?></td>
              </tr>
            </tbody>
          </table>
     </div>
  </div>  
</div>        
<!-- 
          

         

         

     

 -->
  </body>
</html> 
