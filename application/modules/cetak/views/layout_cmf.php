<?php

    $tmpl = array ( 'table_open'  => '<table class="table table-bordered table-cmf" id="table_cmf_tag" width="750" >' );
    $this->table->set_template($tmpl);
    $cell_first_heading = array('data' => 'Description of Fish', 'colspan' => 7,'class' => 'text-left','style' =>'background-color: #ddd;');
    $this->table->set_heading($cell_first_heading);
    $heading_table = array( array( 'data' => '<strong>Product: F (Fresh)/ FR (Frozen)</strong>',
                              'style' => 'background-color:#ddd;'),
                            array( 'data' => '<strong>Type: RD/GGO/GGT/DRO/DRT/FL/OT*</strong>',
                              'style' => 'background-color:#ddd;'),
                            array( 'data' => '<strong>Month of Catch/Harvest (mm/yy)</strong>',
                              'style' => 'background-color:#ddd;'),
                            array( 'data' => '<strong>Gear Code</strong>',
                              'style' => 'background-color:#ddd;'),
                            array( 'data' => '<strong>CCSBT Statistical Area</strong>',
                              'style' => 'background-color:#ddd;'),
                            array( 'data' => '<strong>Net Weight (Kg)</strong>',
                              'style' => 'background-color:#ddd;'),
                            array( 'data' => '<strong>Total Number of whole Fish (including RD/GGO/GGT/DRO/DRT)</strong>',
                              'style' => 'background-color:#ddd;')
                          );
    $this->table->add_row($heading_table);
    // vdump($detail_domestic); 
    $limit_row = 5;
    
    if($detail_cmf_fish)
    {
      foreach ($detail_cmf_fish as $key => $item) {
        // $product = $item->product_type === 'F' ? 'FRESH' : 'FROZEN';
        $product = $item->product_type;
        $this->table->add_row(
          $product,
          $item->code_tag,
          tgl($item->month_of_harvest, 'm/Y'),
          $item->nama_gear_code,
          $item->ccsbt_statistical_area,
          $item->net_weight,
          angka($item->total_fish,0) );
        $limit_row--;
      }
    }

    for ($i=0; $i < $limit_row ; $i++) { 
      $this->table->add_row(
                              '-',
                              '-',
                              '-',
                              '-',
                              '-',
                              '-',
                              '-');
    }

    $cell_OT_product = array('data' => 'For Other (OT): Describe the Type of Product', 'colspan' => 2);
    $cell_empty_colspan = array('data' => kos($hasil['description_for_other']), 'colspan' => 2);
    $cell_OT_conversion = array('data' => 'For Other (OT): Specify Conversion Factor', 'colspan' => 2);
    $OT_conversion = kos($hasil['description_for_other'],'-') === '-' ? '-' : '1.15';
    $this->table->add_row($cell_OT_product, $cell_empty_colspan, $cell_OT_conversion, $OT_conversion);
    $table_cmf_tag = $this->table->generate();

    $this->table->clear();
?>

<html lang="en">
<head>

  <!-- <title>ECDS</title> -->
  <!-- Bootstrap core CSS -->

  <!-- Custom styles for this template -->
  <!--  <link href="assets/ecds/css/style.css" rel="stylesheet"> -->
  

</head>

<body>

  <div class="container">

    <div class="row">
      <div class="col-lg-12">
        <div class="table-responsive">
          <table class="table table-bordered" width="750" >
          <tr>
            <th rowspan="2" width="15%" style="text-align:center;background-color:black;"><img src= "<?php echo base_url("assets/third_party/images/ccsbt-logo.jpg"); ?>" width="70" height="70"> </th>
            <th rowspan="2" width="20%" style="font-family:arial;color:white;font-size:14px;background-color:black;">Commision for the Conservation of Southern Bluefin Tuna</th>
            <th width="30%" style="text-align: center;font-family:arial;color:white;font-size:20px;background-color:black;">CATCH MONITORING FORM</th>
            <th width="20%" style="font-family:arial;color:white;font-size:12px;background-color:black;text-align:center">Document Number</th>
            <th rowspan="2" width="15%" style="font-family:arial;color:white;font-size:18px;background-color:black;text-align:center;"><img src="<?php echo base_url("assets/third_party/images/kkp.png"); ?>" width="70" height="70"></th>
          </tr>
          <tr>
          </tr>
          <tr>
            <td style="font-family:arial;color:white;font-size:10px;text-align:center;background-color:black;">Catch Documentation Scheme</td>
            <td style="background-color:white;font-size: 1.4em;"><?php echo kos($hasil['document_number'], 'Temporary:'.$hasil['temp_doc_id']); ?></td>
          </tr>
        </table>
        </div>
      </div>      
    </div>
<!-- 
    <div class="row">
      <div class="col-lg-12">
        <div class="table-responsive">
          <table class="table table-bordered" class="table table-bordered" style="font-size:10px;">
            <thead>
            </thead>
            <tbody>
              
            </tbody>
          </table>
        </div>
      </div>      
    </div> -->

    <div class="row">
      <div class="col-lg-12">
        <!-- <div class="panel panel-default"> -->
        <!-- <div class="panel-heading">
          <h3 class="panel-title">CATCH / HARVEST SECTION - Tick and complete only one part</h3>
        </div> -->
        <!-- <div class="panel-body"> -->
        <div class="table-responsive">
          <table class="table table-bordered"  width="750">
            <thead>
              <tr>
                <td colspan="3" style="background-color: #ddd;text-align: center;">
                  <strong>
                    Catch Tagging Form Document Numbers
                  </strong>
                </td>
                <td colspan="2" style="font-size: 1.4em;"><?php echo kos($hasil['ctf_document_number']); ?></td>
              </tr>
              <tr>
                <th colspan="5" style="font-family:arial;color:white;font-size:10px;text-align:center;background-color:black;">CATCH / HARVEST SECTION - Tick and complete only one part</th>
              </tr>
              <tr>
               <th width="3%">
                  <img src="<?php echo base_url("assets/third_party/images/checkbox.png"); ?>" height="20px">
               </th>  
               <th  width="17%" style="background-color: #ddd;">For Wild Fishery</th>
               <th width="40%" style="background-color: #ddd;">Name of Catching Vessel</th>
               <th width="20%" style="background-color: #ddd;">Registration Number</th>
               <th width="20%" style="background-color: #ddd;">Flag State/Fishing Entity</th>
             </tr>
           </thead>
           <tbody>
            <tr>
              <td colspan="2" style="border-left-color: #fff;border-bottom-color: #fff;"></td>
              <td><?php echo $hasil['vessel_name']; ?></td>
              <td><?php echo $hasil['vessel_registration_number']; ?></td>
              <td><?php echo 'INDONESIA'; ?></td>
            </tr>
          </tbody>
        </table>
        <table class="table table-bordered" width="750" >
          <thead>
            <tr>
              <th width="3%">
                  <img src="<?php echo base_url("assets/third_party/images/checkbox_kosong.png"); ?>" height="20px">                
              </th>
              <th  width="17%" style="background-color: #ddd;">For Farmed SBT</th>
              <th width="20%" style="background-color: #ddd;">CCSBT Farm Serial Number</th>
              <th width="60%" style="background-color: #ddd;">Name of Farm</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td colspan="2" rowspan="3" height="20px" style="border-left-color: #fff;border-bottom-color: #fff;"></td>
              <td height="20px"></td>
              <td height="20px"></td>
            </tr>
            <tr>
              <td colspan="2" style="background-color: #ddd;">Document Number(s) of associated Farm Stocking (FS) Form(s)</td>
            </tr>
            <tr>
              <td colspan="2" height="20px"></td>
            </tr>
          </tbody>
        </table>
      </div>
    <!-- </div> -->

   <!--  <div class="col-lg-12"> -->
    <!-- <table class="table table-bordered" class="table table-bordered">
      <thead>
        <tr><th colspan="7" style="text-align: center;">Description of Fish</th></tr>
      </thead>
    </table> -->
    <?php echo $table_cmf_tag; ?>
    <table class="table table-bordered" width="750" style="font-size:10px;margin-bottom:5px;">
     <thead>
      <tr>
        <th style="background-color: #ddd;">Name of Processing Establishment (if applicable)</th>
        <th style="background-color: #ddd;">Address of Processing Establishment (if applicable)</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><?php echo strtoupper( $hasil['name_processing_establishment'] ); ?></td>  
        <td><?php echo strtoupper( $hasil['address_processing_establishment'] ); ?></td>  
      </tr>
    </tbody>
  </table>

  <table class="table table-bordered" width="750" style="font-size:10px;margin-bottom:5px;">
   <thead>
              <!-- <th colspan="4">Validation by Authority (not required for exports transhipped at sea): I validate that the above information is complete, trure and correct to the best of my knowledge and belief</th>
              <th></th> -->
            </thead>
            <tbody>
              <tr>
                <td colspan="4" width="75%" style="background-color: #ddd;">Validation by Authority (not required for exports transhipped at sea): I validate that the above information is complete, trure and correct to the best of my knowledge and belief</td>
                <td rowspan="3" width="25%"></td>
              </tr> 
              <tr>
                <td rowspan="2" width="10%" style="background-color: #ddd;text-align: center;">Name and Title</td>  
                <td rowspan="2" width="40%"style="text-align: center;" ><?php echo strtoupper( $hasil['cmf_authority_name_title'] ); ?></td>
                <td width="5%" style="background-color: #ddd;padding: 10px 10px;text-align: center;">Signature</td>
                <td width="20%">&nbsp;</td>
                <!--  <td rowspan="2"></td> -->
              </tr>
              <tr>
                <td style="background-color: #ddd;padding: 10px 10px;text-align: center;">Date</td>
                <td style="padding: 10px 10px;text-align: center;"><?php echo tgl($hasil['tgl_verifikasi'],'F jS,Y'); ?></td>
                <!-- <td></td>  -->
              </tr>
            </tbody>
          </table>
        
      <!-- </div> -->
    </div>
      </div>

      <div class="row">
       <div class="col-lg-12">
         <table class="table table-bordered" width="750" >
          <thead>
            <tr>
                    <th colspan="7" style="font-family:arial;color:white;font-size:10px;text-align:center;background-color:black;">INTERMEDIATE PRODUCT DESTINATION SECTION - (only for transhipments and/or exports) - tick and complete required part(s)</th>
                  </tr>
            <tr >
              <th><input type="checkbox"></th>
              <th style="background-color: #ddd;">Transhipment</th>
              <th colspan="5" style="background-color: #ddd;">Certification by Master of Fishing Vessel: I certify that the catch/harvest information is complete, true and correct to the best of my knowledge and belief</th>
            </tr>
          </thead>
          <tbody>
            <tr>
             <td width="4%" style="border-left-color: #fff;border-bottom-color: #fff;"></td>
              <td width="10%" style="background-color: #ddd;">Name</td>
            <td width="26%"></td>
            <td width="10%" style="background-color: #ddd;">Date</td>
            <td width="20%"></td>
            <td width="10%" style="background-color: #ddd;">Signature</td>
            <td width="20%"></td>
           </tr>
         </tbody>
       </table>
       <table class="table table-bordered" width="750" style="font-size:10px;margin-bottom:5px;margin-left:3.5em;">
        <thead>
          <tr>
            <th style="background-color: #ddd;">Name of Receiving Vessel</th>
            <th style="background-color: #ddd;">Registration Number</th>
            <th style="background-color: #ddd;">Flag State/Fishing Entity</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td height="15px"></td>
            <td height="15px"></td>
            <td height="15px"></td>
          </tr>
        </tbody>
      </table>
      <table class="table table-bordered" width="750" style="font-size:10px;margin-bottom:5px;margin-left:3.5em;">
        <thead>
          <tr>
            <th colspan="6" style="background-color: #ddd;">Certification by Master of Receiving Vessel: I certify that the above information is complete, true and correct to the best of my knowledge and belief</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td width="10%" style="background-color: #ddd;">Name</td>
            <td width="30%"></td>
            <td width="10%" style="background-color: #ddd;">Date</td>
            <td width="20%"></td>
            <td width="10%" style="background-color: #ddd;">Signature</td>
            <td width="20%"></td>
          </tr>
        </tbody>
      </table>
      <table class="table table-bordered" width="750" style="font-size:10px;margin-bottom:5px;margin-left:3.5em;">
        <thead>
          <tr>
            <th colspan="6" style="background-color: #ddd;">Signature of Observer (only for transhipment at sea):</th>
          </tr>
        </thead>
        <tbody>
          <tr>
             <td width="10%" style="background-color: #ddd;">Name</td>
            <td width="30%"></td>
            <td width="10%" style="background-color: #ddd;">Date</td>
            <td width="20%"></td>
            <td width="10%" style="background-color: #ddd;">Signature</td>
            <td width="20%"></td>
          </tr>
        </tbody>
      </table>
    </div>

    <div class="col-lg-10">
     <table class="table table-bordered" width="750" >
      <thead>
        <tr>
          <th width="5%">
            <?php if($destination === 'export') :?>
              <img src="<?php echo base_url("assets/third_party/images/checkbox.png"); ?>" height="20px">
            <?php else: ?>
              <img src="<?php echo base_url("assets/third_party/images/checkbox_kosong.png"); ?>" height="20px">
            <?php endif ?>            
          </th>
          <th style="background-color: #ddd;">Export</th>
          <th colspan="5" style="text-align:center;background-color: #ddd;">Point of Export*</th>
          <th rowspan="2" style="background-color: #ddd;">Destination (State/Fishing Entity)</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td width="5%" style="border-left-color: #fff;border-bottom-color: #fff;"></td>
          <td width="10%" style="background-color: #ddd;">City</td>
          <td width="20%"><?php echo strtoupper( kos($detail_export['kota_asal']) ); ?></td>
          <td width="10%" style="background-color: #ddd;">State or Province</td>
          <td width="20%"><?php echo strtoupper( kos($detail_export['propinsi_asal']) ); ?></td>
          <td width="10%" style="background-color: #ddd;">State/Fishing Entity</td>
          <td width="25%"><?php echo strtoupper( kos($detail_export['state_entity']) ); ?></td>
       </tr>
       <tr>
          <td colspan="7" style="border-left-color: #fff;border-bottom-color: #fff;border-top-color: #fff;">&nbsp;</td>
          <td style="padding: 10px 10px;"><?php echo strtoupper( kos($detail_export['negara_export']) ); ?></td>
       </tr>
     </tbody>
   </table>
 </div>


<div class="col-lg-12">
 <table class="table table-bordered" width="750" style="font-size:10px;margin-bottom:5px;margin-left:3.5em;">
  <thead>
    <tr>
      <th colspan="4" style="background-color: #ddd;">Certfication by Exporter: I certify that the above information is complete, true and correct to the best pf my knowledge and belief</th>
    </tr>
  </thead>
  <tbody>
    <tr>
     <td style="background-color: #ddd;">Name</td>
     <td style="background-color: #ddd;">License No./Company Name</td>
     <td style="background-color: #ddd;">Date</td>
     <td style="background-color: #ddd;">Signature</td>
   </tr>
   <tr>
    <td style="padding: 10px 10p;"><?php if($destination === 'export') { echo kos($detail_export['name']); }else{ echo '-';} ?></td>
    <td style="padding: 10px 10p;"><?php if($destination === 'export') { echo kos($hasil['exporter_company']); }else{ echo '-';} ?></td>
    <td style="padding: 10px 10p;"><?php if($destination === 'export') { echo tgl($hasil['tgl_verifikasi'],'F jS,Y'); }else{ echo '-';} ?></td>
    <td style="padding: 10px 10p;"></td>
  </tr>
</tbody>
</table>
<table class="table table-bordered" width="750" style="font-size:10px;margin-bottom:5px;margin-left:3.5em;">
 <thead>
              <!-- <th colspan="4">Validation by Authority (not required for exports transhipped at sea): I validate that the above information is complete, trure and correct to the best of my knowledge and belief</th>
              <th></th> -->
            </thead>
            <tbody>
              <tr>
                <td colspan="4" width="75%" style="background-color: #ddd;">Validation by Authority: I validate that the above information is complete, trure and correct to the best of my knowledge and belief</td>
                <td rowspan="3" width="25%"></td>
              </tr> 
              <tr>
                <td rowspan="2" width="10%" style="background-color: #ddd;">Name and Title</td>  
                <td rowspan="2" width="30%"><?php if($destination === 'export') { echo strtoupper( $hasil['cmf_authority_name_title'] );} ?></td>
                <td width="5%" style="background-color: #ddd;">Signature</td>
                <td width="20%"></td>
                <!--  <td rowspan="2"></td> -->

              </tr>
              <tr>
                <td style="background-color: #ddd;">Date</td>
                <td><?php if($destination === 'export') {  echo tgl($hasil['tgl_verifikasi'],'F jS,Y'); }?></td>
                <!-- <td></td>  -->
              </tr>
            </tbody>
          </table>
        </div>

      </div>

      <div class="row">
        <div class="col-lg-12">
          <!-- <div class="panel panel-default"> -->
            <!-- <div class="panel-heading">
              <h3 class="panel-title">FINAL PRODUCT DESTINATION SECTION - tick and complete only one destination</h3>
            </div> -->
            <!-- <div class="panel-body"> -->
              <div class="table-responsive">
                <table class="table table-bordered" width="750" >
                  <thead>
                   <tr>
                    <th colspan="7" style="font-family:arial;color:white;font-size:10px;text-align:center;background-color:black;">FINAL PRODUCT DESTINATION SECTION - tick and complete only one destination</th>
                  </tr>
                    <tr>
                     <th width="5%">
                        <?php if($destination === 'domestic') :?>
                          <img src="<?php echo base_url("assets/third_party/images/checkbox.png"); ?>" height="20px">
                        <?php endif ?>
                     </th> 
                     <th style="background-color: #ddd;">Landing of Domestic Product for Domestic sale</th>
                     <th colspan="5" style="background-color: #ddd;">Certification of Domestic Sale. I certify that the above information is complete, true and correct to the best of my knowledge and belief.</th>
                   </tr>
                   <tr>
                     <th style="border-left-color: #fff; border-bottom-color: #fff;"></th>
                     <th style="background-color: #ddd;" width="20%">Name</th>  
                     <th style="background-color: #ddd;" width="20%">Address</th>
                     <th style="background-color: #ddd;" width="15%">Date</th>
                     <th style="background-color: #ddd;" width="15%">Signature</th>
                     <th style="background-color: #ddd;" width="15%">Type: RD/GGO/GGT/DRO/DRT/FL/OT</th>
                     <th style="background-color: #ddd;" width="15%">Weight (kg)</th>
                   </tr>
                 </thead>
                 <tbody>
                  <tr >
                    <?php if ($destination === 'domestic'): ?>
                    <tr>
                      <td rowspan='3' style="text-align: center;padding: 5px 10px;border-left-color: #fff;border-bottom-color: #fff;border-top-color: #fff;"></td>
                      <td rowspan='3' style="text-align: center;padding: 5px 10px;"><?php echo kos($detail_domestic['name']); ?></td>
                      <td rowspan='3' style="text-align: center;padding: 5px 10px;"><?php echo kos($detail_domestic['address']); ?></td>
                      <td rowspan='3' style="text-align: center;padding: 5px 10px;"><?php echo tgl($hasil['tgl_verifikasi'],'F jS,Y'); ?></td>
                      <td rowspan='3' style="text-align: center;padding: 5px 10px;"></td>
                      <td style="text-align: center;padding: 5px 10px;"><?php echo kos(array_keys($list_domestic_fish)[0]); ?></td>
                      <td style="text-align: center;padding: 5px 10px;"><?php echo kos(array_values($list_domestic_fish)[0]);?></td>
                    </tr>
                    <tr>
                      <td style="text-align: center;padding: 5px 10px;"><?php echo kos(array_keys($list_domestic_fish)[1],'-'); ?></td>
                      <td style="text-align: center;padding: 5px 10px;"><?php echo kos(array_values($list_domestic_fish)[1],'-');?></td>
                    </tr>
                    <tr>
                       <td style="text-align: center;padding: 5px 10px;"><?php echo kos(array_keys($list_domestic_fish)[2],'-'); ?></td>
                        <td style="text-align: center;padding: 5px 10px;"><?php echo kos(array_values($list_domestic_fish)[2],'-');?></td>
                    </tr>
                    <?php else: ?>
                    <tr>
                      <td rowspan='3' style="text-align: center;padding: 5px 10px;border-left-color: #fff;border-bottom-color: #fff;"></td>
                      <td rowspan='3' style="text-align: center;padding: 5px 10px;">-</td>
                      <td rowspan='3' style="text-align: center;padding: 5px 10px;">-</td>
                      <td rowspan='3' style="text-align: center;padding: 5px 10px;">-</td>
                      <td rowspan='3' style="text-align: center;padding: 5px 10px;">-</td>
                      <td style="text-align: center;padding: 5px 10px;">-</td> 
                      <td style="text-align: center;padding: 5px 10px;">-</td>
                    </tr>
                    <tr>
                      <td style="text-align: center;padding: 5px 10px;">-</td>
                      <td style="text-align: center;padding: 5px 10px;">-</td>
                    </tr>
                    <tr>
                        <td style="text-align: center;padding: 5px 10px;">-</td>
                        <td style="text-align: center;padding: 5px 10px;">-</td>
                    </tr>
                    <?php endif ?>
                </tbody>
              </table>

             <table class="table table-bordered" width="750" >
                <thead>
                  <tr>
                   <th width="5%">
                      <img src="<?php echo base_url("assets/third_party/images/checkbox_kosong.png"); ?>" height="20px"> 
                   </th>  
                   <th width="10%" style="background-color: #ddd;">Import</th>
                   <th colspan="6" width="85%" style="text-align: center;background-color: #ddd;">Final Point of Import</th>
                 </tr>
                 <tbody>
                  <tr>
                    <td width="5%" style="padding: 10px 10px;border-left-color: #fff;border-bottom-color: #fff;"></td>
                    <td width="10%" style="background-color: #ddd;">City</td>
                    <td width="20%"><?php echo kos($hasil['city']); ?></td>
                    <td width="10%" style="background-color: #ddd;">State or Province</td>
                    <td width="20%"><?php echo kos($hasil['province']); ?></td>
                    <td width="10%" style="background-color: #ddd;">State/Fishing Entity</td>
                    <td width="25%"><?php echo kos($hasil['state_entity']); ?></td>
                  </tr>
                </tbody>
              </table>

              <table class="table table-bordered" width="750" style="font-size:10px;margin-bottom:5px;margin-left:3.5em;">
                <thead>
                  <tr>
                   <th colspan="7" style="background-color: #ddd;">Certification by Importer: I certify that the above information is complete, true and correct to the best of my knowledge and belief</th>
                 </tr>
                 <tr>
                     <!-- <th style="border-left-color: #fff; border-bottom-color: #fff;"></th> -->
                     <th style="background-color: #ddd;" width="20%">Name</th>  
                     <th style="background-color: #ddd;" width="20%">Address</th>
                     <th style="background-color: #ddd;" width="15%">Date</th>
                     <th style="background-color: #ddd;" width="15%">Signature</th>
                     <th style="background-color: #ddd;" width="15%">Type: RD/GGO/GGT/DRO/DRT/FL/OT</th>
                     <th style="background-color: #ddd;" width="15%">Weight (kg)</th>
                   </tr>
                 <tbody>
                <tr>
                      <!-- <td rowspan='3' style="text-align: center;padding: 5px 10px;border-left-color: #fff;border-bottom-color: #fff;"></td> -->
                      <td rowspan='3' style="text-align: center;padding: 5px 10px;">&nbsp;&nbsp;&nbsp;</td>
                      <td rowspan='3' style="text-align: center;padding: 5px 10px;">&nbsp;&nbsp;&nbsp;</td>
                      <td rowspan='3' style="text-align: center;padding: 5px 10px;">&nbsp;&nbsp;&nbsp;</td>
                      <td rowspan='3' style="text-align: center;padding: 5px 10px;">&nbsp;&nbsp;&nbsp;</td>
                      <!-- <td rowspan='3' style="text-align: center;padding: 5px 10px;">&nbsp;&nbsp;&nbsp;</td> -->
                      <td style="text-align: center;padding: 5px 10px;">&nbsp;&nbsp;&nbsp;</td> 
                      <td style="text-align: center;padding: 5px 10px;">&nbsp;&nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                      <td style="text-align: center;padding: 5px 10px;">&nbsp;&nbsp;&nbsp;</td>
                      <td style="text-align: center;padding: 5px 10px;">&nbsp;&nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align: center;padding: 5px 10px;">&nbsp;&nbsp;&nbsp;</td>
                        <td style="text-align: center;padding: 5px 10px;">&nbsp;&nbsp;&nbsp;</td>
                    </tr>
                </tbody>
              </table>

            </div>
          <!-- </div> -->
        <!-- </div> -->
      </div>
    </div>


  </div>        
<!-- 
          

         

         

     

-->
</body>
</html> 
