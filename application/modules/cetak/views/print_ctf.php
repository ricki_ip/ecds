<?php if ($is_preview): ?>
	<style type="text/css">
   		body {
	        margin: 0;
	        padding: 0;
	        background-color: #FAFAFA;
		}
	    * {
	        box-sizing: border-box;
	        -moz-box-sizing: border-box;
	    }
	    .page {
	        width: 21cm;
	        min-height: 29.7cm;
	        padding: 2cm;
	        margin: 1cm auto;
	        border: 1px #D3D3D3 solid;
	        border-radius: 5px;
	        background: white;
	        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
	    }
	    .subpage {
	        padding: 1cm;
	        border: 5px black dotted;
	        height: 237mm;
	        outline: 2cm #FFEAEA solid;
	    }
	    
	    @page {
	        size: A4;
	        margin: 0;
	    }
	</style>
<?php endif ?>
<?php 
 	$tmpl = array ( 'table_open'  => '<table id="table_detail_ctf">' );
    $this->table->set_template($tmpl);

    $this->table->add_row('1.','Document Number ',': '.$detail_ctf['document_number']);
    $this->table->add_row('2.','Vessel Name ',': '.$detail_ctf['vessel_name']);
    // $this->table->add_row('3.','Pengurus', ': '.$detail_ctf['nama_pemohon']);
    // $this->table->add_row('4.','No. Telepon', ': '.$detail_ctf['no_telp_pemohon']);
    // $this->table->add_row('5.','Nama Kapal', ': '.$detail_ctf['nama_kapal']);
    // $this->table->add_row('6.','Jenis Permohonan', ': '.$detail_ctf['tipe_permohonan']);

   
    $html = $this->table->generate();
    $this->table->clear();

    // $tmpl = array ( 'table_open'  => '<table id="table_detail_dokumen">' );
    // $this->table->set_template($tmpl);

    // $this->table->set_heading('No.', 'Uraian','Kelengkapan');
    
    // $checkbox_tag = Modules::run('refkapi/mst_dokumen/list_trs_pendok_dokumen_per_id',$detail_pendok['id_pendok']);
    // $counter = 1;
    // foreach ($checkbox_tag as $key) {
    // 	$ceklis = $key->isi === 'ADA' ? '&radic;' : '-';
    // 	$this->table->add_row($counter.'. ', $key->text, $ceklis);
    // 	$counter++;
    // }
    $html .= "<hr>";
    $html .= '<div class="datagrid">';
    $html .= $this->table->generate();
    $html .= '</div>';

 ?>
 <div class="book">
    <div class="page">
        <div class="subpage">
            <p><h3 align="center">Form CTF</h3></p>

        	<?php echo $html; ?>
        	<?php 
        		$i = 0;
	        	while($i < 5){
	        		echo '<br>';
	        		$i++;        		
	        	}
        	?>
        	<table style="width: 300px; margin-left: 65%;">
        	<tbody>
        	<!-- <tr>
        		<td align="center"><p><?php 
        		$dow = intval( date('w', now()) );
        		$dt = date('d', now() );
        		$yr = date('Y', now() );
        		$indm = intval( date('n', now() ));
        		$month = $nama_bulan[$indm-1]; 
        		$day = $nama_hari[$dow];
        		echo $day.', '.$dt.' '.$month.' '.$yr;
        		//echo date('l, d F Y ',now())
        		 ?></p></td>
        	</tr> -->
        	<tr>
        		<td align="center"><p>Petugas Penerima Dokumen</p></td>
        	</tr>
            <tr>
                <td align="center"><p>ttd</p></td>
            </tr>
        	<tr>
        		<td align="center">
        		<?php 
        		$i = 0;
	        	while($i < 3){
	        		echo '<br>';
	        		$i++;        		
	        	}
        		?>
        		</td>
        	</tr>
        	<tr>
        		<td align="center">Eko</td>
        	</tr>
        	</tbody>
        	</table>
            <p>
                *) Lembar putih : Petugas KAPI
                <br>
                *) Lembar merah : Pemohon
            </p>
        </div>    
    </div>
</div>

