<?php 
  // Table CMF
    $tmpl = array ( 'table_open'  => '<table class="table table-bordered table-cmf" id="table_cmf_tag" width="750" >' );
    $this->table->set_template($tmpl);
    $cell_first_heading = array('data' => 'Description of Fish from previous CDS Document', 'colspan' => 4,'class' => 'text-center','style' =>'background-color: #ddd;padding: 10px;');
    $this->table->set_heading($cell_first_heading);
    $pre_heading_table = array( array( 'data' => '<strong>Flag State/Fishing Entity</strong>',
                              'colspan' => 2,
                              'style' => 'background-color:#ddd;padding: 5px;'),
                            array( 'data' => '<strong>Date of previous Import/Landing</strong>',
                              'colspan' => 2,
                              'style' => 'background-color:#ddd;padding: 5px;'),
                          );
    $this->table->add_row($pre_heading_table);
    $pre_value_table = array( array( 'data' => 'INDONESIA',
                              'colspan' => 2,
                              ),
                            array( 'data' => $hasil['previous_date_landing'],
                              'colspan' => 2,
                              ),
                          );
    $this->table->add_row($pre_value_table);
    $heading_table = array( array( 'data' => '<strong>Product: F (Fresh)/ FR (Frozen)</strong>',
                              'style' => 'background-color:#ddd;padding: 5px;'),
                            array( 'data' => '<strong>Type: RD/GGO/GGT/DRO/DRT/FL/OT*</strong>',
                              'style' => 'background-color:#ddd;padding: 5px;'),
                            array( 'data' => '<strong>Net Weight (Kg)</strong>',
                              'style' => 'background-color:#ddd;padding: 5px;'),
                            array( 'data' => '<strong>Total Number of whole Fish (including RD/GGO/GGT/DRO/DRT)</strong>',
                              'style' => 'background-color:#ddd;padding: 5px;')
                          );
    $this->table->add_row($heading_table);
    // vdump($detail_domestic); 
    $limit_row = 5;
    
    if($list_cmf_fish)
    {
      foreach ($list_cmf_fish as $key => $item) {
        // $product = $item->product_type === 'F' ? 'FRESH' : 'FROZEN';
        $product = $item->product_type;
        $this->table->add_row(
          $product,
          $item->code_tag,
          $item->net_weight,
          angka($item->total_fish));
        $limit_row--;
      }
    }

    for ($i=0; $i < $limit_row ; $i++) { 
      $this->table->add_row(
                              '-',
                              '-',
                              '-',
                              '-');
    }

    $cell_OT_product = array('data' => 'For Other (OT): Describe the Type of Product', 'colspan' => 2,'style' => 'padding: 5px;');
    $cell_empty_colspan = array('data' => kos($hasil['cmf_description_for_other']), 'colspan' => 2,'style' => 'height: 40px;');
    $this->table->add_row($cell_OT_product, $cell_empty_colspan);
    $table_cmf_tag = $this->table->generate();

    $this->table->clear();

    // Table REEF
    $tmpl = array ( 'table_open'  => '<table class="table table-bordered table-reef" id="table_reef_tag" width="750" >' );
    $this->table->set_template($tmpl);
    $cell_first_heading = array('data' => 'Description of Fish Being Exported', 'colspan' => 4,'class' => 'text-center','style' =>'background-color: #ddd;height: 65px;');
    $this->table->set_heading($cell_first_heading);
    $heading_table = array( array( 'data' => '<strong>Product: F (Fresh)/ FR (Frozen)</strong>',
                              'style' => 'background-color:#ddd;padding: 5px;'),
                            array( 'data' => '<strong>Type: RD/GGO/GGT/DRO/DRT/FL/OT*</strong>',
                              'style' => 'background-color:#ddd;padding: 5px;'),
                            array( 'data' => '<strong>Net Weight (Kg)</strong>',
                              'style' => 'background-color:#ddd;padding: 5px;'),
                            array( 'data' => '<strong>Total Number of whole Fish (including RD/GGO/GGT/DRO/DRT)</strong>',
                              'style' => 'background-color:#ddd;padding: 5px;')
                          );
    $this->table->add_row($heading_table);
    // vdump($detail_domestic); 
    $limit_row = 5;
    
    if($list_reef_fish)
    {
      foreach ($list_reef_fish as $key => $item) {
        $product = $item->product_type === 'F' ? 'FRESH' : 'FROZEN';
        $this->table->add_row(
          $product,
          $item->code_tag,
          $item->net_weight,
          $item->total_fish);
        $limit_row--;
      }
    }

    for ($i=0; $i < $limit_row ; $i++) { 
      $this->table->add_row(
                              '-',
                              '-',
                              '-',
                              '-');
    }

    $cell_OT_product = array('data' => 'For Other (OT): Describe the Type of Product', 'colspan' => 2,'style' => 'padding: 5px;');
    $cell_empty_colspan = array('data' => kos($hasil['description_for_other'],'-'), 'colspan' => 2,'style' => 'height: 40px; font-size: 0.8em;text-align: justify;padding: 5px;');
    $this->table->add_row($cell_OT_product, $cell_empty_colspan);
    $table_reef_tag = $this->table->generate();

    $this->table->clear();

 ?>
<html lang="en">
<head>

  <!-- Bootstrap core CSS -->
  
  <!-- Custom styles for this template -->
  <!--  <link href="assets/ecds/css/style.css" rel="stylesheet"> -->


</head>

<body>
  <div class="container" >
   <div class="row">
      <div class="col-lg-12">
        <div class="table-responsive">
          <table class="table table-bordered" width="750" >
          <tr>
            <th rowspan="2" width="15%" style="text-align:center;background-color:black;"><img src= "<?php echo base_url("assets/third_party/images/ccsbt-logo.jpg"); ?>" width="70" height="70"> </th>
            <th rowspan="2" width="20%" style="font-family:arial;color:white;font-size:14px;background-color:black;">Commision for the Conservation of Southern Bluefin Tuna</th>
            <th width="30%" style="text-align: center;font-family:arial;color:white;font-size:16px;background-color:black;">RE-EXPORT/EXPORT AFTER LANDING OF DOMESTIC PRODUCT FORM</th>
            <th width="20%" style="font-family:arial;color:white;font-size:12px;background-color:black;text-align:center">Document Number</th>
            <th rowspan="2" width="15%" style="font-family:arial;color:white;font-size:18px;background-color:black;text-align:center;"><img src="<?php echo base_url("assets/third_party/images/kkp.png"); ?>" width="70" height="70"></th>
          </tr>
          <tr>
          </tr>
          <tr>
            <td style="font-family:arial;color:white;font-size:10px;text-align:center;background-color:black;">Catch Documentation Scheme</td>
            <td style="background-color:white;font-size: 1.4em;"><?php echo kos($hasil['document_number'], 'Temporary:'.$hasil['temp_doc_id']); ?></td>
          </tr>
        </table>
        </div>
      </div>      
    </div>
<div class="dokumen_wrapper">
    <div class="row">
      <div class="col-lg-12">
        <div class="table-responsive">
          <table class="table" style="font-size:10px;margin-bottom:5px;">
            <thead>
              <tr>  
              </tr>
            </thead>
            <tbody>
              <tr>
                <td> <?php if($hasil['type_of_export'] !== 'Export after Landing of Domestic Product') :?>
                          <img src="<?php echo base_url("assets/third_party/images/checkbox.png"); ?>" height="20px">
                     <?php else: ?>
                          <img src="<?php echo base_url("assets/third_party/images/checkbox_kosong.png"); ?>" height="20px">                          
                     <?php endif ?>Re-Export</td>
                <td><img src="<?php echo base_url("assets/third_party/images/panah.png"); ?>"></td>
                <td><?php if($hasil['type_of_export'] === 'Export after Landing of Domestic Product') :?>
                          <img src="<?php echo base_url("assets/third_party/images/checkbox.png"); ?>" height="20px">
                    <?php else: ?>
                          <img src="<?php echo base_url("assets/third_party/images/checkbox_kosong.png"); ?>" height="20px">                          
                    <?php endif ?>Export after Landing of Domestic Product</td>
                <td>(tick only one)</td>
              </tr> 
            </tbody>
          </table>
          <p><strong>Within this form, the term "Export" includes both export and re-exports</strong></p>
          <table class="table" style="font-size:10px;margin-bottom:5px;">
            <thead>
              <tr>  
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><?php if($hasil['type_of_shipment'] === 'FULL') :?>
                          <img src="<?php echo base_url("assets/third_party/images/checkbox.png"); ?>" height="20px">
                     <?php else: ?>
                          <img src="<?php echo base_url("assets/third_party/images/checkbox_kosong.png"); ?>" height="20px">                          
                     <?php endif ?>Full Shipment</td>
                <td><img src="<?php echo base_url("assets/third_party/images/panah.png"); ?>"></td>
                <td><?php if($hasil['type_of_shipment'] !== 'FULL') :?>
                          <img src="<?php echo base_url("assets/third_party/images/checkbox.png"); ?>" height="20px">
                     <?php else: ?>
                          <img src="<?php echo base_url("assets/third_party/images/checkbox_kosong.png"); ?>" height="20px">                          
                     <?php endif ?>Partial Shipment</td>
                <td>(tick only one)</td>
              </tr> 
            </tbody>
          </table>
        </div>
      </div> 
      <div class="col-lg-12">
        <div class="table-responsive">
          <table class="table table-bordered" style="font-size:10px;margin-bottom:5px;">
            <thead>
            </thead>
            <tbody>
              <tr width="50%">
                <td style="background-color: #ddd;text-align: center;padding: 5px;">Form Number of Preceding Document (Catch Monitoring Form, or Re-Export/Export After Landing of Domestic Product Form)</td>
                <td width="50%" style="text-align: center;"><?php echo $hasil['cmf_document_numbers'] ?></td>
              </tr> 
            </tbody>
          </table>
        </div>
      </div>      
    </div>

    <div class="row">
     <!-- <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">EXPORT SECTION</h3>
      </div>
      <div class="panel-body"> -->
      <div class="col-lg-12">
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th colspan="5" style="font-family:arial;color:white;font-size:15px;padding:5px;background-color:black;">
                  EXPORT SECTION
                </th>
              </tr>
              <tr>
               <th rowspan="2" width="25%" style="background-color: #ddd;padding: 5px;">Exporting State/Fishing Entity</th>  
               <th colspan="3" width="75%" style="background-color: #ddd;padding: 5px;text-align: center;">Point of Export</th>
             </tr>
             <tr>
              <th width="25%" style="background-color: #ddd;padding: 5px;">City</th>
              <th width="25%" style="background-color: #ddd;padding: 5px;">State or Province</th>
              <th width="25%" style="background-color: #ddd;padding: 5px;">State/Fishing Entity</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>INDONESIA</td>
              <td style="padding:5px;"><?php echo $hasil['asal_export_city']; ?></td>
              <td style="padding:5px;"><?php echo $hasil['asal_export_propinsi']; ?></td>
              <td style="padding:5px;"><?php echo $hasil['point_export_state_entity'] ?></td>
            </tr>
          </tbody>
        </table>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th width="40%" style="background-color: #ddd;padding: 5px;">Name of Processing Establishment (if applicable)</th>
              <th width="60%" style="background-color: #ddd;padding: 5px;">Address of Processing Establishment (if applicable)</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td style="padding:5px;"><?php echo $hasil['name_processing_establishment']; ?></td>
              <td style="padding:5px;"><?php echo $hasil['address_processing_establishment'] ?></td>
            </tr>
          </tbody>
        </table>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th width="40%" style="background-color: #ddd;padding: 5px;">Catch Tagging Form Document Numbers (if applicable)</th>
              <td width="60%" style="text-align: center;"><?php echo $hasil['ctf_document_numbers']; ?></td>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="row" style="overflow: hidden;">
      <div class="col-lg-12">
      <div style="float: right;width: 48%;height: 100px;margin-right: 2px;">
      <?php echo $table_reef_tag; ?>
      </div>
      <div style="overflow: hidden;width: 50%;height: 100px;">
        <?php echo $table_cmf_tag ?>       
      </div> 
    </div>
  </div>
<div class="row">
      <div class="col-lg-12">
        <table class="table table-bordered" width="50%">
          <thead>
            <tr>
              <th style="background-color: #ddd;padding: 5px;">Destination (State/Fishing Entity</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td style="padding: 5px;"><?php echo $hasil['nama_negara_export']; ?></td>
            </tr>
          </tbody>
        </table>
      </div> 
</div>
<div class="row">
      <div class="col-lg-12">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th colspan="4" style="background-color: #ddd;padding: 5px;">Certification by Exporter: I certify that the above information is complete, true and correct to the best of my knowledge and belief</th>
            </tr>
            <tr>
              <td width="35%" style="background-color: #ddd;padding: 5px;">Name</td>
              <td width="25%" style="background-color: #ddd;padding: 5px;">Signature</td>
              <td width="15%" style="background-color: #ddd;padding: 5px;">Date</td>
              <td width="25%" style="background-color: #ddd;padding: 5px;">License No. / Company Name</td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td style="text-align:center;height: 65px;padding: 5px;"><?php echo strtoupper(kos($hasil['exporter_name'])); ?></td>
              <td style="text-align:center;height: 65px;padding: 5px;"></td>
              <td style="text-align:center;height: 65px;padding: 5px;"><?php echo tgl($hasil['tgl_verifikasi'],'F jS,Y'); ?></td>
              <td style="text-align:center;height: 65px;padding: 5px;"><?php echo strtoupper(kos($hasil['exporter_company_name'])); ?></td>
            </tr>
          </tbody>
        </table>
        <table class="table table-bordered">
          <thead>
          </thead>
          <tbody>
            <tr>
              <th colspan="3" width="75%" >Validation by Authority: I validate that the above information is complete, true and correct to the best of my knowledge and belief</th>
              <td rowspan="4" width="25%"></td>
            </tr>
            <tr>
              <td style="background-color: #ddd;padding: 5px;">Name and Title</td>
              <td colspan="2" style="background-color: #ddd;padding: 5px;">Signature</td>
            </tr>
            <tr>
              <td rowspan="2" style="text-align:center;height: 65px;padding: 5px;">
                <strong><?php echo strtoupper(kos($hasil['authority_name_title'],'-')); ?></strong>
              </td>
              <td colspan="2" style="height: 65px;padding: 5px;"></td>
            </tr>
            <tr>
              <td width="5%" style="background-color: #ddd;padding: 5px;">Date</td>
              <td width="25%" style="text-align:center;padding: 5px;"><?php echo tgl($hasil['tgl_verifikasi'],'F jS,Y'); ?></td>
            </tr>
          </tbody>
        </table>
      </div>
</div>

<div class="row">
  <div class="col-lg-12">
    <table class="table table-bordered">
      <thead>
        <tr>
                <th colspan="5" style="font-family:arial;color:white;font-size:15px;padding:5px;background-color:black;">
                  IMPORT SECTION
                </th>
        </tr>
        <tr>
          <th colspan="3" style="background-color: #ddd;padding: 5px; text-align: center;">Final Point of Import</th>
        </tr>
        <tr>
          <td width="30%" style="background-color: #ddd;padding: 5px;">City</td>
          <td width="35%" style="background-color: #ddd;padding: 5px;">State or Province</td>
          <td width="35%" style="background-color: #ddd;padding: 5px;">State/Fishing Entity</td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="height: 25px;">&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td style="height: 25px;">&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td style="height: 25px;">&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th colspan="4" style="background-color: #ddd;padding: 5px;">Certification by Importer: I certify that the above information is complete, true and correct to the best of my knowledge and belief</th>
        </tr>
        <tr>
          <td width="25%" style="background-color: #ddd;padding: 5px;">Name</td>
          <td width="35%" style="background-color: #ddd;padding: 5px;">Address</td>
          <td width="25%" style="background-color: #ddd;padding: 5px;">Signature</td>
          <td width="15%" style="background-color: #ddd;padding: 5px;">Date</td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="height: 65px;">&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td style="height: 65px;">&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td style="height: 65px;">&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td style="height: 65px;">&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
      </tbody>
    </table>
    <table class="table table-bordered">
      <thead>
      </thead>
      <tbody>
        <tr>
          <td>NOTE: The organization/person which validates the Export section shall verify the copy of the original CCSBT CDS Document. Sucha verified copy of the original CCSBT CDS.
            document must be attached to the Re-export/Export after Landing of Domestic Product (RE) Form. When SBT is Exported, all verified copies of concerned forms must be attached.</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
</body>
</html> 
