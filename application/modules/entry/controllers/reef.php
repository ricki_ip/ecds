<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reef extends MX_Controller {

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   *    http://example.com/index.php/welcome
   *  - or -  
   *    http://example.com/index.php/welcome/index
   *  - or -
   * Since this controller is set as the default controller in 
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see http://codeigniter.com/user_guide/general/urls.html
   */
  private $special_edit_privillege = array(1,2,3);
  private $verify_privillege = array(1,2,3);

  function __construct()
  {
      parent::__construct();
      $this->load->config('menus');
      $this->load->config('db_timeline');
      $this->load->config('globals');
      $this->load->model('mdl_reef_entry');
  }

  public function _generate_doc_number()
  {
    $data['doc_area'] = $this->user->code_area();
    $data['doc_prefix'] = "RE-ID";
    $data['doc_year'] = date("y");

    //get last reef id from table counter
    $get_last_reef_id = $this->mdl_reef_entry->last_reef_id($data['doc_area'], $data['doc_year']);
    $last_reef_id = $get_last_reef_id->last_reef_id;
    $last_reef_id++;
    $reef_doc_id = str_pad($last_reef_id, 4, "0", STR_PAD_LEFT);

    $data['doc_id'] = $reef_doc_id;

    $data['calon_doc_number'] = $data['doc_prefix'].$data['doc_year'].'-'.$data['doc_area'].'-'.$data['doc_id'];

    return $data;
  }
  
  public function index()
  {
    $add_js = array('select2.min.js', 'jquery.dataTables.min.js');
    $add_css = array('select2.css', 'jquery.dataTables.css');


    $template = 'templates/page/v_form';
    $modules = 'entry';
    $views = 'form_entry_reef';
    $labels = 'form_reef';

    $data['submit_form'] = 'entry/reef/input';

    $data['opsi_type_tag'] = Modules::run('vessel/mst_type_tag/list_type_tag_json');
    $data['opsi_gear_code'] = Modules::run('vessel/mst_gear/list_gear_code_json');
    $data['opsi_ccsbt_area'] = Modules::run('vessel/mst_ccsbt_area/list_ccsbt_area_json');
    $data['opsi_pejabat'] = Modules::run('pengaturan/mst_pejabat/list_pejabat_json');
    $data['opsi_processing'] = Modules::run('pengaturan/mst_processing/list_processing_json','processing');
    $data['opsi_perusahaan'] = Modules::run('pengaturan/mst_perusahaan/list_perusahaan_json','perusahaan');
    $data['opsi_negara'] = Modules::run('pengaturan/mst_negara/list_negara_json');
    $data['opsi_kabkota'] = Modules::run('pengaturan/mst_kabupaten_kota/list_kabupaten_kota_json');

     
    echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
  }

  public function input()
  {
    $this->load->model('mdl_cmf_entry');
    $array_input = $this->input->post(NULL, TRUE);

    

    $array_input['id_pengguna_buat'] = $this->user->id_pengguna();    
    $array_input['tanggal_buat'] = date('Y-m-d H:i:s');


    $array_reef = array();
    $array_reef_fish = array();
    
    $array_reef['id_ctf'] = $array_input['id_ctf'];
    $array_reef['id_cmf'] = $array_input['id_cmf'];
    $array_reef['ctf_document_numbers'] = $array_input['ctf_document_numbers'];
    $array_reef['cmf_document_numbers'] = $array_input['cmf_document_numbers'];
    
    // $array_reef['id_tags'] = $array_input['id_tags'];
    $array_reef['id_perusahaan'] = empty($array_input['id_exporter_company']) ? $this->user->id_perusahaan() : $array_input['id_exporter_company'];
    unset($array_input['id_exporter_company']);
    
    $array_reef['name_processing_establishment'] = $array_input['name_processing_establishment'];
    $array_reef['address_processing_establishment'] = $array_input['address_processing_establishment'];

      // Kalau user bukan perusahaan
    if($this->user->level() < 5)
    {
      $array_cmf['id_pejabat'] = $array_input['id_pejabat'];  
      $array_reef['authority_name_title'] = $array_input['authority_name_title'];
      $array_reef['authority_date'] = $array_input['authority_date'];
    }
    $array_reef['export_destination'] = $array_input['export_destination'];
    $array_reef['name_export_destination'] = $array_input['name_export_destination'];
    $array_reef['point_export_city'] = $array_input['point_export_city'];
    $array_reef['point_export_state_entity'] = $array_input['point_export_state_entity'];
    $array_reef['exporter_name'] = $array_input['exporter_name'];
    $array_reef['exporter_company'] = $array_input['exporter_company'];
    $array_reef['exporter_company_name'] = $array_input['exporter_company_name'];
    $array_reef['exporter_company_address'] = $array_input['exporter_company_address'];
    $array_reef['description_for_other'] = $array_input['description_for_other'];
    $array_reef['previous_state_entity'] = $array_input['previous_state_entity'];
    $array_reef['previous_date_landing'] = $array_input['previous_date_landing'];
    $array_reef['previous_date_landing'] = $array_input['previous_date_landing'];
    $array_reef['type_of_shipment'] = $array_input['type_of_shipment'];
    $array_reef['type_of_export'] = $array_input['type_of_export'];
    $array_reef['id_pengguna_buat'] = $array_input['id_pengguna_buat'];
    $array_reef['tanggal_buat'] = $array_input['tanggal_buat'];
    $other_type = isset($array_input['other_type']) ? $array_input['other_type'] : '' ;

    // vdump($array_reef, true);
    //insert_reef
    //get last id
    $id_reef = $this->mdl_reef_entry->input_reef($array_reef);

    
      $index = 0;
      $total_weight = 0;
      foreach ($array_input as $name => $value) {

          if(preg_match('/product_/', $name) && strpos($name, 'destination') === FALSE)
                {
                  $pos = strripos($name, '_')+1;
                    $num = substr($name, $pos);
                    if($array_input[$name] !== ''){
                      array_push( $array_reef_fish, array(
                                      'product_type' => $array_input['product_'.$num],
                                      'id_reef' => $id_reef,
                                      'type_tag' => $array_input['typetag_'.$num],
                                      'net_weight' => $array_input['weight_'.$num],
                                      'total_fish' => $array_input['total_'.$num],
                                      'other_type' => $other_type,
                                      'tanggal_buat' => date('Y-m-d H:i:s'),
                                      'id_pengguna_buat' => 7,
                                      'aktif' => 'Ya'
                                    )
                                );
                      $total_weight += $array_input['weight_'.$num];
                      $index++;
                    }
                }
        }
      // vdump($array_input, true);
      // vdump($array_reef_fish, true);

      //insert batch CMF FISH
      $this->mdl_reef_entry->input_fish($array_reef_fish);
      $this->mdl_cmf_entry->update_cmf_current_weight($array_input['id_cmf'], $total_weight);

    $url = site_url('preview/reef');
    redirect($url);

  }

  public function verifikasi($id_reef)
  {
    if(in_array($this->user->level(), $this->verify_privillege))
    {
      $data = $this->_generate_doc_number();
      $data['id_reef'] = $id_reef;
      $this->mdl_reef_entry->verifikasi_reef($data);

      $this->mdl_reef_entry->update_counter($data['doc_area']);
    }
    $url_redir = site_url('preview/reef/view/'.$id_reef);
    redirect($url_redir);
  }

  public function edit($id_reef)
  {
    $this->load->model('preview/mdl_reef_preview');
    $get_detail_reef = $this->mdl_reef_preview->detail_reef($id_reef);
    // print_r($get_detail_reef);
    $data['detail_reef'] = (array) $get_detail_reef;
    
    $data['reef_fish'] = $this->mdl_reef_preview->detail_reef_fish($id_reef);
    // $data['count_tags'] = $this->mdl_reef_preview->reef_count_tags($id_reef);
    $data['calon_doc_number'] = $this->_generate_doc_number();

    $data['submit_form'] = 'entry/reef/update';

    $data['opsi_type_tag'] = Modules::run('vessel/mst_type_tag/list_type_tag_json');
    $data['opsi_gear_code'] = Modules::run('vessel/mst_gear/list_gear_code_json');
    $data['opsi_ccsbt_area'] = Modules::run('vessel/mst_ccsbt_area/list_ccsbt_area_json');
    $data['opsi_pejabat'] = Modules::run('pengaturan/mst_pejabat/list_pejabat_json');
    $data['opsi_processing'] = Modules::run('pengaturan/mst_processing/list_processing_json','processing');
    $data['opsi_perusahaan'] = Modules::run('pengaturan/mst_perusahaan/list_perusahaan_json','perusahaan');

    $data['opsi_negara'] = Modules::run('pengaturan/mst_negara/list_negara_json');
    $data['opsi_kabkota'] = Modules::run('pengaturan/mst_kabupaten_kota/list_kabupaten_kota_json');

    $add_js = array('select2.min.js', 'jquery.validationEngine.js','jquery.validationEngine-en.js','jquery.dataTables.min.js');
    $add_css = array('select2.css', 'validationEngine.jquery.css','jquery.dataTables.css');
    echo Modules::run('templates/page/v_form', //tipe template
              'entry', //nama module
              'form_edit_reef', //nama file view
              'form_edit_reef', //dari labels.php
              $add_js, //plugin javascript khusus untuk page yang akan di load
              $add_css, //file css khusus untuk page yang akan di load
              $data); //array data yang akan digunakan di file view

  }

  public function update()
  {
    $array_input = $this->input->post(NULL, TRUE);
    // vdump($array_input,true);
    
    $id_reef = $array_input['id_reef'];
    //get last reef id from table counter
    // $get_last_reef_id = $this->mdl_reef_entry->last_reef_id("B", 14);
    // $last_reef_id = $get_last_reef_id->last_reef_id;
    // $last_reef_id++;
    // $reef_doc_id = str_pad($last_reef_id, 4, "0", STR_PAD_LEFT);

    // $array_input['doc_id'] = $reef_doc_id;
    // $array_input['doc_prefix'] = "T-ID";
    // $array_input['doc_year'] = 
    // $array_input['doc_area'] = "B";
    $array_input['id_pengguna_ubah'] = $this->user->id_pengguna();
    $array_input['id_perusahaan'] = empty($array_input['id_exporter_company']) ? $this->user->id_perusahaan() : $array_input['id_exporter_company'];
    unset($array_input['id_exporter_company']);

    // $array_input['date'] = date('Y-m-d H:i:s');

    $array_input['tanggal_ubah'] = date('Y-m-d H:i:s');

    
    $this->mdl_reef_entry->edit($array_input);
    
    $url_redirect = '/entry/reef/edit/'.$id_reef;
    redirect($url_redirect, 'refresh');
  }

  public function delete($id_reef)
  {
      $this->mdl_reef_entry->delete($id_reef);

      $url = base_url('preview/reef/index');
      redirect($url);
  }

  public function check_tag()
  {
    $tag_number = $this->input->get('tagnumber');

    $jumlah_tag = $this->mdl_reef_entry->check_tag($tag_number);

    echo json_encode(array('jumlah_tag'=> $jumlah_tag));

  }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */