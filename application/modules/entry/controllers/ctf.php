<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Ctf extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	private $special_edit_privillege = array(1,2,3);
	private $verify_privillege = array(1,2,3);

	function __construct()
	{
			parent::__construct();
			$this->load->config('menus');
			$this->load->config('db_timeline');
			$this->load->config('globals');
			$this->load->model('mdl_ctf_entry');
	}

	public function _generate_doc_number()
	{
		$data['doc_area'] = $this->user->code_area();
		$data['doc_prefix'] = "T-ID";
		$data['doc_year'] = date("y");

		//get last ctf id from table counter
		$get_last_ctf_id = $this->mdl_ctf_entry->last_ctf_id($data['doc_area'], $data['doc_year']);
		$last_ctf_id = $get_last_ctf_id->last_ctf_id;
		$last_ctf_id++;
		$ctf_doc_id = str_pad($last_ctf_id, 4, "0", STR_PAD_LEFT);

		$data['doc_id'] = $ctf_doc_id;

		$data['calon_doc_number'] = $data['doc_prefix'].$data['doc_year'].'-'.$data['doc_area'].'-'.$data['doc_id'];

		return $data;
	}
	
	public function index()
	{

		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');


		$template = 'templates/page/v_form';
		$modules = 'entry';
		$views = 'form_entry_ctf';
		$labels = 'form_ctf';

		$data['submit_form'] = 'entry/ctf/input';
		$data['opsi_type_tag'] = Modules::run('vessel/mst_type_tag/list_type_tag_json');
		$data['opsi_gear_code'] = Modules::run('vessel/mst_gear/list_gear_code_json');
		$data['opsi_ccsbt_area'] = Modules::run('vessel/mst_ccsbt_area/list_ccsbt_area_json');
		$data['limit_num_tag'] = Modules::run('pengaturan/tag_range/json');
		$data['opsi_tanggal'] = json_encode( genMonth() );
		$data['kuota_spent'] = 0;

		if( $this->user->is_perusahaan() || $this->user->id_perusahaan() !== "0" ):
			$this->load->model('preview/mdl_ctf_preview');
			$get_kuota_spent = $this->mdl_ctf_preview->total_weight_perusahaan($this->user->id_perusahaan() );
			$data['kuota_spent'] = $get_kuota_spent;
			$data['kuota_left'] = $this->user->kuota_awal_perusahaan() - $data['kuota_spent'];
		endif;


		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}

	public function input()
	{
		$array_input = $this->input->post(NULL, TRUE);

		//get last ctf id from table counter
		// $get_last_ctf_id = $this->mdl_ctf_entry->last_ctf_id("B", 14);
		// $last_ctf_id = $get_last_ctf_id->last_ctf_id;
		// $last_ctf_id++;
		// $ctf_doc_id = str_pad($last_ctf_id, 4, "0", STR_PAD_LEFT);

		// $array_input['doc_id'] = $ctf_doc_id;
		// $array_input['doc_prefix'] = "T-ID";
		// $array_input['doc_year'] = 
		// $array_input['doc_area'] = "B";
		$array_input['id_pengguna_buat'] = $this->user->id_pengguna();

		$this->load->model('preview/mdl_ctf_preview');
		$get_kuota_spent = $this->mdl_ctf_preview->total_weight_perusahaan($this->user->id_perusahaan() );
		$array_input['kuota_left'] = $this->user->kuota_awal_perusahaan() - $get_kuota_spent;

		if( $this->user->is_perusahaan() || $this->user->id_perusahaan() !== "0" )
		{
			$array_input['id_perusahaan'] = $this->user->id_perusahaan();
			$array_input['id_asosiasi'] = $this->user->id_asosiasi();
		}
		// $array_input['date'] = date('Y-m-d H:i:s');

		$array_input['tanggal_buat'] = date('Y-m-d H:i:s');

		
		// vdump($array_input,true);
		$this->mdl_ctf_entry->input($array_input);
		
		$url_redirect = site_url('/entry/ctf');
		redirect($url_redirect, 'refresh');

	}

	public function verifikasi($id_ctf)
	{
		if(in_array($this->user->level(), $this->verify_privillege))
		{
			$data = $this->_generate_doc_number();

			$data['id_ctf'] = $id_ctf;
			$this->mdl_ctf_entry->verifikasi_ctf($data);

			$this->mdl_ctf_entry->update_counter($data['doc_area']);
		}
		$url_redir = site_url('preview/ctf/view/'.$id_ctf);
		redirect($url_redir);
	}

	public function edit($id_ctf)
	{

		$this->load->model('preview/mdl_ctf_preview');
		$get_detail_ctf = $this->mdl_ctf_preview->detail_ctf($id_ctf);
		$data['calon_doc_number'] = $this->_generate_doc_number();

		$data['detail_ctf'] = (array) $get_detail_ctf;
		
		$data['list_tags'] = $this->mdl_ctf_preview->ctf_tags($id_ctf);
		$data['count_tags'] = $this->mdl_ctf_preview->ctf_count_tags($id_ctf);

		$data['submit_form'] = 'entry/ctf/update';
		$data['submit_edit_fish'] = 'entry/ctf/update_fish';
		
		if( $this->user->is_perusahaan() || $this->user->id_perusahaan() !== "0" ):
			$this->load->model('preview/mdl_ctf_preview');
			$get_kuota_spent = $this->mdl_ctf_preview->total_weight_perusahaan($this->user->id_perusahaan() );
			$data['kuota_spent'] = $get_kuota_spent;
			$data['kuota_left'] = $this->user->kuota_awal_perusahaan() - $data['kuota_spent'];
		endif;


		$data['opsi_type_tag'] = Modules::run('vessel/mst_type_tag/list_type_tag_json');
		$data['opsi_gear_code'] = Modules::run('vessel/mst_gear/list_gear_code_json');
		$data['opsi_ccsbt_area'] = Modules::run('vessel/mst_ccsbt_area/list_ccsbt_area_json');
		$data['opsi_tanggal'] = json_encode( genMonth() ) ;

	
		$add_js = array('select2.min.js', 'jquery.validationEngine.js','jquery.validationEngine-en.js','jquery.dataTables.min.js');
		$add_css = array('select2.css', 'validationEngine.jquery.css','jquery.dataTables.css');
		echo Modules::run('templates/page/v_form', //tipe template
							'entry', //nama module
							'form_edit_ctf', //nama file view
							'form_edit_ctf', //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view

	}

	public function update_fish()
	{
		$array_input = $this->input->post(NULL, TRUE);
		// vdump($array_input,true);
		
		$id_ctf = $array_input['id_ctf'];
		$id_tag_information = $array_input['id_tag_information'];

		unset($array_input['id_ctf']);
		unset($array_input['id_tag_information']);
		
		$array_input['id_pengguna_ubah'] = $this->user->id_pengguna();

		$array_input['tanggal_ubah'] = date('Y-m-d H:i:s');

		
		$this->mdl_ctf_entry->edit_fish($id_tag_information,$array_input);
		
		$url_redirect = 'entry/ctf/edit/'.$id_ctf;
		redirect($url_redirect, 'refresh');
	}

	public function update()
	{
		$array_input = $this->input->post(NULL, TRUE);
		// vdump($array_input,true);
		
		$id_ctf = $array_input['id_ctf'];

		$this->load->model('preview/mdl_ctf_preview');
		$get_kuota_spent = $this->mdl_ctf_preview->total_weight_perusahaan($this->user->id_perusahaan() );
		$array_input['kuota_left'] = $this->user->kuota_awal_perusahaan() - $get_kuota_spent;

		if( $this->user->is_perusahaan() || $this->user->id_perusahaan() !== "0" )
		{
			$array_input['id_perusahaan'] = $this->user->id_perusahaan();
			$array_input['id_asosiasi'] = $this->user->id_asosiasi();
		}
		//get last ctf id from table counter
		// $get_last_ctf_id = $this->mdl_ctf_entry->last_ctf_id("B", 14);
		// $last_ctf_id = $get_last_ctf_id->last_ctf_id;
		// $last_ctf_id++;
		// $ctf_doc_id = str_pad($last_ctf_id, 4, "0", STR_PAD_LEFT);

		// $array_input['doc_id'] = $ctf_doc_id;
		// $array_input['doc_prefix'] = "T-ID";
		// $array_input['doc_year'] = 
		// $array_input['doc_area'] = "B";
		$array_input['id_pengguna_ubah'] = $this->user->id_pengguna();
		// $array_input['date'] = date('Y-m-d H:i:s');

		$array_input['tanggal_ubah'] = date('Y-m-d H:i:s');

		
		$this->mdl_ctf_entry->edit($array_input);
		
		$url_redirect = site_url('/entry/ctf/edit/'.$id_ctf);
		redirect($url_redirect, 'refresh');
	}

  public function delete($id)
  {
      $this->mdl_ctf_entry->delete($id);

      $url = site_url('preview/ctf');
      redirect($url);
  }

  public function check_tag()
  {
  	$tag_number = $this->input->get('tagnumber');

  	$result = $this->mdl_ctf_entry->check_tag($tag_number);

  	echo json_encode(array('result'=> $result));

  }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */