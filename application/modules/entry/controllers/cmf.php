<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cmf extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	private $special_edit_privillege = array(1,2,3);
	private $verify_privillege = array(1,2,3);

	function __construct()
	{
			parent::__construct();
			$this->load->config('menus');
			$this->load->config('db_timeline');
			$this->load->config('globals');
			$this->load->model('mdl_cmf_entry');
	}

	public function _generate_doc_number()
	{
		$data['doc_area'] = $this->user->code_area();
		$data['doc_prefix'] = "CM-ID";
		$data['doc_year'] = date("y");

		//get last cmf id from table counter
		$get_last_cmf_id = $this->mdl_cmf_entry->last_cmf_id($data['doc_area'], $data['doc_year']);
		$last_cmf_id = $get_last_cmf_id->last_cmf_id;
		$last_cmf_id++;
		$cmf_doc_id = str_pad($last_cmf_id, 4, "0", STR_PAD_LEFT);

		$data['doc_id'] = $cmf_doc_id;

		$data['calon_doc_number'] = $data['doc_prefix'].$data['doc_year'].'-'.$data['doc_area'].'-'.$data['doc_id'];

		return $data;
	}
	
	public function index()
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
	
		$template = 'templates/page/v_form';
		$modules = 'entry';
		$views = 'form_entry_cmf';
		$labels = 'form_entry_cmf';

		$data['opsi_type_tag'] = Modules::run('vessel/mst_type_tag/list_type_tag_json');
		$data['opsi_gear_code'] = Modules::run('vessel/mst_gear/list_gear_code_json');
		$data['opsi_ccsbt_area'] = Modules::run('vessel/mst_ccsbt_area/list_ccsbt_area_json');
		$data['opsi_pejabat'] = Modules::run('pengaturan/mst_pejabat/list_pejabat_json');
		$data['opsi_processing'] = Modules::run('pengaturan/mst_processing/list_processing_json','processing');
		$data['opsi_perusahaan'] = Modules::run('pengaturan/mst_perusahaan/list_perusahaan_json','perusahaan');
		$data['opsi_perusahaan_atli'] = Modules::run('pengaturan/mst_perusahaan/list_perusahaan_atli_json','atli');
		$data['opsi_negara'] = Modules::run('pengaturan/mst_negara/list_negara_json');
		$data['opsi_kabkota'] = Modules::run('pengaturan/mst_kabupaten_kota/list_kabupaten_kota_json');

		$data['submit_form'] = 'entry/cmf/input_cmf';
		// $data['opsi_type_tag'] = Modules::run('vessel/mst_type_tag/list_type_tag_json');
		// $data['opsi_gear_code'] = Modules::run('vessel/mst_gear/list_gear_code_json');
		// $data['opsi_ccsbt_area'] = Modules::run('vessel/mst_ccsbt_area/list_ccsbt_area_json');
 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}

	public function input_cmf()
	{
		$array_input = $this->input->post(NULL, TRUE);
		// vdump($array_input, true);
		if(empty($array_input['product_destination'])):
			echo "Failed to entry CMF. Please set product destination.";
			die;
		endif;
		$array_input['id_pengguna_buat'] = $this->user->id_pengguna();
		$array_input['tanggal_buat'] = date('Y-m-d H:i:s');

		$array_cmf = array();
		$array_cmf_fish = array();
		$array_domestic = array();
		$array_export = array();

		$array_cmf['id_ctf'] = $array_input['id_ctf'];
		$array_cmf['id_tags'] = $array_input['id_tags'];
		$array_cmf['id_perusahaan'] = empty($array_input['id_exporter_company_name']) ? $this->user->id_perusahaan() : $array_input['id_exporter_company_name'];
		$array_cmf['id_processing_establishment'] = $array_input['id_processing_establishment'];
		
		$array_cmf['id_exporter_company_name'] = empty($array_input['id_exporter_company_name']) ? $this->user->id_perusahaan() : $array_input['id_exporter_company_name'];

		$array_cmf['product_destination'] = $array_input['product_destination'];
		$array_cmf['ctf_document_numbers'] = $array_input['ctf_document_numbers'];
		$array_cmf['vessel_name'] = $array_input['vessel_name'];
		$array_cmf['vessel_registration_number'] = $array_input['registration_number'];
		$array_cmf['name_processing_establishment'] = $array_input['name_processing_establishment'];
		$array_cmf['address_processing_establishment'] = $array_input['address_processing_establishment'];
		$array_cmf['description_for_other'] = $array_input['description_for_other'];
		// $array_cmf['authority_date'] = $array_input['authority_date'];
		$array_cmf['id_pengguna_buat'] = $array_input['id_pengguna_buat'];
		$array_cmf['tanggal_buat'] = $array_input['tanggal_buat'];

		unset($array_input['id_processing_establishment']);

		// Kalau user bukan perusahaan
		if($this->user->level() < 5)
        {
			$array_cmf['id_pejabat'] = $array_input['id_pejabat'];  
        }

		// vdump($array_input, true);
		//insert_cmf
		//get last id
		$id_cmf = $this->mdl_cmf_entry->input_cmf($array_cmf);
		// vdump($id_cmf
    	$this->mdl_cmf_entry->seal_ctf_tags($id_cmf);
		
			$index = 0;
      $total_weight = 0;
			
			foreach ($array_input as $name => $value) {

					if(preg_match('/product_/', $name) && strpos($name, 'destination') === FALSE)
            		{
            			$pos = strripos($name, '_')+1;
                		$num = substr($name, $pos);
                		if($array_input[$name] !== ''){
                			array_push( $array_cmf_fish, array(
                										  'product_type' => $array_input['product_'.$num],
                										  'id_cmf' => $id_cmf,
                										  'type_tag' => $array_input['typetag_'.$num],
                										  'month_of_harvest' => $array_input['month_'.$num],
                										  'gear_code' => $array_input['gearcode_'.$num],
                										  'ccsbt_statistical_area' => $array_input['area_'.$num],
                										  'net_weight' => $array_input['weight_'.$num],
                										  'total_fish' => $array_input['total_'.$num],
                										  'tanggal_buat' => date('Y-m-d H:i:s'),
                										  'id_pengguna_buat' => 7,
                										  'aktif' => 'Ya'
                										)
                								);
                			$total_weight += $array_input['weight_'.$num];
                			$index++;
                		}
            		}
				}
			// vdump($array_cmf_fish, true);
			//insert batch CMF FISH
			$this->mdl_cmf_entry->input_fish($array_cmf_fish);
      $this->mdl_cmf_entry->update_cmf_total_weight($id_cmf, $total_weight);
			
		if($array_cmf['product_destination'] === 'domestic')
		{	
			$array_domestic['id_cmf'] = $id_cmf;
			$array_domestic['id_tags'] = $array_input['id_tags'];
			$array_domestic['name'] = $array_input['domestic_name'];
			$array_domestic['address'] = $array_input['domestic_address'];
			$array_domestic['weight'] = $array_input['domestic_weight'];
			$array_domestic['date'] = $array_input['tanggal_buat'];
			$array_domestic['id_pengguna_buat'] = $array_input['id_pengguna_buat'];
			$array_domestic['tanggal_buat'] = $array_input['tanggal_buat'];

			$this->mdl_cmf_entry->inputFinalDomestic($array_domestic);
		}else{
			$array_export['id_cmf'] = $id_cmf;
			$array_export['id_tags'] = $array_input['id_tags'];
			$array_export['destination'] = $array_input['export_destination'];
			$array_export['city'] = $array_input['export_city'];
			$array_export['state_entity'] = $array_input['export_state_entity'];
			$array_export['name'] = $array_input['exporter_name'];
			$array_export['company_name'] = $array_input['id_exporter_company_name'];
			$array_export['date'] = $array_input['tanggal_buat'];
			$array_export['id_pengguna_buat'] = $array_input['id_pengguna_buat'];
			$array_export['tanggal_buat'] = $array_input['tanggal_buat'];

			$this->mdl_cmf_entry->inputFinalExport($array_export);
		}
		// vdump($array_cmf);

		

		// vdump($array_input);


		// if( $this->mdl_ctf->input($array_input) ){
		// 	$url = base_url('ctf/ctf');
		// 	redirect($url);
		// }else{
		// 	$url = base_url('ctf/ctf');
		// 	redirect($url);
		// }

		$url = site_url('preview/cmf');
		redirect($url);
	}

	public function verifikasi($id_cmf, $id_ctf)
	{
		if(in_array($this->user->level(), $this->verify_privillege))
		{
			$data = $this->_generate_doc_number();

			$data['id_cmf'] = $id_cmf;
			$this->mdl_cmf_entry->verifikasi_cmf($data);

			$this->mdl_cmf_entry->update_counter($data['doc_area']);
		}
		$url_redir = site_url('preview/cmf/view/'.$id_cmf);
		redirect($url_redir);
	}

	public function edit($id_cmf)
	{

		$this->load->model('preview/mdl_cmf_preview');

		$data['calon_doc_number'] = $this->_generate_doc_number();
		
		$get_detail_cmf = $this->mdl_cmf_preview->detail_cmf($id_cmf);
		$get_detail_cmf_fish = $this->mdl_cmf_preview->detail_cmf_fish($id_cmf);
		
		// vdump($get_detail_cmf);
		$data['destination'] = $get_detail_cmf->product_destination;
		$data['detail_domestic'] = FALSE;
		$data['detail_export'] = FALSE;
		
		if($data['destination'] === 'domestic')
		{
			$data['detail_domestic'] = (array) $this->mdl_cmf_preview->detail_domestic($id_cmf);
		}else{ //export
			$data['detail_export'] = (array) $this->mdl_cmf_preview->detail_export($id_cmf);
		}
		// print_r($get_detail_ctf);
		$data['detail_cmf'] = (array) $get_detail_cmf;
		$data['detail_cmf_fish'] =  $get_detail_cmf_fish;
		
		$data['opsi_type_tag'] = Modules::run('vessel/mst_type_tag/list_type_tag_json');
		$data['opsi_gear_code'] = Modules::run('vessel/mst_gear/list_gear_code_json');
		$data['opsi_ccsbt_area'] = Modules::run('vessel/mst_ccsbt_area/list_ccsbt_area_json');
		$data['opsi_pejabat'] = Modules::run('pengaturan/mst_pejabat/list_pejabat_json');
		$data['opsi_processing'] = Modules::run('pengaturan/mst_processing/list_processing_json','processing');
		$data['opsi_perusahaan'] = Modules::run('pengaturan/mst_perusahaan/list_perusahaan_json','perusahaan');
		$data['opsi_perusahaan_atli'] = Modules::run('pengaturan/mst_perusahaan/list_perusahaan_atli_json','atli');

		$data['opsi_negara'] = Modules::run('pengaturan/mst_negara/list_negara_json');
		$data['opsi_kabkota'] = Modules::run('pengaturan/mst_kabupaten_kota/list_kabupaten_kota_json');

		//$status_dokumen = $data['detail_pendok']['status_pendok'];
		$data['submit_form'] = 'entry/cmf/update';

		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		echo Modules::run('templates/page/v_form', //tipe template
							'entry', //nama module
							'form_edit_cmf', //nama file view
							'form_edit_cmf', //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view

	}

	public function update()
	{
		$array_input = $this->input->post(NULL, TRUE);
		// vdump($array_input,true);
		
		$id_cmf = $array_input['id_cmf'];

		$array_input['id_pengguna_ubah'] = $this->user->id_pengguna();
		$array_input['id_perusahaan'] = empty($array_input['id_exporter_company_name']) ? $this->user->id_perusahaan() : $array_input['id_exporter_company_name'];
		
		$array_input['tanggal_ubah'] = date('Y-m-d H:i:s');
		
		$this->mdl_cmf_entry->edit($array_input);
		
		$url_redirect = '/entry/cmf/edit/'.$id_cmf;
		redirect($url_redirect, 'refresh');
	}

	public function delete($id_cmf)
    {
        $this->mdl_cmf_entry->delete($id_cmf);

        $url = base_url('preview/cmf/index');
        redirect($url);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */