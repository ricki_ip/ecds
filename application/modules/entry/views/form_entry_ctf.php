<?php 
    
    

    $tmpl = array ( 'table_open'  => '<table id="table_ctf_tag" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('No.','CCSBT Tag Number','Tag Status', 'Type','Weight (Kg)','Fork length (cm)','Gear Code','Area Of Catch', 'Month of Harvest');

    $table_ctf_tag = $this->table->generate();

?>

<?php
    //IF Kuota Habis
    // NO ENTRY
    if($this->user->is_asosiasi() ):
      
?>

<div class="row">
  <div class="col-lg-4 col-lg-offset-4">
    <div class="panel panel-danger">
      <div class="panel-body">
      Maaf user anda tidak diperkenankan untuk entry CTF. 
      </div>
    </div>
  </div>
</div>

<?php
    //IF Kuota Habis
    // NO ENTRY
    elseif($kuota_left < 5):
      
?>

<div class="row">
  <div class="col-lg-4 col-lg-offset-4">
    <div class="panel panel-danger">
      <div class="panel-body">
      Maaf kuota anda sudah habis. Anda tidak bisa melakukan entry CTF.
      </div>
    </div>
  </div>
</div>
  
<?php
//ENDIF Kuota Habis
  //IF Belum set perusahaan
  // NO ENTRY
  elseif($this->user->level() > 3 && $this->user->is_perusahaan_kuota() === FALSE):

?>
<div class="row">
  <div class="col-lg-4 col-lg-offset-4">
    <div class="panel panel-danger">
      <div class="panel-body">
      Maaf user ini belum terhubung ke perusahaan. Harap hubungi administrator untuk menghubungkan user anda ke data perusahaan anda.
      </div>
    </div>
  </div>
</div>

<?php
 //ENDIF Belum set perusahaan
  
  //IF User Admin
  //IF Kuota belum habis
  //IF Sudah set perusahaan 
  else :

?>


<div class="row">
  <div class="col-lg-12">
    <?php echo Modules::run('vessel/pilih_vessel_perusahaan'); ?>   
  </div>
</div>
<?php echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');?>
<?php 
      // if($this->user->is_perusahaan()) : 
      // $attr_id_perusahaan = array( 'name' => 'id_perusahaan',
      //                             'label' => 'Sebagai Perusahaan :',
      //                             'opsi' => Modules::run('pengaturan/mst_perusahaan/list_perusahaan_array')
      //           );
      // echo $this->mkform->input_select2($attr_id_perusahaan);
      // endif; 
?>
<div class="row">
  <div class="col-lg-12">      
  <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Selected Vessel Information : 
              <a id="vessel_name_info" href="-" target="_blank"></a> 
              <small>( Klik vessel name to open vessel data in ccsbt website.)</small>
          </h3>
        </div>
        <div class="panel-body">
          <?php 
          $attr_vessel_name = array(
                                    'name' => $form['vessel_name']['name'],
                                    'label' => $form['vessel_name']['label']
                                  );
          echo $this->mkform->input_text($attr_vessel_name); 

          $attr_vessel_registration_number = array(
                                    'name' => $form['vessel_registration_number']['name'],
                                    'label' => $form['vessel_registration_number']['label']
                                  );
          echo $this->mkform->input_text($attr_vessel_registration_number); 

          $attr_flag_state = array(
                                    'name' => $form['flag_state']['name'],
                                    'label' => $form['flag_state']['label']
                                  );
          echo $this->mkform->input_text($attr_flag_state); 
          ?>
          <hr>
          <p class="text-center"> Certification</p>
          <?php
          $attr_name = array( 'name' => $form['name']['name'],
                                        'label' => $form['name']['label']
                    );
          echo $this->mkform->input_text($attr_name);

          $attr_title = array( 'name' => $form['title']['name'],
                                        'label' => $form['title']['label'],
                                        'value' => $this->user->nama_perusahaan()
                    );
          echo $this->mkform->input_text($attr_title);
          ?>

          <input type="hidden" name="id_vessel_ccsbt" id="id_vessel_ccsbt" class="form-control" value="">
        </div>
    </div>
  </div>
</div>
<?php $kuota_left_percentage = ( $kuota_left / $this->user->kuota_awal_perusahaan() ) * 100; ?>
<div class="row">
 <div class="row">
        <div class="col-lg-6"> 
                <p>
                <small>Perusahaan:</small> <?php echo $this->user->nama_perusahaan();?>. 
                </p>
                <p>
                <small>Kuota:</small> <?php echo angka($kuota_spent);?> / <?php echo angka($this->user->kuota_awal_perusahaan());?><small>Kg</small>
                </p>
                <p>
                  Tersedia: <strong><?php echo angka($kuota_left);  ?></strong> <small>Kg.</small>  .
                </p>
        </div>
        <div class="col-lg-4 col-lg-offset-2">
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="btn-group-horizontal">
                  <button type="button" class="btn btn-info add_row">Tambah Baris</button>
                  <button type="button" class="btn btn-warning del_row">Kurang Baris</button>
                  <button type="button" class="btn btn-default rowcount_info" disabled></button>
                </div>
              </div>
            </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
         <?php echo $table_ctf_tag; ?>
        </div>     
      </div>
      <div class="row">
        <p></p>
      <div class="col-lg-4 col-lg-offset-8">
          <div class="panel panel-default">
            <div class="panel-body">
              <div class="btn-group-horizontal">
                <button type="button" class="btn btn-info add_row">Tambah Baris</button>
                <button type="button" class="btn btn-warning del_row">Kurang Baris</button>
                <button type="button" class="btn btn-default rowcount_info" disabled></button>
              </div>
            </div>
          </div>
        </div>
      </div> 
</div>

<hr>
<div class="row">
  <div class="col-lg-12">
   <button id="submit_ctf" type="SUBMIT" class="btn btn-large btn-block btn-success">SUBMIT CTF</button>
  </div>     
</div>      
<?php echo form_close(); ?>

<!-- Bagian untuk Content pada table, tambah dan kurang bari -->
<div id="template_inputs" class="hide">
  <input type="number" name="tagnumber_0" id="inputTagnumber_0" class="inputTagnumber form-control" value="" pattern="[0-9]{10}" title="">
  <button type="button" id="btnChecktag_0" class="btn btn-checktag btn-primary">CHECK</button>
  <input type="text" name="typetag_0" id="inputTypetag_0" class="opsi_type_tag" value="" required="required" title="" disabled>
  <input type="number" name="weight_0" id="inputWeight_0" class="form-control input_weight" value="" required="required" pattern="[0-9]{10}" title="" disabled>
  <input type="number" name="forklength_0" id="inputForklength_0" class="form-control" value="" required="required" pattern="[0-9]{10}" title="" disabled>
  <input type="text" name="gearcode_0" id="inputGearcode_0" class="opsi_gear_code" value="" required="required" title="" disabled>
  <input type="text" name="area_0" id="inputArea_0" class="opsi_area_ccsbt" value="" required="required" title="" disabled>
  <input type="text" name="month_0" id="inputMonth_0" class="form-control" value="" required="required" title="" disabled>
</div>        
<script>
    var opsi_type_tag = <?php echo $opsi_type_tag; ?>;
    var opsi_gear_code = <?php echo $opsi_gear_code; ?>;
    var opsi_ccsbt_area = <?php echo $opsi_ccsbt_area; ?>;
    var limit_num_tag = <?php echo $limit_num_tag; ?>;
    var kuota_left = <?php echo $kuota_left; ?>;
    var total_weight = 0;

    var url_check_tag = "<?php echo site_url('entry/ctf/check_tag'); ?>";
    var rowCount = 0, // rowCount untuk counter jumlah baris table
        defaultRowCount = 5, // TODO : valuenya nanti dari konfigurasi global
        shortcut_tambah_baris = 'alt+t', // TODO : Calon jadi library javascript
        shortcut_kurang_baris = 'alt+k'; // TODO : Calon jadi library javascript
    

    var init_form = function()
    {
      $(".mkform-text input").prop("readonly", true);
      $("#id_name").prop("readonly", false);
      $("#id_title").prop("readonly", false);
      $("#form_entry").submit(function(e){

          if($("#id_vessel_ccsbt").val() === '')
          {
            e.preventDefault();
            alert('Kapal belum dipilih. Pilih kapal untuk melanjutkan proses.');
            return false;
          }else if(count_kuota() < 1){
            e.preventDefault();
            alert('Data SBT tidak boleh kosong!');
            return false;
          }else{
              if( handler_kuota() === false){
                e.preventDefault();
                return false;
              }else{
                return true;
              }
          }
      });

    }

    var updateRowCount = function()
    {
      $(".rowcount_info").text(rowCount+" baris");
    }
    // Dari module pilih vessel, jadi ga perlu di masukin ke s_func
    var set_data_vessel = function(data)
    {
        var url_detail_ccsbt = link_detail_vessel_ccsbt_web+data.id_vessel_ccsbt;
        $('#id_vessel_name').val(data.vessel_name);
        $('#id_vessel_registration_number').val(data.ccsbt_registration_number);
        $('#id_flag_state').val(data.flag);
        $('#id_vessel_ccsbt').val(data.id_vessel_ccsbt);

        $('#vessel_name_info').text('');
        $('#vessel_name_info').attr('href', url_detail_ccsbt);
        $('#vessel_name_info').text(data.vessel_name);    
    }

    
    
    
    var check_tag_in_table = function(eval_tagnumber, row_id)
    {
      var recent_tags = [];
      var duplicates = 0;
      $(".inputTagnumber").each(function(i,d){ 
          var cur_value = $(this).val();
          var cur_row = i+1;
          var items = { val: cur_value, row: cur_row};            
            recent_tags.push(items);
      });
      if(recent_tags.length === 1)
      {
        duplicates = 0; 
      }else if(recent_tags.length > 1)
      {
        var counter = 1;
          recent_tags.forEach(function(d){
            if(counter !== row_id)
            {
              if( d.val === eval_tagnumber)
              {
                duplicates++;
              } 
            }
            counter++;
          });
      }
      return duplicates;

    }

    var count_kuota = function()
    {
      var count_total_weight = 0;
      $('.input_weight:enabled').each(function(index, elm){
        if( $(elm).val() > 0) 
        {
          var wgt = +$(elm).val();        
          count_total_weight = count_total_weight + wgt;
        }      
      });

      return count_total_weight;
    }

    var handler_kuota = function()
    {
        total_weight = count_kuota();
        if(total_weight > kuota_left)
        {
          alert('Total tangkapan > Sisa kuota!! ('+total_weight+' > '+kuota_left+'). Kuota tidak mencukupi. Anda tidak bisa melanjutkan entry CTF selama berat tangkapan melebihi kuota anda.');
          $("#submit_ctf").attr('disabled', true);
          return false;
        }else{
          $("#submit_ctf").attr('disabled', false);
          return true;
        }
    }

    var listener_kuota = function()
    {
       $("#table_ctf_tag").on("change",".input_weight:enabled", function(){
          handler_kuota();
       });
    }

    var listener_checktag = function()
    {
      $("#table_ctf_tag").on("click",".btn-checktag", function(){
          
          var attr_id = $(this).attr('id'),
              split_id = attr_id.split("_"),
              this_id = split_id[1],
              evaluated_jumlah_tag = 0,
              eval_db = 0,
              eval_table = 0,
              eval_range = true,
              pesan = '',
              curr_state = $(this).text();

          var target_elemen = $("#tagnumber_"+this_id);
          var evaluated_tag = target_elemen.val();     

          if(curr_state === 'OK')
          {
                $(this).removeClass("btn-primary");
                $(this).removeClass("btn-success");
                $(this).removeClass("btn-danger");
                $(this).addClass("btn-primary");
                $(this).text("CHECK");
                $(".fieldrow_"+this_id).prop("disabled", true);
                target_elemen.prop("readonly", false);
                target_elemen.attr("placeholder",target_elemen.val());
                target_elemen.val("");  
                return true;
          }


              
          

              console.log(target_elemen);
          if(evaluated_tag !== "")
          {
            pesan = 'Kesalahan pada Nomor TAG: '+evaluated_tag+' Info: ';

            $(this).removeClass("btn-primary");
            $(this).removeClass("btn-success");
            $(this).removeClass("btn-danger");

            $(this).text('Checking');
            eval_range = (evaluated_tag >= +limit_num_tag.min && evaluated_tag <= +limit_num_tag.max );
            eval_table = +check_tag_in_table(evaluated_tag, +this_id);
            var eval_db = { jumlah_tag: 0, tag_info: {} };
            eval_db.jumlah_tag = function()
                      {
                          var return_jumlah = 0;
                          var jqxhr = $.ajax({
                                async: false,
                                global: false,
                                dataType: "json",
                                url: url_check_tag,
                                data: { tagnumber: evaluated_tag},
                                complete: function(data) {
                                  // console.log(+data.responseJSON.jumlah_tag);
                                  // return_jumlah = +data.responseJSON.jumlah_tag;
                                  // return return_jumlah;
                                }
                              });

                          jqxhr.always(function(data) {
                                console.log(data.result);
                                return_jumlah = +data.result.jumlah_tag;
                                eval_db.tag_info = data.result.tag_info;
                                // return +data.jumlah_tag;
                              });   

                          return return_jumlah;
                      }();

            // eval_db = check_tag_in_db(evaluated_tag);

            evaluated_jumlah_tag =  eval_table + eval_db.jumlah_tag;
            // console.log(eval_db.tag_info);
            console.log(eval_range);

            if(!eval_range)
            {
              pesan += " Penomoran ID Tag diluar batas yang ditentukan. ";
              // return false;
            }

            if(eval_table > 0)
            {
              pesan += " ID Tag telah diinput di tabel entry di halaman ini. "
            }
            if(eval_db.jumlah_tag > 0)
            {
              var info = eval_db.tag_info;
              console.log(info.document_number === null);
              var doc_number = info.document_number === null ? info.temp_doc_id : info.document_number;
              pesan += " ID Tag telah digunakan di form CTF "+doc_number+" oleh kapal "+info.vessel_name+" - "+info.vessel_registration_number+"";
              pesan += " pada ("+info.tanggal_buat+")";
            }
              if(evaluated_jumlah_tag === 0 && eval_range)
              {
                $(this).addClass("btn-success");
                $(this).text("OK");
                $(this).attr("title","Klik untuk mengubah tag number.");
                $(".fieldrow_"+this_id).prop("disabled", false);
                target_elemen.prop("readonly", true);
                return true;
              }else{
                $(this).addClass("btn-danger");
                $(this).text("DUP");
                $(".fieldrow_"+this_id).prop("disabled", true);
                alert(pesan);
                return false;
              }


          }
      });
    }

      /* TODO : Calon jadi library javascript
       * Desc : Fungsi untuk duplikasi elemen html
       * Untuk rubahan attributenya di bikin array
       */
      function cloner(targetClone, cloneName)
      {
          var cloneItem = $(targetClone).clone();
            cloneItem.attr("id", cloneName).attr("name", cloneName);
          return cloneItem[0].outerHTML;
      }

      /* Desc : Listener untuk tambah baris table, 
       * pake fungsi plugin dataTable.fnAddData
       */
      function fnTableAddRow() {
        var tempRowNum = rowCount+1;
            // clone tiap kolom dari templateinputs
            $('#table_ctf_tag').dataTable().fnAddData( [
              tempRowNum+".",
              cloner("#template_inputs #inputTagnumber_0", "tagnumber_"+tempRowNum),
              cloner("#template_inputs #btnChecktag_0", "btnChecktag_"+tempRowNum),
              cloner("#template_inputs #inputTypetag_0", "typetag_"+tempRowNum),
              cloner("#template_inputs #inputWeight_0", "weight_"+tempRowNum),
              cloner("#template_inputs #inputForklength_0", "forklength_"+tempRowNum),
              cloner("#template_inputs #inputGearcode_0", "gearcode_"+tempRowNum), 
              cloner("#template_inputs #inputArea_0", "area_"+tempRowNum),
              cloner("#template_inputs #inputMonth_0", "month_"+tempRowNum)
            ] );

            // init kolom yang jadi select2
            $("#typetag_"+tempRowNum).select2({  width: "100%",
                                              data: opsi_type_tag
                                            });
            $("#gearcode_"+tempRowNum).select2({  width: "100%",
                                              data: opsi_gear_code
                                            });
            $("#area_"+tempRowNum).select2({  width: "100%",
                                              data: opsi_ccsbt_area
                                            });

            $("#typetag_"+tempRowNum).addClass("fieldrow_"+tempRowNum);
            $("#weight_"+tempRowNum).addClass("fieldrow_"+tempRowNum);
            $("#forklength_"+tempRowNum).addClass("fieldrow_"+tempRowNum);
            $("#gearcode_"+tempRowNum).addClass("fieldrow_"+tempRowNum);
            $("#area_"+tempRowNum).addClass("fieldrow_"+tempRowNum);
            $("#month_"+tempRowNum).addClass("fieldrow_"+tempRowNum);

            set_datepicker("#month_"+tempRowNum);
            rowCount++;

            updateRowCount();
      }

      /* Desc : Insialisasi jumlah baris table
       * jumlahnya berdasarkan defaultRowCount
       */
      function initTableRows() {
        while(rowCount < defaultRowCount)
        {
            fnTableAddRow();
        }
      }

      /* Desc : Listener untuk hapus baris table, 
       * pake fungsi plugin dataTable.fnDeleteRow
       * parameter fnDeleteRow itu nomor urut barisnya, 
       * dataTable ngitung nomor urutnya dari 0 (baris pertama = 0)
       * Setelah barisnya hilang, rowCount dikurangi.
       */
      function fnTableDelRow() {
        var lastRow = rowCount-1; 
        $('#table_ctf_tag').dataTable().fnDeleteRow(lastRow);
        if(rowCount !== 0){ rowCount--;}
        updateRowCount();

      }

    var init_data_tables = function()
    {
     $('#table_ctf_tag').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "2.5  %" , "sClass": "text-center"},
                        { "sWidth": "17.5%" , "sClass": "text-center"},
                        { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                      ],
        "bFilter": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": false
      });
      // Pasang tooltip dan listener untuk button tambah dan kurang baris
      $(".add_row").tooltip({ 
        title: "Shortcut : "+shortcut_tambah_baris,
        placement: "bottom"
      })
      .click(fnTableAddRow);

      $(".del_row").tooltip({ 
        title: "Shortcut : "+shortcut_kurang_baris,
        placement: "bottom" 
      })
      .click(fnTableDelRow);

      /* TODO : Calon jadi library javascript
       * Bikin global shortcuts 
       * Keyboard Shortcuts
       */
      $(document).jkey(shortcut_tambah_baris,function(){
        fnTableAddRow();
      });

      $(document).jkey(shortcut_kurang_baris,function(){
        fnTableDelRow();
      });
    }

var set_datepicker = function(elemen_id)
{
     $(elemen_id).datepicker({ 
        dateFormat: 'yy-mm',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
 
        onClose: function(dateText, inst) {  
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val(); 
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val(); 
            $(this).val($.datepicker.formatDate('mm/yy', new Date(year, month, 1)));
        }
    });
 
    $(elemen_id).focus(function () {
        $(".ui-datepicker-calendar").hide();
        $("#ui-datepicker-div").position({
            my: "center top",
            at: "center bottom",
            of: $(this)
        });    
    });
}
   

s_func.push(init_data_tables);
s_func.push(initTableRows);
s_func.push(init_form);
s_func.push(updateRowCount);
s_func.push(listener_checktag);
s_func.push(listener_kuota);

</script>

<?php
  endif;
 //ENDIF YES ENTRY
?>