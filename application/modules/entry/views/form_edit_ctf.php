<?php
    $tmpl = array ( 'table_open'  => '<table id="table_ctf_tag" class="table table-hover">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('No.',
                              'CCSBT Tag Number',
                              'Type','Weight (Kg)',
                              'Fork length (cm)',
                              'Gear Code',
                              'Area Of Catch',
                              'Month of Harvest',
                              'Available',
                              'Edit');

    // var_dump($list_tags);  

    $number = 1;
    $total_last_weight = 0;
    foreach ($list_tags as $key => $items) {
      $link_edit = "<a href='#modal-edit' class='btn btn-info btn-xs btn-edit' data-toggle='modal' 
                      data-idfish='".$items->id_tag_information."'
                      data-tagnumber='".$items->ccsbt_tag_number."'                      
                      data-typetag='".$items->type_tag."'
                      data-weight='".$items->weight."'
                      data-forklength='".$items->fork_length."'
                      data-gearcode='".$items->tti_gear_code."'
                      data-ccsbtarea='".$items->ccsbt_statistical_area."'
                      data-month='".$items->month_of_harvest."'
                      > Edit</a>";  
      $this->table->add_row($number.'. ',
                            tagnumber($items->ccsbt_tag_number),
                            $items->code_tag,
                            $items->weight,
                            $items->fork_length,
                            $items->gear_code,
                            $items->ccsbt_statistical_area,
                            tgl($items->month_of_harvest,'m/Y'),
                            $items->is_free,
                            $link_edit
                            );
      $total_last_weight = $total_last_weight + intval($items->weight);
      $number++;
    }
    $table_ctf_tag = $this->table->generate();
    $this->table->clear();

    $tmpl = array ( 'table_open'  => '<table id="table_ctf_tag_empty" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('No.','CCSBT Tag Number','Tag Status', 'Type','Weight (Kg)','Fork length (cm)','Gear Code','Area Of Catch', 'Month of Harvest');

    $table_ctf_tag_empty = $this->table->generate();
?>  
 <?php
          $hidden_input = array('id_ctf' => $detail_ctf['id_ctf']);
          echo form_open($submit_form,'id="form_entry" class="form-horizontal" role="form"', $hidden_input);

?>

<div class="row">
  <div class="col-lg-12 document_form">
         
         <?php 
          $attr_document_number = array( 'name' => $form['document_number']['name'],
                                        'label' => $form['document_number']['label'],
                                        'value' => kos($detail_ctf['document_number'],'Belum verifikasi. Nomor sementara:'.$detail_ctf['temp_doc_id'])
                    );
          echo $this->mkform->input_text($attr_document_number);

          $attr_vessel_name = array( 'name' => $form['vessel_name']['name'],
                                        'label' => $form['vessel_name']['label'],
                                        'value' => $detail_ctf['vessel_name']
                    );
          echo $this->mkform->input_text($attr_vessel_name);

          $attr_vessel_registration_number = array( 'name' => $form['vessel_registration_number']['name'],
                                        'label' => $form['vessel_registration_number']['label'],
                                        'value' => $detail_ctf['vessel_registration_number']
                    );
          echo $this->mkform->input_text($attr_vessel_registration_number);

          $attr_flag_state = array( 'name' => $form['flag_state']['name'],
                                        'label' => $form['flag_state']['label'],
                                        'value' => $detail_ctf['flag_state']
                    );
          echo $this->mkform->input_text($attr_flag_state);

          $attr_name = array( 'name' => $form['name']['name'],
                                        'label' => $form['name']['label'],
                                        'value' => $detail_ctf['name']
                    );
          echo $this->mkform->input_text($attr_name);

          $attr_title = array( 'name' => $form['title']['name'],
                                        'label' => $form['title']['label'],
                                        'value' => $detail_ctf['title']
                    );
          echo $this->mkform->input_text($attr_title);
          $attr_date = array( 'name' => $form['date']['name'],
                                        'label' => $form['date']['label'],
                                        'value' => tgl($detail_ctf['tanggal_buat'], 'Y-m-d')
                    );
          echo $this->mkform->input_date($attr_date);

         ?>
  </div>
</div>
<?php $kuota_left_percentage = ( $kuota_left / $this->user->kuota_awal_perusahaan() ) * 100; ?>
<div class="row">
        <div class="col-lg-6"> 
                <p>
                <small>Perusahaan:</small> <?php echo $this->user->nama_perusahaan();?>. 
                </p>
                <p>
                <small>Kuota:</small> <?php echo angka($kuota_left_percentage); ?> % (<?php echo angka($kuota_spent);?> / <?php echo angka($this->user->kuota_awal_perusahaan());?><small>Kg</small>) 
                </p>
                <p>
                  Tersedia: <strong><?php echo angka($kuota_left);  ?></strong> <small>Kg.</small>  .
                </p>
        </div>
</div>
<hr>
<ul class="nav nav-tabs">
  <li class="active"><a href="#tagged_fish" data-toggle="tab">Previous Entries</a></li>
  <li><a href="#entry_fish" data-toggle="tab">Add More</a></li>
</ul>

<!-- Tab panes -->
<div class="row">

  <div class="tab-content">
    <div class="tab-pane active" id="tagged_fish">
    <hr>
        <div class="col-lg-12"> 
          <p>Previous Entries:
          <button type="button" class="btn btn-info">Tagged Fishes : <?php echo $count_tags->total_fish; ?></button>
          <button type="button" class="btn btn-info">Total Weight: <?php echo $count_tags->total_weight; ?> Kg</button>
          </p>
        </div>
        <div class="col-lg-12"> 
          <div id="ctf_tags_container">
                  <?php echo $table_ctf_tag; ?>      
          </div>
        </div>
    </div>
    <div class="tab-pane" id="entry_fish">
    <hr>
      <div class="row">
        <div class="col-lg-4 col-lg-offset-8">
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="btn-group-horizontal">
                  <button type="button" class="btn btn-info add_row">Tambah Baris</button>
                  <button type="button" class="btn btn-warning del_row">Kurang Baris</button>
                  <button type="button" class="btn btn-default rowcount_info" disabled></button>
                </div>
              </div>
            </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12"> 
            <div id="empty_ctf_tags_container">
                    <?php echo $table_ctf_tag_empty; ?>      
            </div>
          </div>
      </div>
      <div class="row">
        <p></p>
      <div class="col-lg-4 col-lg-offset-8">
          <div class="panel panel-default">
            <div class="panel-body">
              <div class="btn-group-horizontal">
                <button type="button" class="btn btn-info add_row">Tambah Baris</button>
                <button type="button" class="btn btn-warning del_row">Kurang Baris</button>
                <button type="button" class="btn btn-default rowcount_info" disabled></button>
              </div>
            </div>
          </div>
        </div>
      </div>  
    </div>
  </div>

</div>
<hr>
<div class="row">
  <div class="col-lg-12">
    <button id="save_changes" type="submit" class="btn btn-block btn-primary">SAVE CHANGES</button>
  </div>     
</div>  

</form>
<hr>
<div class="row">
  <div class="col-lg-6">
   <a href="<?php echo site_url('cetak/layout_ctf/'.$detail_ctf['id_ctf']); ?>" class="btn btn-large btn-block btn-warning">CETAK FORM</a>
  </div>
  <?php if ($this->user->level() < 5): ?>
    <div class="col-lg-6">
    <?php if ($detail_ctf['is_verified'] === 'TIDAK'): ?>
      <a href='#modal-id' class="btn btn-large btn-block btn-info" data-toggle="modal" >VERIFIKASI</a>
    <?php else: ?>
    <a href="" class="btn btn-large btn-block btn-success">SUDAH DIVERIFIKASI</a>
    <?php endif ?>
    </div>    
  <?php endif ?>
</div>  


<div class="modal fade" id="modal-id">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Verifikasi dokumen CTF ini?</h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-warning">
            <p>Berikut nomor dokumen CTF yang akan didapatkan jika dokumen ini diverifikasi. </p>
        </div>

        <ul class="list-group">
          <li class="list-group-item">
            <strong>Document Number</strong>: <?php echo $calon_doc_number['calon_doc_number']; ?>
          </li> 
        </ul>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <a href="<?php echo site_url('entry/ctf/verifikasi/'.$detail_ctf['id_ctf']); ?>"
          class="btn btn-primary">Verifikasi</a>
      </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal -->
<!-- Bagian untuk Content pada table, tambah dan kurang bari -->
<div id="template_inputs" class="hide">
  <input type="number" name="tagnumber_0" id="inputTagnumber_0" class="inputTagnumber form-control" value="" pattern="[0-9]{10}" title="">
  <button type="button" id="btnChecktag_0" class="btn btn-checktag btn-primary">CHECK</button>
  <input type="text" name="typetag_0" id="inputTypetag_0" class="opsi_type_tag" value="" required="required" title="" disabled>
  <input type="number" name="weight_0" id="inputWeight_0" class="form-control input_weight" value="" required="required" pattern="[0-9]{10}" title="" disabled>
  <input type="number" name="forklength_0" id="inputForklength_0" class="form-control" value="" required="required" pattern="[0-9]{10}" title="" disabled>
  <input type="text" name="gearcode_0" id="inputGearcode_0" class="opsi_gear_code" value="" required="required" title="" disabled>
  <input type="text" name="area_0" id="inputArea_0" class="opsi_area_ccsbt" value="" required="required" title="" disabled>
  <input type="text" name="month_0" id="inputMonth_0" class="form-control" value="" required="required" title="" disabled>
</div>   

<div class="modal fade" id="modal-edit">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Edit Fish</h4>
      </div>
      <div class="modal-body">
        <?php 
          echo form_open($submit_edit_fish,'id="form_edit_fish" class="form-horizontal" role="form"');
        ?>
      <table class="table table-hover">
               <thead>
                 <tr>
                   <th>Tag Number</th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <td>
                      <input type="number" name="ccsbt_tag_number" id="inputTagnumber" class="inputTagnumber form-control" value="" pattern="[0-9]{10}" title="">
                   </td>
                 </tr>
               </tbody>
      </table>       
      
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Type Tag</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
                <input type="text" name="type_tag" id="inputTypetag" class="opsi_type_tag" value="" required="required" title="" >
            </td>
          </tr>
        </tbody>
      </table>

      <table class="table table-hover">
        <thead>
          <tr>
            <th>Weight</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
                <input type="number" name="weight" id="inputWeight" class="form-control" value="" required="required" pattern="[0-9]{10}" title="" >
            </td>
          </tr>
        </tbody>
      </table>

      <table class="table table-hover">
        <thead>
          <tr>
            <th>Fork Length</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
                <input type="number" name="fork_length" id="inputForklength" class="form-control" value="" required="required" pattern="[0-9]{10}" title="" >
            </td>
          </tr>
        </tbody>
      </table>

      <table class="table table-hover">
        <thead>
          <tr>
            <th>Gear Code</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <input type="text" name="gear_code" id="inputGearcode" class="opsi_gear_code" value="" required="required" title="">
            </td>
          </tr>
        </tbody>
      </table>

      <table class="table table-hover">
        <thead>
          <tr>
            <th>CCSBT Statistical Area</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <input type="text" name="ccsbt_statistical_area" id="inputArea" class="opsi_area_ccsbt" value="" required="required" title="">
            </td>
          </tr>
        </tbody>
      </table>

      <table class="table table-hover">
        <thead>
          <tr>
            <th>Month</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
                <input type="text" name="month_of_harvest" id="inputMonth" value="" required="required" title="">
            </td>
          </tr>
        </tbody>
      </table>

      <input type="hidden" name="id_tag_information" id="inputId_tag_information" class="form-control" value="">
      <input type="hidden" name="id_ctf" id="inputIdctf" class="form-control" value="<?php echo $detail_ctf['id_ctf']; ?>">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </form>

      </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal -->

<script>
  var opsi_type_tag = <?php echo $opsi_type_tag; ?>;
  var opsi_gear_code = <?php echo $opsi_gear_code; ?>;
  var opsi_ccsbt_area = <?php echo $opsi_ccsbt_area; ?>;
  var opsi_tanggal = <?php echo $opsi_tanggal; ?>;

  var kuota_left = <?php echo $kuota_left; ?>;
    var total_weight = <?php echo $total_last_weight; ?>;

  var init_edit_form = function(){
        $(".document_form input").prop("disabled", true);
        $(".document_form select").prop("disabled", true);
        $(".document_form textarea").prop("disabled", true);

        $("#id_name,#id_title,#id_date, #id_date_display").prop("disabled", false);

          $("#modal-edit #inputTypetag").select2({  width: "100%",
                                              data: opsi_type_tag
                                            });
          $("#modal-edit #inputGearcode").select2({  width: "100%",
                                            data: opsi_gear_code
                                          });
          $("#modal-edit #inputArea").select2({  width: "100%",
                                            data: opsi_ccsbt_area
                                          });
          $("#modal-edit #inputMonth").select2({  width: "100%",
                                            data: opsi_tanggal
                                          });

        $("#form_entry").submit(function(e){
        
              if( handler_kuota() === false){
                e.preventDefault();
                return false;
              }else{
                return true;
              }
      });
  }

  var init_data_tables = function(){
     $('#table_ctf_tag').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "2.5  %" , "sClass": "text-center"},
                        { "sWidth": "17.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "12.5%" , "sClass": "text-center"},
                        { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "5%" , "sClass": "text-center"},
                      ],
        "bFilter": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": false
      });

     $('#table_ctf_tag_empty').dataTable( {
       "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        // "aoColumns":  [
        //                 { "sWidth": "2.5  %" , "sClass": "text-center"},
        //                 { "sWidth": "17.5%" , "sClass": "text-center"},
        //                 { "sWidth": "5%" , "sClass": "text-center"},
        //                 { "sWidth": "12.5%" , "sClass": "text-center"},
        //                 { "sWidth": "12.5%" , "sClass": "text-center"},
        //                 { "sWidth": "12.5%" , "sClass": "text-center"},
        //                 { "sWidth": "12.5%" , "sClass": "text-center"},
        //                 { "sWidth": "12.5%" , "sClass": "text-center"},
        //                 { "sWidth": "12.5%" , "sClass": "text-center"},
        //               ],
        "bFilter": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": false
      });
      // Pasang tooltip dan listener untuk button tambah dan kurang baris
      $(".add_row").tooltip({ 
        title: "Shortcut : "+shortcut_tambah_baris,
        placement: "bottom"
      })
      .click(fnTableAddRow);

      $(".del_row").tooltip({ 
        title: "Shortcut : "+shortcut_kurang_baris,
        placement: "bottom" 
      })
      .click(fnTableDelRow);

      /* TODO : Calon jadi library javascript
       * Bikin global shortcuts 
       * Keyboard Shortcuts
       */
      $(document).jkey(shortcut_tambah_baris,function(){
        fnTableAddRow();
      });

      $(document).jkey(shortcut_kurang_baris,function(){
        fnTableDelRow();
      });
    }

    var opsi_type_tag = <?php echo $opsi_type_tag; ?>;
    var opsi_gear_code = <?php echo $opsi_gear_code; ?>;
    var opsi_ccsbt_area = <?php echo $opsi_ccsbt_area; ?>;

    var url_check_tag = "<?php echo site_url('entry/ctf/check_tag'); ?>";
    var rowCount = 0, // rowCount untuk counter jumlah baris table
        defaultRowCount = 5, // TODO : valuenya nanti dari konfigurasi global
        shortcut_tambah_baris = 'alt+t', // TODO : Calon jadi library javascript
        shortcut_kurang_baris = 'alt+k'; // TODO : Calon jadi library javascript
    
    var init_form = function()
    {
      $(".mkform-text input").prop("readonly", true);
      $("#id_name").prop("readonly", false);
      $("#id_title").prop("readonly", false);
    }

    var updateRowCount = function()
    {
      $(".rowcount_info").text(rowCount+" baris");
    }
    // Dari module pilih vessel, jadi ga perlu di masukin ke s_func
    var set_data_vessel = function(data)
    {
        var url_detail_ccsbt = link_detail_vessel_ccsbt_web+data.id_vessel_ccsbt;
        $('#id_vessel_name').val(data.vessel_name);
        $('#id_vessel_registration_number').val(data.ccsbt_registration_number);
        $('#id_flag_state').val(data.flag);
        $('#id_vessel_ccsbt').val(data.id_vessel_ccsbt);

        $('#vessel_name_info').text('');
        $('#vessel_name_info').attr('href', url_detail_ccsbt);
        $('#vessel_name_info').text(data.vessel_name);    
    }

    
    var count_kuota = function()
    {
      var count_total_weight = 0;
      $('.input_weight:enabled').each(function(index, elm){
        if( $(elm).val() > 0) 
        {
          var wgt = +$(elm).val();        
          count_total_weight = count_total_weight + wgt;
        }      
      });

      return count_total_weight;
    }

    var handler_kuota = function()
    {
        var cur_total_weight = total_weight + count_kuota();
        console.log(cur_total_weight);
        if(cur_total_weight > kuota_left)
        {
         alert('Total tangkapan > Sisa kuota!! ('+cur_total_weight+' > '+kuota_left+'). Kuota tidak mencukupi. Anda tidak bisa melanjutkan entry CTF selama berat tangkapan melebihi kuota anda.');
           $("#save_changes").attr('disabled', true);
          return false;
        }else{
          $("#save_changes").attr('disabled', false);
          return true;
        }
    }

    var listener_kuota = function()
    {
       $("#table_ctf_tag_empty").on("change",".input_weight:enabled", function(){
          handler_kuota();
       });
    }

    var check_tag_in_table = function(eval_tagnumber, row_id)
    {
      var recent_tags = [],
          duplicates = 0;
      $(".inputTagnumber").each(function(){ 
          var cur_value = $(this).val();            
            recent_tags.push(cur_value);
      });

      if(recent_tags.length === 1)
      {
        duplicates = 0; 
      }else if(recent_tags.length > 1)
      {
        var counter = 1;
          recent_tags.forEach(function(d){
            if(counter !== row_id)
            {
              if( d === eval_tagnumber)
              {
                duplicates++;
              } 
            }
            counter++;
          });
      }
      // console.log(duplicates);
      return duplicates;

    }

    var listener_checktag = function()
    {
      $("#table_ctf_tag_empty").on("click",".btn-checktag", function(){
               
          var attr_id = $(this).attr('id'),
              split_id = attr_id.split("_"),
              this_id = split_id[1],
              evaluated_jumlah_tag = 0,
              eval_db = 0,
              eval_table = 0,
              pesan = '',
              curr_state = $(this).text();

          var target_elemen = $("#tagnumber_"+this_id);
          var evaluated_tag = target_elemen.val();     

          if(curr_state === 'OK')
          {
                $(this).removeClass("btn-primary");
                $(this).removeClass("btn-success");
                $(this).removeClass("btn-danger");
                $(this).addClass("btn-primary");
                $(this).text("CHECK");
                $(".fieldrow_"+this_id).prop("disabled", true);
                target_elemen.prop("readonly", false);
                target_elemen.attr("placeholder",target_elemen.val());
                target_elemen.val("");  
                return true;
          }
              
          

              console.log(target_elemen);
          if(evaluated_tag !== "")
          {
            pesan = 'Ditemukan tag duplikat! Nomor TAG: '+evaluated_tag+' Info: ';

            $(this).removeClass("btn-primary");
            $(this).removeClass("btn-success");
            $(this).removeClass("btn-danger");

            $(this).text('Checking');
            eval_table = +check_tag_in_table(evaluated_tag, +this_id);
            var eval_db = { jumlah_tag: 0, tag_info: {} };
            eval_db.jumlah_tag = function()
                      {
                          var return_jumlah = 0;
                          var jqxhr = $.ajax({
                                async: false,
                                global: false,
                                dataType: "json",
                                url: url_check_tag,
                                data: { tagnumber: evaluated_tag},
                                complete: function(data) {
                                  // console.log(+data.responseJSON.jumlah_tag);
                                  // return_jumlah = +data.responseJSON.jumlah_tag;
                                  // return return_jumlah;
                                }
                              });

                          jqxhr.always(function(data) {
                                console.log(data.result);
                                return_jumlah = +data.result.jumlah_tag;
                                eval_db.tag_info = data.result.tag_info;
                                // return +data.jumlah_tag;
                              });   

                          return return_jumlah;
                      }();

            // eval_db = check_tag_in_db(evaluated_tag);

            evaluated_jumlah_tag =  eval_table + eval_db.jumlah_tag;
            // console.log(eval_db.tag_info);
            if(eval_table > 0)
            {
              pesan += " ID Tag telah diinput di tabel entry di halaman ini. "
            }
            if(eval_db.jumlah_tag > 0)
            {
              var info = eval_db.tag_info;
              var doc_number = info.document_number === '' ? info.temp_doc_id : info.document_number;
              pesan += " ID Tag telah digunakan di form CTF "+doc_number+" oleh kapal "+info.vessel_name+" - "+info.vessel_registration_number+"";
              pesan += " pada ("+info.tanggal_buat+")";
            }
              if(evaluated_jumlah_tag === 0)
              {
                $(this).addClass("btn-success");
                $(this).text("OK");
                $(this).attr("title","Klik untuk mengubah tag number.");
                $(".fieldrow_"+this_id).prop("disabled", false);
                target_elemen.prop("readonly", true);
                return true;
              }else{
                $(this).addClass("btn-danger");
                $(this).text("DUP");
                $(".fieldrow_"+this_id).prop("disabled", true);
                alert(pesan);
                return false;
              }
          }
      });
    }

      /* TODO : Calon jadi library javascript
       * Desc : Fungsi untuk duplikasi elemen html
       * Untuk rubahan attributenya di bikin array
       */
      function cloner(targetClone, cloneName)
      {
          var cloneItem = $(targetClone).clone();
            cloneItem.attr("id", cloneName).attr("name", cloneName);
          return cloneItem[0].outerHTML;
      }

      /* Desc : Listener untuk tambah baris table, 
       * pake fungsi plugin dataTable.fnAddData
       */
      function fnTableAddRow() {
        var tempRowNum = rowCount+1;
            // clone tiap kolom dari templateinputs
            $('#table_ctf_tag_empty').dataTable().fnAddData( [
              tempRowNum+".",
              cloner("#template_inputs #inputTagnumber_0", "tagnumber_"+tempRowNum),
              cloner("#template_inputs #btnChecktag_0", "btnChecktag_"+tempRowNum),
              cloner("#template_inputs #inputTypetag_0", "typetag_"+tempRowNum),
              cloner("#template_inputs #inputWeight_0", "weight_"+tempRowNum),
              cloner("#template_inputs #inputForklength_0", "forklength_"+tempRowNum),
              cloner("#template_inputs #inputGearcode_0", "gearcode_"+tempRowNum), 
              cloner("#template_inputs #inputArea_0", "area_"+tempRowNum),
              cloner("#template_inputs #inputMonth_0", "month_"+tempRowNum)
            ] );

            // init kolom yang jadi select2
            $("#typetag_"+tempRowNum).select2({  width: "100%",
                                              data: opsi_type_tag
                                            });
            $("#gearcode_"+tempRowNum).select2({  width: "100%",
                                              data: opsi_gear_code
                                            });
            $("#area_"+tempRowNum).select2({  width: "100%",
                                              data: opsi_ccsbt_area
                                            });

            $("#typetag_"+tempRowNum).addClass("fieldrow_"+tempRowNum);
            $("#weight_"+tempRowNum).addClass("fieldrow_"+tempRowNum);
            $("#forklength_"+tempRowNum).addClass("fieldrow_"+tempRowNum);
            $("#gearcode_"+tempRowNum).addClass("fieldrow_"+tempRowNum);
            $("#area_"+tempRowNum).addClass("fieldrow_"+tempRowNum);
            $("#month_"+tempRowNum).addClass("fieldrow_"+tempRowNum);

            set_datepicker("#month_"+tempRowNum);
            rowCount++;

            updateRowCount();
      }

      /* Desc : Insialisasi jumlah baris table
       * jumlahnya berdasarkan defaultRowCount
       */
      function initTableRows() {
        while(rowCount < defaultRowCount)
        {
            fnTableAddRow();
        }
      }

      /* Desc : Listener untuk hapus baris table, 
       * pake fungsi plugin dataTable.fnDeleteRow
       * parameter fnDeleteRow itu nomor urut barisnya, 
       * dataTable ngitung nomor urutnya dari 0 (baris pertama = 0)
       * Setelah barisnya hilang, rowCount dikurangi.
       */
      function fnTableDelRow() {
        var lastRow = rowCount-1; 
        $('#table_ctf_tag_empty').dataTable().fnDeleteRow(lastRow);
        if(rowCount !== 0){ rowCount--;}
        updateRowCount();

      }


var set_datepicker = function(elemen_id)
{
     $(elemen_id).datepicker({ 
        dateFormat: 'yy-mm',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
 
        onClose: function(dateText, inst) {  
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val(); 
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val(); 
            $(this).val($.datepicker.formatDate('mm/yy', new Date(year, month, 1)));
        }
    });
 
    $(elemen_id).focus(function () {
        $(" .ui-datepicker-calendar").hide();
        $(" #ui-datepicker-div").position({
            my: "center top",
            at: "center bottom",
            of: $(this)
        });    
    });
}

var listener_edit_fish = function () {
      $('.btn-edit').click(function(){
        var fishData = $(this).data(); 
        console.log(fishData);

        $("#modal-edit #inputId_tag_information").val(fishData.idfish);        
        $("#modal-edit #inputTagnumber").val(fishData.tagnumber);
        $("#modal-edit #inputWeight").val(fishData.weight);
        $("#modal-edit #inputForklength").val(fishData.forklength);

        $("#modal-edit #inputTypetag").select2("val", fishData.typetag);
        $("#modal-edit #inputGearcode").select2("val", fishData.gearcode);
        $("#modal-edit #inputArea").select2("val", fishData.ccsbtarea);
        $("#modal-edit #inputMonth").select2("val", fishData.month);

      });
}
   
s_func.push(listener_edit_fish);
s_func.push(init_data_tables);
s_func.push(initTableRows);
s_func.push(init_edit_form);
s_func.push(updateRowCount);
s_func.push(listener_checktag);
s_func.push(listener_kuota);


</script>