<?php 
    
    $button_check_all = 'Check All <input type="checkbox" name="check_all" id="btn_checkall" value="checkall">';
    $tmpl = array ( 'table_open'  => '<table id="table_ctf_tag" class="table table-bordered table-hover">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('No.','CCSBT Tag Num.','Type', 'Month of Catch','Gear Code','CCSBT Statistical Area', 'Net Weight (Kg)', $button_check_all);

    $table_ctf_tag = $this->table->generate();

    $this->table->clear();
    $tmpl = array ( 'table_open'  => '<table id="table_cmf_tag" class="table table-bordered table-hover">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('No.','Product', 'Type', 'Month of Catch','Gear Code','CCSBT Statistical Area', 'Net Weight (Kg)','Total Number of whole Fish' );

    $table_cmf_tag = $this->table->generate();

    $this->table->clear();

    
?>
<div class="row">
  <div class="col-lg-12">
    <?php echo Modules::run('preview/ctf/pilih_ctf'); ?>        
  </div>
  <div class="col-lg-12">
    <div id="alert-for-ctf" class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Peringatan!</strong> <div id="pesan-alert"></div>
    </div>
  </div>
</div>
<?php
  echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
?>


<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">CATCH SECTION</h3>
        </div>
        <div class="panel-body">
          <?php 
          // $attr_ctf_document_numbers = array( 'name' => $form['ctf_document_numbers']['name'],
          //                               'label' => $form['ctf_document_numbers']['label']
          //           );
          // echo $this->mkform->input_text($attr_ctf_document_numbers); 
          ?>
          <!-- <hr> -->
          <?php
          // $attr_document_number = array( 'name' => $form['document_number']['name'],
          //                               'label' => $form['document_number']['label']
          //           );
          // echo $this->mkform->input_text($attr_document_number);

          
          $attr_vessel_name = array( 'name' => $form['vessel_name']['name'],
                                        'label' => $form['vessel_name']['label']
                    );
          echo $this->mkform->input_text($attr_vessel_name);

          $attr_registration_number = array( 'name' => $form['registration_number']['name'],
                                        'label' => $form['registration_number']['label']
                    );
          echo $this->mkform->input_text($attr_registration_number);

          $attr_flag_state = array( 'name' => $form['flag_state']['name'],
                                        'label' => $form['flag_state']['label']
                    );
          echo $this->mkform->input_text($attr_flag_state);

          $attr_name_processing_establishment = array( 'name' => 'id_processing_establishment',
                                        'label' => $form['name_processing_establishment']['label'],
                                        'value' => kos($detail_cmf['name_processing_establishment'])
                    );
          echo $this->mkform->input_text($attr_name_processing_establishment);


          ?>
          <input type="hidden" name="name_processing_establishment" id="inputEstablishment_name" class="form-control" value="">
          <input type="hidden" name="address_processing_establishment" id="inputEstablishment_address" class="form-control" value="">  
      
      <?php if ($this->user->level() < 5): ?>
      <hr>
      <p class="text-center"> Validation by Authority </p> 
      <?php
    
      $attr_authority_name_title = array( 'name' => 'id_pejabat',
                                    'label' => $form['authority_name_title']['label'],
                                    'value' => kos($detail_cmf['id_pejabat'])
                );
      echo $this->mkform->input_text($attr_authority_name_title);

      $attr_authority_date = array( 'name' => $form['authority_date']['name'],
                                    'label' => $form['authority_date']['label'],
                                    'value' => kos($detail_cmf['authority_date'])
                );
      echo $this->mkform->input_date($attr_authority_date);

      ?>
      <?php endif ?>

         <input type="hidden" name="authority_name_title" id="inputAuthority_name_title" class="form-control" value="">
         <?php echo $table_ctf_tag; ?>         
         <input type="hidden" name="id_ctf" id="inputId_ctf" class="form-control" value="">
         <input type="hidden" name="ctf_document_numbers" id="inputCtf_document_numbers" class="form-control" value="">
         <hr>
         <button type="button" id="btn_set_cmf" class="btn btn-medium btn-block btn-success" disabled>SET</button>
        </div>
      </div>  
  </div>
  
</div>

<div class="row">
  <div class="col-lg-12">
  <div class="panel panel-default">
      <div class="panel-heading">
      Description of fish
      </div>
      <div class="panel-body">
        <p class="text-right">
         <button type="button" id="btn_reset_cmf" class="btn btn-medium btn-danger" disabled>RESET</button>            
        </p>
         <?php echo $table_cmf_tag; ?>

        <hr>
          <?php           
              $attr_description_for_other = array( 'name' => 'description_for_other',
                                        'label' => 'For Other (OT): Describe the Type Of Product :',
                                        'value' => kos($detail_cmf['description_for_other'])
                    );
              echo $this->mkform->input_text($attr_description_for_other);
            ?>
         <input type="hidden" name="id_tags" id="inputCtf_tags" class="form-control" value="">
      </div>
    </div>
  </div>
</div>
<hr>
<div id="form_destination">
  <div class="row">
    <div class="col-lg-12">
        <p class="text-center lead">Select Product Destination</p>
    </div>
    <div class="col-lg-6">
           <button type="button" id="button_domestic" class="btn btn-large btn-block btn-default">DOMESTIC</button>
    </div>
    <div class="col-lg-6">
           <button type="button" id="button_export" class="btn btn-large btn-block btn-default">EXPORT</button>
    </div>
  </div>
  <hr> 
  <div class="row">
    <div class="col-lg-6">
      <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Landing of Domestic Product</h3>
          </div>
          <div class="panel-body domestic-input">
            
            <?php 
              $attr_domestic_name = array( 'name' => $form['domestic_name']['name'],
                                        'label' => $form['domestic_name']['label']
                    );
              echo $this->mkform->input_text($attr_domestic_name);

              $attr_domestic_address = array( 'name' => $form['domestic_address']['name'],
                                        'label' => $form['domestic_address']['label']
                    );
              echo $this->mkform->input_text($attr_domestic_address);

              $attr_domestic_weight = array( 'name' => $form['domestic_weight']['name'],
                                        'label' => $form['domestic_weight']['label']
                    );
              echo $this->mkform->input_text($attr_domestic_weight);  
            ?>
          </div>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Export</h3>
          </div>
          <div class="panel-body export-input">
             <?php
              $attr_export_destination = array( 'name' => $form['export_destination']['name'],
                                        'label' => $form['export_destination']['label'],
                                        'value' => kos($detail_cmf['export_destination'])
                    );
              echo $this->mkform->input_text($attr_export_destination);
             ?>
            <hr>
            <p class="text-right">Point of Export</p>
            <?php
              $attr_export_city = array( 'name' => $form['export_city']['name'],
                                        'label' => $form['export_city']['label'],
                                        'value' => kos($detail_cmf['export_city'])
                    );
              echo $this->mkform->input_text($attr_export_city);

              $attr_export_state_entity = array( 'name' => $form['export_state_entity']['name'],
                                        'label' => $form['export_state_entity']['label'],
                                        'value' => kos($detail_cmf['export_state_entity'])
                    );
              echo $this->mkform->input_text($attr_export_state_entity);
             ?>
            <hr>
            <p class="text-right">Certification by Exporter</p>
             <?php
              $attr_exporter_name = array( 'name' => $form['exporter_name']['name'],
                                        'label' => $form['exporter_name']['label'],
                                        'value' => kos($detail_cmf['exporter_name'])
                    );
              echo $this->mkform->input_text($attr_exporter_name);

              $attr_exporter_company_name = array( 'name' => $form['exporter_company_name']['name'],
                                        'label' => $form['exporter_company_name']['label'],
                                        'value' => kos($detail_cmf['exporter_company_name'])
                    );
              echo $this->mkform->input_text($attr_exporter_company_name);
             ?>
          </div>
      </div>
    </div>
  </div>         
</div>

<input type="hidden" name="product_destination" id="inputProduct_destination" class="form-control" value="">
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-body">
          <button type="submit" class="btn btn-large btn-block btn-success">SUBMIT CMF</button>
        </div>
    </div>
  </div>
</div>
</form>


<div id="template_inputs" class="hide">
  <input type="text" name="product_0" id="inputProduct_0" class="input-product" value="" required="required" title="">
  <input type="text" name="typetag_0" id="inputTypetag_0" class="form-control type-inputs" value="" required="required" title="">
  <input type="number" name="weight_0" id="inputWeight_0" data-ori-weight="0" class="form-control weight-inputs" value="" required="required" pattern="[0-9]{10}" title="">
  <input type="text" name="gearcode_0" id="inputGearcode_0" class="form-control" value="" required="required" title="">
  <input type="text" name="area_0" id="inputArea_0" class="form-control" value="" required="required" title="">
  <input type="text" name="month_0" id="inputMonth_0" class="form-control" value="" required="required" title="">
  <input type="text" name="total_0" id="inputTotal_0" class="form-control" value="" required="required" title="">
  <input type="checkbox" name="check_0" id="inputCheck_0" value="" class="check_row" data-idtag="">
</div>  

<script>
   
  var opsi_type_tag = <?php echo $opsi_type_tag; ?>;
  var opsi_gear_code = <?php echo $opsi_gear_code; ?>;
  var opsi_ccsbt_area = <?php echo $opsi_ccsbt_area; ?>;
  var opsi_pejabat = <?php echo $opsi_pejabat; ?>;
  var opsi_processing = <?php echo $opsi_processing; ?>;
  var opsi_perusahaan = <?php echo $opsi_perusahaan; ?>;
  var opsi_perusahaan_atli = <?php echo $opsi_perusahaan_atli; ?>;

  var opsi_negara = <?php echo $opsi_negara; ?>;
  var opsi_kabkota = <?php echo $opsi_kabkota; ?>;

  var action_export = '<?php echo 'cmf/input_export'; ?>';
  var action_domestic = '<?php echo 'cmf/input_domestic'; ?>';

  var init_form = function()
  {
    $("#alert-for-ctf").hide();
    $("#id_vessel_name").prop('readonly',true);
    $("#id_registration_number").prop('readonly',true);
    $("#id_flag_state").prop('readonly',true);
    $("#id_domestic_weight").prop('readonly', true);

    $("#id_pejabat").removeClass('form-control');
    $("#id_pejabat").select2({  width: "100%",
                                              data: opsi_pejabat
                                            }).on("change", function(e){
                                                var name_title = e.added.text;

                                                $("#inputAuthority_name_title").val(name_title);
                                                
                                            });

    $('#id_export_state_entity').prop('readonly',true).val('INDONESIA');
    // $("#id_name_processing_establishment").prop('readonly',true);
    // $("#id_address_processing_establishment").prop('readonly',true);

    
    $("#id_export_destination").removeClass('form-control');
    $("#id_export_destination").select2({  width: "100%",
                                              data: opsi_negara
                                        });

    $("#id_export_city").removeClass('form-control');
    $("#id_export_city").select2({  width: "100%",
                                              data: opsi_kabkota
                                        });
    $("#id_export_city").select2('val','271');

    

    $("#id_processing_establishment").removeClass('form-control');
    $("#id_processing_establishment").select2({  width: "100%",
                                              data: opsi_processing
                                            }).on("change", function(e){
                                                var establishment = { name: '', address: ''};
                                                var text = e.added.text;
                                                var split_text = text.split('|');
                                                
                                                establishment.name = split_text[0];
                                                establishment.address = split_text[1];

                                                input_establishment(establishment);
                                                
                                            });
    $("#id_exporter_company_name").removeClass('form-control');
    if(user_data.is_artisenal === true){
      $("#id_exporter_company_name").select2({  width: "100%", data: opsi_perusahaan_atli}); 
    }else{
      $("#id_exporter_company_name").select2({  width: "100%", data: opsi_perusahaan});       
    }
    if(user_data.id_perusahaan !== '0')
    { 
      // if(user_data.is_artisenal === true){
      //   $("#id_exporter_company_name").select2('readonly', false );    
      // }else{
      //   $("#id_exporter_company_name").select2('readonly', true );  
      // }

      $("#id_exporter_company_name").select2('val', user_data.id_exporter_company_name ); 
    }
    $("#table_cmf_tag").on("change", ".type-inputs", function(){ 
        var row_num = $(this).attr("id").split("_")[1];
        var typetag = $(this).select2("data").text;
        var weight_data = $("#weight_"+row_num).data();
 
        // console.log(typetag === "OT");
        // console.log("#weight_"+row_num);

        if(typetag === "GGO")
        {
          if(weight_data.oriType === "GGO")
          {
            $("#weight_"+row_num).val(weight_data.oriWeight);
            $("#weight_"+row_num).prop("readonly", true);
          }else{
            $("#weight_"+row_num).prop("readonly", false);                                  
          }                                                             
        }else{          
          $("#weight_"+row_num).prop("readonly", false);                      
        }

    });                                        
 
  }

  var input_establishment = function(data_perusahaan)
  {
      $("#inputEstablishment_name").val(data_perusahaan.name);
      $("#inputEstablishment_address").val(data_perusahaan.address);

      $("#id_domestic_name").val(data_perusahaan.name);
      $("#id_domestic_address").val(data_perusahaan.address);
  }

  var toggle_destination_form = function()
  {
      var destination = $("#inputProduct_destination").val();

      switch (destination) {
        case "domestic":
          $("#button_domestic").removeClass('btn-default').addClass('btn-success');
          $("#button_export").removeClass('btn-success').addClass('btn-default');
          disable_domestic(false);
          disable_export(true);
          break;
        case "export":
          $("#button_domestic").removeClass('btn-success').addClass('btn-default');
          $("#button_export").removeClass('btn-default').addClass('btn-success');
          disable_domestic(true);          
          disable_export(false);
          break;
      }

      // console.log(destination);
  }

  var disable_domestic = function(state)
  {
      $("#id_domestic_name").prop('disabled', state);
      $("#id_domestic_address").prop('disabled', state);
      $("#id_domestic_weight").prop('disabled', state);
  }

  var disable_export = function(state)
  { 
      var reverse_state = state === true ? false : true;
      $('#id_export_state_entity').prop('disabled', state);
      $("#id_exporter_name").prop('disabled', state);

      $("#id_export_destination").select2('enable', reverse_state);
      $("#id_export_city").select2('enable', reverse_state);
      $("#id_exporter_company_name").select2('enable', reverse_state);
  }

  var listener_destination = function()
  {
    $("#button_domestic").click(function(e){
      // $("#form_entry").attr('action',action_domestic);
      //Validation here
      var total_weight = 0;
      $('#table_cmf_tag .weight-inputs').each(function(){ var weight = +$(this).val(); total_weight = total_weight + weight;});
      $("#id_domestic_weight").val(total_weight);
      $("#inputProduct_destination").val('domestic');
      toggle_destination_form();
    });

    $("#button_export").click(function(e){
      // $("#form_entry").attr('action',action_export);
      $("#inputProduct_destination").val('export');
      toggle_destination_form();

      //Validation here
    });


  }
  
  var url_json_tags = "<?php echo site_url('preview/ctf/json_ctf_tags'); ?>";
  var get_json_tags = function(id_ctf)
  {
    var return_jumlah = 0;
    var jqxhr = $.ajax({
          async: false,
          global: false,
          dataType: "json",
          url: url_json_tags,
          data: { id_ctf: id_ctf},
          success: function(data) {
            set_table_ctf(data.result);
          }
        });

    jqxhr.always(function(data) {
          console.log(data);
        });   

    // return return_jumlah;
  }

  var url_json_tags_all = "<?php echo site_url('preview/ctf/json_ctf_tags_all'); ?>";
  var get_json_tags_all = function(id_tags)
  {
    var return_jumlah = 0;
    var jqxhr = $.ajax({
          async: false,
          global: false,
          dataType: "json",
          url: url_json_tags_all,
          data: { id_tags: id_tags, sealed: 'true'},
          success: function(data) {
            set_table_cmf(data.result);
          }
        });

    jqxhr.always(function(data) {
          console.log(data);
        });   

    // return return_jumlah;
  }
  var set_table_ctf = function(results)
  {
      if(results)
      {
        $("#alert-for-ctf").hide();                
        results.forEach(function(d){
          fnTableAddCTF(d);
        });
      }else{
        $("#pesan-alert").html('').html('Semua tag ikan di dokumen CTF ini sudah terpakai.');
        $("#alert-for-ctf").show();
      }
  }

  var set_table_cmf = function(results)
  {
    if(results)
    {
      results.forEach(function(d){
        fnTableAddCMF(d);
      });
    }
  }

  var set_data_ctf = function(data)
  {
    fnClearThisTable('#table_cmf_tag', 'cmf');
    fnClearThisTable('#table_ctf_tag', 'ctf');
    $('#btn_checkall').prop('checked', false);

    $("#id_vessel_name").val(data.vessel_name);
    $("#id_registration_number").val(data.ccsbt_registration_number);
    $("#id_flag_state").val(data.flag_state);

    get_json_tags(data.id_ctf);

    $("#btn_set_cmf").prop('disabled', false);
    $("#btn_reset_cmf").prop('disabled', false);
    $("#inputId_ctf").val(data.id_ctf);
    $("#inputCtf_document_numbers").val(data.document_number);
  }

  var listener_set_to_cmf = function()
  {
    $("#btn_set_cmf").on('click', function(){
        var arrTags = [];
        var selectedTags = '';
      $('#table_ctf_tag .check_row').each(function(){
        var dataIdtag = $(this).data('idtag');
              if( $(this).is(':checked') && !$(this).is(':disabled') )
              {
                arrTags.push(dataIdtag);
                $(this).prop('disabled', true);
              }
      });
      currentTags = arrTags.join(',');
      selectedTags = get_selected_tags();
      // console.log(selectedTags);

      get_json_tags_all(currentTags);
      $("#inputCtf_tags").val(selectedTags);      
    });
  }

  var listener_reset_table_cmf = function()
  {

    $("#btn_reset_cmf").on('click', function(){

          $('#table_ctf_tag .check_row').each(function(){
                  
                  if( $(this).is(':checked') )
                  {
                    $(this).prop('checked', false);
                  }

                  if( $(this).is(':disabled') )
                  {
                    $(this).prop('disabled', false);
                  }
          });

          fnClearThisTable('#table_cmf_tag', 'cmf');

          $("#inputCtf_tags").val(''); 

    });

  }

  var get_selected_tags = function() 
  {
      var arrTags = [];
      var selectedTags = '';

      var arrTags = [];
      var selectedTags = '';
      $('#table_ctf_tag .check_row').each(function(){
        var dataIdtag = $(this).data('idtag');
              if( $(this).is(':checked') )
              {
                arrTags.push(dataIdtag);
                $(this).prop('disabled', true);
              }
      });
      selectedTags = arrTags.join(',');

      return selectedTags;
  }

  var listener_btn_checkall = function()
  {
    $("#table_ctf_tag").on('click','#btn_checkall',function(){
        $('.check_row').each(function(){
              if( $(this).is(':checked') )
              {
                $(this).prop('checked', false);
              }else{
                $(this).prop('checked', true);
              }
        });
    });
  }
  var rowCount = { ctf: 0, cmf: 0 };
  var opsi_product = [{ id: "F", text: "FRESH"}, { id: "FR", text: "FROZEN"}];
  var init_data_tables = function()
  {
      $('#table_ctf_tag').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "15%" , "sClass": "text-center"},
                        { "sWidth": "20%" , "sClass": "text-center"},
                        { "sWidth": "15%" , "sClass": "text-center"},
                        { "sWidth": "10%" , "sClass": "text-center"},
                        { "sWidth": "15%" , "sClass": "text-center"},
                        { "sWidth": "15%" , "sClass": "text-center"},
                      ],
        "bFilter": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": false
      });

       $('#table_cmf_tag').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "15%" , "sClass": "text-center"},
                        { "sWidth": "20%" , "sClass": "text-center"},
                        { "sWidth": "15%" , "sClass": "text-center"},
                        { "sWidth": "10%" , "sClass": "text-center"},
                        { "sWidth": "10%" , "sClass": "text-center"},
                        { "sWidth": "15%" , "sClass": "text-center"},
                        { "sWidth": "10%" , "sClass": "text-center"},
                      ],
        "bFilter": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": false
      });
      
    }
    /* TODO : Calon jadi library javascript
       * Desc : Fungsi untuk duplikasi elemen html
       * Untuk rubahan attributenya di bikin array
       */
      function cloner(targetClone, cloneName, placeholder)
      {
          var cloneItem = $(targetClone).clone();
            cloneItem.attr("id", cloneName).attr("name", cloneName).attr("placeholder",placeholder);            
          return cloneItem[0].outerHTML;
      }

      /* Desc : Listener untuk tambah baris table, 
       * pake fungsi plugin dataTable.fnAddData
       */
      function fnTableAddCTF(data) {
        var tempRowNum = rowCount.ctf+1;
            // clone tiap kolom dari templateinputs
            $('#table_ctf_tag').dataTable().fnAddData( [
              tempRowNum+".",
              "ID"+data.ccsbt_tag_number,
              data.code_tag,
              data.fmt_month_catch,
              data.gear_code, 
              data.ccsbt_statistical_area,
              data.weight,
              cloner("#template_inputs #inputCheck_0", "CTFcheck_"+tempRowNum, ''),
            ] );

            // $("#CTFtypetag_"+tempRowNum).prop('readonly',true);
            // $("#CTFweight_"+tempRowNum).prop('readonly',true);
            // $("#CTFgearcode_"+tempRowNum).prop('readonly',true);
            // $("#CTFarea_"+tempRowNum).prop('readonly',true);
            // $("#CTFmonth_"+tempRowNum).prop('readonly',true);
            

            $("#CTFcheck_"+tempRowNum).data('idtag', data.id_tag_information);
         
            rowCount.ctf++;

            updateRowCount();
      }

      function fnTableAddCMF(data) {
        var tempRowNum = rowCount.cmf+1;
            // clone tiap kolom dari templateinputs
            $('#table_cmf_tag').dataTable().fnAddData( [
              tempRowNum+".",
              cloner("#template_inputs #inputProduct_0", "product_"+tempRowNum,''),
              cloner("#template_inputs #inputTypetag_0", "typetag_"+tempRowNum,data.code_tag),
              cloner("#template_inputs #inputMonth_0", "month_"+tempRowNum,data.month_of_harvest),
              cloner("#template_inputs #inputGearcode_0", "gearcode_"+tempRowNum,data.gear_code), 
              cloner("#template_inputs #inputArea_0", "area_"+tempRowNum, data.ccsbt_statistical_area),
              cloner("#template_inputs #inputWeight_0", "weight_"+tempRowNum, data.total_weight),
              cloner("#template_inputs #inputTotal_0", "total_"+tempRowNum, data.total_fish),
            ] );


            $("#typetag_"+tempRowNum).prop('readonly',false).removeClass('form-control');
            $("#gearcode_"+tempRowNum).prop('readonly',true).removeClass('form-control');
            $("#area_"+tempRowNum).prop('readonly',true).removeClass('form-control');

            $("#weight_"+tempRowNum).prop('readonly',true);
            $("#month_"+tempRowNum).prop('readonly',true);
            $("#total_"+tempRowNum).prop('readonly',true);

            $("#product_"+tempRowNum).select2({  width: "100%",
                                              data: opsi_product
                                            });
            $("#typetag_"+tempRowNum).select2({  width: "100%",
                                              data: opsi_type_tag
                                            });


            $("#gearcode_"+tempRowNum).select2({  width: "100%",
                                              data: opsi_gear_code
                                            });


            $("#area_"+tempRowNum).select2({  width: "100%",
                                              data: opsi_ccsbt_area
                                            });

            $("#product_"+tempRowNum).select2('val', 'F');
            $("#gearcode_"+tempRowNum).select2('val', data.tti_gear_code);
            $("#typetag_"+tempRowNum).select2('val', data.type_tag);
            $("#area_"+tempRowNum).select2('val', data.ccsbt_statistical_area);
            $("#weight_"+tempRowNum).val(data.total_weight);
            
                          
            $("#weight_"+tempRowNum).data("oriWeight", data.total_weight);
            $("#weight_"+tempRowNum).data("oriType", data.code_tag);
            
            
            $("#month_"+tempRowNum).val(data.month_of_harvest);
            $("#total_"+tempRowNum).val(data.total_fish);

            rowCount.cmf++;

            updateRowCount();
      }

      function fnTableDelRow(tableSelector, counterIndex) {
        var lastRow = rowCount[counterIndex]-1; 
        $(tableSelector).dataTable().fnDeleteRow(lastRow);
        if(rowCount[counterIndex] !== 0){ rowCount[counterIndex]--;}
        updateRowCount();

      }

    function fnClearThisTable(tableSelector, counterIndex ){
      $(tableSelector).dataTable().fnClearTable(); 
      rowCount[counterIndex] = 0;
    }
    var updateRowCount = function()
    {
      //$("#rowcount_info").text(rowCount.ctf+" baris");
    }
  
  s_func.push(init_form);
  s_func.push(listener_destination);
  s_func.push(listener_btn_checkall);
  s_func.push(listener_set_to_cmf);
  s_func.push(listener_reset_table_cmf);
  s_func.push(init_data_tables);


</script>