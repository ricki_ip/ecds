<?php 
    
    $button_check_all = 'Check All <input type="checkbox" name="check_all" id="btn_checkall" value="checkall">';
    $tmpl = array ( 'table_open'  => '<table id="table_reef_fish" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('No.','Product', 'Type', 'Weight (Kg)','Total Number of whole Fish');

    $table_reef_fish = $this->table->generate();

    $this->table->clear();
    $tmpl = array ( 'table_open'  => '<table id="table_cmf_tag" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('No.','Product', 'Type', 'Weight (Kg)','Total Number of whole Fish' );

    $table_cmf_tag = $this->table->generate();

    $this->table->clear();

    
?>
<div class="row">
  <div class="col-lg-12">
    <?php echo Modules::run('preview/cmf/pilih_cmf'); ?>      
  </div>
</div>
<?php
  echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
?>


<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">EXPORT SECTION</h3>
        </div>
        <div class="panel-body">
            <?php
              $array_opsi_export_type = array('Export after Landing of Domestic Product' => 'Export after Landing of Domestic Product',
                                              'Re-Export' => 'Re-Export');
              $attr_type_of_export = array( 'name' => $form['type_of_export']['name'],
                                        'label' => $form['type_of_export']['label'],
                                        'opsi' => $array_opsi_export_type,
                                        'value' => kos($detail_reef['type_of_export'])
                    );
              echo $this->mkform->input_select($attr_type_of_export);

              $attr_export_destination = array( 'name' => $form['export_destination']['name'],
                                        'label' => $form['export_destination']['label'],
                                        'value' => kos($detail_reef['export_destination'])
                    );
              echo $this->mkform->input_text($attr_export_destination);
             ?>
            <input type="hidden" name="name_export_destination" id="inputProduct_destination" class="form-control" value="">
            <hr>
            <p class="text-center">Point of Export</p>
            <?php
              $attr_point_export_city = array( 'name' => $form['point_export_city']['name'],
                                        'label' => $form['point_export_city']['label'],
                                        'value' => kos($detail_reef['point_export_city'])
                    );
              echo $this->mkform->input_text($attr_point_export_city);

              // $attr_point_export_province = array( 'name' => $form['point_export_province']['name'],
              //                           'label' => $form['point_export_province']['label'],
              //                           'value' => kos($detail_reef['point_export_province'])
              //       );
              // echo $this->mkform->input_text($attr_point_export_province);

              $attr_point_export_state_entity = array( 'name' => $form['point_export_state_entity']['name'],
                                        'label' => $form['point_export_state_entity']['label'],
                                        'value' => kos($detail_reef['point_export_state_entity'])
                    );
              echo $this->mkform->input_text($attr_point_export_state_entity);
             ?>
            <hr>
            <p class="text-center">Certification by Exporter</p>
             <?php
              $attr_exporter_name = array( 'name' => $form['exporter_name']['name'],
                                        'label' => $form['exporter_name']['label'],
                                        'value' => kos($detail_reef['exporter_name'])
                    );
              echo $this->mkform->input_text($attr_exporter_name);

              $attr_exporter_company = array( 'name' => 'id_exporter_company',
                                        'label' => $form['exporter_company']['label'],
                                        'value' => kos($detail_reef['exporter_company'])
                    );
              echo $this->mkform->input_text($attr_exporter_company);
             ?>
          <input type="hidden" name="exporter_company_name" id="inputExporter_company_name" class="form-control" value="">
          <input type="hidden" name="exporter_company_address" id="inputExporter_company_address" class="form-control" value="">
         <?php if ($this->user->level() < 5): ?>
          <hr>
          <p class="text-center"> Validation by Authority </p> 
          <?php
          if($this->user->level() < 5):
          $attr_authority_name_title = array( 'name' => 'id_pejabat',
                                        'label' => $form['authority_name_title']['label'],
                                        'value' => kos($detail_cmf['id_pejabat'])
                    );
          echo $this->mkform->input_text($attr_authority_name_title);

          $attr_authority_date = array( 'name' => $form['authority_date']['name'],
                                        'label' => $form['authority_date']['label'],
                                        'value' => kos($detail_cmf['authority_date'])
                    );
          echo $this->mkform->input_date($attr_authority_date);
          endif;
          ?>
        <?php endif ?>
         <input type="hidden" name="authority_name_title" id="inputAuthority_name_title" class="form-control" value="">
         <?php //echo $table_reef_fish; ?>
         <input type="hidden" name="id_ctf" id="inputId_ctf" class="form-control" value="">
         <input type="hidden" name="id_cmf" id="inputId_cmf" class="form-control" value="">
         <input type="hidden" name="ctf_document_numbers" id="inputCtf_document_numbers" class="form-control" value="">
         <input type="hidden" name="cmf_document_numbers" id="inputCmf_document_numbers" class="form-control" value="">
         <input type="hidden" name="type_of_shipment" id="inputShipment_type" class="form-control" value="FULL">
         <hr>
        </div>
      </div>  
  </div>
</div>
<hr>
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
        Description of fish from previous CDS Document
        </div>
        <div class="panel-body">
        <?php 
              $attr_name_processing_establishment = array( 'name' => 'id_processing_establishment',
                                        'label' => $form['name_processing_establishment']['label'],
                                        'value' => kos($detail_reef['name_processing_establishment'])
                    );
              echo $this->mkform->input_text($attr_name_processing_establishment);
          
              $attr_previous_state_entity = array( 'name' => $form['previous_state_entity']['name'],
                                        'label' => $form['previous_state_entity']['label'],
                                        'value' => kos($detail_reef['previous_state_entity'])
                    );
              echo $this->mkform->input_text($attr_previous_state_entity);

              $attr_previous_date_landing = array( 'name' => $form['previous_date_landing']['name'],
                                        'label' => $form['previous_date_landing']['label'],
                                        'value' => kos($detail_reef['previous_date_landing'])
                    );
              echo $this->mkform->input_text($attr_previous_date_landing);
        ?>
        <input type="hidden" name="name_processing_establishment" id="inputEstablishment_name" class="form-control" value="">
        <input type="hidden" name="address_processing_establishment" id="inputEstablishment_address" class="form-control" value="">  
          
        <?php echo $table_cmf_tag; ?>
        </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
        <p class="text-center lead">Select Shipment</p>
  </div>
  <div class="col-lg-6">
    <button type="button" id="btn_set_reef_full" class="btn btn-medium btn-block btn-success" disabled>FULL SHIPMENT</button>           
  </div>
  <div class="col-lg-6">
    <button type="button" id="btn_set_reef_part" class="btn btn-medium btn-block btn-default" disabled>PARTIAL SHIPMENT</button>
  </div>
  

</div>
<hr>
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
        Description of fish being exported
        </div>
        <div class="panel-body">   
          <div id="ReefTableControl" class="panel panel-default">
            <div class="panel-body">
              <p class="pull-left">
                Info partial shipment here..
              </p>
              <div class="btn-group-horizontal pull-right">
                <button type="button" id="add_row" class="btn btn-info">Tambah Baris</button>
                <button type="button" id="del_row" class="btn btn-warning">Kurang Baris</button>
                <button type="button" id="rowcount_info" class="btn btn-default" disabled></button>
              </div>
            </div>
          </div>       
           <?php echo $table_reef_fish; ?>
           <hr>
            <?php           
              $attr_description_for_other = array( 'name' => 'description_for_other',
                                        'label' => 'For Other (OT): Describe the Type Of Product :',
                                        'value' => kos($detail_reef['description_for_other'])
                    );
              echo $this->mkform->input_text($attr_description_for_other);
            ?>
           <!-- <input type="hidden" name="id_tags" id="inputCtf_tags" class="form-control" value=""> -->
        </div>
    </div>
  </div>
</div>

<hr>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-body">
          <button type="submit" class="btn btn-large btn-block btn-success">SUBMIT REEF</button>
        </div>
    </div>
  </div>
</div>
</form>


<div id="template_inputs" class="hide">
  <input type="text" name="product_0" id="inputProduct_0" class="input-product" value="" required="required" title="">
  <input type="text" name="typetag_0" id="inputTypetag_0" class="form-control type-inputs" value="" required="required" title="">
  <input type="number" name="weight_0" id="inputWeight_0" class="inputWeight form-control weight-inputs" value="" required="required" pattern="[0-9]{10}" title="">
  <input type="text" name="gearcode_0" id="inputGearcode_0" class="form-control" value="" required="required" title="">
  <input type="text" name="area_0" id="inputArea_0" class="form-control" value="" required="required" title="">
  <input type="text" name="month_0" id="inputMonth_0" class="form-control" value="" required="required" title="">
  <input type="text" name="total_0" id="inputTotal_0" class="inputTotalfish form-control" value="" required="required" title="">
  <input type="checkbox" name="check_0" id="inputCheck_0" value="" class="check_row" data-idtag="">
</div>  

<script>
   
  var opsi_type_tag = <?php echo $opsi_type_tag; ?>;
  var opsi_gear_code = <?php echo $opsi_gear_code; ?>;
  var opsi_ccsbt_area = <?php echo $opsi_ccsbt_area; ?>;
  var opsi_pejabat = <?php echo $opsi_pejabat; ?>;
  var opsi_perusahaan = <?php echo $opsi_perusahaan; ?>;
  var opsi_processing = <?php echo $opsi_processing; ?>;
  var opsi_negara = <?php echo $opsi_negara; ?>;
  var opsi_kabkota = <?php echo $opsi_kabkota; ?>;

  var rowCount = { reef: 0, cmf: 0 };
  var opsi_product = [{ id: "F", text: "FRESH"}, { id: "FR", text: "FROZEN"}];
  // var action_export = '<?php echo 'cmf/input_export'; ?>';
  // var action_domestic = '<?php echo 'cmf/input_domestic'; ?>';

  var init_form = function()
  {
    $("#id_previous_state_entity").prop('readonly', true);
    $("#id_previous_date_landing").prop('readonly', true);
    $("#id_type_of_export").attr('readonly', true);
    $("#id_vessel_name").prop('readonly',true);
    $("#id_registration_number").prop('readonly',true);
    $("#id_flag_state").prop('readonly',true);
    $("#id_domestic_weight").prop('readonly', true);


    $("#id_pejabat").removeClass('form-control');
    $("#id_pejabat").select2({  width: "100%",
                                              data: opsi_pejabat
                                            }).on("change", function(e){
                                                var name_title = e.added.text;

                                                $("#inputAuthority_name_title").val(name_title);
                                                
                                            });

    $('#id_point_export_state_entity').prop('readonly',true).val('INDONESIA');
    // $("#id_name_processing_establishment").prop('readonly',true);
    // $("#id_address_processing_establishment").prop('readonly',true);

    
    $("#id_export_destination").removeClass('form-control');
    $("#id_export_destination").select2({  width: "100%",
                                              data: opsi_negara
                                        }).on("change", function(e){
                                                var selected_value = e.added.text;
                                                $("#inputProduct_destination").val(selected_value);
                                            });

    $("#id_point_export_city").removeClass('form-control');
    $("#id_point_export_city").select2({  width: "100%",
                                              data: opsi_kabkota
                                        });
    $("#id_point_export_city").select2('val','271');

    

    $("#id_processing_establishment").removeClass('form-control');
    $("#id_processing_establishment").select2({  width: "100%",
                                              data: opsi_processing
                                            }).on("change", function(e){
                                                var text = e.added.text;
                                                var element = { name: "#inputEstablishment_name",
                                                                address: "#inputEstablishment_address"
                                                              };
      console.log(text, element);
          
                                                input_perusahaan(text, element);                                                
                                            });
    $("#id_exporter_company").removeClass('form-control');
    $("#id_exporter_company").select2({  width: "100%",
                                              data: opsi_perusahaan
                                            }).on("change", function(e){
                                                var text = e.added.text;
                                                var element = { name: "#inputExporter_company_name",
                                                                address: "#inputExporter_company_address"
                                                              };  
                                                console.log(text, element);

                                                input_perusahaan(text, element);   
                                            });

    if(user_data.id_perusahaan !== '0')
    {
      $("#id_exporter_company").select2('val', user_data.id_perusahaan );
      $("#id_exporter_company").select2('readonly', true );      
    }                                        
    
    $("#table_reef_fish").on("change", ".type-inputs", function(){ 
        var row_num = $(this).attr("id").split("_")[1];
        var typetag = $(this).select2("data").text;
        var weight_data = $("#weight_"+row_num).data();
 
        // console.log(typetag === "OT");
        // console.log("#weight_"+row_num);

        if(typetag === "GGO")
        {
          if(weight_data.oriType === "GGO")
          {
            $("#weight_"+row_num).val(weight_data.oriWeight);
            $("#weight_"+row_num).prop("readonly", true);
          }else{
            $("#weight_"+row_num).prop("readonly", false);                                  
          }                                                             
        }else{          
          $("#weight_"+row_num).prop("readonly", false);                      
        }

    });      
  }



  var input_perusahaan = function(text,element)
  {
      console.log('TEXT',text);
      var company = { name: '', address: ''};
      // var text = e.added.text;
      var split_text = text.split('|');
      
      company.name = split_text[0];
      company.address = split_text[1];

      $(element.name).val(company.name);
      $(element.address).val(company.address);    
  }

  var toggle_shipment = function()
  {
      var shipment = $("#inputShipment_type").val();
      $('#ReefTableControl').slideToggle();

      switch (shipment) {
        case "FULL":
          $("#btn_set_reef_full").removeClass('btn-default').addClass('btn-success');
          $("#btn_set_reef_part").removeClass('btn-success').addClass('btn-default');
          disable_fullshipment(false);
          disable_partshipment(true);
          break;
        case "PARTIAL":
          $("#btn_set_reef_full").removeClass('btn-success').addClass('btn-default');
          $("#btn_set_reef_part").removeClass('btn-default').addClass('btn-success');
          disable_fullshipment(true);
          disable_partshipment(false);
      break;
      }
  }

  var disable_fullshipment = function(state)
  {
      // var reverse_state = state === true ? false : true;
      $('#table_reef_fish .inputWeight').prop('readonly', state);
      $('#table_reef_fish .inputTotalfish').prop('readonly', state);
      // $("#id_domestic_name").prop('disabled', state);
      // $("#id_domestic_address").prop('disabled', state);
      // $("#id_domestic_weight").prop('disabled', state);
  }

  var disable_partshipment = function(state)
  { 
      // var reverse_state = state === true ? false : true;
      $('#table_reef_fish .inputWeight').prop('readonly', state);
      $('#table_reef_fish .inputTotalfish').prop('readonly', state);      
      // $('#id_export_state_entity').prop('disabled', state);
      // $("#id_exporter_name").prop('disabled', state);

      // $("#id_export_shipment").select2('enable', reverse_state);
      // $("#id_export_city").select2('enable', reverse_state);
      // $("#id_exporter_company_name").select2('enable', reverse_state);

  }

  var listener_shipment = function()
  {
    $("#btn_set_reef_full").click(function(e){
      // var net_weight = 0;
      // $('#table_cmf_tag .weight-inputs').each(function(){ var weight = +$(this).val(); net_weight = net_weight + weight;});
      // $("#id_domestic_weight").val(net_weight);
      $("#inputShipment_type").val('FULL');
      toggle_shipment();
      console.log(this);
    });

    $("#btn_set_reef_part").click(function(e){
      $("#inputShipment_type").val('PARTIAL');
      toggle_shipment();
      console.log(this);

      //Validation here
    });


  }
  
  var url_json_tags = "<?php echo site_url('preview/cmf/json_cmf_tags'); ?>";
  var get_json_tags = function(id_cmf)
  {
    var return_jumlah = 0;
    var jqxhr = $.ajax({
          async: false,
          global: false,
          dataType: "json",
          url: url_json_tags,
          data: { id_cmf: id_cmf},
          success: function(data) {
            set_table_cmf(data.result);
            set_table_reef(data.result);
          }
        });

    jqxhr.always(function(data) {
          console.log(data);
        });   

    // return return_jumlah;
  }

  var url_json_tags_all = "<?php echo site_url('preview/cmf/json_cmf_tags_all'); ?>";
  var get_json_tags_all = function(id_tags)
  {
    var return_jumlah = 0;
    var jqxhr = $.ajax({
          async: false,
          global: false,
          dataType: "json",
          url: url_json_tags_all,
          data: { id_tags: id_tags},
          success: function(data) {
            set_table_cmf(data.result);
          }
        });

    jqxhr.always(function(data) {
          console.log(data);
        });   

    // return return_jumlah;
  }
  var set_table_reef = function(results)
  {
      results.forEach(function(d){
        fnTableAddREEF(d);
      });
  }

  var set_table_cmf = function(results)
  {
      results.forEach(function(d){
        fnTableAddCMF(d);
      });
  }

  var set_data_cmf = function(data)
  {
    console.log('ini');
    console.log(data);
    fnClearThisTable('#table_cmf_tag', 'cmf');
    fnClearThisTable('#table_reef_fish', 'ctf');
    $('#btn_checkall').prop('checked', false);

    $("#id_previous_state_entity").val('INDONESIA');
    $("#id_previous_date_landing").val(data.fmt_tanggal_buat);
    $("#id_exporter_company").select2('val',data.id_perusahaan);
    
    if(data.id_processing_establishment !== null)
    {
      $("#id_processing_establishment").select2('val',data.id_processing_establishment);            
    }

    // var establishment_data = $("#id_perusahaan").select2('data');

    // var element = { name: "#inputEstablishment_name",
    //                 address: "#inputEstablishment_address"
    //               };  
    // // console.log(establishment_data.text, element);
    // input_perusahaan(establishment_data.text, element);   
    // // $("#id_previous_date_landing").val(data.fmt_tanggal_buat);

    get_json_tags(data.id_cmf);

    $("#btn_set_reef_full").prop('disabled', false);
    $("#btn_set_reef_part").prop('disabled', false);
    $("#inputId_ctf").val(data.id_ctf);
    $("#inputCtf_document_numbers").val(data.ctf_document_number);
    $("#inputId_cmf").val(data.id_cmf);
    $("#inputCmf_document_numbers").val(data.document_number);
  }

  var listener_set_partial = function()
  {
    $("#btn_set_reef_full").on('click', function(){
        fnClearThisTable('#table_reef_tag', 'cmf');
      
      get_json_tags_all(selectedTags);
      $("#inputCtf_tags").val(selectedTags);      
    });
  }

  var listener_btn_checkall = function()
  {
    $("#table_reef_fish").on('click','#btn_checkall',function(){
        $('.check_row').each(function(){
              if( $(this).is(':checked') )
              {
                $(this).prop('checked', false);
              }else{
                $(this).prop('checked', true);
              }
        });
    });
  }
  var init_data_tables = function()
  {
      $('#table_reef_fish').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                       { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "25%" , "sClass": "text-center"},
                        { "sWidth": "20%" , "sClass": "text-center"},
                        { "sWidth": "20%" , "sClass": "text-center"},
                        { "sWidth": "20%" , "sClass": "text-center"},
                      ],
        "bFilter": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": false
      });

       $('#table_cmf_tag').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "15%" , "sClass": "text-center"},
                        { "sWidth": "20%" , "sClass": "text-center"},
                        { "sWidth": "15%" , "sClass": "text-center"},
                        { "sWidth": "35%" , "sClass": "text-center"},
                      ],
        "bFilter": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": false
      });
      
      $("#add_row").click(fnTableAddREEF);

      $("#del_row").click(fnTableDelREEF);

      $('#ReefTableControl').hide();      
      updateRowCount();
    }
    /* TODO : Calon jadi library javascript
       * Desc : Fungsi untuk duplikasi elemen html
       * Untuk rubahan attributenya di bikin array
       */
      function cloner(targetClone, cloneName, placeholder)
      {
          var cloneItem = $(targetClone).clone();
            cloneItem.attr("id", cloneName).attr("name", cloneName).attr("placeholder",placeholder);            
          return cloneItem[0].outerHTML;
      }

      /* Desc : Listener untuk tambah baris table, 
       * pake fungsi plugin dataTable.fnAddData
       */
      function fnTableAddREEF(data) {
        var tempRowNum = rowCount.reef+1;
            // clone tiap kolom dari templateinputs
            $('#table_reef_fish').dataTable().fnAddData( [
              tempRowNum+".",
              cloner("#template_inputs #inputProduct_0", "product_"+tempRowNum,''),
              cloner("#template_inputs #inputTypetag_0", "typetag_"+tempRowNum,data.code_tag),
              cloner("#template_inputs #inputWeight_0", "weight_"+tempRowNum, data.net_weight),
              cloner("#template_inputs #inputTotal_0", "total_"+tempRowNum, data.total_fish),
            ] );



            $("#typetag_"+tempRowNum).prop('readonly',false).removeClass('form-control');

            $("#weight_"+tempRowNum).prop('readonly',true);

            $("#total_"+tempRowNum).prop('readonly',true);

            $("#product_"+tempRowNum).select2({  width: "100%",
                                              data: opsi_product
                                            });
            $("#typetag_"+tempRowNum).select2({  width: "100%",
                                              data: opsi_type_tag
                                            });
            $("#gearcode_"+tempRowNum).select2({  width: "100%",
                                              data: opsi_gear_code
                                            });
            $("#area_"+tempRowNum).select2({  width: "100%",
                                              data: opsi_ccsbt_area
                                            });

            $("#product_"+tempRowNum).select2('val', 'FR');
            if(data.product_type === 'FR')
            {
              $("#product_"+tempRowNum).select2('readonly', true);
            }

            $("#typetag_"+tempRowNum).select2('val', data.type_tag);
            $("#weight_"+tempRowNum).val(data.net_weight);

            $("#weight_"+tempRowNum).data("oriWeight", data.net_weight);
            $("#weight_"+tempRowNum).data("oriType", data.code_tag);

            $("#month_"+tempRowNum).val(data.month_of_harvest);
            $("#total_"+tempRowNum).val(data.total_fish);
            rowCount.reef++;

            updateRowCount();
      }

      function fnTableAddCMF(data) {
        var tempRowNum = rowCount.cmf+1;
            // clone tiap kolom dari templateinputs
            $('#table_cmf_tag').dataTable().fnAddData( [
              tempRowNum+".",
              data.product_type,
              data.code_tag,
              data.net_weight,
              data.total_fish
            ] );


            rowCount.cmf++;

            updateRowCount();
      }

      function fnTableDelREEF() {
        var lastRow = rowCount.reef-1; 
        $("#table_reef_fish").dataTable().fnDeleteRow(lastRow);
        if(rowCount.reef !== 0){ rowCount.reef--;}
        updateRowCount();

      }

    function fnClearThisTable(tableSelector, counterIndex ){
      $(tableSelector).dataTable().fnClearTable(); 
      rowCount[counterIndex] = 0;
    }
    var updateRowCount = function()
    {
      $("#rowcount_info").text(rowCount.reef+" baris");
    }
  
  s_func.push(init_form);
  s_func.push(listener_shipment);
  s_func.push(listener_btn_checkall);
  s_func.push(init_data_tables);


</script>