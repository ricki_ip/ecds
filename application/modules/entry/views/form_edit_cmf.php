<?php 
    $tmpl = array ( 'table_open'  => '<table id="table_cmf_tag" class="table table-bordered">' );
    $this->table->set_template($tmpl);
    $this->table->set_heading('No.','Product', 'Type', 'Month of Catch','Gear Code','CCSBT Statistical Area', 'Net Weight (Kg)','Total Number of whole Fish' );

    if($detail_cmf_fish)
    {
      $number = 1;
      foreach ($detail_cmf_fish as $key => $item) {
        $product = $item->product_type === 'F' ? 'FRESH' : 'FROZEN';
        $this->table->add_row( $number.". ",
          $product,
          $item->code_tag,
          tgl($item->month_of_harvest,'m/Y'),
          $item->nama_gear_code,
          $item->ccsbt_statistical_area,
          $item->net_weight,
          $item->total_fish);
        $number++;
      }
    }

    $table_cmf_tag = $this->table->generate();

    $this->table->clear();
    // vdump($destination);
 ?>
  <?php
          
          $hidden_input = array('id_cmf' => $detail_cmf['id_cmf'],'destination' => $destination);
          echo form_open($submit_form,'id="form_entry" class="form-horizontal" role="form"', $hidden_input);

  ?>
  <?php if ($detail_cmf['is_verified'] === 'TIDAK'): ?>
<div class="row">
  <div class="col-lg-12">
    <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Info!</strong> Dokumen ini belum diverifikasi. Lakukan verifikasi dokumen untuk mendapatkan nomor dokumen.
        <br>
        <strong>Nomor Dokumen Sementara: </strong> <?php echo $detail_cmf['temp_doc_id']; ?>
        <br>
    </div>
  </div>     
</div>   
<?php endif ?>

<div class="row">
  <div class="col-lg-12 document_form">
         
         <?php 
          $attr_document_number = array( 'name' => $form['document_number']['name'],
                                        'label' => $form['document_number']['label'],
                                        'value' => kos($detail_cmf['document_number'], 'Belum verifikasi. Nomor sementara:'.$detail_cmf['temp_doc_id'])
                    );
          echo $this->mkform->input_text($attr_document_number);

          $attr_ctf_document_numbers = array( 'name' => $form['ctf_document_numbers']['name'],
                                        'label' => $form['ctf_document_numbers']['label'],
                                        'value' => kos($detail_cmf['ctf_document_number'], 'Belum verifikasi. Nomor sementara:'.$detail_cmf['ctf_temp_doc_id'])
                    );
          ?>
          <p class="text-center">
            <strong>Link to CTF : </strong> 
            <a href="<?php echo site_url('preview/ctf/view/'.$detail_cmf['id_ctf']); ?>">
              <?php echo $detail_cmf['ctf_temp_doc_id']; ?>
            </a>
          </p>
          <?php
          // echo '<pre>';
          // print_r($detail_cmf);
          // echo '</pre>';
          echo $this->mkform->input_text($attr_ctf_document_numbers);

          $attr_name_processing_establishment = array( 'name' => 'id_processing_establishment',
                                        'label' => $form['name_processing_establishment']['label'],
                                        'value' => $detail_cmf['id_processing_establishment']
                    );
          echo $this->mkform->input_text($attr_name_processing_establishment);
          ?>
          <input type="hidden" name="name_processing_establishment" 
            id="inputEstablishment_name" class="form-control" value="<?php echo kos($detail_cmf['name_processing_establishment']) ?>">
          <input type="hidden" name="address_processing_establishment"
            id="inputEstablishment_address" class="form-control" value="<?php echo kos($detail_cmf['address_processing_establishment']) ?>">  
        <?php if ($this->user->level() < 5): ?>
          <hr>
          <p class="text-center"> Validation by Authority </p> 
          <?php
        
          $attr_authority_name_title = array( 'name' => 'id_pejabat',
                                        'label' => $form['authority_name_title']['label'],
                                        'value' => kos($detail_cmf['id_pejabat'])
                    );
          echo $this->mkform->input_text($attr_authority_name_title);

          $attr_authority_date = array( 'name' => $form['authority_date']['name'],
                                        'label' => $form['authority_date']['label'],
                                        'value' => kos($detail_cmf['authority_date'])
                    );
          echo $this->mkform->input_date($attr_authority_date);

          ?>
        <?php endif ?>
          
  </div>
</div>

<div class="row">
  <div class="col-lg-12"> 
          <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">Description of Fish</h3>
              </div>
              <div class="panel-body">
                Tagging: 
                <?php echo $table_cmf_tag; ?>

                <hr>
                <?php           
                  $attr_description_for_other = array( 'name' => 'description_for_other',
                                        'label' => 'For Other (OT): Describe the Type Of Product :',
                                        'value' => kos($detail_cmf['description_for_other'])
                    );
                  echo $this->mkform->input_text($attr_description_for_other);
                 ?>
              </div>
          </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12"> 
          <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">Product Destination : 
                <?php echo strtoupper($destination); ?>
                </h3>
              </div>
              <div class="panel-body">
                <?php if ($destination === 'domestic'): ?>
                   <?php 
              $attr_domestic_name = array( 'name' => $form['domestic_name']['name'],
                                        'label' => $form['domestic_name']['label'],
                                        'value' => kos($detail_domestic['name'])
                    );
              echo $this->mkform->input_text($attr_domestic_name);

              $attr_domestic_address = array( 'name' => $form['domestic_address']['name'],
                                        'label' => $form['domestic_address']['label'],
                                        'value' => kos($detail_domestic['address'])
                    );
              echo $this->mkform->input_text($attr_domestic_address);

              $attr_domestic_weight = array( 'name' => $form['domestic_weight']['name'],
                                        'label' => $form['domestic_weight']['label'],
                                        'value' => kos($detail_domestic['weight'])
                    );
              echo $this->mkform->input_text($attr_domestic_weight);  
            ?>
                <?php else: ?> 
                    <!-- EXPORT -->
                   <?php
              $attr_export_destination = array( 'name' => $form['export_destination']['name'],
                                        'label' => $form['export_destination']['label'],
                                        'value' => kos($detail_export['destination'])
                    );
              echo $this->mkform->input_text($attr_export_destination);
             ?>
              <hr>
              <p class="text-right">Point of Export</p>
              <?php
              $attr_export_city = array( 'name' => $form['export_city']['name'],
                                        'label' => $form['export_city']['label'],
                                        'value' => kos($detail_export['kota_asal'])
                    );
              echo $this->mkform->input_text($attr_export_city);

              // $attr_export_province = array( 'name' => $form['export_province']['name'],
              //                           'label' => $form['export_province']['label'],
              //                           'value' => kos($detail_export['propinsi_asal'])
              //       );
              // echo $this->mkform->input_text($attr_export_province);

              $attr_export_state_entity = array( 'name' => $form['export_state_entity']['name'],
                                        'label' => $form['export_state_entity']['label'],
                                        'value' => kos($detail_export['state_entity'])
                    );
              echo $this->mkform->input_text($attr_export_state_entity);
             ?>
              <hr>
              <p class="text-right">Certification by Exporter</p>
             <?php
              $attr_exporter_name = array( 'name' => $form['exporter_name']['name'],
                                        'label' => $form['exporter_name']['label'],
                                        'value' => kos($detail_export['name'])
                    );
              echo $this->mkform->input_text($attr_exporter_name);

              $attr_exporter_company_name = array( 'name' => $form['exporter_company_name']['name'],
                                        'label' => $form['exporter_company_name']['label'],
                                        'value' => kos($detail_export['company_name'])
                    );
              echo $this->mkform->input_text($attr_exporter_company_name);
              ?>
              <?php endif ?>
              </div>
          </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
    <button type="submit" class="btn btn-block btn-primary">SAVE CHANGES</button>
  </div>     
</div> 
<p></p>


</form>

<?php if ($this->user->level() < 5): ?>
  <hr>
<div class="row">
  <div class="col-lg-6">
   <a href="<?php echo site_url('cetak/layout_cmf/'.$detail_cmf['id_cmf']); ?>" class="btn btn-large btn-block btn-warning">CETAK FORM</a>
  </div>

    <div class="col-lg-6">
    <?php if ($detail_cmf['is_verified'] === 'TIDAK'): ?>
      <a href='#modal-id' class="btn btn-large btn-block btn-info" data-toggle="modal" >VERIFIKASI</a>
    <?php else: ?>
    <a href="" class="btn btn-large btn-block btn-success">SUDAH DIVERIFIKASI</a>
    <?php endif ?>
    </div>    

</div>  


<div class="modal fade" id="modal-id">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Verifikasi dokumen CMF ini?</h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-warning">
            <p>Berikut nomor dokumen CMF yang akan didapatkan jika dokumen ini diverifikasi. </p>
        </div>

        <ul class="list-group">
          <li class="list-group-item">
            <strong>Document Number</strong>: <?php echo $calon_doc_number['calon_doc_number']; ?>
          </li> 

        </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <a href="<?php echo site_url('entry/cmf/verifikasi/'.$detail_cmf['id_cmf'].'/'.$detail_cmf['id_ctf']); ?>"
          class="btn btn-primary">Verifikasi</a>
      </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal -->
<?php endif ?>

<script>
  var id_cmf = <?php echo $detail_cmf['id_cmf'] ?>;
  var id_ctf = <?php echo $detail_cmf['id_ctf'] ?>;
  var url_reset = "<?php echo site_url('entry/cmf/reset/');?>";

  var opsi_type_tag = <?php echo $opsi_type_tag; ?>;
  var opsi_gear_code = <?php echo $opsi_gear_code; ?>;
  var opsi_ccsbt_area = <?php echo $opsi_ccsbt_area; ?>;
  var opsi_pejabat = <?php echo $opsi_pejabat; ?>;
  var opsi_processing = <?php echo $opsi_processing; ?>;
  var opsi_perusahaan = <?php echo $opsi_perusahaan; ?>;
  var opsi_negara = <?php echo $opsi_negara; ?>;
  var opsi_kabkota = <?php echo $opsi_kabkota; ?>;

  var action_export = '<?php echo 'cmf/input_export'; ?>';
  var action_domestic = '<?php echo 'cmf/input_domestic'; ?>';

  var init_form = function()
  {
    $("#id_ctf_document_numbers").prop('disabled',true);
    $("#id_document_number").prop('disabled',true);
    $("#id_flag_state").prop('disabled',true);
    $("#id_domestic_weight").prop('disabled', true);

    $("#id_pejabat").removeClass('form-control');
    $("#id_pejabat").select2({  width: "100%",
                                              data: opsi_pejabat
                                            }).on("change", function(e){
                                                var name_title = e.added.text;

                                                $("#inputAuthority_name_title").val(name_title);
                                                
                                            });

    $('#id_export_state_entity').prop('disabled',true).val('INDONESIA');
    // $("#id_name_processing_establishment").prop('readonly',true);
    // $("#id_address_processing_establishment").prop('readonly',true);

    
    $("#id_export_destination").removeClass('form-control');
    $("#id_export_destination").select2({  width: "100%",
                                              data: opsi_negara
                                        });

    $("#id_export_city").removeClass('form-control');
    $("#id_export_city").select2({  width: "100%",
                                              data: opsi_kabkota
                                        });
    $("#id_export_city").select2('val','271');

    

    $("#id_processing_establishment").removeClass('form-control');
    $("#id_processing_establishment").select2({  width: "100%",
                                              data: opsi_processing
                                            }).on("change", function(e){
                                                var establishment = { name: '', address: ''};
                                                var text = e.added.text;
                                                var split_text = text.split('|');
                                                
                                                establishment.name = split_text[0];
                                                establishment.address = split_text[1];

                                                input_establishment(establishment);
                                                
                                            });
    $("#id_exporter_company_name").removeClass('form-control');
    $("#id_exporter_company_name").select2({  width: "100%",
                                              data: opsi_perusahaan
                                            });

    if(user_data.id_perusahaan !== '0')
    {
      $("#id_exporter_company_name").select2('val', user_data.id_perusahaan );
      $("#id_exporter_company_name").select2('readonly', true );      
    }
    $("#delete_doc").click(function(){
        var pesan = "Anda yakin akan menghapus dokumen ini? ";
        var link_del = url_reset+'/'+id_cmf;
        if(confirm(pesan))
        {
          window.open(link_del);
        }
    });
 
  }

  var input_establishment = function(data_perusahaan)
  {
      $("#inputEstablishment_name").val(data_perusahaan.name);
      $("#inputEstablishment_address").val(data_perusahaan.address);

      $("#id_domestic_name").val(data_perusahaan.name);
      $("#id_domestic_address").val(data_perusahaan.address);
  }

  var toggle_destination_form = function()
  {
      var destination = $("#inputProduct_destination").val();

      switch (destination) {
        case "domestic":
          $("#button_domestic").removeClass('btn-default').addClass('btn-success');
          $("#button_export").removeClass('btn-success').addClass('btn-default');
          disable_domestic(false);
          disable_export(true);
          break;
        case "export":
          $("#button_domestic").removeClass('btn-success').addClass('btn-default');
          $("#button_export").removeClass('btn-default').addClass('btn-success');
          disable_domestic(true);          
          disable_export(false);
          break;
      }

      // console.log(destination);
  }

  var disable_domestic = function(state)
  {
      $("#id_domestic_name").prop('disabled', state);
      $("#id_domestic_address").prop('disabled', state);
      $("#id_domestic_weight").prop('disabled', state);
  }

  var disable_export = function(state)
  { 
      var reverse_state = state === true ? false : true;
      $('#id_export_state_entity').prop('disabled', state);
      $("#id_exporter_name").prop('disabled', state);

      $("#id_export_destination").select2('enable', reverse_state);
      $("#id_export_city").select2('enable', reverse_state);
      $("#id_exporter_company_name").select2('enable', reverse_state);
  }

  var listener_destination = function()
  {
    $("#button_domestic").click(function(e){
      // $("#form_entry").attr('action',action_domestic);
      //Validation here
      var total_weight = 0;
      $('#table_cmf_tag .weight-inputs').each(function(){ var weight = +$(this).val(); total_weight = total_weight + weight;});
      $("#id_domestic_weight").val(total_weight);
      $("#inputProduct_destination").val('domestic');
      toggle_destination_form();
    });

    $("#button_export").click(function(e){
      // $("#form_entry").attr('action',action_export);
      $("#inputProduct_destination").val('export');
      toggle_destination_form();

      //Validation here
    });


  }
  
  var url_json_tags = "<?php echo site_url('preview/ctf/json_ctf_tags'); ?>";
  var get_json_tags = function(id_ctf)
  {
    var return_jumlah = 0;
    var jqxhr = $.ajax({
          async: false,
          global: false,
          dataType: "json",
          url: url_json_tags,
          data: { id_ctf: id_ctf},
          success: function(data) {
            set_table_ctf(data.result);
          }
        });

    jqxhr.always(function(data) {
          console.log(data);
        });   

    // return return_jumlah;
  }

  var url_json_tags_all = "<?php echo site_url('preview/ctf/json_ctf_tags_all'); ?>";
  var get_json_tags_all = function(id_tags)
  {
    var return_jumlah = 0;
    var jqxhr = $.ajax({
          async: false,
          global: false,
          dataType: "json",
          url: url_json_tags_all,
          data: { id_tags: id_tags, sealed: 'true'},
          success: function(data) {
            set_table_cmf(data.result);
          }
        });

    jqxhr.always(function(data) {
          console.log(data);
        });   

    // return return_jumlah;
  }
  var set_table_ctf = function(results)
  {
      if(results)
      {
        $("#alert-for-ctf").hide();                
        results.forEach(function(d){
          fnTableAddCTF(d);
        });
      }else{
        $("#pesan-alert").html('').html('Semua tag ikan di dokumen CTF ini sudah terpakai.');
        $("#alert-for-ctf").show();
      }
  }

  var set_table_cmf = function(results)
  {
    if(results)
    {
      results.forEach(function(d){
        fnTableAddCMF(d);
      });
    }
  }

  var set_data_ctf = function(data)
  {
    fnClearThisTable('#table_cmf_tag', 'cmf');
    fnClearThisTable('#table_ctf_tag', 'ctf');
    $('#btn_checkall').prop('checked', false);

    $("#id_vessel_name").val(data.vessel_name);
    $("#id_registration_number").val(data.ccsbt_registration_number);
    $("#id_flag_state").val(data.flag_state);

    get_json_tags(data.id_ctf);

    $("#btn_set_cmf").prop('disabled', false);
    $("#inputId_ctf").val(data.id_ctf);
    $("#inputCtf_document_numbers").val(data.document_number);
  }

  var listener_set_to_cmf = function()
  {
    $("#btn_set_cmf").on('click', function(){
        fnClearThisTable('#table_cmf_tag', 'cmf');
        var arrTags = [];
        var selectedTags = '';
      $('#table_ctf_tag .check_row').each(function(){
        var dataIdtag = $(this).data('idtag');
              if( $(this).is(':checked') )
              {
                arrTags.push(dataIdtag);
              }
      });
      selectedTags = arrTags.join(',');
      // console.log(selectedTags);

      get_json_tags_all(selectedTags);
      $("#inputCtf_tags").val(selectedTags);      
    });
  }

  var listener_btn_checkall = function()
  {
    $("#table_ctf_tag").on('click','#btn_checkall',function(){
        $('.check_row').each(function(){
              if( $(this).is(':checked') )
              {
                $(this).prop('checked', false);
              }else{
                $(this).prop('checked', true);
              }
        });
    });
  }
  var rowCount = { ctf: 0, cmf: 0 };
  var opsi_product = [{ id: "F", text: "FRESH"}, { id: "FR", text: "FROZEN"}];
  var init_data_tables = function()
  {
      $('#table_ctf_tag').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "15%" , "sClass": "text-center"},
                        { "sWidth": "20%" , "sClass": "text-center"},
                        { "sWidth": "15%" , "sClass": "text-center"},
                        { "sWidth": "10%" , "sClass": "text-center"},
                        { "sWidth": "20%" , "sClass": "text-center"},
                        { "sWidth": "15%" , "sClass": "text-center"},
                      ],
        "bFilter": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": false
      });

       $('#table_cmf_tag').dataTable( {
        "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "aoColumns":  [
                        { "sWidth": "5%" , "sClass": "text-center"},
                        { "sWidth": "15%" , "sClass": "text-center"},
                        { "sWidth": "20%" , "sClass": "text-center"},
                        { "sWidth": "15%" , "sClass": "text-center"},
                        { "sWidth": "10%" , "sClass": "text-center"},
                        { "sWidth": "10%" , "sClass": "text-center"},
                        { "sWidth": "15%" , "sClass": "text-center"},
                        { "sWidth": "10%" , "sClass": "text-center"},
                      ],
        "bFilter": false,
        "bInfo": false,
        "bPaginate": false,
        "bSort": false
      });
      
    }
    /* TODO : Calon jadi library javascript
       * Desc : Fungsi untuk duplikasi elemen html
       * Untuk rubahan attributenya di bikin array
       */
      function cloner(targetClone, cloneName, placeholder)
      {
          var cloneItem = $(targetClone).clone();
            cloneItem.attr("id", cloneName).attr("name", cloneName).attr("placeholder",placeholder);            
          return cloneItem[0].outerHTML;
      }

      /* Desc : Listener untuk tambah baris table, 
       * pake fungsi plugin dataTable.fnAddData
       */
      function fnTableAddCTF(data) {
        var tempRowNum = rowCount.ctf+1;
            // clone tiap kolom dari templateinputs
            $('#table_ctf_tag').dataTable().fnAddData( [
              tempRowNum+".",
              data.code_tag,
              data.month_of_harvest,
              data.gear_code, 
              data.ccsbt_statistical_area,
              data.weight,
              cloner("#template_inputs #inputCheck_0", "CTFcheck_"+tempRowNum, ''),
            ] );

            // $("#CTFtypetag_"+tempRowNum).prop('readonly',true);
            // $("#CTFweight_"+tempRowNum).prop('readonly',true);
            // $("#CTFgearcode_"+tempRowNum).prop('readonly',true);
            // $("#CTFarea_"+tempRowNum).prop('readonly',true);
            // $("#CTFmonth_"+tempRowNum).prop('readonly',true);
            

            $("#CTFcheck_"+tempRowNum).data('idtag', data.id_tag_information);
         
            rowCount.ctf++;

            updateRowCount();
      }

      function fnTableAddCMF(data) {
        var tempRowNum = rowCount.cmf+1;
            // clone tiap kolom dari templateinputs
            $('#table_cmf_tag').dataTable().fnAddData( [
              tempRowNum+".",
              cloner("#template_inputs #inputProduct_0", "product_"+tempRowNum,''),
              cloner("#template_inputs #inputTypetag_0", "typetag_"+tempRowNum,data.code_tag),
              cloner("#template_inputs #inputMonth_0", "month_"+tempRowNum,data.month_of_harvest),
              cloner("#template_inputs #inputGearcode_0", "gearcode_"+tempRowNum,data.gear_code), 
              cloner("#template_inputs #inputArea_0", "area_"+tempRowNum, data.ccsbt_statistical_area),
              cloner("#template_inputs #inputWeight_0", "weight_"+tempRowNum, data.total_weight),
              cloner("#template_inputs #inputTotal_0", "total_"+tempRowNum, data.total_fish),
            ] );


            $("#typetag_"+tempRowNum).prop('readonly',true).removeClass('form-control');
            $("#gearcode_"+tempRowNum).prop('readonly',true).removeClass('form-control');
            $("#area_"+tempRowNum).prop('readonly',true).removeClass('form-control');

            $("#weight_"+tempRowNum).prop('readonly',true);
            $("#month_"+tempRowNum).prop('readonly',true);
            $("#total_"+tempRowNum).prop('readonly',true);

            $("#product_"+tempRowNum).select2({  width: "100%",
                                              data: opsi_product
                                            });
            $("#typetag_"+tempRowNum).select2({  width: "100%",
                                              data: opsi_type_tag
                                            });


            $("#gearcode_"+tempRowNum).select2({  width: "100%",
                                              data: opsi_gear_code
                                            });


            $("#area_"+tempRowNum).select2({  width: "100%",
                                              data: opsi_ccsbt_area
                                            });

            $("#product_"+tempRowNum).select2('val', 'F');
            $("#gearcode_"+tempRowNum).select2('val', data.tti_gear_code);
            $("#typetag_"+tempRowNum).select2('val', data.type_tag);
            $("#area_"+tempRowNum).select2('val', data.ccsbt_statistical_area);
            $("#weight_"+tempRowNum).val(data.total_weight);
            $("#month_"+tempRowNum).val(data.month_of_harvest);
            $("#total_"+tempRowNum).val(data.total_fish);

            rowCount.cmf++;

            updateRowCount();
      }

      function fnTableDelRow(tableSelector, counterIndex) {
        var lastRow = rowCount[counterIndex]-1; 
        $(tableSelector).dataTable().fnDeleteRow(lastRow);
        if(rowCount[counterIndex] !== 0){ rowCount[counterIndex]--;}
        updateRowCount();

      }

    function fnClearThisTable(tableSelector, counterIndex ){
      $(tableSelector).dataTable().fnClearTable(); 
      rowCount[counterIndex] = 0;
    }
    var updateRowCount = function()
    {
      //$("#rowcount_info").text(rowCount.ctf+" baris");
    }
  
  s_func.push(init_form);
  s_func.push(listener_destination);
  s_func.push(listener_btn_checkall);
  s_func.push(listener_set_to_cmf);
  s_func.push(init_data_tables);

</script>