<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_ctf_entry extends CI_Model
{
	

    function __construct()
    {
        

    }

    public function list_ctf()
    {
        $query = ' SELECT * FROM trs_vessel';

        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function last_ctf_id($area_code ="B", $year_code = "14")
    {

        $this->db->select("MAX(last_id) AS last_ctf_id");
        $this->db->where("area_code", $area_code);
        $this->db->where("year_code", $year_code);

        $run_query = $this->db->get("trs_ctf_counter");                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    } 

    public function verifikasi_ctf($input)
    {
        $data = array(
               'is_verified' => 'YA',
               'tgl_verifikasi' => date('Y-m-d H:i:s'),
               'document_number' => $input['calon_doc_number'],
               'doc_area' => $input['doc_area'],
               'doc_year' => $input['doc_year'],
               'doc_id' => $input['doc_id'],
               'doc_prefix' => $input['doc_prefix'],
               'signature' => 'Ya',
               'date' => date('Y-m-d H:i:s'),
               'id_pengguna_ubah' => $this->user->id_pengguna(),
               'tanggal_ubah' => date('Y-m-d H:i:s')
            );

        $this->db->where('id_ctf', $input['id_ctf']);
        $this->db->update('trs_ctf', $data); 
    }   

    public function update_counter($area_code)
    {
        $sql_update = "UPDATE trs_ctf_counter SET last_id = last_id + 1 WHERE area_code = '$area_code' ";

        $run_query = $this->db->query($sql_update);
    }

    public function associate_cmf($id_ctf, $document_number)
    {
        $sql_update = "UPDATE trs_ctf SET document_number_associated_cmf = '$document_number' WHERE id_ctf = '$id_ctf' ";

        $run_query = $this->db->query($sql_update);
    }

    public function check_tag($tag_number)
    {
        $this->db->select('COUNT(*) AS jumlah_tag', TRUE);
        $this->db->where('ccsbt_tag_number', $tag_number);
        $run_query = $this->db->get('trs_ctf_fish');

        $result = $run_query->row();

        $jumlah_tag = $result->jumlah_tag;
        $tag_info = array();
        if($jumlah_tag > 0)
        {
            $sql = " SELECT ctf.*, ctfish.* FROM trs_ctf_fish ctfish
                     LEFT JOIN trs_ctf ctf ON ctfish.id_ctf = ctf.id_ctf
                     WHERE ctfish.ccsbt_tag_number = ".$tag_number."
                   ";

            $run_query = $this->db->query($sql);

            $tag_info = $run_query->row();
        }else{
            $alt_tag_number = "ID".$tag_number;

            $this->db->select('COUNT(*) AS jumlah_tag', TRUE);
            $this->db->where('tag_number', $alt_tag_number);
            $run_query_alt = $this->db->get('ctf_tag_report');

            $result_alt = $run_query_alt->row();

            $jumlah_tag_alt = $result_alt->jumlah_tag;

            // vdump($jumlah_tag_alt);
            if($jumlah_tag_alt > 0)
            {
                $sql_alt = "SELECT ctf_document_number as document_number,
                            vessel_name,
                            vessel_reg AS vessel_registration_number,
                            validation_date AS tanggal_buat
                            FROM ctf_tag_report WHERE tag_number='".$alt_tag_number."'";

                $run_query = $this->db->query($sql_alt);

                $tag_info = $run_query->row();
                $jumlah_tag = $jumlah_tag_alt;
            }
        }        

        return array('jumlah_tag' => $jumlah_tag, 'tag_info' => $tag_info);
    }

    public function update_temp_doc_id($id_ctf)
    {
        $temp_doc_id = 'CT'.$id_ctf;
        $data = array( 'temp_doc_id' => $temp_doc_id );

        $this->db->where('id_ctf', $id_ctf);
        $this->db->update('trs_ctf', $data);
    }

    public function input($array_input)
    {
        $input_ctf = array('id_vessel_ccsbt' =>  $array_input['id_vessel_ccsbt'],
                           'id_asosiasi' => $array_input['id_asosiasi'],
                           'id_perusahaan' => $array_input['id_perusahaan'],
                           'vessel_name' =>  $array_input['vessel_name'],
                           'name' =>  $array_input['name'],
                           'title' => $array_input['title'],
                           'vessel_registration_number' =>  $array_input['vessel_registration_number'],
                           'flag_state' =>  $array_input['flag_state'],
                           'id_pengguna_buat' => $array_input['id_pengguna_buat'],
                           'tanggal_buat' => $array_input['tanggal_buat']
                           );

        $this->db->insert('trs_ctf', $input_ctf);

        $id_ctf = $this->db->insert_id();

        $this->update_temp_doc_id($id_ctf);

        $input_tags = array();
        $index = 1;
        $total_weight = 0;
        foreach ($array_input as $name => $value) {
                    if(preg_match('/tagnumber/', $name))
                    {
                        $pos = strripos($name, '_')+1;
                        $num = substr($name, $pos);
                        if($array_input[$name] !== ''){
                            $split_date = explode("/", $array_input['month_'.$num]);
                            $mod_date = $split_date[1].'-'.$split_date[0].'-01'; 
                            $input_tags[$index] = array(
                                                          'id_asosiasi' => $array_input['id_asosiasi'],
                                                          'id_perusahaan' => $array_input['id_perusahaan'],
                                                          'ccsbt_tag_number' => $array_input['tagnumber_'.$num],
                                                          'type_tag' => $array_input['typetag_'.$num],
                                                          'weight' => $array_input['weight_'.$num],
                                                          'fork_length' => $array_input['forklength_'.$num],
                                                          'gear_code' => $array_input['gearcode_'.$num],
                                                          'ccsbt_statistical_area' => $array_input['area_'.$num],
                                                          'month_of_harvest' => $mod_date,
                                                          'tanggal_buat' => $array_input['tanggal_buat'],
                                                          'id_pengguna_buat' => $array_input['id_pengguna_buat'],
                                                          'id_ctf' => $id_ctf,
                                                          'aktif' => 'Ya'
                                                        );
                            $total_weight = $total_weight + intval($array_input['weight_'.$num]);
                            $index++;
                        }
                    }
        }

        // var_dump($input_ctf);
        // echo "<hr>";
        // vdump($input_tags, TRUE);
        // die;
        $aff_rows = 0;
        if(!empty($input_tags))
        {
            $this->db->insert_batch('trs_ctf_fish',$input_tags);
        }elseif($total_weight > $array_input['kuota_left'])
        {
            echo "<h3>Pelanggaran sistem. Entry melebihi sisa kuota, tidak bisa melanjutkan. Kegiatan anda akan dilaporkan.</h3>";
            die;
        }
        
        $aff_rows = $this->db->affected_rows();

        if($aff_rows === 0)
        {
            $id_ctf = false;
        }

        return $id_ctf;
    }

    public function edit($array_input)
    {
        $id_ctf = $array_input['id_ctf'];

        $input_ctf = array(
                           // 'id_vessel_ccsbt' =>  $array_input['id_vessel_ccsbt'],
                           // 'vessel_name' =>  $array_input['vessel_name'],
                           'name' =>  $array_input['name'],
                           'title' => $array_input['title'],
                           // 'vessel_registration_number' =>  $array_input['vessel_registration_number'],
                           // 'flag_state' =>  $array_input['flag_state'],
                           'id_pengguna_ubah' => $array_input['id_pengguna_ubah'],
                           'tanggal_ubah' => $array_input['tanggal_ubah']
                           );
        $this->db->where('id_ctf', $id_ctf);
        $this->db->update('trs_ctf', $input_ctf);


        $input_tags = array();
        $index = 1;
        foreach ($array_input as $name => $value) {
                    if(preg_match('/tagnumber/', $name))
                    {
                        $pos = strripos($name, '_')+1;
                        $num = substr($name, $pos);
                        if($array_input[$name] !== ''){
                            $split_date = explode("/", $array_input['month_'.$num]);
                            $mod_date = $split_date[1].'-'.$split_date[0].'-01'; 
                            $input_tags[$index] = array(
                                                          'id_asosiasi' => $array_input['id_asosiasi'],
                                                          'id_perusahaan' => $array_input['id_perusahaan'],
                                                          'ccsbt_tag_number' => $array_input['tagnumber_'.$num],
                                                          'type_tag' => $array_input['typetag_'.$num],
                                                          'weight' => $array_input['weight_'.$num],
                                                          'fork_length' => $array_input['forklength_'.$num],
                                                          'gear_code' => $array_input['gearcode_'.$num],
                                                          'ccsbt_statistical_area' => $array_input['area_'.$num],
                                                          'month_of_harvest' => $mod_date,
                                                          'tanggal_buat' => $array_input['tanggal_ubah'],
                                                          'id_pengguna_buat' => $array_input['id_pengguna_ubah'],
                                                          'id_ctf' => $id_ctf,
                                                          'aktif' => 'Ya'
                                                        );
                            $index++;
                        }
                    }
        }

        // var_dump($input_ctf);
        // echo "<hr>";
        // vdump($input_tags);
        // die;

        $aff_rows = 0;
        if(!empty($input_tags))
        {
            $this->db->insert_batch('trs_ctf_fish',$input_tags);
        }elseif($total_weight > $array_input['kuota_left'])
        {
            echo "<h3>Pelanggaran sistem. Entry melebihi sisa kuota, sehingga tidak bisa dilanjutkan. Kegiatan anda akan dilaporkan.</h3>";
            die;
        }
        
        $aff_rows = $this->db->affected_rows();
        if($aff_rows === 0)
        {
            $id_ctf = false;
        }

        return $id_ctf;
    }

    public function edit_fish($id_tag_information, $array_input)
    {
        // Check kalau belum exist
        $this->db->select('id_tag_information');
        $this->db->where('id_tag_information !=', $id_tag_information);
        $this->db->where('ccsbt_tag_number', $array_input['ccsbt_tag_number']);
        $check_tag = $this->db->get('trs_ctf_fish');

        if($check_tag->num_rows() > 0 )
        {
            return false;
        }else{
             $this->db->where('id_tag_information', $id_tag_information);
            $query = $this->db->update('trs_ctf_fish',$array_input);

            if($this->db->affected_rows() > 0){
                $result = true;
            }else{
                $result = false;
            }

            return $result;
        }
    }

    public function get_detail($id_ctf)
    {
        $query = "  SELECT 
                        pendok.*,
                        perusahaan.*,
                        pemohon.*
                    FROM 
                        db_pendaftaran_kapal.trs_pendok pendok
                    LEFT JOIN (
                        db_pendaftaran_kapal.mst_pemohon pemohon,
                        db_master.mst_perusahaan perusahaan
                            )
                    ON (
                        pendok.id_pemohon = pemohon.id_pemohon
                        AND pendok.id_perusahaan = perusahaan.id_perusahaan
                            )
                    WHERE 
                        pendok.id_pendok = '".$id_pendok."'
                        ";

        $run_query = $this->db->query($query);

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detil_kapal($id)
    {
        $sql = "SELECT *
                    FROM kapi_monev.mst_inka_mina
                    WHERE mst_inka_mina.id_kapal = $id ";

        $run_query = $this->db_kapi->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function delete($id_ctf)
    {

        $this->db->select('id_cmf');
        $this->db->where('id_ctf', $id_ctf);
        
        $query_check_cmf = $this->db->get('trs_cmf');
        $array_id_cmfs = array();
        if($query_check_cmf->num_rows() > 0){
            $check_result = $query_check_cmf->result_array();            
            foreach ($check_result as $key => $value) {
                array_push($array_id_cmfs, $value['id_cmf']);
            }
        }else{
            $check_result = false;
        }


        $ctf_tables = array('trs_ctf', 'trs_ctf_fish');
        $this->db->where('id_ctf', $id_ctf);
        $this->db->delete($ctf_tables);

        if(count($array_id_cmfs) > 0)
        {
            $id_cmfs = implode(",", $array_id_cmfs);
            // vdump($id_cmfs, true);
            $cmf_tables = array('trs_cmf', 'trs_cmf_fish','trs_cmf_final_domestic','trs_cmf_final_export');
            $this->db->where_in('id_cmf', $id_cmfs);
            $this->db->delete($cmf_tables);
        }
        
        // $query = $this->db->query($sql);
        //var_dump($sql);

    }

	public function from_dss()
    {
    	$query = "";

    	$run_query = $this->db_dss->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function from_kapi()
    {
    	$query = "";

    	$run_query = $this->db_dss->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}