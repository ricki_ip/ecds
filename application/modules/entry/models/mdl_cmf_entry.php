<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_cmf_entry extends CI_Model
{
	

    function __construct()
    {
        

    }

    public function list_cmf()
    {
        $query = ' SELECT * FROM trs_cmf';

        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function update_temp_doc_id($id_cmf)
    {
        $temp_doc_id = 'CM'.$id_cmf;
        $data = array( 'temp_doc_id' => $temp_doc_id );

        $this->db->where('id_cmf', $id_cmf);
        $this->db->update('trs_cmf', $data);
    }

    public function update_cmf_total_weight($id_cmf, $weight)
    {
        $data = array('total_weight' => $weight, 'curr_weight' => $weight);

        $this->db->where('id_cmf', $id_cmf);
        $this->db->update('trs_cmf', $data);
    }

    public function update_cmf_current_weight($id_cmf, $cut_weight)
    {
        $this->db->select('curr_weight');
        $this->db->where('id_cmf', $id_cmf);
        $get_cmf = $this->db->get('trs_cmf');
        $curr_weight = $get_cmf->row()->curr_weight;
        $new_weight = $curr_weight - $cut_weight;
        if($new_weight > -1)
        {
            $data = array('curr_weight' => $new_weight);

            $this->db->where('id_cmf', $id_cmf);
            $this->db->update('trs_cmf', $data);
        }         
    }

    public function input_cmf($data)
    {
        $this->db->insert('trs_cmf', $data);

        $id_cmf = $this->db->insert_id(); 
        
        $this->update_temp_doc_id($id_cmf);
        
        return $id_cmf; 
    }

    public function input_fish($datas)
    {
        if(count($datas) < 2)
        {
            $this->db->insert('trs_cmf_fish', $datas[0]);         
        }else{
            $this->db->insert_batch('trs_cmf_fish', $datas);            
        }
    }

    public function inputFinalDomestic($datas)
    {
        $this->db->insert('trs_cmf_final_domestic', $datas);
    }

    public function inputFinalExport($datas)
    {
        $this->db->insert('trs_cmf_final_export', $datas);
    }

    public function update($data)
    {
        $this->db->where('id_cmf', $data['id_cmf']);
        $query = $this->db->update('trs_cmf',$data);

        if($this->db->affected_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }

        return $result;
    }

    public function last_cmf_id($area_code ="B", $year_code = "14")
    {

        $this->db->select("MAX(last_id) AS last_cmf_id");
        $this->db->where("area_code", $area_code);
        $this->db->where("year_code", $year_code);

        $run_query = $this->db->get("trs_cmf_counter");                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    } 

    public function seal_ctf_tags($id_cmf)
    {
        $this->db->select('id_tags');
        $this->db->where('id_cmf', $id_cmf);
        $query = $this->db->get('trs_cmf');
        $run_query = $query->row();

        $id_tags = $run_query->id_tags;


        $arr_tags = explode(',',$id_tags);
        // vdump($arr_tags, true);
        $seal_tags = array(
                            'is_free' => 'TIDAK',
                            'id_pengguna_ubah' => $this->user->id_pengguna(),
                            'tanggal_ubah' => date('Y-m-d H:i:s')
                          );

        $this->db->where_in('id_tag_information', $arr_tags);
        $this->db->update('trs_ctf_fish', $seal_tags);
    }

    public function verifikasi_cmf($input)
    {
        $data = array(
               'is_verified' => 'YA',
               'tgl_verifikasi' => date('Y-m-d H:i:s'),
               'document_number' => $input['calon_doc_number'],
               'doc_area' => $input['doc_area'],
               'doc_year' => $input['doc_year'],
               'doc_id' => $input['doc_id'],
               'doc_prefix' => $input['doc_prefix'],
               'id_pengguna_ubah' => $this->user->id_pengguna(),
               'tanggal_ubah' => date('Y-m-d H:i:s')
            );

        $this->db->where('id_cmf', $input['id_cmf']);
        $this->db->update('trs_cmf', $data); 

        // $this->seal_ctf_tags($input['id_cmf']);
    }   

    public function update_counter($area_code)
    {
        $sql_update = "UPDATE trs_cmf_counter SET last_id = last_id + 1 WHERE area_code = '$area_code' ";

        $run_query = $this->db->query($sql_update);
    }

    public function edit($array_input)
    {
        $id_cmf = $array_input['id_cmf'];
        unset($array_input['id_cmf']);
        // vdump($array_input,true);
        // Kalau user bukan perusahaan
        if($this->user->level() < 5)
        {
                    $input_cmf = array(
                        'id_perusahaan' => $array_input['id_perusahaan'],
                        'id_processing_establishment' => $array_input['id_processing_establishment'],
                        'id_pejabat' => $array_input['id_pejabat'],
                        'name_processing_establishment' => $array_input['name_processing_establishment'],
                        'address_processing_establishment' => $array_input['address_processing_establishment']
                        );    
        }else{
             $input_cmf = array(
                        'id_perusahaan' => $array_input['id_perusahaan'],
                        'id_processing_establishment' => $array_input['id_processing_establishment'],
                        'name_processing_establishment' => $array_input['name_processing_establishment'],
                        'address_processing_establishment' => $array_input['address_processing_establishment']
                        );    
        }




        $this->db->where('id_cmf', $id_cmf);
        $this->db->update('trs_cmf', $input_cmf);

        $table_destination = '';
        $input_cmf_extra = array();
        if($array_input['destination'] === 'export')
        {
            $input_cmf_extra = array(
                        'destination' => $array_input['export_destination'],
                        'city' => $array_input['export_city'],
                        'name' => $array_input['exporter_name'],
                        'company_name' => $array_input['id_exporter_company_name']
                        );
            $table_destination = 'trs_cmf_final_export';
        }else{
            $input_cmf_extra = array(
                        'name' => $array_input['domestic_name'],
                        'address' => $array_input['domestic_address']
                        );
            $table_destination = 'trs_cmf_final_domestic';

        }
        
        $this->db->where('id_cmf', $id_cmf);
        $this->db->update($table_destination, $input_cmf_extra);


    }

    public function delete($id_cmf)
    {

        // 1. Ambil id_tags di cmf
        $sql_check_used_tags = "SELECT cmf.id_tags FROM trs_cmf cmf
                                    WHERE cmf.id_cmf = $id_cmf";
        $get_result = $this->db->query($sql_check_used_tags);
        $result_tags = $get_result->row();
        $list_tags  = $result_tags->id_tags;
        $array_tags = explode(",", $list_tags);
        $update_tags = array();

        // 2. Ambil id_reef yang sudah terhubung ke cmf ini 
        $sql_check_reef = "SELECT reef.id_reef FROM trs_reef reef
                            WHERE reef.id_cmf = $id_cmf";
        $get_result_check = $this->db->query($sql_check_reef);
        $list_id_reef = $get_result_check->result();
        // $list_id_reef  = $result_id_reef->id_reef;
        // $array_id_reef = implode(",", $list_id_reef);
        // $delete_reefs = array();
        
        // vdump($array_id_reef, true);

        if($result_tags)
        {
            //hapus cmf fish
            //hapus domestic
            //hapus export
            $cmf_tables = array('trs_cmf','trs_cmf_fish','trs_cmf_final_domestic','trs_cmf_final_export');
            $this->db->where('id_cmf', $id_cmf);
            $this->db->delete($cmf_tables);

            // Reset CTF tagged fish 
            foreach ($array_tags as $key => $id_tag_information)
            {
                    $tmp_data = array(
                    'id_tag_information' => $id_tag_information,
                    'is_free' => 'YA'
                    );
                    array_push($update_tags,$tmp_data);
            }
            $this->db->update_batch('trs_ctf_fish', $update_tags, 'id_tag_information');

        }

        // Hapus dokumen REEF terhubung
        if($list_id_reef)
        {
            //  // 3. Ambil id_reef_fish yang sudah terhubung ke reef ini
            // $this->db->select('id_reef_fish');
            // $this->db->where_in('id_reef', $array_id_reef);
            // $get_reefish = $this->db->get('trs_reef_fish');
            // $result_id_reefish = $get_reefish->row();
            // $list_id_reefish  = $result_id_reefish->id_reef;
            // $array_id_reefish = explode(",", $list_id_reefish);
            // $delete_reefish = array();

            foreach ($list_id_reef as $key => $id_reef) {
                $reef_table = array('trs_reef','trs_reef_fish');
                $this->db->where('id_reef', $id_reef);
                $this->db->delete($reef_table);                
                vdump($this->db->last_query());
            }
            
        }            

        // vdump($list_id_reef, true);

    }

	public function from_dss()
    {
    	$query = "";

    	$run_query = $this->db_dss->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function from_kapi()
    {
    	$query = "";

    	$run_query = $this->db_dss->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}