<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_reef_entry extends CI_Model
{
  

    function __construct()
    {
        

    }

    public function list_reef()
    {
        $query = ' SELECT * FROM trs_reef';

        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function update_temp_doc_id($id_reef)
    {
        $temp_doc_id = 'RE'.$id_reef;
        $data = array( 'temp_doc_id' => $temp_doc_id );

        $this->db->where('id_reef', $id_reef);
        $this->db->update('trs_reef', $data);
    }

    public function input_reef($data)
    {
        $this->db->insert('trs_reef', $data);

        $id_reef = $this->db->insert_id(); 
        
        $this->update_temp_doc_id($id_reef);
        
        return $id_reef; 
    }

    public function input_fish($datas)
    {
        if(count($datas) < 2)
        {
            $this->db->insert('trs_reef_fish', $datas[0]);         
        }else{
            $this->db->insert_batch('trs_reef_fish', $datas);            
        }
    }

    public function inputFinalDomestic($datas)
    {
        $this->db->insert('trs_reef_final_domestic', $datas);
    }

    public function inputFinalExport($datas)
    {
        $this->db->insert('trs_reef_final_export', $datas);
    }

    public function update($data)
    {
        $this->db->where('id_reef', $data['id_reef']);
        $query = $this->db->update('trs_reef',$data);

        if($this->db->affected_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }

        return $result;
    }

    public function last_reef_id($area_code ="B", $year_code = "14")
    {

        $this->db->select("MAX(last_id) AS last_reef_id");
        $this->db->where("area_code", $area_code);
        $this->db->where("year_code", $year_code);

        $run_query = $this->db->get("trs_reef_counter");                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    } 

    public function verifikasi_reef($input)
    {
        $data = array(
               'is_verified' => 'YA',
               'tgl_verifikasi' => date('Y-m-d H:i:s'),
               'document_number' => $input['calon_doc_number'],
               'doc_area' => $input['doc_area'],
               'doc_year' => $input['doc_year'],
               'doc_id' => $input['doc_id'],
               'doc_prefix' => $input['doc_prefix'],
               'id_pengguna_ubah' => $this->user->id_pengguna(),
               'tanggal_ubah' => date('Y-m-d H:i:s')
        );

        $this->db->where('id_reef', $input['id_reef']);
        $this->db->update('trs_reef', $data); 

        // $this->seal_ctf_tags($input['id_reef']);
    }

    public function edit($array_input)
    {
        $id_reef = $array_input['id_reef'];
        unset($array_input['id_reef']);

        $this->db->where('id_reef', $id_reef);
        $this->db->update('trs_reef', $array_input);

    }   

    public function update_counter($area_code)
    {
        $sql_update = "UPDATE trs_reef_counter SET last_id = last_id + 1 WHERE area_code = '$area_code' ";

        $run_query = $this->db->query($sql_update);
    }

    public function delete($id_reef)
    {
      
            $reef_tables = array('trs_reef','trs_reef_fish');
            $this->db->where('id_reef', $id_reef);
            $this->db->delete($reef_tables);


    }

}