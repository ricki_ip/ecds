<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
/**
 * libary untuk ambil data dari session
 */
 
class User
{


	private $CI;
	private $session_data;

	public function __construct()
    {
        $this->CI =& get_instance();

        $this->CI->load->helper('url');
        $this->CI->load->library(array('form_validation','session'));

        $this->session_data = $this->CI->session->userdata('info');
    }

    function all_userdata()
    {
    	return $this->session_data;
    }

  	function nama_pengguna()
  	{ 
  		return $this->session_data['nama_pengguna'];
  	}

    function id_lokasi()
    {
      return $this->session_data['id_lokasi'];
    }

    function nama_lokasi()
    {
      return $this->session_data['nama_lokasi'];
    }

  	function id_pengguna()
  	{
  		return $this->session_data['id_pengguna'];
  	}

    function is_superadmin()
    {
      if($this->session_data['id_grup_pengguna'] === '1')
      {
        return true;
      }else{
        return false;
      }
    }

    function is_administrator()
    {
      if($this->session_data['id_grup_pengguna'] === '2')
      {
        return true;
      }else{
        return false;
      }
    }

    function is_operator()
    {
      if($this->session_data['id_grup_pengguna'] === '3')
      {
        return true;
      }else{
        return false;
      }
    }

    function is_asosiasi()
    {
      if($this->session_data['id_grup_pengguna'] === '4')
      {
        return true;
      }else{
        return false;
      }
    }

    function is_perusahaan()
    {
      if($this->session_data['id_grup_pengguna'] === '5')
      {
        return true;
      }else{
        return false;
      }
    }

    // Jika user perusahaan
    function is_perusahaan_kuota()
    {
        if( $this->is_perusahaan() && $this->session_data['id_perusahaan'] === '0' )
        {
          return false;
        }elseif ( $this->is_perusahaan() && $this->session_data['id_perusahaan'] !== '0'  ){
          return true;
        }
    }

    function code_area()
    {
      return $this->session_data['code_area'];      
    }

    function nama_perusahaan()
    {
      return $this->session_data['company_name'];
    }

    function nama_asosiasi()
    {
      return $this->session_data['asosiasi_name'];
    }

    function kuota_awal_perusahaan()
    {
      return intval( $this->session_data['kuota_perus_awal'] );
    }

    function kuota_awal_asosiasi()
    {
      return intval( $this->session_data['kuota_asos_awal'] );
    }


  	function grup_pengguna()
  	{
  		return $this->session_data['nama_grup_pengguna'];
  	}

    function level()
    {
      return intval($this->session_data['id_grup_pengguna']);
    }

  	function id_perusahaan()
  	{
  		return $this->session_data['id_perusahaan'];
  	}

    function is_artisenal()
    {
      return $this->session_data['company_name'] === 'ARTISENAL';
    }

    function id_asosiasi()
    {
      return $this->session_data['id_asosiasi'];
    }

    function id_asosiasi_user()
    {
      return $this->session_data['id_asosiasi_user'];
    }

    function company_name()
    {
      return $this->session_data['company_name'];
    }
}