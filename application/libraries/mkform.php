<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
/**
 * Library untuk bikin form input 
 */
 
class Mkform
{
	/*  @input_text
		$attr = array( 'value' => 'default value', // optional
					   'name' => 'input name',
					   'label' => 'text label for input'
	 				);
	 */

	function input($attr)
      { 
        $is_hidden = preg_match('/hide/',$attr['input_class']) ? 'hide' : '';
         $html = '<div class="form-group '.$is_hidden.' ">';
         $html .= $attr['label_class'] !== "hide" ? '<label for="'.$attr['input_id'].'" class="'.$attr['label_class'].' control-label'.'">'.$attr['label_text'].'</label>' : '';
         $html .=       '<div class="'.$attr['input_width'].'">
                         <input id="'.$attr['input_id'].'" name="'.$attr['input_name'].'" type="'.$attr['input_type'].'" class="'.$attr['input_class'].'" value="'.$attr['input_value'].'" placeholder="'.$attr['input_placeholder'].'">
                         </div>
                       </div>';                       
         return $html;
      }

	function input_text($attr)
	{
		$default_value = isset($attr['value']) ? $attr['value'] : '';
		$placeholder_text = isset($attr['placeholder']) ? $attr['placeholder'] : '';
		$input_id = substr($attr['name'], 0, 3) === 'id_' ? $attr['name'] : 'id_'.$attr['name']; 
		$html = '<div class="form-group mkform-text"><!-- mulai form text '.$attr['name'].' -->
	    		<label for="'.$attr['name'].'" class="col-sm-3 control-label">'.$attr['label'].'</label>
	    		<div class="col-sm-8">
	      			<input id="'.$input_id.'" 
	      				name="'.$attr['name'].'"
	      				type="text"
	      				class="form-control"
	      				placeholder="'.$placeholder_text.'"
	      				value="'.$default_value.'" >
	    		</div>
	  		</div><!-- akhir form text '.$attr['name'].' -->';
	 return $html;
	}

	/*  @input_file
		$attr = array( 'value' => 'default value', // optional
					   'name' => 'upload name',
					   'label' => 'text label for input'
	 				);
	 */
	function input_file($attr)
	{
		$default_value = isset($attr['value']) ? $attr['value'] : '';
		$placeholder_text = isset($attr['placeholder']) ? $attr['placeholder'] : '';
		$html = '<div class="form-group mkform-text"><!-- mulai form text '.$attr['name'].' -->
	    		<label for="'.$attr['name'].'" class="col-sm-3 control-label">'.$attr['label'].'</label>
	    		<div class="col-sm-8">
	      			<input id="id_'.$attr['name'].'" 
	      				name="'.$attr['name'].'"
	      				type="file"
	      				class="form-control"
	      				placeholder="'.$placeholder_text.'"
	      				value="'.$default_value.'" >
	    		</div>
	  		</div><!-- akhir form text '.$attr['name'].' -->';
	 return $html;
	}

	/*  @input_textarea
		$attr = array( 'value' => 'default value', // optional
					   'rows' => '3', // wajib ada
					   'name' => 'input name', // wajib ada
					   'label' => 'text label for input' // wajib ada
	 				);
	 */
	function input_textarea($attr)
	{
		$default_value = isset($attr['value']) ? $attr['value'] : '';
		$placeholder_text = isset($attr['placeholder']) ? $attr['placeholder'] : '';
		$html = '<div class="form-group mkform-text"><!-- mulai form text '.$attr['name'].' -->
	    		<label for="'.$attr['name'].'" class="col-sm-3 control-label">'.$attr['label'].'</label>
	    		<div class="col-sm-8">
	      			<textarea id="id_'.$attr['name'].'" 
	      				name="'.$attr['name'].'"
	      				class="form-control"
	      				placeholder="'.$placeholder_text.'"
	      				rows="'.$attr['rows'].'" >'.$default_value.'</textarea>
	    		</div>
	  		</div><!-- akhir form text '.$attr['name'].' -->';
	 return $html;
	}

	/*  @input_select
		$attr = array( 'array_opsi' => array('1' => 'Baru', '2'=> 'Lama'), // optional 
					   'value' => 'selected value', // optional
					   'name' => 'input name',
					   'label' => 'text label for input'
	 				);
	 */
	/*function input_select($attr)
	{
		$array_opsi = !empty($attr['opsi']) ? $attr['opsi'] : array('0' => 'kosong');
		$default_value = !empty($attr['value']) ? $attr['value'] : '';
		$html = '<div class="form-group mkform-select"><!-- mulai form select '.$attr['name'].' -->
	    		<label for="'.$attr['name'].'" class="col-sm-3 control-label">'.$attr['label'].'</label>
	    		<div class="col-sm-8">
	      			<select id="id_'.$attr['name'].'" name="'.$attr['name'].'" class="form-control">';
	      			foreach ($array_opsi as $value => $text) {
	      				$selected = $default_value === $value ? 'selected' : ''; 
	      				$html .= '<option value="'.$value.'">'.$text.'</option>';
	      			}
		$html .= '</select>
	    		</div>
	  		</div><!-- akhir form select '.$attr['name'].' -->';
	 return $html;
	}*/

	function input_select($attr)
	{
		$array_opsi = !empty($attr['opsi']) ? $attr['opsi'] : array('0' => 'kosong');
		$default_value = !empty($attr['value']) ? $attr['value'] : '';
		$html = '
				<script>
					
					function auto_width(){
				      //jQuery.fn.exists = function(){return this.length>0;}
				        if($("#id_'.$attr['name'].'").val().length < 30  )
				        {
							$("#id_'.$attr['name'].'").css({"width":"32%"});
				        }
				        else{
							var panjang = $("#id_'.$attr['name'].'").val().length;
				            $("#id_'.$attr['name'].'").css({"width":(panjang/3)+32+"%"}); 
				        }
				    }

				    s_func.push(auto_width);
      
				</script>
				<div class="form-group mkform-select"><!-- mulai form select '.$attr['name'].' -->
	    		<label for="'.$attr['name'].'" class="col-sm-3 control-label">'.$attr['label'].'</label>
	    		<div class="col-sm-8">
	      			<select id="id_'.$attr['name'].'" name="'.$attr['name'].'" class="form-control" ';

	    if(isset($attr['OnChange'])):
	    	if($attr['OnChange']!==''): $html .= 'OnChange='.$attr['OnChange'];endif;
	    endif;

	    $html .= ' >';
	      			foreach ($array_opsi as $value => $text) {
	      				/*if($default_value == $value)
	      				{
	      					$selected = 'selected';
	      				}
	      				else{
	      					$selected = '';
	      				}*/
	      				$selected = (String)$default_value == $value ? 'selected' : ''; 
	      				$html .= '<option value="'.$value.'" '.$selected.' >'.$text.'</option>';
	      			}
		$html .= '</select>
	    		</div>
	  		</div><!-- akhir form select '.$attr['name'].' -->';
	 return $html;
	}

	/*  @input_select
		$attr = array( 'array_opsi' => array('1' => 'Baru', '2'=> 'Lama'), // optional 
					   'value' => 'selected value', // optional
					   'name' => 'input name',
					   'label' => 'text label for input'
	 				);
	 */
	function input_select2($attr)
	{
		$array_opsi = !empty($attr['opsi']) ? $attr['opsi'] : array('0' => 'kosong');
		$default_value = !empty($attr['value']) ? $attr['value'] : '';
		$id_elemen = str_replace('[]', '', $attr['name']);

		if(isset($attr['is_multipel']) && $attr['is_multipel'] === TRUE){
			
			$multipel = 'multiple';

			if(!empty($attr['value_m'])){
				
				$value = 'var values="'.$attr['value_m'].'";
						$.each(values.split(","), function(i,e){
						    $("#id_'.$id_elemen.' option[value=\'" + e + "\']").prop("selected", true);
						});';
			}
			else $value = '';
		} 
		else {
			$multipel = '';
			$value ='';
		}

		$html = '<div class="form-group mkform-select"><!-- mulai form select '.$attr['name'].' -->
	    		<label for="'.$attr['name'].'" class="col-sm-3 control-label">'.$attr['label'].'</label>
	    		<div class="col-sm-8">
	      			<select '.$multipel.' id="id_'.$id_elemen.'" name="'.$attr['name'].'" style="width: 100% !important;">
	      			<option value="0">'.(isset($attr['all']) && $attr['all']===TRUE ? 'Semua':'-').'</option>';
	      			foreach ($array_opsi as $item) {
	      				$selected = $default_value === $item->id ? 'selected' : ''; 
	      				$html .= '<option value="'.$item->id.'" '.$selected.'>'.$item->text.'</option>';
	      			}
		$html .= '</select>
	    		</div>
	  		</div>';
	  	$js   = '<script>
	  				
	  				'.$value.'
	  				var func_'.$id_elemen.' = function() {
									  					$("#id_'.$id_elemen.'").select2();
									  				};
					s_func.push(func_'.$id_elemen.');
	  			</script>
	  			<!-- akhir form select '.$id_elemen.' -->';
	 return $html.$js;
	}

	function input_select_level($attr)
	{
		$array_opsi = !empty($attr['opsi']) ? $attr['opsi'] : array('0' => 'kosong');
		// print_r($attr['opsi']);die;
		$default_value = !empty($attr['value']) ? $attr['value'] : '';
		$id_elemen = str_replace('[]', '', $attr['name']);

		$html = '<div class="form-group mkform-select"><!-- mulai form select '.$attr['name'].' -->
	    		<label for="'.$attr['name'].'" class="col-sm-3 control-label">'.$attr['label'].'</label>
	    		<div class="col-sm-8">
	      			<select id="id_'.$id_elemen.'" name="'.$attr['name'].'" style="width: 100% !important;">
	      				<option value="0">-</option>';
                   $t_label = '####';
                   foreach ($array_opsi as $item) {
	      				$selected = $default_value === $item->id ? 'selected' : ''; 
                   		if(strcmp($item->label, $t_label) != 0 )
                   		{
                   			if(strcmp('####', $t_label)!=0 ) $html .='</optgroup>';
                   			$t_label = $item->label;
                   			$html .= '<optgroup label="'.$item->label.'">';

                   		}
	      				$html .= '<option value="'.$item->id.'" '.$selected.'>'.$item->text.'</option>';
	      			}
		$html .= '</select>
	    		</div>
	  		</div>';
	  	$js   = '<script>
        			var func_'.$id_elemen.' = function() {
									  					$("#id_'.$id_elemen.'").select2();
									  				};
					s_func.push(func_'.$id_elemen.');
    			</script>
	  			<!-- akhir form select '.$id_elemen.' -->';
	  	return $html.$js;
	}

	/*  @input_checkbox
		$attr = array( 'value' => 'checked value', // Wajib ada
					   'name' => 'input name', // Wajib ada
					   'label' => 'text label for input' // Wajib ada
					   'is_checked' => '1' // 1 : checked , 0 : not checked , default is 0
	 				);
	 */
	function input_checkbox($attr)
	{
		$is_checked = '';
		if(isset($attr['is_checked']))
		{
			$is_checked = $attr['is_checked'] === $attr['value'] ? ' checked ' : '';
		}
		$html = '<input type="checkbox" id="id_'.$attr['name'].'" name="'.$attr['name'].'" value="'.$attr['value'].'" '.$is_checked.'>
        		 <label for="id_'.$attr['name'].'">'.$attr['label'].'</label>';
        return $html;
	}


	/*  @input_date
		$attr = array( 'mindate' => 'various', // opsi: '', array('time' => '1 year'), '2012-10-11', tidak wajib ada
					   'maxdate' => 'various', // opsi: '', array('time' => '1 year'), '2013-10-11', tidak wajib ada
					   'defaultdate' => '2013-20-12', // opsi: '', tidak wajib ada
					   'placeholder' => '', // wajib ada atau '' (kosong)
					   'name' => 'input name', // wajib ada
					   'label' => 'text label for input' // wajib ada
	 				);
	 */
	function input_date($attr)
	{	

		if( !isset($attr['mindate']) || empty($attr['mindate']) )
		{
			$set_mindate = '';
		}elseif(is_array($attr['mindate'])){
			$timestamp = strtotime('-'.$attr['mindate']['time']);
			$mindate =  date('Y-m-d', $timestamp);
			$set_mindate = 'minDate: new Date("'.$mindate.'"),';
		}else{
			$set_mindate = 'new Date("'.$attr['mindate'].'"),';
		}

		if( !isset($attr['maxdate']) || empty($attr['mindate']) )
		{
			$set_maxdate = 'maxDate: new Date(),';
		}elseif(is_array($attr['maxdate'])){
			$timestamp = strtotime('+'.$attr['maxdate']['time']);
			$maxdate =  date('Y-m-d', $timestamp);
			$set_maxdate = 'minDate: new Date("'.$maxdate.'"),';
		}else{
			$set_maxdate = 'new Date("'.$attr['maxdate'].'"),';
		}

		if( !isset($attr['default_date']) || empty($attr['default_date']) )
		{
			$set_default_date = "new Date()";
		}else{
			$set_default_date = "new Date('".$attr['default_date']."')";
		}
         $html = '<div class="form-group mkform-date"><!-- mulai form date '.$attr['name'].' -->';
         $html .= '<label for="id_'.$attr['name'].'" class="col-sm-3 control-label">'.$attr['label'].'</label>';
         $html .=       '<div class="col-sm-8">
                         <input id="id_'.$attr['name'].'" 
                         		name="'.$attr['name'].'"
                         		type="hidden"
                         		>
                         <input id="id_'.$attr['name'].'_display" 
                         			readonly="true"
                         			type="text"
                         			class="form-control">
                         </div>
                       </div>';
         $js =  "<script>
                $(document).ready(function() {
		                $('#id_".$attr['name']."_display').datepicker({  
		                                                            altField: '#id_".$attr['name']."',
		                                                            altFormat: 'yy-mm-dd',
		                                                            ".$set_mindate."
		                                                            ".$set_maxdate."
		                                                            changeMonth: true,
		                                                            changeYear: true,
		                                                            dateFormat: 'dd/mm/yy',
		                                                            monthNamesShort: ['Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agst','Sept','Okt','Nov','Des'],
		                                                            dayNamesMin: ['Ming','Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab']
		                                                         });
		                  $('#id_".$attr['name']."_display').datepicker( 'setDate', ".$set_default_date." );
		                  $('#id_".$attr['name']."_display').focus(function(){ 
		                  $('#ui-datepicker-div').css('z-index', '9999');
	                  	});";
          $js .= "});
                </script> <!-- akhir form date ".$attr['name']." -->";                      
         return $html.$js;
	}
}