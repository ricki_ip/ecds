<?php
	$config = Array(
					'app' => Array(
		        					'header_title' => 'SDI | Sistem Aplikasi CDS',
		        					'footer_text' => 'Hak cipta © 2014 | <strong>Direktorat Sumber Daya Ikan</strong> | Direktorat Jenderal Perikanan Tangkap | Kementerian Kelautan dan Perikanan <br> Republik Indonesia<br>
														Jl. Medan Merdeka Timur No. 16 Jakarta Pusat. Telp. (021)-3453008 '
		        					),
					'assets_paths' => Array(
	        						'misc_css' => base_url('assets/third_party/css'),
	        						'misc_js' => base_url('assets/third_party/js'),
	        						'misc_sounds' => base_url('assets/third_party/sounds'),
	        						'main_css' => base_url('assets/ecds/css'),
	        						'main_js' => base_url('assets/ecds/js'),
	        						'kapi_images' => base_url('assets/ecds/images'),
	        						'kapo_uploads' => base_url('assets/ecds/uploads'),
	        						'mockup_images' => base_url('assets/ecds/images/mockup')
	        						),
					'Hari_f'	=> Array('Senin', 'Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu'),
					'Hari_s'	=> Array('Sen', 'Sel','Rab','Kam','Jum','Sab','Ming'),
					'Bulan_f' => Array('Januari', 'Februari', 'Maret',
											'April','Mei','Juni','Juli',
												'Agustus','September','Oktober','November','Desember'),
					'Bulan_s'	=> Array('Jan', 'Feb', 'Mar',
											'Apr','Mei','Jun','Jul',
												'Agst','Sept','Okt','Nov','Des')					
					);
?>