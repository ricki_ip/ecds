<?php
	$item = '$item->';
	$config = Array(
					'form_ctf' => Array(
									'title' => Array( 
													'page' => 'Entry CTF',
													'panel' => 'Entry CTF'
													),
									'form' => Array(
													'document_number' 				=> Array( 	'name' 	=> 'document_number',
																					 		'label'	=> 'Document Number'
																						),
													'document_number_associated_reef' 					=> Array( 	'name' 	=> 'document_number_associated_cmf',
																 					  		'label' => 'Document Number of Associated Catch Monitoring Form'
 																						),
													'vessel_name' 					=> Array( 	'name' 	=> 'vessel_name',
																 					  		'label' => 'Name of Fishing Vessel'
 																						),
													'vessel_registration_number' 		=> Array( 	'name' 	=> 'vessel_registration_number',
																					 		'label'	=> 'Vessel Registration Number (or CCSBT Form Serial Number)'
																						),
													'flag_state' 				=> Array( 	'name' 	=> 'flag_state',
																					 		'label'	=> 'Flag State / Fishing Entity'
																						),
													'information_other_form' 				=> Array( 	'name' 	=> 'information_other_form',
																					 		'label'	=> 'Information on Other form(s) of Capture (eg. Trap)'
																						),

													'ccsbt_tag_number' 				=> Array( 	'name' 	=> 'ccsbt_tag_number',
																					 		'label'	=> 'CCSBT Tag Number'
																						),
													'type_tag' 				=> Array( 	'name' 	=> 'type_tag',
																					 		'label'	=> 'Type : RD/GGO/GGT/DRO/DRT'
																						),
													'weight' 				=> Array( 	'name' 	=> 'weight',
																					 		'label'	=> 'Weight (kg)'
																						),
													'fork_length' 				=> Array( 	'name' 	=> 'fork_length',
																					 		'label'	=> 'Fork Length (cm)'
																						),

													'gear_code' 				=> Array( 	'name' 	=> 'gear_code',
																					 		'label'	=> 'Gear Code (if applicable)'
																						),
													'ccsbt_area' 				=> Array( 	'name' 	=> 'ccsbt_area',
																					 		'label'	=> 'CCSBT Statistical Area of Catch (if applicable)'
																						),
													'month_of_harvest' 				=> Array( 	'name' 	=> 'month_of_harvest',
																					 		'label'	=> 'Month of Harvest (mm/yy)'
																						),
													'name' 				=> Array( 	'name' 	=> 'name',
																					 		'label'	=> 'Name'
																						),
													'signature' 				=> Array( 	'name' 	=> 'signature',
																					 		'label'	=> 'Signature'
																						),
													'date' 				=> Array( 	'name' 	=> 'date',
																					 		'label'	=> 'Date'
																						),
													'title' 				=> Array( 	'name' 	=> 'title',
																					 		'label'	=> 'Title'
																						)
													)
									),
					'form_edit_ctf' => Array(
									'title' => Array( 
													'page' => 'Edit CTF',
													'panel' => 'Edit CTF'
													),
									'form' => Array(
													'document_number' 				=> Array( 	'name' 	=> 'document_number',
																					 		'label'	=> 'Document Number'
																						),
													'document_number_associated_cmf' 					=> Array( 	'name' 	=> 'document_number_associated_cmf',
																 					  		'label' => 'Document Number of Associated Catch Monitoring Form'
 																						),
													'vessel_name' 					=> Array( 	'name' 	=> 'vessel_name',
																 					  		'label' => 'Name of Fishing Vessel'
 																						),
													'vessel_registration_number' 		=> Array( 	'name' 	=> 'vessel_registration_number',
																					 		'label'	=> 'Vessel Registration Number (or CCSBT Form Serial Number)'
																						),
													'flag_state' 				=> Array( 	'name' 	=> 'flag_state',
																					 		'label'	=> 'Flag State / Fishing Entity'
																						),
													'information_other_form' 				=> Array( 	'name' 	=> 'information_other_form',
																					 		'label'	=> 'Information on Other form(s) of Capture (eg. Trap)'
																						),

													'ccsbt_tag_number' 				=> Array( 	'name' 	=> 'ccsbt_tag_number',
																					 		'label'	=> 'CCSBT Tag Number'
																						),
													'type_tag' 				=> Array( 	'name' 	=> 'type_tag',
																					 		'label'	=> 'Type : RD/GGO/GGT/DRO/DRT'
																						),
													'weight' 				=> Array( 	'name' 	=> 'weight',
																					 		'label'	=> 'Weight (kg)'
																						),
													'fork_length' 				=> Array( 	'name' 	=> 'fork_length',
																					 		'label'	=> 'Fork Length (cm)'
																						),

													'gear_code' 				=> Array( 	'name' 	=> 'gear_code',
																					 		'label'	=> 'Gear Code (if applicable)'
																						),
													'ccsbt_area' 				=> Array( 	'name' 	=> 'ccsbt_area',
																					 		'label'	=> 'CCSBT Statistical Area of Catch (if applicable)'
																						),
													'month_of_harvest' 				=> Array( 	'name' 	=> 'month_of_harvest',
																					 		'label'	=> 'Month of Harvest (mm/yy)'
																						),
													'name' 				=> Array( 	'name' 	=> 'name',
																					 		'label'	=> 'Name'
																						),
													'signature' 				=> Array( 	'name' 	=> 'signature',
																					 		'label'	=> 'Signature'
																						),
													'date' 				=> Array( 	'name' 	=> 'date',
																					 		'label'	=> 'Date'
																						),
													'title' 				=> Array( 	'name' 	=> 'title',
																					 		'label'	=> 'Title'
																						)
													)
									),
					'view_kapal_table' => Array(
											'title' => Array(
															'page' => 'Data CCSBT Authorised Vessel',
															'panel' => 'Data CCSBT Authorised Vessel'
															),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => 'Vessel Name',
															'2' => Array('data' => 'CCSBT Registration Number', 'width' => '100'),
															'3' => 'Perusahaan',
															'6' => Array('data' => 'Vessel Registration Number', 'width' => '150'),
															'7' => Array('data' => 'Call Sign', 'width' => '100'),
															'8' => Array('data' => 'GT', 'width' => '100'),
															'9' => Array('data' => 'Date Authorisation Starts', 'width' => '100'),
															'10' => Array('data' => 'Date Authorisation Ends', 'width' => '100'),
															'11' => Array('data' => 'Vessel New to LSFV List', 'width' => '100'),
															'12' => Array('data' => 'Date Retrieved', 'width' => '100'),
															'13' => Array('data' => 'Owner Name', 'width' => '100'),
															'14' => Array('data' => 'Owner Address', 'width' => '100'),
															'15' => Array('data' => 'Operator Name', 'width' => '100'),
															'16' => Array('data' => 'Operator Address', 'width' => '100'),
														)
											),
					'form_entry_cmf' => Array(
									'title' => Array( 
													'page' => 'Entry CMF',
													'panel' => 'Entry CMF'
													),
									'form' => Array(
													'document_number'					=> Array(	'name' => 'document_number',
																							'label' => 'Document Number'
																						),
													'ctf_document_numbers' 				=> Array( 	'name' 	=> 'ctf_document_numbers',
																					 		'label'	=> 'Catch Tagging Form Document Number'
																						),
													'type_of_catch'		=> Array(	'name' => 'type_of_catch',
																							'label' => 'Type Of Catch'
																						),
													'vessel_name'		=> Array(	'name' => 'vessel_name',
																							'label' => 'Name of Catching Vessel'
																						),
													'registration_number' 		=> Array( 	'name' 	=> 'registration_number',
																					 		'label'	=> 'Registration Number (or CCSBT Form Serial Number)'
																						),
													'flag_state' 				=> Array( 	'name' 	=> 'flag_state',
																					 		'label'	=> 'Flag State / Fishing Entity'
																						),
													'type_of_landing'		=> Array(	'name' => 'type_of_landing',
																							'label' => 'Type Of Landing'
																						),
													'name_processing_establishment' => Array(	'name' => 'name_processing_establishment',
																							'label' => 'Name of Processing Establishment (if applicable)'
																						),
													'address_processing_establishment'		=> Array(	'name' => 'address_processing_establishment',
																							'label' => 'Address of Processing Establishment (if applicable)'
																						),
													'authority_name_title'		=> Array(	'name' => 'authority_name_title',
																							'label' => 'Name and Title'
																						),
													'authority_signature'		=> Array(	'name' => 'authority_signature',
																							'label' => 'Signature'
																						),
													'authority_date'		=> Array(	'name' => 'authority_date',
																							'label' => 'Date'
																						),
													'fishing_vessel_name'		=> Array(	'name' => 'fishing_vessel_name',
																							'label' => 'Name'
																						),
													'fishing_vessel_date'		=> Array(	'name' => 'fishing_vessel_date',
																							'label' => 'Date'
																						),
													'fishing_vessel_signature'		=> Array(	'name' => 'fishing_vessel_signature',
																							'label' => 'Signature'
																						),
													'master_name'		=> Array(	'name' => 'master_name',
																							'label' => 'Name'
																						),
													'master_date'		=> Array(	'name' => 'master_date',
																							'label' => 'Date'
																						),
													'master_signature'		=> Array(	'name' => 'master_signature',
																							'label' => 'Signature'
																						),
													'observer_name'		=> Array(	'name' => 'observer_name',
																							'label' => 'Name'
																						),
													'observer_date'		=> Array(	'name' => 'observer_date',
																							'label' => 'Date'
																						),
													'observer_signature'		=> Array(	'name' => 'observer_signature',
																							'label' => 'Signature'
																						),
													'export_city'		=> Array(	'name' => 'export_city',
																							'label' => 'City'
																						),
													'export_province'		=> Array(	'name' => 'export_province',
																							'label' => 'State or Province'
																						),
													'export_state_entity'		=> Array(	'name' => 'export_state_entity',
																							'label' => 'State / Fishing Entity'
																						),
													'export_destination'		=> Array(	'name' => 'export_destination',
																							'label' => 'Destination (State/Fishing Entity)'
																						),
													'exporter_name'		=> Array(	'name' => 'exporter_name',
																							'label' => 'Name'
																						),
													'exporter_company_name'		=> Array(	'name' => 'id_exporter_company_name',
																							'label' => 'License No./Company Name'
																						),
													'exporter_date'		=> Array(	'name' => 'exporter_date',
																							'label' => 'Date'
																						),
													'exporter_signature'		=> Array(	'name' => 'exporter_signature',
																							'label' => 'Signature'
																						),
													'export_authority_name_title'		=> Array(	'name' => 'export_authority_name_title',
																							'label' => 'Name and Title'
																						),
													'export_authority_signature'		=> Array(	'name' => 'export_authority_signature',
																							'label' => 'Signature'
																						),
													'export_authority_date'		=> Array(	'name' => 'export_authority_date',
																							'label' => 'Date'
																						),
													'domestic_name'		=> Array(	'name' => 'domestic_name',
																							'label' => 'Name'
																						),
													'domestic_address'		=> Array(	'name' => 'domestic_address',
																							'label' => 'Address'
																						),
													'domestic_date'		=> Array(	'name' => 'domestic_date',
																							'label' => 'Date'
																						),
													'domestic_weight'		=> Array(	'name' => 'domestic_weight',
																							'label' => 'Weight (Kg)'
																						),
													'domestic_signature'		=> Array(	'name' => 'domestic_signature',
																							'label' => 'Signature'
																						),
													'import_city'		=> Array(	'name' => 'import_city',
																							'label' => 'City'
																						),
													'import_province'		=> Array(	'name' => 'import_province',
																							'label' => 'State or Province'
																						),
													'import_state_entity'		=> Array(	'name' => 'import_state_entity',
																							'label' => 'State / Fishing Entity'
																						),
													'importer_name'		=> Array(	'name' => 'importer_name',
																							'label' => 'Name'
																						),
													'importer_address'		=> Array(	'name' => 'importer_address',
																							'label' => 'Address'
																						),
													'importer_date'		=> Array(	'name' => 'importer_date',
																							'label' => 'Date'
																						),
													'importer_signature'		=> Array(	'name' => 'importer_signature',
																							'label' => 'Signature'
																						)

													
													)
					
									),
					'form_edit_cmf' => Array(
									'title' => Array( 
													'page' => 'Edit CMF',
													'panel' => 'Edit CMF'
													),
									'form' => Array(
													'document_number'					=> Array(	'name' => 'document_number',
																							'label' => 'Document Number'
																						),
													'ctf_document_numbers' 				=> Array( 	'name' 	=> 'ctf_document_numbers',
																					 		'label'	=> 'Catch Tagging Form Document Number'
																						),
													'type_of_catch'		=> Array(	'name' => 'type_of_catch',
																							'label' => 'Type Of Catch'
																						),
													'vessel_name'		=> Array(	'name' => 'vessel_name',
																							'label' => 'Name of Catching Vessel'
																						),
													'registration_number' 		=> Array( 	'name' 	=> 'registration_number',
																					 		'label'	=> 'Registration Number (or CCSBT Form Serial Number)'
																						),
													'flag_state' 				=> Array( 	'name' 	=> 'flag_state',
																					 		'label'	=> 'Flag State / Fishing Entity'
																						),
													'type_of_landing'		=> Array(	'name' => 'type_of_landing',
																							'label' => 'Type Of Landing'
																						),
													'name_processing_establishment' => Array(	'name' => 'name_processing_establishment',
																							'label' => 'Name of Processing Establishment (if applicable)'
																						),
													'address_processing_establishment'		=> Array(	'name' => 'address_processing_establishment',
																							'label' => 'Address of Processing Establishment (if applicable)'
																						),
													'authority_name_title'		=> Array(	'name' => 'authority_name_title',
																							'label' => 'Name and Title'
																						),
													'authority_signature'		=> Array(	'name' => 'authority_signature',
																							'label' => 'Signature'
																						),
													'authority_date'		=> Array(	'name' => 'authority_date',
																							'label' => 'Date'
																						),
													'fishing_vessel_name'		=> Array(	'name' => 'fishing_vessel_name',
																							'label' => 'Name'
																						),
													'fishing_vessel_date'		=> Array(	'name' => 'fishing_vessel_date',
																							'label' => 'Date'
																						),
													'fishing_vessel_signature'		=> Array(	'name' => 'fishing_vessel_signature',
																							'label' => 'Signature'
																						),
													'master_name'		=> Array(	'name' => 'master_name',
																							'label' => 'Name'
																						),
													'master_date'		=> Array(	'name' => 'master_date',
																							'label' => 'Date'
																						),
													'master_signature'		=> Array(	'name' => 'master_signature',
																							'label' => 'Signature'
																						),
													'observer_name'		=> Array(	'name' => 'observer_name',
																							'label' => 'Name'
																						),
													'observer_date'		=> Array(	'name' => 'observer_date',
																							'label' => 'Date'
																						),
													'observer_signature'		=> Array(	'name' => 'observer_signature',
																							'label' => 'Signature'
																						),
													'export_city'		=> Array(	'name' => 'export_city',
																							'label' => 'City'
																						),
													'export_province'		=> Array(	'name' => 'export_province',
																							'label' => 'State or Province'
																						),
													'export_state_entity'		=> Array(	'name' => 'export_state_entity',
																							'label' => 'State / Fishing Entity'
																						),
													'export_destination'		=> Array(	'name' => 'export_destination',
																							'label' => 'Destination (State/Fishing Entity)'
																						),
													'exporter_name'		=> Array(	'name' => 'exporter_name',
																							'label' => 'Name'
																						),
													'exporter_company_name'		=> Array(	'name' => 'id_exporter_company_name',
																							'label' => 'License No./Company Name'
																						),
													'exporter_date'		=> Array(	'name' => 'exporter_date',
																							'label' => 'Date'
																						),
													'exporter_signature'		=> Array(	'name' => 'exporter_signature',
																							'label' => 'Signature'
																						),
													'export_authority_name_title'		=> Array(	'name' => 'export_authority_name_title',
																							'label' => 'Name and Title'
																						),
													'export_authority_signature'		=> Array(	'name' => 'export_authority_signature',
																							'label' => 'Signature'
																						),
													'export_authority_date'		=> Array(	'name' => 'export_authority_date',
																							'label' => 'Date'
																						),
													'domestic_name'		=> Array(	'name' => 'domestic_name',
																							'label' => 'Name'
																						),
													'domestic_address'		=> Array(	'name' => 'domestic_address',
																							'label' => 'Address'
																						),
													'domestic_date'		=> Array(	'name' => 'domestic_date',
																							'label' => 'Date'
																						),
													'domestic_weight'		=> Array(	'name' => 'domestic_weight',
																							'label' => 'Weight (Kg)'
																						),
													'domestic_signature'		=> Array(	'name' => 'domestic_signature',
																							'label' => 'Signature'
																						),
													'import_city'		=> Array(	'name' => 'import_city',
																							'label' => 'City'
																						),
													'import_province'		=> Array(	'name' => 'import_province',
																							'label' => 'State or Province'
																						),
													'import_state_entity'		=> Array(	'name' => 'import_state_entity',
																							'label' => 'State / Fishing Entity'
																						),
													'importer_name'		=> Array(	'name' => 'importer_name',
																							'label' => 'Name'
																						),
													'importer_address'		=> Array(	'name' => 'importer_address',
																							'label' => 'Address'
																						),
													'importer_date'		=> Array(	'name' => 'importer_date',
																							'label' => 'Date'
																						),
													'importer_signature'		=> Array(	'name' => 'importer_signature',
																							'label' => 'Signature'
																						)

													
													)
					
									),
					'view_ctf_table' => Array(
											'title' => Array(
															'page' => 'Data Catch Tagging Form',
															'panel' => 'Data Catch Tagging Form'
															),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => Array('data' => 'Document Number', 'width' => '20'),
															'2' => Array('data' => 'Name of Fishing Vessel - Registration Number', 'width' => '100'),
															'3' => Array('data' => 'Entry Date', 'width' => '100'),
															'4' => Array('data' => 'Status Verifikasi', 'width' => '100')
														)
											),
					'view_cmf_table' => Array(
											'title' => Array(
															'page' => 'Data Catch Monitoring Form',
															'panel' => 'Data Catch Monitoring Form'
															),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => Array('data' => 'Document Number', 'width' => '20'),
															'2' => Array('data' => 'Catch Tagging Form Document Number', 'width' => '100'),
															'3' => Array('data' => 'Name of Fishing Vessel', 'width' => '100'),
															'4' => Array('data' => 'Entry Date', 'width' => '100'),
															'5' => Array('data' => 'Status Verifikasi', 'width' => '100')
															// '5' => Array('data' => 'Name and Title', 'width' => '50'),
															// '6' => Array('data' => 'Signature', 'width' => '20'),
															// '7' => Array('data' => 'Date', 'width' => '20')
															
														)
											),
						'view_reef_table' => Array(
											'title' => Array(
															'page' => 'Data Catch Monitoring Form',
															'panel' => 'Data Catch Monitoring Form'
															),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => Array('data' => 'Document Number', 'width' => '20'),
															'2' => Array('data' => 'Catch Monitoring Form Document Number', 'width' => '100'),
															'3' => Array('data' => 'Exporter Name', 'width' => '100'),
															'4' => Array('data' => 'Exporter Company Name', 'width' => '100'),
															'5' => Array('data' => 'Status Verifikasi', 'width' => '100')
															// '5' => Array('data' => 'Name and Title', 'width' => '50'),
															// '6' => Array('data' => 'Signature', 'width' => '20'),
															// '7' => Array('data' => 'Date', 'width' => '20')
															
														)
											),
					'form_entry_pengguna' => Array(
									'title' => Array( 
													'page' => 'Entry Pengguna',
													'panel' => 'Entry Pengguna'
													),
									'form' => Array(
													'id_grup_pengguna' 				=> Array( 	'name' 	=> 'id_grup_pengguna',
																					 		'label'	=> 'Grup Pengguna'
																						),
													'id_asosiasi' 				=> Array( 	'name' 	=> 'id_asosiasi',
																					 		'label'	=> 'Asosiasi'
																						),
													'kode_pengguna' 				=> Array( 	'name' 	=> 'kode_pengguna',
																					 		'label'	=> 'Username'
																						),
													'nama_pengguna' 				=> Array( 	'name' 	=> 'nama_pengguna',
																					 		'label'	=> 'Nama Pengguna'
																						),
													'nip' 				=> Array( 	'name' 	=> 'nip',
																					 		'label'	=> 'NIP'
																						),
													'full_name' 				=> Array( 	'name' 	=> 'full_name',
																					 		'label'	=> 'Full Name'
																						),
													'jenis_kelamin' 				=> Array( 	'name' 	=> 'jenis_kelamin',
																					 		'label'	=> 'Jenis Kelamin'
																						),
													'nama_jabatan' 				=> Array( 	'name' 	=> 'nama_jabatan',
																					 		'label'	=> 'Jabatan'
																						),
													'kata_kunci' 				=> Array( 	'name' 	=> 'kata_kunci',
																					 		'label'	=> 'Password'
																						),
													'no_telp1' 				=> Array( 	'name' 	=> 'no_telp1',
																					 		'label'	=> 'No Telepon 1'
																						),
													'no_telp2' 				=> Array( 	'name' 	=> 'no_telp2',
																					 		'label'	=> 'No Telepon 2'
																						),
													'no_hp' 				=> Array( 	'name' 	=> 'no_hp',
																					 		'label'	=> 'No HP'
																						),
													'email' 				=> Array( 	'name' 	=> 'email',
																					 		'label'	=> 'Email'
																						),
													'id_lokasi' 		=> Array( 	'name' 	=> 'id_lokasi',
																					 		'label'	=> 'Lokasi'
																						),
													'id_perusahaan' => Array( 	'name' 	=> 'id_perusahaan',
																					 		'label'	=> 'Perusahaan'
																						),
													'aktif'		=> Array(	'name' => 'aktif',
																							'label' => 'aktif'
																						)																																											
													)
									),
					'form_reef' => Array(
									'title' => Array( 
													'page' => 'Entry REEF',
													'panel' => 'Entry REEF'
													),
									'form' => Array(
													'document_number'					=> Array(	'name' => 'document_number',
																							'label' => 'Document Number'
																						),
													'type_of_export'					=> Array(	'name' => 'type_of_export',
																							'label' => 'Type of Export'
																						),
													'ctf_document_numbers' 				=> Array( 	'name' 	=> 'ctf_document_numbers',
																					 		'label'	=> 'Catch Tagging Form Document Number'
																						),
													'cmf_document_numbers' 				=> Array( 	'name' 	=> 'cmf_document_numbers',
																					 		'label'	=> 'Catch Monitoring Form Document Number'
																						),
													'type_of_catch'		=> Array(	'name' => 'type_of_catch',
																							'label' => 'Type Of Catch'
																						),
													'vessel_name'		=> Array(	'name' => 'vessel_name',
																							'label' => 'Name of Catching Vessel'
																						),
													'registration_number' 		=> Array( 	'name' 	=> 'registration_number',
																					 		'label'	=> 'Registration Number (or CCSBT Form Serial Number)'
																						),
													'flag_state' 				=> Array( 	'name' 	=> 'flag_state',
																					 		'label'	=> 'Flag State / Fishing Entity'
																						),
													'type_of_landing'		=> Array(	'name' => 'type_of_landing',
																							'label' => 'Type Of Landing'
																						),
													'name_processing_establishment' => Array(	'name' => 'name_processing_establishment',
																							'label' => 'Name of Processing Establishment (if applicable)'
																						),
													'address_processing_establishment'		=> Array(	'name' => 'address_processing_establishment',
																							'label' => 'Address of Processing Establishment (if applicable)'
																						),
													'authority_name_title'		=> Array(	'name' => 'authority_name_title',
																							'label' => 'Name and Title'
																						),
													'authority_signature'		=> Array(	'name' => 'authority_signature',
																							'label' => 'Signature'
																						),
													'authority_date'		=> Array(	'name' => 'authority_date',
																							'label' => 'Date'
																						),
													'fishing_vessel_name'		=> Array(	'name' => 'fishing_vessel_name',
																							'label' => 'Name'
																						),
													'fishing_vessel_date'		=> Array(	'name' => 'fishing_vessel_date',
																							'label' => 'Date'
																						),
													'fishing_vessel_signature'		=> Array(	'name' => 'fishing_vessel_signature',
																							'label' => 'Signature'
																						),
													'master_name'		=> Array(	'name' => 'master_name',
																							'label' => 'Name'
																						),
													'master_date'		=> Array(	'name' => 'master_date',
																							'label' => 'Date'
																						),
													'master_signature'		=> Array(	'name' => 'master_signature',
																							'label' => 'Signature'
																						),
													'observer_name'		=> Array(	'name' => 'observer_name',
																							'label' => 'Name'
																						),
													'observer_date'		=> Array(	'name' => 'observer_date',
																							'label' => 'Date'
																						),
													'observer_signature'		=> Array(	'name' => 'observer_signature',
																							'label' => 'Signature'
																						),
													'point_export_city'		=> Array(	'name' => 'point_export_city',
																							'label' => 'City'
																						),
													'point_export_province'		=> Array(	'name' => 'point_export_province',
																							'label' => 'State or Province'
																						),
													'point_export_state_entity'		=> Array(	'name' => 'point_export_state_entity',
																							'label' => 'Exporting State/Fishing Entity'
																						),
													'previous_state_entity' => Array(	'name' => 'previous_state_entity',
																							'label' => 'State / Fishing Entity'
																						),
													'previous_date_landing' => Array(	'name' => 'previous_date_landing',
																							'label' => 'Date of Previous Import/Landing'
																						),
													'export_destination'		=> Array(	'name' => 'export_destination',
																							'label' => 'Destination (State/Fishing Entity)'
																						),
													'exporter_name'		=> Array(	'name' => 'exporter_name',
																							'label' => 'Name'
																						),
													'exporter_company'		=> Array(	'name' => 'exporter_company',
																							'label' => 'License No./Company Name'
																						),
													'exporter_company_name'		=> Array(	'name' => 'exporter_company_name',
																							'label' => 'License No./Company Name'
																						),
													'exporter_date'		=> Array(	'name' => 'exporter_date',
																							'label' => 'Date'
																						),
													'exporter_signature'		=> Array(	'name' => 'exporter_signature',
																							'label' => 'Signature'
																						),
													'export_authority_name_title'		=> Array(	'name' => 'export_authority_name_title',
																							'label' => 'Name and Title'
																						),
													'export_authority_signature'		=> Array(	'name' => 'export_authority_signature',
																							'label' => 'Signature'
																						),
													'export_authority_date'		=> Array(	'name' => 'export_authority_date',
																							'label' => 'Date'
																						),
													'domestic_name'		=> Array(	'name' => 'domestic_name',
																							'label' => 'Name'
																						),
													'domestic_address'		=> Array(	'name' => 'domestic_address',
																							'label' => 'Address'
																						),
													'domestic_date'		=> Array(	'name' => 'domestic_date',
																							'label' => 'Date'
																						),
													'domestic_weight'		=> Array(	'name' => 'domestic_weight',
																							'label' => 'Weight (Kg)'
																						),
													'domestic_signature'		=> Array(	'name' => 'domestic_signature',
																							'label' => 'Signature'
																						),
													'import_city'		=> Array(	'name' => 'import_city',
																							'label' => 'City'
																						),
													'import_province'		=> Array(	'name' => 'import_province',
																							'label' => 'State or Province'
																						),
													'import_state_entity'		=> Array(	'name' => 'import_state_entity',
																							'label' => 'State / Fishing Entity'
																						),
													'importer_name'		=> Array(	'name' => 'importer_name',
																							'label' => 'Name'
																						),
													'importer_address'		=> Array(	'name' => 'importer_address',
																							'label' => 'Address'
																						),
													'importer_date'		=> Array(	'name' => 'importer_date',
																							'label' => 'Date'
																						),
													'importer_signature'		=> Array(	'name' => 'importer_signature',
																							'label' => 'Signature'
																						)
													)
					
									),
					'form_edit_reef' => Array(
									'title' => Array( 
													'page' => 'Edit REEF',
													'panel' => 'Edit REEF'
													),
									'form' => Array(
													'document_number'					=> Array(	'name' => 'document_number',
																							'label' => 'Document Number'
																						),
													'type_of_export'					=> Array(	'name' => 'type_of_export',
																							'label' => 'Type of Export'
																						),
													'ctf_document_numbers' 				=> Array( 	'name' 	=> 'ctf_document_numbers',
																					 		'label'	=> 'Catch Tagging Form Document Number'
																						),
													'cmf_document_numbers' 				=> Array( 	'name' 	=> 'cmf_document_numbers',
																					 		'label'	=> 'Catch Monitoring Form Document Number'
																						),
													'type_of_catch'		=> Array(	'name' => 'type_of_catch',
																							'label' => 'Type Of Catch'
																						),
													'vessel_name'		=> Array(	'name' => 'vessel_name',
																							'label' => 'Name of Catching Vessel'
																						),
													'registration_number' 		=> Array( 	'name' 	=> 'registration_number',
																					 		'label'	=> 'Registration Number (or CCSBT Form Serial Number)'
																						),
													'flag_state' 				=> Array( 	'name' 	=> 'flag_state',
																					 		'label'	=> 'Flag State / Fishing Entity'
																						),
													'type_of_landing'		=> Array(	'name' => 'type_of_landing',
																							'label' => 'Type Of Landing'
																						),
													'name_processing_establishment' => Array(	'name' => 'name_processing_establishment',
																							'label' => 'Name of Processing Establishment (if applicable)'
																						),
													'address_processing_establishment'		=> Array(	'name' => 'address_processing_establishment',
																							'label' => 'Address of Processing Establishment (if applicable)'
																						),
													'authority_name_title'		=> Array(	'name' => 'authority_name_title',
																							'label' => 'Name and Title'
																						),
													'authority_signature'		=> Array(	'name' => 'authority_signature',
																							'label' => 'Signature'
																						),
													'authority_date'		=> Array(	'name' => 'authority_date',
																							'label' => 'Date'
																						),
													'fishing_vessel_name'		=> Array(	'name' => 'fishing_vessel_name',
																							'label' => 'Name'
																						),
													'fishing_vessel_date'		=> Array(	'name' => 'fishing_vessel_date',
																							'label' => 'Date'
																						),
													'fishing_vessel_signature'		=> Array(	'name' => 'fishing_vessel_signature',
																							'label' => 'Signature'
																						),
													'master_name'		=> Array(	'name' => 'master_name',
																							'label' => 'Name'
																						),
													'master_date'		=> Array(	'name' => 'master_date',
																							'label' => 'Date'
																						),
													'master_signature'		=> Array(	'name' => 'master_signature',
																							'label' => 'Signature'
																						),
													'observer_name'		=> Array(	'name' => 'observer_name',
																							'label' => 'Name'
																						),
													'observer_date'		=> Array(	'name' => 'observer_date',
																							'label' => 'Date'
																						),
													'observer_signature'		=> Array(	'name' => 'observer_signature',
																							'label' => 'Signature'
																						),
													'point_export_city'		=> Array(	'name' => 'point_export_city',
																							'label' => 'City'
																						),
													'point_export_province'		=> Array(	'name' => 'point_export_province',
																							'label' => 'State or Province'
																						),
													'point_export_state_entity'		=> Array(	'name' => 'point_export_state_entity',
																							'label' => 'Exporting State/Fishing Entity'
																						),
													'previous_state_entity' => Array(	'name' => 'previous_state_entity',
																							'label' => 'State / Fishing Entity'
																						),
													'previous_date_landing' => Array(	'name' => 'previous_date_landing',
																							'label' => 'Date of Previous Import/Landing'
																						),
													'export_destination'		=> Array(	'name' => 'export_destination',
																							'label' => 'Destination (State/Fishing Entity)'
																						),
													'exporter_name'		=> Array(	'name' => 'exporter_name',
																							'label' => 'Name'
																						),
													'exporter_company'		=> Array(	'name' => 'exporter_company',
																							'label' => 'License No./Company Name'
																						),
													'exporter_company_name'		=> Array(	'name' => 'exporter_company_name',
																							'label' => 'License No./Company Name'
																						),
													'exporter_date'		=> Array(	'name' => 'exporter_date',
																							'label' => 'Date'
																						),
													'exporter_signature'		=> Array(	'name' => 'exporter_signature',
																							'label' => 'Signature'
																						),
													'export_authority_name_title'		=> Array(	'name' => 'export_authority_name_title',
																							'label' => 'Name and Title'
																						),
													'export_authority_signature'		=> Array(	'name' => 'export_authority_signature',
																							'label' => 'Signature'
																						),
													'export_authority_date'		=> Array(	'name' => 'export_authority_date',
																							'label' => 'Date'
																						),
													'domestic_name'		=> Array(	'name' => 'domestic_name',
																							'label' => 'Name'
																						),
													'domestic_address'		=> Array(	'name' => 'domestic_address',
																							'label' => 'Address'
																						),
													'domestic_date'		=> Array(	'name' => 'domestic_date',
																							'label' => 'Date'
																						),
													'domestic_weight'		=> Array(	'name' => 'domestic_weight',
																							'label' => 'Weight (Kg)'
																						),
													'domestic_signature'		=> Array(	'name' => 'domestic_signature',
																							'label' => 'Signature'
																						),
													'import_city'		=> Array(	'name' => 'import_city',
																							'label' => 'City'
																						),
													'import_province'		=> Array(	'name' => 'import_province',
																							'label' => 'State or Province'
																						),
													'import_state_entity'		=> Array(	'name' => 'import_state_entity',
																							'label' => 'State / Fishing Entity'
																						),
													'importer_name'		=> Array(	'name' => 'importer_name',
																							'label' => 'Name'
																						),
													'importer_address'		=> Array(	'name' => 'importer_address',
																							'label' => 'Address'
																						),
													'importer_date'		=> Array(	'name' => 'importer_date',
																							'label' => 'Date'
																						),
													'importer_signature'		=> Array(	'name' => 'importer_signature',
																							'label' => 'Signature'
																						)
													)
					
									),
					'form_edit_pengguna' => Array(
									'title' => Array( 
													'page' => 'Edit Pengguna',
													'panel' => 'Edit Pengguna'
													),
									'form' => Array(
													'id_grup_pengguna' 				=> Array( 	'name' 	=> 'id_grup_pengguna',
																					 		'label'	=> 'Grup Pengguna'
																						),
													'id_asosiasi' 				=> Array( 	'name' 	=> 'id_asosiasi',
																					 		'label'	=> 'Asosiasi'
																						),
													'id_perusahaan' 				=> Array( 	'name' 	=> 'id_perusahaan',
																					 		'label'	=> 'Perusahaan'
																						),
													'kode_pengguna' 				=> Array( 	'name' 	=> 'kode_pengguna',
																					 		'label'	=> 'Kode Pengguna'
																						),
													'nama_pengguna' 				=> Array( 	'name' 	=> 'nama_pengguna',
																					 		'label'	=> 'Nama Pengguna'
																						),
													'kode_pengguna' 				=> Array( 	'name' 	=> 'kode_pengguna',
																					 		'label'	=> 'Kode Pengguna'
																						),
													'nip' 				=> Array( 	'name' 	=> 'nip',
																					 		'label'	=> 'NIP'
																						),
													'full_name' 				=> Array( 	'name' 	=> 'full_name',
																					 		'label'	=> 'Full Name'
																						),
													'jenis_kelamin' 				=> Array( 	'name' 	=> 'jenis_kelamin',
																					 		'label'	=> 'Jenis Kelamin'
																						),
													'nama_jabatan' 				=> Array( 	'name' 	=> 'nama_jabatan',
																					 		'label'	=> 'Jabatan'
																						),
													'kata_kunci' 				=> Array( 	'name' 	=> 'kata_kunci',
																					 		'label'	=> 'Kata Kunci'
																						),
													'no_telp1' 				=> Array( 	'name' 	=> 'no_telp1',
																					 		'label'	=> 'No Telepon 1'
																						),
													'no_telp2' 				=> Array( 	'name' 	=> 'no_telp2',
																					 		'label'	=> 'No Telepon 2'
																						),
													'no_hp' 				=> Array( 	'name' 	=> 'no_hp',
																					 		'label'	=> 'No HP'
																						),
													'email' 				=> Array( 	'name' 	=> 'email',
																					 		'label'	=> 'Email'
																						),
													'id_lokasi' 				=> Array( 	'name' 	=> 'id_lokasi',
																					 		'label'	=> 'Lokasi'
																						),
													'aktif'		=> Array(	'name' => 'aktif',
																							'label' => 'aktif'
																						)																																											
													)
					
									),
					'view_pengguna_table' => Array(
											'title' => Array(
															'page' => 'Data Pengguna',
															'panel' => 'Data Pengguna'
															),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => Array('data' => 'Nama Pengguna', 'width' => '20'),
															'2' => Array('data' => 'Grup Pengguna', 'width' => '100'),
															'3' =>  Array('data' => 'Lokasi', 'width' => '100'),
															'4' => Array('data' => 'Login Terakhir', 'width' => '100'),
															'5' => Array('data' => 'Aktif', 'width' => '100')
														)
											),
					'form_entry_perusahaan' => Array(
									'title' => Array( 
													'page' => 'Entry Perusahaan',
													'panel' => 'Entry Perusahaan'
													),
									'form' => Array(
													'id_asosiasi' 				=> Array( 	'name' 	=> 'id_asosiasi',
																					 		'label'	=> 'Asosiasi'
																						),
													'company_name' 				=> Array( 	'name' 	=> 'company_name',
																					 		'label'	=> 'Company Name'
																						),
													'company_address' 				=> Array( 	'name' 	=> 'company_address',
																					 		'label'	=> 'Company Address'
																						),
													'owner_name' 				=> Array( 	'name' 	=> 'owner_name',
																					 		'label'	=> 'Owner Name'
																						),
													'owner_address' 				=> Array( 	'name' 	=> 'owner_address',
																					 		'label'	=> 'Owner Address'
																						),
													'kuota_perus_awal' 				=> Array( 	'name' 	=> 'kuota_perus_awal',
																					 		'label'	=> 'Kuota Awal(Kg)'
																						),
													'no_telp' 				=> Array( 	'name' 	=> 'no_telp',
																					 		'label'	=> 'Telephone Number'
																						),
													'aktif'		=> Array(	'name' => 'aktif',
																							'label' => 'aktif'
																						)																																											
													)
									),
					'form_edit_perusahaan' => Array(
									'title' => Array( 
													'page' => 'Edit Perusahaan',
													'panel' => 'Edit Perusahaan'
													),
									'form' => Array(
													'id_asosiasi' 				=> Array( 	'name' 	=> 'id_asosiasi',
																					 		'label'	=> 'Asosiasi'
																						),
													'company_name' 				=> Array( 	'name' 	=> 'company_name',
																					 		'label'	=> 'Company Name'
																						),
													'company_address' 				=> Array( 	'name' 	=> 'company_address',
																					 		'label'	=> 'Company Address'
																						),

													'owner_name' 				=> Array( 	'name' 	=> 'owner_name',
																					 		'label'	=> 'Owner Name'
																						),
													'owner_address' 				=> Array( 	'name' 	=> 'owner_address',
																					 		'label'	=> 'Owner Address'
																						),
													'kuota_perus_awal' 				=> Array( 	'name' 	=> 'kuota_perus_awal',
																					 		'label'	=> 'Kuota Awal(Kg)'
																						),
													'no_telp' 				=> Array( 	'name' 	=> 'no_telp',
																					 		'label'	=> 'Telephone Number'
																						),
													'aktif'		=> Array(	'name' => 'aktif',
																							'label' => 'aktif'
																						)																																											
													)
									),
					'view_perusahaan_table' => Array(
											'title' => Array(
															'page' => 'Data Perusahaan',
															'panel' => 'Data Perusahaan'
															),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => Array('data' => 'Company Name', 'width' => '20'),
															'2' => Array('data' => 'Asosiasi', 'width' => '100'),
															'3' =>  Array('data' => 'Addres', 'width' => '100'),
															'4' => Array('data' => 'Kuota', 'width' => '100')
														)
											),
					'form_entry_asosiasi' => Array(
									'title' => Array( 
													'page' => 'Entry Asosiasi',
													'panel' => 'Entry Asosiasi'
													),
									'form' => Array(
													'asosiasi_name' 				=> Array( 	'name' 	=> 'asosiasi_name',
																					 		'label'	=> 'Association Name'
																						),
													'asosiasi_address' 				=> Array( 	'name' 	=> 'asosiasi_address',
																					 		'label'	=> 'Address'
																						),
													'kuota_asos_awal' 				=> Array( 	'name' 	=> 'kuota_asos_awal',
																					 		'label'	=> 'Kuota Awal(Kg)'
																						),
													'no_telp' 				=> Array( 	'name' 	=> 'no_telp',
																					 		'label'	=> 'Telephone Number'
																						),
													'aktif'		=> Array(	'name' => 'aktif',
																							'label' => 'aktif'
																						)																																											
													)
									),
					'form_edit_asosiasi' => Array(
									'title' => Array( 
													'page' => 'Edit Asosiasi',
													'panel' => 'Edit Asosiasi'
													),
									'form' => Array(
													'asosiasi_name' 				=> Array( 	'name' 	=> 'asosiasi_name',
																					 		'label'	=> 'Association Name'
																						),
													'asosiasi_address' 				=> Array( 	'name' 	=> 'asosiasi_address',
																					 		'label'	=> 'Address'
																						),
													'kuota_asos_awal' 				=> Array( 	'name' 	=> 'kuota_asos_awal',
																					 		'label'	=> 'Kuota Awal(Kg)'
																						),												
													'no_telp' 				=> Array( 	'name' 	=> 'no_telp',
																					 		'label'	=> 'Telephone Number'
																						),
													'aktif'		=> Array(	'name' => 'aktif',
																							'label' => 'aktif'
																						)																																											
													)
									),
					'view_asosiasi_table' => Array(
											'title' => Array(
															'page' => 'Data Asosiasi',
															'panel' => 'Data Asosiasi'
															),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => Array('data' => 'Association Name', 'width' => '20'),
															'2' => Array('data' => 'Address', 'width' => '100'),
															'3' => Array('data' => 'Kuota', 'width' => '100')
														)
											),
					'form_entry_processing' => Array(
									'title' => Array( 
													'page' => 'Entry Processing Establishment',
													'panel' => 'Entry Processing Establishment'
													),
									'form' => Array(
													'company_name' 				=> Array( 	'name' 	=> 'company_name',
																					 		'label'	=> 'Company Name'
																						),
													'company_address' 				=> Array( 	'name' 	=> 'company_address',
																					 		'label'	=> 'Company Address'
																						),
													'owner_name' 				=> Array( 	'name' 	=> 'owner_name',
																					 		'label'	=> 'Owner Name'
																						),
													'owner_address' 				=> Array( 	'name' 	=> 'owner_address',
																					 		'label'	=> 'Owner Address'
																						),
													'no_telp' 				=> Array( 	'name' 	=> 'no_telp',
																					 		'label'	=> 'Telephone Number'
																						),
													'is_processing_establishment' => Array( 'name' => 'is_processing_establishment',
																							'label' => 'Is Processing Establishment'),
													'aktif'		=> Array(	'name' => 'aktif',
																							'label' => 'aktif'
																						)																																											
													)
									),
					'form_edit_processing' => Array(
									'title' => Array( 
													'page' => 'Edit Processing Establishment',
													'panel' => 'Edit Processing Establishment'
													),
									'form' => Array(
													'company_name' 				=> Array( 	'name' 	=> 'company_name',
																					 		'label'	=> 'Company Name'
																						),
													'company_address' 				=> Array( 	'name' 	=> 'company_address',
																					 		'label'	=> 'Company Address'
																						),

													'owner_name' 				=> Array( 	'name' 	=> 'owner_name',
																					 		'label'	=> 'Owner Name'
																						),
													'owner_address' 				=> Array( 	'name' 	=> 'owner_address',
																					 		'label'	=> 'Owner Address'
																						),
													'no_telp' 				=> Array( 	'name' 	=> 'no_telp',
																					 		'label'	=> 'Telephone Number'
																						),
													'is_processing_establishment' => Array( 'name' => 'is_processing_establishment',
																							'label' => 'Is Processing Establishment'),
													'aktif'		=> Array(	'name' => 'aktif',
																							'label' => 'aktif'
																						)																																											
													)
									),
					'view_processing_table' => Array(
											'title' => Array(
															'page' => 'Data Processing Establishment',
															'panel' => 'Data Processing Establishment'
															),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => Array('data' => 'Company Name', 'width' => '20'),
															'2' => Array('data' => 'Address', 'width' => '100'),
															'3' =>  Array('data' => 'Owner Name', 'width' => '100'),
															'4' => Array('data' => 'Telephone Number', 'width' => '100')
														)
											),
					'form_entry_pejabat' => Array(
									'title' => Array( 
													'page' => 'Entry Petugas Validasi',
													'panel' => 'Entry Petugas Validasi'
													),
									'form' => Array(
													'nama_pejabat' 				=> Array( 	'name' 	=> 'nama_pejabat',
																					 		'label'	=> 'Validator'
																						),
													'title' 				=> Array( 	'name' 	=> 'title',
																					 		'label'	=> 'Jabatan'
																						),

													'id_lokasi' 				=> Array( 	'name' 	=> 'id_lokasi',
																					 		'label'	=> 'Unit Kerja'
																						),
													'aktif'		=> Array(	'name' => 'aktif',
																							'label' => 'aktif'
																						)																																											
													)
									),
					'form_edit_pejabat' => Array(
									'title' => Array( 
													'page' => 'Edit Petugas Validasi',
													'panel' => 'Edit Petugas Validasi'
													),
									'form' => Array(
													'nama_pejabat' 				=> Array( 	'name' 	=> 'nama_pejabat',
																					 		'label'	=> 'Nama Pejabat'
																						),
													'title' 				=> Array( 	'name' 	=> 'title',
																					 		'label'	=> 'Jabatan'
																						),

													'id_lokasi' 				=> Array( 	'name' 	=> 'id_lokasi',
																					 		'label'	=> 'Unit Kerja'
																						),
													'aktif'		=> Array(	'name' => 'aktif',
																							'label' => 'aktif'
																						)																																											
													)
									),
					'view_pejabat_table' => Array(
											'title' => Array(
															'page' => 'Data Petugas Validasi',
															'panel' => 'Data Petugas Validasi'
															),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => Array('data' => 'Validator', 'width' => '20'),
															'2' => Array('data' => 'Jabatan', 'width' => '100'),
															'3' =>  Array('data' => 'Unit Kerja', 'width' => '100')
														)
											),
					'form_entry_lokasi' => Array(
									'title' => Array( 
													'page' => 'Entry Unit Kerja',
													'panel' => 'Entry Unit Kerja'
													),
									'form' => Array(
													'nama_lokasi' 				=> Array( 	'name' 	=> 'nama_lokasi',
																					 		'label'	=> 'Nama Unit Kerja'
																						),
													'code_area' 				=> Array( 	'name' 	=> 'code_area',
																					 		'label'	=> 'Kode Area'
																						),
													'aktif'		=> Array(	'name' => 'aktif',
																							'label' => 'aktif'
																						)																																											
													)
									),
					'form_edit_lokasi' => Array(
									'title' => Array( 
													'page' => 'Edit Unit Kerja',
													'panel' => 'Edit Unit Kerja'
													),
									'form' => Array(
													'nama_lokasi' 				=> Array( 	'name' 	=> 'nama_lokasi',
																					 		'label'	=> 'Nama Unit Kerja'
																						),
													'code_area' 				=> Array( 	'name' 	=> 'code_area',
																					 		'label'	=> 'Kode Area'
																						),
													'aktif'		=> Array(	'name' => 'aktif',
																							'label' => 'aktif'
																						)																																											
													)
									),
					'view_lokasi_table' => Array(
											'title' => Array(
															'page' => 'Data Unit Kerja',
															'panel' => 'Data Unit Kerja'
															),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => Array('data' => 'Unit Kerja', 'width' => '20'),
															'2' => Array('data' => 'Kode Area', 'width' => '20'),
															'3' => Array('data' => 'Aktif', 'width' => '20')															
														)
											),
					'form_entry_negara' => Array(
									'title' => Array( 
													'page' => 'Entry Data Negara',
													'panel' => 'Entry Data Negara'
													),
									'form' => Array(
													'nama_negara' 				=> Array( 	'name' 	=> 'nama_negara',
																					 		'label'	=> 'Nama negara'
																						),
													
													'aktif'		=> Array(	'name' => 'aktif',
																							'label' => 'aktif'
																						)																																											
													)
									),
					'form_edit_negara' => Array(
									'title' => Array( 
													'page' => 'Edit Data Negara',
													'panel' => 'Edit Data Negara'
													),
									'form' => Array(
													'nama_negara' 				=> Array( 	'name' 	=> 'nama_negara',
																					 		'label'	=> 'Nama negara'
																						),
													
													'aktif'		=> Array(	'name' => 'aktif',
																							'label' => 'aktif'
																						)																																											
													)
									),
					'view_negara_table' => Array(
											'title' => Array(
															'page' => 'Data Negara',
															'panel' => 'Data Negara'
															),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => Array('data' => 'Negara', 'width' => '20'),
															'2' => Array('data' => 'Aktif', 'width' => '20')															
														)
											),
					'form_entry_kabupaten_kota' => Array(
									'title' => Array( 
													'page' => 'Entry Kabupaten Kota',
													'panel' => 'Entry Kabupaten Kota'
													),
									'form' => Array(
													'nama_kabupaten_kota' 				=> Array( 	'name' 	=> 'nama_kabupaten_kota',
																					 		'label'	=> 'Nama Kabupaten Kota'
																						),
													
													'aktif'		=> Array(	'name' => 'aktif',
																							'label' => 'aktif'
																						)																																											
													)
									),
					'form_edit_kabupaten_kota' => Array(
									'title' => Array( 
													'page' => 'Edit Kabupaten Kota',
													'panel' => 'Edit kabupaten Kota'
													),
									'form' => Array(
													'nama_kabupaten_kota' 				=> Array( 	'name' 	=> 'nama_kabupaten_kota',
																					 		'label'	=> 'Nama Kabupaten Kota'
																						),
													
													'aktif'		=> Array(	'name' => 'aktif',
																							'label' => 'aktif'
																						)																																											
													)
									),
					'view_kabupaten_kota_table' => Array(
											'title' => Array(
															'page' => 'Data Kabupaten Kota',
															'panel' => 'Data Kabupaten Kota'
															),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => Array('data' => 'Nama Kabupaten / Kota', 'width' => '20'),
															'2' => Array('data' => 'Aktif', 'width' => '20')

															
														)
											),
					'view_login' => Array(
											'title' => Array(
															'page' => 'Login Aplikasi Sistem CDS',
															'panel' => 'CDS'
															),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => Array('data' => 'Nama Kabupaten / Kota', 'width' => '20'),
															'2' => Array('data' => 'Aktif', 'width' => '20')

															
														)
											)


			);
/* PETUNJUK PEMAKAIAN :

---Masih di controller---
Load dulu file config labels.php:
$this->load->config('labels');

Simpan ke data constant:
$data['constant'] = $this->config->item('form_pendok');
---Selesai di controller---

---Penggunaan Di file views---
// Menampilkan text untuk page title (biasanya di gunakan di templates header untuk judul page)
echo $constant['title']['page']; 

// Menampilkan text untuk judul panel
echo $constant['title']['panel'];

// Saat set attribut untuk form input, misal field Nomor Surat Permohonan
<label for="<?php echo $constant['form']['no_surat_permohonan']['name'];?>"> 
<?php echo $constant['form']['no_surat_permohonan']['label'];?>
</label>
<input 
type="text" 
id="<?php echo $constant['form']['no_surat_permohonan']['name'];?>"
name="<?php echo $constant['form']['no_surat_permohonan']['name'];?>"
value=""/>

---Selesai di file views---
*/



/* TEMPLATE:
	// PERHATIKAN TANDA KOMA SEBELUM DAN SESUDAH ARRAY
					'form_kapal' => Array(
									'title' => Array( 
													'page' => 'Halaman Entry Kapal',
													'panel' => 'Entry Kapal'
													),
									'form' => Array(
													'nama_kapal' => Array( 'name' => 'nama_kapal',
																					'label'	=> 'Nama Kapal'
																				),
													'' => Array( 'name' => '',
																 'label' => ''
 																)
													)
									)
*/
?>