<?php
	$config = Array(
					'admin_lists' => Array(
		        					'CTF' => Array(	'Entry CTF' => 'entry/ctf',
			        								'Table CTF' => 'preview/ctf'
		        									),
		        					'CMF' => Array(	'Entry CMF' => 'entry/cmf',
			        								'Table CMF' => 'preview/cmf'
		        									),
		        					'REEF' =>Array(	'Entry REEF' => 'entry/reef',
			        								'Table REEF' => 'preview/reef'
		        									),
		        					'Laporan' => Array('CDS Olahan' => 'report/cds/index',
		        										'CTF' => 'report/ctf/index',
		        										'CMF' => 'report/cmf/index',
		        										'REEF' => 'report/reef/index'
		        										)
		        					),
					'admin_settings' => Array(
		        					'Pengaturan' => Array(	'Forum Pengguna CDS' => '/help',
		        											'Pengguna' => 'pengaturan/mst_pengguna/index',
		        											'Perusahaan' => 'pengaturan/mst_perusahaan/index',		        						
		        											'Asosiasi' => 'pengaturan/mst_asosiasi/index',		        						
		        											'Processing Establishment' => 'pengaturan/mst_processing/index',
		        											'Negara Export' => 'pengaturan/mst_negara/index',
		        											'Petugas Validasi' => 'pengaturan/mst_pejabat/index',
		        											'Unit Kerja' => 'pengaturan/mst_lokasi/index',
		        											'Kabupaten / Kota' => 'pengaturan/mst_kabupaten_kota/index',
		        											'Quota CCSBT' => 'pengaturan/quota',
		        											'Tag Range' => 'pengaturan/tag_range',
		        											'CCSBT Data' => Array('Authorised Vessel' => 'kapal')
		        										),
		        											
		        					),
					'perusahaan_lists' => Array(
		        					'CTF' => Array(	'Entry CTF' => 'entry/ctf',
			        								'Table CTF' => 'preview/ctf'
		        									),
		        					'CMF' => Array(	'Entry CMF' => 'entry/cmf',
			        								'Table CMF' => 'preview/cmf'
		        									),
		        					'REEF' =>Array(	'Entry REEF' => 'entry/reef',
			        								'Table REEF' => 'preview/reef'
		        									)
		        					),
					'perusahaan_settings' => Array(
		        					'Pengaturan' => Array(	'Pengguna' => 'pengaturan/mst_pengguna/index',
		        											'Forum Pengguna CDS' => 'help'
														 )
		        					)
					);
?>