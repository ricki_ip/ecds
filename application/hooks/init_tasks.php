<?php 
  /**
  * 
  */
  class Init_tasks
  {
    
    private $CI;
    private $url_login = 'http://localhost/ecds/login';
    private $url_menu_integrasi = 'http://integrasi.djpt.kkp.go.id/login_baru/portal/main';
    private $public_access = array();
    private $id_aplikasi_otoritas_kapi = "1";
    private $is_logged_in = FALSE;

    function __construct()
    {
      $this->CI =& get_instance();
      // $this->CI->load->config('custom_constants');
      // $this->public_access = $this->CI->config->item('public_access'); 
    }

    function check_login()
    {
      // var_dump(strpos($this->CI->session->userdata('id_aplikasi'), $this->id_aplikasi_otoritas_kapi ) !== FALSE);
      $id_pengguna = $this->CI->session->userdata('id_pengguna');
      // $whitelist = array('127.0.0.1', '::1');
      $whitelist = array('');
      $is_login = $_SERVER['REQUEST_URI'] === 'ecds/login' ? TRUE : FALSE;
      // var_dump($_SERVER['REQUEST_URI']);
      
        if( empty($id_pengguna) && !in_array($_SERVER['REMOTE_ADDR'], $whitelist ) )
        {
          // var_dump('empty_id_pengguna');
          // die;
          if($_SERVER['REQUEST_URI'] !== '/ecds/login')
          {
            header( 'Location: '.$this->url_login ) ;
          }elseif ($_SERVER['REQUEST_URI'] === '/ecds/login/c_verifylogin') {
            continue;
          }
        }
        // elseif($id_pengguna !== FALSE && strpos($id_pengguna, $this->id_aplikasi_otoritas_kapi ) === FALSE ){
        //   // die;
        //   header( 'Location: '.$this->url_menu_integrasi ) ;
        // }   
      
    }

    function load_custom_constants()
    {
      $this->CI->load->config('custom_constants');
    }

    function ci_profiler()
    {
      //Check if in dev_mode
      $isDebugMode = $this->CI->input->get('dbg');

      if($isDebugMode === '1'){
        $this->CI->output->enable_profiler(TRUE);       
      }
    }

    
  }
?>