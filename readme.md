SETUP AWAL
=========

Aplikasi E-CDS.

### SETUP APLIKASI CODEIGNITER:
  - Clone via source tree ke folder dev_ecds di htdocs
  - Buat folder uploads/document di root folder dev_ecds
  - buat folder assets/ecds/cs, assets/ecds/images/, assets/ecds/js
  - buat folder assets/third_party/
  - Extract semua zip file di folder misc/bahan_awal/
  - file assets_awal.zip mengandung tiga folder (css, js, images), Copy/paste tiga folder ini ke dalam folder assets/third_party/
  - extract file zip system.zip dan paste di root folder dev_ecds
  - extract file zip config_awal.zip (config.php, database.php), Copy/Paste ke folder application/config/

### SETUP DATABASE MYSQL:
  - Masuk ke phpmyadmin
  - Buat database db_ecds

### SETUP MPDF:
  - Download MPDF : [MPDF57.zip](http://mpdf1.com/repos/MPDF57.zip)
  - Extract MPDF57.zip, rename jadi folder mpdf. MOVE ke folder application/libraries/
